package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.SslConfig;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNull;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.common.config.SslConfigs.*;

public class KafkaUtilTest {

    @Test
    public void parseSslConfig_defaults() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);

        // JKS
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, "KEYSTORE_LOCATION");
        config.put(SSL_KEYSTORE_PASSWORD_CONFIG, "KEYSTORE_PASSWORD");
        config.put(SSL_KEY_PASSWORD_CONFIG, "KEY_PASSWORD");
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, "TRUSTSTORE_LOCATION");
        config.put(SSL_TRUSTSTORE_PASSWORD_CONFIG, "TRUSTSTORE_PASSWORD");

        SslConfig sslConfig = KafkaUtil.parseSslConfig(config);

        assertEquals(SslConfig.KeystoreType.JKS, sslConfig.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.JKS, sslConfig.getTruststoreType());
        assertEquals(SslConfig.DEFAULT_SSL_PROTOCOL, sslConfig.getSslProtocol());
        assertFalse(sslConfig.getEnableHostnameVerification());
    }

    @Test
    public void parseSslConfig_typeStrings() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);

        // PEM
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, "KEYSTORE_CERTIFICATE_CHAIN");
        config.put(SSL_KEYSTORE_KEY_CONFIG, "KEY");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_TRUSTSTORE_CERTIFICATES_CONFIG, "TRUSTSTORE_CERTIFICATES");

        SslConfig sslConfig = KafkaUtil.parseSslConfig(config);

        assertEquals(SslConfig.KeystoreType.PEM, sslConfig.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PEM, sslConfig.getTruststoreType());
    }

    @Test
    public void parseSslConfig_emptySecurityProtocol() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, "");
        assertNull(KafkaUtil.parseSslConfig(config));
    }

    @Test
    public void parseSslConfig_noSecurityProtocol() {
        Map<String, Object> config = new HashMap<>();
        assertNull(KafkaUtil.parseSslConfig(config));
    }


    @Test
    public void test_schemaRegistrySSLConfig_JKSType() {

        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SSL_PROTOCOL_CONFIG, "TLSv1.3");
        config.put(SSL_ENABLED_PROTOCOLS_CONFIG, "TLSv1.3");
        config.put(SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
        // JKS
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "JKS");
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, "keystore_path");
        config.put(SSL_KEY_PASSWORD_CONFIG, "password");
        config.put(SSL_KEYSTORE_PASSWORD_CONFIG, "password");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "JKS");
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, "truststore_path");
        config.put(SSL_TRUSTSTORE_PASSWORD_CONFIG, "password");

        KafkaUtil.setSchemaRegistrySSLConfig(config);

        assertEquals("JKS", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_TYPE_CONFIG));
        assertEquals("JKS", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_TYPE_CONFIG));

        assertEquals("keystore_path", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_LOCATION_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEY_PASSWORD_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_PASSWORD_CONFIG));
        assertEquals("truststore_path", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_LOCATION_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_PASSWORD_CONFIG));

        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_KEY_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_CERTIFICATES_CONFIG));
    }

    @Test
    public void test_schemaRegistrySSLConfig_PKCS12Type() {

        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SSL_PROTOCOL_CONFIG, "TLSv1.3");
        config.put(SSL_ENABLED_PROTOCOLS_CONFIG, "TLSv1.3");
        config.put(SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
        // JKS
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "PKCS12");
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, "keystore_path");
        config.put(SSL_KEY_PASSWORD_CONFIG, "password");
        config.put(SSL_KEYSTORE_PASSWORD_CONFIG, "password");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "PKCS12");
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, "truststore_path");
        config.put(SSL_TRUSTSTORE_PASSWORD_CONFIG, "password");

        KafkaUtil.setSchemaRegistrySSLConfig(config);

        assertEquals("PKCS12", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_TYPE_CONFIG));
        assertEquals("PKCS12", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_TYPE_CONFIG));

        assertEquals("keystore_path", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_LOCATION_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEY_PASSWORD_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_PASSWORD_CONFIG));
        assertEquals("truststore_path", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_LOCATION_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_PASSWORD_CONFIG));

        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_KEY_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_CERTIFICATES_CONFIG));
    }


    @Test
    public void test_schemaRegistrySSLConfig_PemStringType() {

        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SSL_PROTOCOL_CONFIG, "TLSv1.3");
        config.put(SSL_ENABLED_PROTOCOLS_CONFIG, "TLSv1.3");
        config.put(SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
        // JKS
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, "keystore_certificate_chain");
        config.put(SSL_KEYSTORE_KEY_CONFIG, "key");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_TRUSTSTORE_CERTIFICATES_CONFIG, "truststore_certificates");

        KafkaUtil.setSchemaRegistrySSLConfig(config);

        assertEquals("PEM", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_TYPE_CONFIG));
        assertEquals("PEM", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_TYPE_CONFIG));

        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_LOCATION_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEY_PASSWORD_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_PASSWORD_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_LOCATION_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_PASSWORD_CONFIG));

        assertEquals("keystore_certificate_chain", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG));
        assertEquals("key", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_KEY_CONFIG));
        assertEquals("truststore_certificates", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_CERTIFICATES_CONFIG));
    }


    @Test
    public void test_schemaRegistrySSLConfig_PemFileType() {

        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SSL_PROTOCOL_CONFIG, "TLSv1.3");
        config.put(SSL_ENABLED_PROTOCOLS_CONFIG, "TLSv1.3");
        config.put(SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
        // JKS
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, "keyStoreKeyPairLocation");
        config.put(SSL_KEY_PASSWORD_CONFIG, "password");
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, "trustStoreCertificatesLocation");

        KafkaUtil.setSchemaRegistrySSLConfig(config);

        assertEquals("PEM", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_TYPE_CONFIG));
        assertEquals("PEM", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_TYPE_CONFIG));

        assertEquals("keyStoreKeyPairLocation", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_LOCATION_CONFIG));
        assertEquals("password", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEY_PASSWORD_CONFIG));
        assertEquals("trustStoreCertificatesLocation", config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_LOCATION_CONFIG));

        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_KEYSTORE_KEY_CONFIG));
        assertNull(config.get(KafkaUtil.SCHEMA_REGISTRY_PREFIX + SSL_TRUSTSTORE_CERTIFICATES_CONFIG));
    }
}
