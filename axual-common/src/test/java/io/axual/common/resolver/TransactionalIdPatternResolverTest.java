package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.axual.common.exception.ResolveException;

import static io.axual.common.resolver.TransactionalIdPatternResolver.TRANSACTIONAL_ID_PATTERN_CONFIG;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TransactionalIdPatternResolverTest {

    private static final String TRANSACTION_ID_1 = "txn1";
    private static final String TRANSACTION_ID_2 = "txn2";
    private static final String TENANT_PROPERTY = "tnt";
    private static final String ENVIRONMENT_PROPERTY = "envt";
    private static final String APPLICATION_ID_PROPERTY = "app.id";
    private static final String PATTERN = "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{" + APPLICATION_ID_PROPERTY + "}-{transactional.id}";
    private static final String ENVIRONMENT = "unit";
    private static final String APPLICATION_ID = "appId";
    private static final String TENANT = "axual";
    private static final String RESOLVED_TRANSACTION_ID_1 = TENANT + "-" + ENVIRONMENT + "-" + APPLICATION_ID + "-" + TRANSACTION_ID_1;
    private static final String RESOLVED_TRANSACTION_ID_2 = TENANT + "-" + ENVIRONMENT + "-" + APPLICATION_ID + "-" + TRANSACTION_ID_2;

    private static final String TRANSACTIONAL_RESOLVER_CONFIG = "transactionalId.resolver";

    private TransactionalIdPatternResolver resolver;

    @Before
    public void init() {
        resolver = new TransactionalIdPatternResolver();
        Map<String, Object> properties = new HashMap<>();
        properties.put(TENANT_PROPERTY, TENANT);
        properties.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);
        properties.put(APPLICATION_ID_PROPERTY, APPLICATION_ID);
        properties.put(TRANSACTIONAL_RESOLVER_CONFIG, TransactionalIdPatternResolver.class.getName());
        properties.put(TRANSACTIONAL_ID_PATTERN_CONFIG, PATTERN);

        resolver.configure(properties);
    }

    @Test
    public void test_resolveTransactionId() {
        assertEquals(RESOLVED_TRANSACTION_ID_1, resolver.resolveTransactionalId(TRANSACTION_ID_1));
    }

    @Test(expected = ResolveException.class)
    public void test_resolveTransactionId_null() {
        assertNull(resolver.resolveTransactionalId(null));
    }

    @Test
    public void test_resolveTransactionIds() {
        final Set<String> transactionalIds = resolver.resolveTransactionalIds(Arrays.asList(TRANSACTION_ID_1, TRANSACTION_ID_2));
        assertTrue(transactionalIds.contains(RESOLVED_TRANSACTION_ID_1));
        assertTrue(transactionalIds.contains(RESOLVED_TRANSACTION_ID_2));
    }

    @Test
    public void test_resolveTransactionIds_null() {
        assertEquals(0, resolver.resolveTransactionalIds((List) null).size());
    }

    @Test
    public void test_unresolveTransactionIds() {
        final Set<String> transactionalIds = resolver.unresolveTransactionalIds(Arrays.asList(RESOLVED_TRANSACTION_ID_1, RESOLVED_TRANSACTION_ID_2));
        assertTrue(transactionalIds.contains(TRANSACTION_ID_1));
        assertTrue(transactionalIds.contains(TRANSACTION_ID_2));
    }

    @Test
    public void test_unresolverTansactionIds_null() {
        assertEquals(0, resolver.unresolveTransactionalIds((List) null).size());
    }


}
