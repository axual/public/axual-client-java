package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class InternalPatternResolverTest {

    @Test
    public void testUnresolver() {
        String pattern = "{tenant}-{clown}-{environment}-{topic}";
        String testname = "axual-pipo-multilocal-gotestyourself-konijn";

        InternalPatternResolver resolver = new InternalPatternResolver("pattern", "topic");
        Map<String, Object> configs = new HashMap<>();
        configs.put("pattern", pattern);
        configs.put(Resolver.DEFAULT_PREFIX + "somefield", "somevalue");
        resolver.configure(configs);

        Map<String, String> result = resolver.unresolve(testname);
        Assert.assertTrue(result.containsKey("tenant"));
        Assert.assertTrue(result.containsKey("clown"));
        Assert.assertTrue(result.containsKey("environment"));
        Assert.assertTrue(result.containsKey("topic"));
        Assert.assertEquals("axual", result.get("tenant"));
        Assert.assertEquals("pipo", result.get("clown"));
        Assert.assertEquals("multilocal", result.get("environment"));
        Assert.assertEquals("gotestyourself-konijn", result.get("topic"));
        Assert.assertEquals("somevalue", result.get("somefield"));
        Assert.assertEquals(5, result.size());
    }

    @Test
    public void testUnresolvable() {
        String pattern = "{tenant}-{clown}-{environment}-{topic}";
        String testname = "_schemas";

        InternalPatternResolver resolver = new InternalPatternResolver("pattern", "topic");
        Map<String, Object> configs = new HashMap<>();
        configs.put("pattern", pattern);
        resolver.configure(configs);

        Map<String, String> unresolved = resolver.unresolve(testname);
        Assert.assertNull(unresolved);
    }
}
