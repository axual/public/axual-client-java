package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import io.axual.common.exception.ConfigurationException;

import static io.axual.common.test.TestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SslConfigTest {

    public static final String KEY_PEM_CONTENTS = "-----BEGIN PRIVATE KEY-----CONTENT_K-----END PRIVATE KEY-----";
    public static final String KEYSTORE_PEM_CONTENTS = "-----BEGIN CERTIFICATE-----CONTENT_P-----END CERTIFICATE-----";
    public static final String TRUSTSTORE_PEM_CONTENTS = "-----BEGIN CERTIFICATE-----CONTENT_T-----END CERTIFICATE-----";

    private static final SslConfig VALID_JKS_BUILDER = SslConfig.newBuilder()
            .setKeystoreLocation(KEYSTORE_JKS_PATH)
            .setKeystorePassword(DEFAULT_PASSWORD)
            .setKeyPassword(DEFAULT_PASSWORD)
            .setTruststoreLocation(TRUSTSTORE_JKS_PATH)
            .setTruststorePassword(DEFAULT_PASSWORD).build();
    private static final SslConfig VALID_PEM_BUILDER = SslConfig.newBuilder()
            .setKeystoreType(SslConfig.KeystoreType.PEM)
            .setKeystoreCertificateChain(KEYSTORE_PEM_CONTENTS)
            .setKeystoreKey(KEY_PEM_CONTENTS)
            .setTruststoreType(SslConfig.TruststoreType.PEM)
            .setTruststoreCertificates(TRUSTSTORE_PEM_CONTENTS)
            .build();
    private static final SslConfig VALID_PEM_BUILDER_FILE_BASED = SslConfig.newBuilder()
            .setKeystoreType(SslConfig.KeystoreType.PEM)
            .setKeystoreLocation(KEYSTORE_PEM_PATH)
            .setKeyPassword(KEY_PEM_CONTENTS)
            .setTruststoreType(SslConfig.TruststoreType.PEM)
            .setTruststoreLocation(TRUSTSTORE_PEM_PATH)
            .build();

    @Test
    public void keystoreValid_jks() {
        final SslConfig valid = SslConfig.newBuilder(VALID_JKS_BUILDER).build();

        assertEquals(DEFAULT_PASSWORD, valid.getKeystorePassword().getValue());
        assertEquals(DEFAULT_PASSWORD, valid.getKeyPassword().getValue());
        assertEquals(KEYSTORE_JKS_PATH, valid.getKeystoreLocation());
        assertEquals(TRUSTSTORE_JKS_PATH, valid.getTruststoreLocation());
        assertEquals(DEFAULT_PASSWORD, valid.getTruststorePassword().getValue());
        assertEquals(SslConfig.KeystoreType.JKS, valid.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.JKS, valid.getTruststoreType());
        assertNull(valid.getKeystoreCertificateChain());
        assertNull(valid.getTruststoreCertificates());
    }

    @Test
    public void keystoreValid_pem_string() {
        final SslConfig valid = SslConfig.newBuilder(VALID_PEM_BUILDER).build();

        assertEquals(KEYSTORE_PEM_CONTENTS, valid.getKeystoreCertificateChain().getValue());
        assertEquals(KEY_PEM_CONTENTS, valid.getKeystoreKey().getValue());
        assertEquals(TRUSTSTORE_PEM_CONTENTS, valid.getTruststoreCertificates().getValue());
        assertEquals(SslConfig.KeystoreType.PEM, valid.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PEM, valid.getTruststoreType());
        assertNull(valid.getKeystoreLocation());
        assertNull(valid.getTruststoreLocation());
    }

    @Test
    public void keystoreValid_pem_fileBased() {
        final SslConfig valid = SslConfig.newBuilder(VALID_PEM_BUILDER_FILE_BASED).build();

        assertEquals(KEYSTORE_PEM_PATH, valid.getKeystoreLocation());
        assertEquals(TRUSTSTORE_PEM_PATH, valid.getTruststoreLocation());
        assertEquals(KEY_PEM_CONTENTS, valid.getKeyPassword().getValue());
        assertEquals(SslConfig.KeystoreType.PEM, valid.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PEM, valid.getTruststoreType());
        assertNull(valid.getTruststoreCertificates());
        assertNull(valid.getKeystoreKey());
        assertNull(valid.getKeystoreCertificateChain());
    }

    // Keystore validations

    @Test(expected = ConfigurationException.class)
    public void keystoreJks_keystoreKey() {
        SslConfig.newBuilder(VALID_JKS_BUILDER)
                .setKeystoreKey("key").build();
    }

    @Test(expected = ConfigurationException.class)
    public void keystorePem_keystoreKey_noChain() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreCertificateChain((PasswordConfig) null).build();
    }

    @Test(expected = ConfigurationException.class)
    public void keystorePem_keystoreKey_withLocation() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreLocation(KEYSTORE_JKS_PATH).build();
    }

    // Both SSL key store location and separate private key are specified.
    @Test(expected = ConfigurationException.class)
    public void keystorePemKey_withKeystoreLocation() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreKey(DEFAULT_PASSWORD)
                .setKeystoreLocation(KEYSTORE_PEM_PATH)
                .build();
    }

    // SSL key store password cannot be specified with PEM format, only key password may be specified.
    @Test(expected = ConfigurationException.class)
    public void keystorePemKey_withKeystorePassword() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreKey(DEFAULT_PASSWORD)
                .setKeystorePassword(DEFAULT_PASSWORD)
                .build();
    }

    // SSL certificate chain is specified, but private key is not specified.
    @Test(expected = ConfigurationException.class)
    public void keystorePem_NoPrivateKey() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreKey((PasswordConfig) null)
                .setKeystorePassword(DEFAULT_PASSWORD)
                .setKeystoreLocation(KEYSTORE_PEM_PATH)
                .build();
    }

    // SSL key store password cannot be specified with PEM format, only key password may be specified
    @Test(expected = ConfigurationException.class)
    public void keystorePemFileBased_nullKeystorePassword() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreKey((PasswordConfig) null)
                .setKeystoreLocation(KEYSTORE_PEM_PATH)
                .setKeystorePassword(DEFAULT_PASSWORD)
                .setKeystoreCertificateChain((PasswordConfig) null)
                .build();
    }

    // SSL PEM key store is specified, but key password is not specified
    @Test(expected = ConfigurationException.class)
    public void keystorePemFileBased_nullKeyPassword() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setKeystoreKey((PasswordConfig) null)
                .setKeystoreLocation(KEYSTORE_PEM_PATH)
                .setKeystoreCertificateChain((PasswordConfig) null)
                .build();
    }

    // SSL key store is not specified, but key store password is specified.
    @Test(expected = ConfigurationException.class)
    public void keystoreJks_nullLocation() {
        SslConfig.newBuilder(VALID_JKS_BUILDER)
                .setKeystoreLocation(null)
                .build();
    }

    // SSL key store is specified, but key store password is not specified.
    @Test(expected = ConfigurationException.class)
    public void keystoreJks_nullPassword() {
        SslConfig.newBuilder(VALID_JKS_BUILDER)
                .setKeystorePassword((PasswordConfig) null)
                .build();
    }

    // Truststore validations

    // SSL trust store certs can be specified only for PEM, but trust store.
    @Test(expected = ConfigurationException.class)
    public void truststoreNonPem_nonPemCertificates() {
        SslConfig.newBuilder(VALID_JKS_BUILDER)
                .setTruststoreCertificates(DEFAULT_PASSWORD)
                .build();
    }

    // Both SSL trust store location and separate trust certificates are specified.
    @Test(expected = ConfigurationException.class)
    public void truststore_chainWithLocation() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setTruststoreLocation(DEFAULT_PASSWORD)
                .build();
    }

    // SSL trust store password cannot be specified for PEM format.
    @Test(expected = ConfigurationException.class)
    public void truststorePem_chainWithPassword() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setTruststorePassword(DEFAULT_PASSWORD)
                .build();
    }

    // SSL trust store password cannot be specified for PEM format.
    @Test(expected = ConfigurationException.class)
    public void truststorePem_fileWithPassword() {
        SslConfig.newBuilder(VALID_PEM_BUILDER)
                .setTruststoreCertificates((PasswordConfig) null)
                .setTruststoreLocation(TRUSTSTORE_PEM_PATH)
                .setTruststorePassword(DEFAULT_PASSWORD)
                .build();
    }

    // SSL trust store is not specified, but trust store password is specified.
    @Test(expected = ConfigurationException.class)
    public void truststore_NoStoreWithPassword() {
        SslConfig.newBuilder(VALID_JKS_BUILDER)
                .setTruststoreLocation(null)
                .setTruststorePassword(DEFAULT_PASSWORD)
                .build();
    }
}
