package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.types.Password;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.test.TestUtils;

import static io.axual.common.tools.SslUtilTest.KEYSTORE_PEM_CONTENTS;
import static io.axual.common.tools.SslUtilTest.KEY_PEM_UNENCRYPTED_CONTENTS;
import static io.axual.common.tools.SslUtilTest.TRUSTSTORE_PEM_CONTENTS;
import static org.junit.Assert.assertNotNull;

public class SslEngineConfigTest {

    @Test
    public void valid_jks_jks() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.keystore.jks"));
        configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.truststore.jks"));
        configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));

        SslEngineConfig sslEngineConfig = new SslEngineConfig(configs);

        assertNotNull(sslEngineConfig.keystore());
        assertNotNull(sslEngineConfig.truststore());
    }

    @Test
    public void valid_pkcs12_pkcs12() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "PKCS12");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "PKCS12");
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.keystore.p12"));
        configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.truststore.p12"));
        configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));

        SslEngineConfig sslEngineConfig = new SslEngineConfig(configs);

        assertNotNull(sslEngineConfig.keystore());
        assertNotNull(sslEngineConfig.truststore());
    }

    @Test
    public void valid_pem_pem() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        configs.put(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, new Password(KEYSTORE_PEM_CONTENTS));
        configs.put(SslConfigs.SSL_KEYSTORE_KEY_CONFIG, new Password(KEY_PEM_UNENCRYPTED_CONTENTS));
        configs.put(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG, new Password(TRUSTSTORE_PEM_CONTENTS));

        SslEngineConfig sslEngineConfig = new SslEngineConfig(configs);

        assertNotNull(sslEngineConfig.keystore());
        assertNotNull(sslEngineConfig.truststore());
    }

    @Test
    public void valid_pem_jks() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, new Password(KEYSTORE_PEM_CONTENTS));
        configs.put(SslConfigs.SSL_KEYSTORE_KEY_CONFIG, new Password(KEY_PEM_UNENCRYPTED_CONTENTS));
        configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.truststore.jks"));
        configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));

        SslEngineConfig sslEngineConfig = new SslEngineConfig(configs);

        assertNotNull(sslEngineConfig.keystore());
        assertNotNull(sslEngineConfig.truststore());
    }

    @Test
    public void valid_jks_pem() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.keystore.jks"));
        configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG, new Password(TRUSTSTORE_PEM_CONTENTS));

        SslEngineConfig sslEngineConfig = new SslEngineConfig(configs);

        assertNotNull(sslEngineConfig.keystore());
        assertNotNull(sslEngineConfig.truststore());
    }

    @Test(expected = KafkaException.class)
    public void noSuchFile_jks_jks() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, "JKS");
        configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL);
        configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "no.such.file.jks");
        configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));
        configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, TestUtils.getAbsolutePath("ssl/axual.client.truststore.jks"));
        configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, new Password(TestUtils.DEFAULT_PASSWORD));

        new SslEngineConfig(configs);
    }

}
