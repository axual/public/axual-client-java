package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.security.auth.SslEngineFactory;
import org.apache.kafka.common.security.ssl.DefaultSslEngineFactory;

import java.security.KeyStore;
import java.util.Map;

/**
 * Encapsulates an instance of {@link SslEngineFactory} which is used to load key and trust material.
 *
 * This is utility for proxying calls to SslEngineConfig.
 */
public class SslEngineConfig extends AbstractConfig {

    public static final String SSL_ENGINE_FACTORY_CONFIG = "ssl.engine.factory";
    private static final ConfigDef sslConfigDef;

    static {
        sslConfigDef = new ConfigDef()
                .define(SSL_ENGINE_FACTORY_CONFIG,
                        ConfigDef.Type.CLASS,
                        DefaultSslEngineFactory.class,
                        ConfigDef.Importance.LOW,
                        "");
        SslConfigs.addClientSslSupport(sslConfigDef);
    }

    private final SslEngineFactory sslEngineFactory;

    public SslEngineConfig(Map<String, ?> originals) {
        super(sslConfigDef, originals);
        sslEngineFactory = getConfiguredInstance(SSL_ENGINE_FACTORY_CONFIG, SslEngineFactory.class);
    }

    /**
     * Proxies the call to the {@link SslEngineFactory}
     *
     * @return keystore of configured {@link SslEngineFactory}
     */
    public KeyStore keystore() {
        return sslEngineFactory.keystore();
    }

    /**
     * Proxies the call to the {@link SslEngineFactory}
     *
     * @return truststore of configured {@link SslEngineFactory}
     */
    public KeyStore truststore() {
        return sslEngineFactory.truststore();
    }

}
