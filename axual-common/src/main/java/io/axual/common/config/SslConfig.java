package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.SslConfigs;

import java.util.Objects;

import io.axual.common.exception.ConfigurationException;
import io.axual.common.tools.StringUtil;

/**
 * This class holds the typical parameters used to set up secure connections between applications
 * and application servers within Axual Platform.
 */
public class SslConfig {

  public enum KeystoreType { JKS, PKCS12, PEM }
  public enum TruststoreType { JKS, PKCS12, PEM }

  public static final String DEFAULT_SSL_PROTOCOL = SslConfigs.DEFAULT_SSL_PROTOCOL;
  public static final String DEFAULT_ENABLED_PROTOCOLS = "TLSv1.2,TLSv1.1";

  private final PasswordConfig keyPassword;
  private final String keystoreLocation;
  private final PasswordConfig keystorePassword;
  private final PasswordConfig keystoreCertificateChain;
  private final PasswordConfig keystoreKey;
  private final String truststoreLocation;
  private final PasswordConfig truststorePassword;
  private final PasswordConfig truststoreCertificates;
  private final String sslProtocol;
  private final boolean enableHostnameVerification;
  private final KeystoreType keystoreType;
  private final TruststoreType truststoreType;
  private final boolean enableValidateTruststore;

  private SslConfig(Builder builder) {
    this.keystoreLocation = builder.keystoreLocation;
    this.keystorePassword = builder.keystorePassword;
    this.keystoreCertificateChain = builder.keystoreCertificateChain;
    this.keystoreKey = builder.keystoreKey;
    this.keyPassword = builder.keyPassword;

    this.truststoreLocation = builder.truststoreLocation;
    this.truststorePassword = builder.truststorePassword;
    this.truststoreCertificates = builder.truststoreCertificates;

    this.sslProtocol = builder.sslProtocol;
    this.enableHostnameVerification = builder.enableHostnameVerification;
    this.enableValidateTruststore = builder.enableValidateTruststore;
    this.keystoreType = builder.keystoreType;
    this.truststoreType = builder.truststoreType;

    validateConfigs();
  }

  private void validateConfigs() {
    validateKeystoreConfigs();
    validateTruststoreConfigs();
  }

  /**
   * Validates the keystore related configuration based on logic found in
   * {@link org.apache.kafka.common.security.ssl.DefaultSslEngineFactory}
   */
  private void validateKeystoreConfigs() {
    if (this.keystoreKey != null ) {
      if (!KeystoreType.PEM.equals(keystoreType)) {
        throw new ConfigurationException("SSL private key can be specified only for PEM, but key store type is " + keystoreType + ".");
      } else if (keystoreCertificateChain == null) {
        throw new ConfigurationException("SSL private key is specified, but certificate chain is not specified.");
      } else if (keystoreLocation != null) {
        throw new ConfigurationException("Both SSL key store location and separate private key are specified.");
      } else if (keystorePassword != null) {
        throw new ConfigurationException("SSL key store password cannot be specified with PEM format, only key password may be specified.");
      }
    } else if (keystoreCertificateChain != null) {
      throw new ConfigurationException("SSL certificate chain is specified, but private key is not specified.");
    } else if (KeystoreType.PEM.equals(keystoreType) && keystoreLocation != null) {
      if (keystorePassword != null) {
        throw new ConfigurationException("SSL key store password cannot be specified with PEM format, only key password may be specified.");
      } else if (keyPassword == null) {
        throw new ConfigurationException("SSL PEM key store is specified, but key password is not specified.");
      }
      // FileBasedPemStore
    } else if (keystoreLocation == null && keystorePassword != null) {
      throw new ConfigurationException("SSL key store is not specified, but key store password is specified.");
    } else if (keystoreLocation != null && keystorePassword == null) {
      throw new ConfigurationException("SSL key store is specified, but key store password is not specified.");
    }
  }

  /**
   * Validates the truststore related configuration based on logic found in
   * {@link org.apache.kafka.common.security.ssl.DefaultSslEngineFactory}
   */
  private void validateTruststoreConfigs() {
    if (truststoreCertificates != null) {
      if (!TruststoreType.PEM.equals(truststoreType)) {
        throw new ConfigurationException("SSL trust store certs can be specified only for PEM, but trust store type is " + truststoreType + ".");
      } else if (truststoreLocation != null) {
        throw new ConfigurationException("Both SSL trust store location and separate trust certificates are specified.");
      } else if (truststorePassword != null) {
        throw new ConfigurationException("SSL trust store password cannot be specified for PEM format.");
      }
    } else if (TruststoreType.PEM.equals(truststoreType) && truststoreLocation != null) {
      if (truststorePassword != null) {
        throw new ConfigurationException("SSL trust store password cannot be specified for PEM format.");
      }
    } else if (truststoreLocation == null && truststorePassword != null) {
      throw new ConfigurationException("SSL trust store is not specified, but trust store password is specified.");
    }
  }

  /**
   * Creates a new builder for SslConfig
   *
   * @return the builder
   */
  public static Builder newBuilder() {
    return new Builder();
  }

  /**
   * Creates a new builder for SslConfig  based on the original SslConfig.
   *
   * @param original The configuration from which the builder values will be populated
   * @return the builder using the same values as the original SslConfig
   */
  public static Builder newBuilder(SslConfig original) {
    return new Builder()
            .setKeystoreType(original.getKeystoreType())
            .setKeystoreLocation(original.getKeystoreLocation())
            .setKeystorePassword(original.getKeystorePassword())
            .setKeystoreCertificateChain(original.getKeystoreCertificateChain())
            .setKeystoreKey(original.getKeystoreKey())
            .setKeyPassword(original.getKeyPassword())
            .setTruststoreType(original.getTruststoreType())
            .setTruststoreLocation(original.getTruststoreLocation())
            .setTruststorePassword(original.getTruststorePassword())
            .setTruststoreCertificates(original.getTruststoreCertificates())
            .setSslProtocol(original.getSslProtocol())
            .setEnableHostnameVerification(original.getEnableHostnameVerification())
            .setEnableValidateTruststore(original.getEnableValidateTruststore())
            ;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    SslConfig other = (SslConfig) o;

    if (!Objects.equals(keyPassword, other.keyPassword)) {
      return false;
    }
    if (!Objects.equals(keystoreLocation, other.keystoreLocation)) {
      return false;
    }
    if (!Objects.equals(keystorePassword, other.keystorePassword)) {
      return false;
    }
    if (!Objects.equals(keystoreCertificateChain, other.keystoreCertificateChain)) {
      return false;
    }
    if (!Objects.equals(keystoreKey, other.keystoreKey)) {
      return false;
    }
    if (!Objects.equals(truststoreLocation, other.truststoreLocation)) {
      return false;
    }
    if (!Objects.equals(truststorePassword, other.truststorePassword)) {
      return false;
    }
    if (!Objects.equals(truststoreCertificates, other.truststoreCertificates)) {
      return false;
    }

    return enableHostnameVerification == other.enableHostnameVerification;
  }

  @Override
  public int hashCode() {
    int result = keyPassword != null ? keyPassword.hashCode() : 0;
    result = 31 * result + (keystoreLocation != null ? keystoreLocation.hashCode() : 0);
    result = 31 * result + (keystorePassword != null ? keystorePassword.hashCode() : 0);
    result = 31 * result + (keystoreCertificateChain != null ? keystoreCertificateChain.hashCode() : 0);
    result = 31 * result + (keystoreKey != null ? keystoreKey.hashCode() : 0);
    result = 31 * result + (truststoreLocation != null ? truststoreLocation.hashCode() : 0);
    result = 31 * result + (truststorePassword != null ? truststorePassword.hashCode() : 0);
    result = 31 * result + (truststoreCertificates != null ? truststoreCertificates.hashCode() : 0);
    result = 31 * result + (sslProtocol != null ? sslProtocol.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return StringUtil.join(",",
            keyPassword == null ? "null" : keyPassword.toString(),
            keystoreLocation == null ? "null" : keystoreLocation,
            keystorePassword == null ? "null" : keystorePassword.toString(),
            truststoreLocation == null ? "null" : truststoreLocation,
            truststorePassword == null ? "null" : truststorePassword.toString(),
            sslProtocol,
            "Hostname verification " +
                    (enableHostnameVerification ? "enabled" : "disabled"),
            "Truststore validation " + (enableValidateTruststore ? "enabled" : "disabled")
    );
  }

  public KeystoreType getKeystoreType() {
    return keystoreType;
  }

  public TruststoreType getTruststoreType() {
    return truststoreType;
  }

  /**
   * Gets the password to access the private key in the keystore.
   *
   * @return the key password
   */
  public PasswordConfig getKeyPassword() {
    return keyPassword;
  }

  /**
   * Gets the keystore location, being either a path name to a locally stored keystore, or a
   * location to a resource bundled in the application's binary.
   *
   * @return the keystore location
   */
  public String getKeystoreLocation() {
    return keystoreLocation;
  }

  /**
   * Gets the password to access the keystore.
   *
   * @return the keystore password
   */
  public PasswordConfig getKeystorePassword() {
    return keystorePassword;
  }

  /**
   * Gets the keystore in PEM format
   *
   * @return the keystore certificate chain.
   */
  public PasswordConfig getKeystoreCertificateChain() {
    return keystoreCertificateChain;
  }

  /**
   * Gets the keystore key in PEM format
   *
   * @return the keystore key.
   */
  public PasswordConfig getKeystoreKey() {
    return keystoreKey;
  }

  /**
   * Gets the truststore location, being either a path name to a locally stored truststore, or a
   * location to a resource bundled in the application's binary.
   *
   * @return the truststore location
   */
  public String getTruststoreLocation() {
    return truststoreLocation;
  }

  /**
   * Gets the trusted certificates in the format specified by 'ssl.truststore.type'. Default SSL
   * engine factory supports only PEM format with X.509 certificates.
   *
   * @return the truststore certificates
   */
  public PasswordConfig getTruststoreCertificates() {
    return truststoreCertificates;
  }

  /**
   * Gets the password to access the truststore.
   *
   * @return the truststore password
   */
  public PasswordConfig getTruststorePassword() {
    return truststorePassword;
  }

  /**
   * Gets configured SSL protocols.
   *
   * @return the configured SSL protocol
   */
  public String getSslProtocol() {
    return sslProtocol;
  }

  /**
   * Check whether hostname verification is enabled. When so, HTTPS verification is done by the
   * client application to verify that the server has presented a certificate with the right domain
   * or SAN.
   *
   * @return true if hostname verification is enabled, false otherwise
   */
  public boolean getEnableHostnameVerification() {
    return enableHostnameVerification;
  }

  /**
   * Checks whether the validation of expired or not yet valid certification is enabled.
   *
   * @return true if truststore validation is enabled, false otherwise
   */
  public boolean getEnableValidateTruststore() {
    return enableValidateTruststore;
  }

  /**
   * The Builder class for SslConfig.
   */
  public static class Builder {

    private PasswordConfig keyPassword;
    private String keystoreLocation;
    private PasswordConfig keystorePassword;
    private String truststoreLocation;
    private PasswordConfig truststorePassword;
    private PasswordConfig keystoreCertificateChain;
    private PasswordConfig keystoreKey;
    private PasswordConfig truststoreCertificates;

    private String sslProtocol = DEFAULT_SSL_PROTOCOL;
    private boolean enableHostnameVerification = true;
    private boolean enableValidateTruststore = true;
    private KeystoreType keystoreType = KeystoreType.JKS;
    private TruststoreType truststoreType = TruststoreType.JKS;

    /**
     * Sets the keystore type format
     *
     * @param keystoreType
     * @return
     */
    public Builder setKeystoreType(KeystoreType keystoreType) {
      this.keystoreType = keystoreType;
      return this;
    }

    /**
     * @return the keystore type
     */
    public KeystoreType getKeystoreType() {
      return keystoreType;
    }

    /**
     * Sets truststore type format
     *
     * @param truststoreType
     * @return
     */
    public Builder setTruststoreType(TruststoreType truststoreType) {
      this.truststoreType = truststoreType;
      return this;
    }

    /**
     * @return truststore type
     */
    public TruststoreType getTruststoreType() {
      return truststoreType;
    }


    /**
     * Gets the password to access the private key in the keystore.
     *
     * @return the key password
     */
    public PasswordConfig getKeyPassword() {
      return keyPassword;
    }

    /**
     * Sets the password to access the private key in the keystore.
     *
     * @param keyPassword the key password
     * @return the Builder
     */
    public Builder setKeyPassword(PasswordConfig keyPassword) {
      this.keyPassword = keyPassword;
      return this;
    }

    /**
     * Sets the password to access the private key in the keystore.
     *
     * @param keyPassword the key password
     * @return the Builder
     */
    public Builder setKeyPassword(String keyPassword) {
      return setKeyPassword(new PasswordConfig(keyPassword));
    }

    /**
     * Gets the keystore location, being either a path name to a locally stored keystore, or a
     * location to a resource bundled in the application's binary.
     *
     * @return the keystore location
     */
    public String getKeystoreLocation() {
      return keystoreLocation;
    }

    /**
     * Sets keystore location.
     *
     * @param keystoreLocation the keystore location
     * @return the Builder
     */
    public Builder setKeystoreLocation(String keystoreLocation) {
      this.keystoreLocation = keystoreLocation;
      return this;
    }

    /**
     * Gets the password to access the truststore.
     *
     * @return the truststore password
     */
    public PasswordConfig getKeystorePassword() {
      return keystorePassword;
    }

    /**
     * Sets password to access the keystore.
     *
     * @param keystorePassword the keystore password
     * @return the Builder
     */
    public Builder setKeystorePassword(PasswordConfig keystorePassword) {
      this.keystorePassword = keystorePassword;
      return this;
    }

    /**
     * Sets password to access the keystore.
     *
     * @param keystorePassword the keystore password
     * @return the Builder
     */
    public Builder setKeystorePassword(String keystorePassword) {
      return setKeystorePassword(new PasswordConfig(keystorePassword));
    }

    /**
     * Gets the truststore location, being either a path name to a locally stored truststore, or a
     * location to a resource bundled in the application's binary.
     *
     * @return the truststore location
     */
    public String getTruststoreLocation() {
      return truststoreLocation;
    }

    /**
     * Sets the truststore location, being either a path name to a locally stored truststore, or a
     * location to a resource bundled in the application's binary.
     *
     * @param truststoreLocation the truststore location
     * @return the Builder
     */
    public Builder setTruststoreLocation(String truststoreLocation) {
      this.truststoreLocation = truststoreLocation;
      return this;
    }

    /**
     * Gets the truststore certificates in PEM format.
     *
     * @return the truststore PEM
     */
    public PasswordConfig getTruststoreCertificates() {
      return truststoreCertificates;
    }

    /**
     * Sets the truststore location, being either a path name to a locally stored truststore, or a
     * location to a resource bundled in the application's binary.
     *
     * @param truststoreCertificates the truststore certificate pem
     * @return the Builder
     */
    public Builder setTruststoreCertificates(String truststoreCertificates) {
      return setTruststoreCertificates(new PasswordConfig(truststoreCertificates));
    }

    /**
     * Sets the truststore location, being either a path name to a locally stored truststore, or a
     * location to a resource bundled in the application's binary.
     *
     * @param truststoreCertificates the truststore certificate pem
     * @return the Builder
     */
    public Builder setTruststoreCertificates(PasswordConfig truststoreCertificates) {
      this.truststoreCertificates = truststoreCertificates;
      return this;
    }

    /**
     * Gets the keystore certificates in PEM format.
     *
     * @return the truststore PEM
     */
    public PasswordConfig getKeystoreCertificateChain() {
      return keystoreCertificateChain;
    }

    /**
     * Sets the keystore cert chain.
     *
     * @param keystoreCertificateChain Certificate chain in the format specified by
     *                                 'ssl.keystore.type'. Default SSL engine factory supports only
     *                                 PEM format with a list of X.509 certificates.
     * @return the Builder
     */
    public Builder setKeystoreCertificateChain(String keystoreCertificateChain) {
      return setKeystoreCertificateChain(new PasswordConfig(keystoreCertificateChain));
    }

    /**
     * Sets the keystore cert chain.
     *
     * @param keystoreCertificateChain Certificate chain in the format specified by
     *                                 'ssl.keystore.type'. Default SSL engine factory supports only
     *                                 PEM format with a list of X.509 certificates.
     * @return the Builder
     */
    public Builder setKeystoreCertificateChain(PasswordConfig keystoreCertificateChain) {
      this.keystoreCertificateChain = keystoreCertificateChain;
      return this;
    }


    /**
     * Gets the keystore certificates in form of PEM.
     *
     * @return the truststore PEM
     */
    public PasswordConfig getKeystoreKey() {
      return keystoreKey;
    }

    /**
     * Sets the private key in the format specified by 'ssl.keystore.type'. If the key is encrypted,
     * key password must be specified using 'ssl.key.password'
     *
     * @param keystoreKey the keystore certificate pem
     * @return the Builder
     */
    public Builder setKeystoreKey(PasswordConfig keystoreKey) {
      this.keystoreKey = keystoreKey;
      return this;
    }

    /**
     * Sets the private key in the format specified by 'ssl.keystore.type'. If the key is encrypted,
     * key password must be specified using 'ssl.key.password'
     *
     * @param keystoreKey the keystore certificate pem
     * @return the Builder
     */
    public Builder setKeystoreKey(String keystoreKey) {
      return setKeystoreKey(new PasswordConfig(keystoreKey));
    }


    /**
     * Gets truststore password.
     *
     * @return the truststore password
     */
    public PasswordConfig getTruststorePassword() {
      return truststorePassword;
    }

    /**
     * Sets truststore password.
     *
     * @param truststorePassword the truststore password
     * @return the Builder
     */
    public Builder setTruststorePassword(PasswordConfig truststorePassword) {
      this.truststorePassword = truststorePassword;
      return this;
    }

    /**
     * Sets truststore password.
     *
     * @param truststorePassword the truststore password
     * @return the Builder
     */
    public Builder setTruststorePassword(String truststorePassword) {
      return setTruststorePassword(new PasswordConfig(truststorePassword));
    }

    /**
     * Gets SSL protocol.
     *
     * @return the configured SSL protocol
     */
    public String getSslProtocol() {
      return sslProtocol;
    }

    /**
     * Sets configured SSL protocol. Default is "TLS".
     *
     * @param sslProtocol the protocols by the client application's JVM.
     * @return the SSL protocol
     */
    public Builder setSslProtocol(String sslProtocol) {
      this.sslProtocol = sslProtocol;
      return this;
    }

    /**
     * Check whether hostname verification is enabled. When so, HTTPS verification is done by the
     * client application to verify that the server has presented a certificate with the right
     * domain or SAN.
     *
     * @return true if hostname verification is enabled, false otherwise
     */
    public boolean getEnableHostnameVerification() {
      return enableHostnameVerification;
    }

    /**
     * Sets enable hostname verification.
     *
     * @param enableHostnameVerification the enable hostname verification
     * @return the Builder
     */
    public Builder setEnableHostnameVerification(boolean enableHostnameVerification) {
      this.enableHostnameVerification = enableHostnameVerification;
      return this;
    }

    /**
     * Checks whether the validation of expired or not yet valid certification is enabled.
     *
     * @return true if truststore validation is enabled, false otherwise
     */
    public boolean getEnableValidateTruststore() {
      return enableValidateTruststore;
    }

    /**
     * Sets enable validate truststore.
     *
     * @param enableValidateTruststore enables truststore validation at startup
     * @return the Builder
     */
    public Builder setEnableValidateTruststore(final boolean enableValidateTruststore) {
      this.enableValidateTruststore = enableValidateTruststore;
      return this;
    }

    /**
     * Builds and returns an SslConfig.
     *
     * @return the newly created SslConfig
     */
    public SslConfig build() {
      return new SslConfig(this);
    }
  }
}
