package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Objects;

import io.axual.common.tools.StringUtil;

/**
 * Client applications for Axual Platform are generally configured using only a few parameters that
 * application developers need to worry about. This class holds those elements.
 */
public class ClientConfig {
    private final String applicationId;
    private final String applicationVersion;
    private final String endpoint;
    private final String environment;
    private final String tenant;
    private final SslConfig sslConfig;
    private final String tempDir;
    private final boolean disableTemporarySecurityFile;
    private final boolean idempotenceEnabled;
    private final String transactionalId;

    private ClientConfig(Builder builder) {
        this.applicationId = builder.applicationId;
        this.applicationVersion = builder.applicationVersion;
        this.endpoint = builder.endpoint;
        this.environment = builder.environment;
        this.tenant = builder.tenant;
        this.sslConfig = builder.sslConfig;
        this.tempDir = builder.tempDir;
        this.disableTemporarySecurityFile = builder.disableTemporarySecurityFile;
        this.idempotenceEnabled = builder.idempotenceEnabled;
        this.transactionalId = builder.transactionalId;
    }

    /**
     * Create a new builder for this class.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Create a new builder for this class based on the original ClientConfig.
     *
     * @param original The configuration from which the builder values will be populated
     * @return the builder using the same values as the original ClientConfig
     */
    public static Builder newBuilder(ClientConfig original) {
        return new Builder()
                .setTenant(original.getTenant())
                .setEnvironment(original.getEnvironment())
                .setEndpoint(original.getEndpoint())
                .setApplicationId(original.getApplicationId())
                .setApplicationVersion(original.getApplicationVersion())
                .setTempDir(original.getTempDir())
                .setDisableTemporarySecurityFile(original.getDisableTemporarySecurityFile())
                .setSslConfig(SslConfig.newBuilder(original.getSslConfig()).build());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientConfig other = (ClientConfig) o;

        if (!Objects.equals(applicationId, other.applicationId)) {
            return false;
        }
        if (!Objects.equals(applicationVersion, other.applicationVersion)) {
            return false;
        }
        if (!Objects.equals(endpoint, other.endpoint)) {
            return false;
        }
        if (!Objects.equals(environment, other.environment)) {
            return false;
        }
        if (!Objects.equals(tenant, other.tenant)) {
            return false;
        }
        if (!Objects.equals(transactionalId, other.transactionalId)) {
            return false;
        }
        if (idempotenceEnabled != other.idempotenceEnabled) {
            return false;
        }
        return Objects.equals(sslConfig, other.sslConfig);
    }

    @Override
    public int hashCode() {
        int result = applicationId != null ? applicationId.hashCode() : 0;
        result = 31 * result + (applicationVersion != null ? applicationVersion.hashCode() : 0);
        result = 31 * result + (endpoint != null ? endpoint.hashCode() : 0);
        result = 31 * result + (environment != null ? environment.hashCode() : 0);
        result = 31 * result + (tenant != null ? tenant.hashCode() : 0);
        result = 31 * result + (sslConfig != null ? sslConfig.hashCode() : 0);
        result = 31 * result + (transactionalId != null ? transactionalId.hashCode() : 0);
        result = 31 * result + Boolean.hashCode(idempotenceEnabled);
        return result;
    }

    @Override
    public String toString() {
        return StringUtil.join(",",
                applicationId,
                applicationVersion,
                endpoint,
                environment,
                tenant,
                sslConfig != null ? sslConfig.toString() : "null",
                transactionalId != null ? "transactionalId=" + transactionalId : "",
                "idempotenceEnabled=" + idempotenceEnabled
        );
    }

    public String getTransactionalId() {
        return transactionalId;
    }

    /**
     * Gets the client application's applicationId.
     *
     * @return the application id
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Gets the client application's version.
     *
     * @return the application version
     */
    public String getApplicationVersion() {
        return applicationVersion;
    }

    /**
     * Gets the Axual Platform endpoint, which typically is the URL to the platform's Discovery
     * API.
     *
     * @return the endpoint
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * Gets the client application's environment, eg. "test" or "prod".
     *
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * Gets the client application's tenant.
     *
     * @return the tenant
     */
    public String getTenant() {
        return tenant;
    }

    /**
     * Gets the ssl config with which the client application connects to Axual Platform.
     *
     * @return the ssl config
     */
    public SslConfig getSslConfig() {
        return sslConfig;
    }

    /**
     * Get the temp directory path
     *
     * @return the path to the temp directory
     */
    public String getTempDir() {
        return tempDir;
    }

    /**
     *
     * @return true if the use of a temporary security file is disabled
     */
    public boolean getDisableTemporarySecurityFile() {
        return disableTemporarySecurityFile;
    }

    /** @return true if Kafka transactions are enabled in this configuration. */
    public boolean isIdempotenceEnabled() {
        return idempotenceEnabled;
    }

    /**
     * The type Builder.
     */
    public static class Builder {
        private boolean idempotenceEnabled;
        private String transactionalId;
        private String applicationId;
        private String applicationVersion = "unknown";
        private String endpoint;
        private String environment;
        private String tenant;
        private SslConfig sslConfig;
        private String tempDir;
        private boolean disableTemporarySecurityFile = false;

        /**
         * Enable idempotence. This also requires setting a transactional ID.
         * @return the builder instance.
         */
        public Builder enableIdempotence() {
            this.idempotenceEnabled = true;
            return this;
        }

        /**
         * Sets the transactional ID.
         * @param transactionalId the transaction id
         * @return the builder instance.
         */
        public Builder setTransactionalId(String transactionalId) {
            this.transactionalId = transactionalId;
            return this;
        }

        /**
         * Gets application id.
         *
         * @return the application id
         */
        public String getApplicationId() {
            return applicationId;
        }

        /**
         * Sets application id.
         *
         * @param applicationId the application id
         * @return the Builder
         */
        public Builder setApplicationId(String applicationId) {
            this.applicationId = applicationId;
            return this;
        }

        /**
         * Gets application version.
         *
         * @return the application version
         */
        public String getApplicationVersion() {
            return applicationVersion;
        }

        /**
         * Sets application version.
         *
         * @param applicationVersion the application version
         * @return the Builder
         */
        public Builder setApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        /**
         * Gets the Axual Platform endpoint, which typically is the URL to the platform's Discovery
         * API.
         *
         * @return the endpoint
         */
        public String getEndpoint() {
            return endpoint;
        }

        /**
         * Sets the Axual Platform endpoint, which typically is the URL to the platform's Discovery
         * API.
         *
         * @param endpoint the endpoint
         * @return the Builder
         */
        public Builder setEndpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        /**
         * Gets environment.
         *
         * @return the environment
         */
        public String getEnvironment() {
            return environment;
        }

        /**
         * Sets environment.
         *
         * @param environment the environment
         * @return the Builder
         */
        public Builder setEnvironment(String environment) {
            this.environment = environment;
            return this;
        }

        /**
         * Gets tenant.
         *
         * @return the tenant
         */
        public String getTenant() {
            return tenant;
        }

        /**
         * Sets tenant.
         *
         * @param tenant the tenant
         * @return the Builder
         */
        public Builder setTenant(String tenant) {
            this.tenant = tenant;
            return this;
        }

        /**
         * Gets ssl config.
         *
         * @return the ssl config
         */
        public SslConfig getSslConfig() {
            return sslConfig;
        }

        /**
         * Sets ssl config.
         *
         * @param sslConfig the ssl config
         * @return the Builder
         */
        public Builder setSslConfig(SslConfig sslConfig) {
            this.sslConfig = sslConfig;
            return this;
        }

        /**
         * Build client configuration object.
         *
         * @return the newly created ClientConfig
         */
        public ClientConfig build() {
            return new ClientConfig(this);
        }

        /**
         * Sets temp directory path
         *
         * @param tempDir the path to the new temp directory
         * @return the Builder
         */
        public Builder setTempDir(String tempDir) {
            this.tempDir = tempDir;
            return this;
        }

        /**
         *
         * @return the temp directory path
         */
        public String getTempDir() {
            return tempDir;
        }

        /**
         *
         * @param disableTemporarySecurityFile set to true to disable the use of a temporary file
         * @return the Builder
         */
        public Builder setDisableTemporarySecurityFile(boolean disableTemporarySecurityFile) {
            this.disableTemporarySecurityFile = disableTemporarySecurityFile;
            return this;
        }

        /**
         *
         * @return true if no temporary security file should be used
         */
        public boolean getDisableTemporarySecurityFile() {
            return disableTemporarySecurityFile;
        }
    }
}
