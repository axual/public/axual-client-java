package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Configurable;
import org.apache.kafka.common.config.types.Password;
import org.apache.kafka.common.utils.Utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.axual.common.exception.ConfigurationException;
import io.axual.common.tools.FactoryUtil;

public class BaseConfig {
    protected final Map<String, Object> configs;
    protected final Map<String, Object> downstreamConfigs;

    public BaseConfig(Map<String, Object> configs) {
        this.configs = Collections.unmodifiableMap(configs);
        downstreamConfigs = new HashMap<>(configs);
    }

    public Map<String, Object> getConfigs() {
        return configs;
    }

    public Map<String, Object> getDownstreamConfigs() {
        return downstreamConfigs;
    }

    protected String parseAndFilterStringConfig(String key, boolean mandatory, String defaultValue) {
        return ConfigParser.parseAndFilterStringConfig(configs, key, mandatory, defaultValue, downstreamConfigs);
    }

    protected void filterDownstream(String... keys) {
        if (keys != null) {
            for (String key : keys) {
                downstreamConfigs.remove(key);
            }
        }
    }

    protected void putDownstream(String key, Object value) {
        downstreamConfigs.put(key, value);
    }

    protected void putDownstreamIfAbsent(String key, Object value) {
        if (!downstreamConfigs.containsKey(key)) {
            downstreamConfigs.put(key, value);
        }
    }

    protected Password parsePasswordConfig(String key, boolean mandatory, String defaultValue) {
        return ConfigParser.parsePasswordConfig(configs, key, mandatory, defaultValue);
    }

    protected Password parsePasswordConfig(String key, boolean mandatory, Password defaultValue) {
        return ConfigParser.parsePasswordConfig(configs, key, mandatory, defaultValue);
    }

    protected static <T> T parseConfig(Map<String, Object> configs, String key, boolean mandatory, T defaultValue) {
        return ConfigParser.parseConfig(configs, key, mandatory, defaultValue);
    }

    protected static <T> T parseAndRemoveConfig(Map<String, Object> configs, String key, boolean mandatory, T defaultValue) {
        return ConfigParser.parseAndRemoveConfig(configs, key, mandatory, defaultValue);
    }

    protected static String parseStringConfig(Map<String, Object> configs, String key, boolean mandatory, String defaultValue) {
        return ConfigParser.parseStringConfig(configs, key, mandatory, defaultValue);
    }

    protected static String parseAndRemoveStringConfig(Map<String, Object> configs, String key, boolean mandatory, String defaultValue) {
        String result = ConfigParser.parseStringConfig(configs, key, mandatory, defaultValue);
        configs.remove(key);
        return result;
    }

    protected String parseStringConfig(String key, boolean mandatory, String defaultValue) {
        return parseStringConfig(configs, key, mandatory, defaultValue);
    }

    public <T> T getConfiguredInstance(String key, Class<T> expectedClass) {
        return getConfiguredInstance(key, expectedClass, false);
    }

    public <T> T getConfiguredInstance(String key, Class<T> expectedClass, boolean allowNull) {
        final Object configuredValue = configs.get(key);
        try {
            return getConfiguredInstance(configuredValue, expectedClass, allowNull);
        } catch (ConfigurationException e) {
            throw new ConfigurationException("Property \"" + (key != null ? key : "null") + "\" not set or contains illegal value \"" + (configuredValue != null ? configuredValue : "null") + "\"");
        }
    }

    public <T> T getConfiguredInstance(Object value, Class<T> expectedClass) {
        return getConfiguredInstance(value, expectedClass, false);
    }

    @SuppressWarnings("unchecked")
    public <T> T getConfiguredInstance(Object value, Class<T> expectedClass, boolean allowNull) {
        // Check if the value is an initialized instance of the expected class
        if (expectedClass.isInstance(value)) {
            return (T) value;
        }

        // Check if the value is a string
        if (value instanceof String) {
            // Assume the value represents the object's class name, so return new instance
            T result = FactoryUtil.create((String) value, expectedClass);
            if (result instanceof Configurable) {
                ((Configurable) result).configure(downstreamConfigs);
            }
            return result;
        }

        // Check if the value is an instance of Class
        if (value instanceof Class) {
            Class configuredClass = (Class) value;
            // If expectedClass is (superclass of) configuredClass, then create an instance of the
            // configuredClass and return it casted as the expectedClass
            if (expectedClass.isAssignableFrom(configuredClass)) {
                T result = (T) Utils.newInstance((configuredClass).asSubclass(expectedClass));
                if (result instanceof Configurable) {
                    ((Configurable) result).configure(downstreamConfigs);
                }
                return result;
            }
        }

        if (allowNull) {
            return null;
        }

        throw new ConfigurationException("Can not instantiate object of type " + expectedClass.getSimpleName() + " from value " + (value != null ? value.toString() : "null"));
    }

    public static <T> boolean validate(Object value, Class<T> expectedClass) {
        return validate(value, expectedClass, false);
    }

    @SuppressWarnings("unchecked")
    public static <T> boolean validate(Object value, Class<T> expectedClass, boolean allowNull) {
        // Check if the value is an initialized instance of the expected class
        if (expectedClass.isInstance(value)) {
            return true;
        }

        // Check if the value is a string
        if (value instanceof String) {
            // Assume the value represents the object's class name, so return new instance
            return true;
        }

        // Check if the value is an instance of Class
        if (value instanceof Class) {
            Class configuredClass = (Class) value;
            // If expectedClass is (superclass of) configuredClass, then create an instance of the
            // configuredClass and return it casted as the expectedClass
            if (expectedClass.isAssignableFrom(configuredClass)) {
                return true;
            }
        }

        return allowNull;
    }
}
