package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;

/**
 * Class with sleep helper methods.
 */
public class SleepUtil {
    public interface Interruptor {
        boolean interrupted();
    }

    private SleepUtil() {
    }

    /**
     * Sleep for a maximum of given amount of milliseconds. Allow the thread to be silently woken up
     * by means of an interruption.
     *
     * @param timeout the millis
     */
    public static void sleep(Duration timeout) {
        try {
            Thread.sleep(timeout.toMillis());
        } catch (InterruptedException e) {
            // Filter out the interrupted exception
            Thread.currentThread().interrupt();
        }
    }

    public static void sleepInterruptibly(Duration duration, Interruptor interruptor) throws InterruptedException {
        final long endTime = System.currentTimeMillis() + duration.toMillis();
        long timeToWait = System.currentTimeMillis() - endTime;
        while (timeToWait > 0) {
            if (interruptor != null && !interruptor.interrupted()) {
                throw new InterruptedException();
            }
            sleep(Duration.ofMillis(Math.min(50, timeToWait)));
            timeToWait = System.currentTimeMillis() - endTime;
        }
    }
}
