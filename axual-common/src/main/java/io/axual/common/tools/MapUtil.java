package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Class with generic Map helper methods.
 */
public class MapUtil {
    private MapUtil() {
    }

    public static <K, V> MapBuilder<K, V> builder() {
        return new MapBuilder<>();
    }

    public static <K, V> MapBuilder<K, V> builder(Map<K, V> initialMap) {
        return new MapBuilder<>(initialMap);
    }

    /**
     * Filter a map with Object keys. The result is the same map but with only the keys of type
     * String.
     *
     * @param <V> the value type of the map
     * @param map the input map with Object keys
     * @return the output map containing only String keys
     */
    public static <V> Map<String, V> objectToStringMap(Map<Object, V> map) {
        Map<String, V> result = new HashMap<>();
        for (Map.Entry<Object, V> entry : map.entrySet()) {
            if (entry.getKey() instanceof String) {
                result.put((String) entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    /**
     * Convert a map with String keys to a generic map with Object keys.
     *
     * @param <V> the value type of the map
     * @param map the input map with String keys
     * @return the output map with Object keys
     */
    public static <V> Map<Object, V> stringMapToObjectMap(Map<String, V> map) {
        return new HashMap<>(map);
    }

    /**
     * Convert a generic map to a Properties object.
     *
     * @param map the input map
     * @return the output properties
     */
    public static Properties mapToProperties(Map<?, ?> map) {
        final Properties result = new Properties();
        result.putAll(map);
        return result;
    }

    /**
     * Apply a given prefix to all String keys in a map.
     *
     * @param <V>    the value type of the map
     * @param map    the input map with String keys
     * @param prefix the prefix to apply to every key
     * @return the output map with all prefixed keys
     */
    public static <V> Map<String, V> prefixItems(Map<String, V> map, String prefix) {
        Map<String, V> result = new HashMap<>();
        for (Map.Entry<String, V> entry : map.entrySet()) {
            result.put(prefix + entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static <V> Map<String, V> unprefixItems(Map<String, V> map, String prefix) {
        Map<String, V> result = new HashMap<>();
        for (Map.Entry<String, V> entry : map.entrySet()) {
            if (entry.getKey().startsWith(prefix)) {
                result.put(entry.getKey().substring(prefix.length()), entry.getValue());
            }
        }
        return result;
    }

    /**
     * Convert a map with any type of values to a map with only String values.
     *
     * @param <K> the key type of the map
     * @param map the input map with any type of values
     * @return the output map with String values
     */
    public static <K> Map<K, String> stringValues(Map<K, ?> map) {
        Map<K, String> result = new HashMap<>();
        for (Map.Entry<K, ?> entry : map.entrySet()) {
            result.put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
        }
        return result;
    }

    /**
     * Return the String value of given key from a given map.
     *
     * @param <K> the key type of the map
     * @param map the input map with given key
     * @param key the key to look up
     * @return the string value found in the map at given key, or null if no String value found
     */
    public static <K> String stringValue(Map<K, ?> map, K key) {
        return stringValue(map, key, null);
    }

    /**
     * Return the String value of given key from a given map.
     *
     * @param <K>          the key type of the map
     * @param map          the input map with given key
     * @param key          the key to look up
     * @param defaultValue the default value to return if no String value found for given key
     * @return the string value found, or defaultValue if no String value found
     */
    public static <K> String stringValue(Map<K, ?> map, K key, String defaultValue) {
        Object value = map.get(key);
        return value instanceof String ? (String) value : defaultValue;
    }

    public static <K, V> Map<K, V> putIfAbsent(Map<K, V> target, K key, V value) {
        if (!target.containsKey(key)) {
            target.put(key, value);
        }
        return target;
    }

    public static <K, V> Map<K, V> putAllIfAbsent(Map<K, V> target, Map<? extends K, ? extends V> source) {
        for (Map.Entry<? extends K, ? extends V> entry : source.entrySet()) {
            putIfAbsent(target, entry.getKey(), entry.getValue());
        }
        return target;
    }

    public static <K, V> Map<K, V> put(Map<K, V> source, K key, V value) {
        source.put(key, value);
        return source;
    }
}
