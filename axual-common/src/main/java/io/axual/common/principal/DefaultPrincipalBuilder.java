package io.axual.common.principal;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.security.auth.AuthenticationContext;
import org.apache.kafka.common.security.auth.KafkaPrincipal;
import org.apache.kafka.common.security.auth.KafkaPrincipalBuilder;
import org.apache.kafka.common.security.auth.SslAuthenticationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.security.cert.X509Certificate;

public class DefaultPrincipalBuilder implements KafkaPrincipalBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultPrincipalBuilder.class);

    @Override
    public KafkaPrincipal build(AuthenticationContext context) {
        if (context instanceof SslAuthenticationContext) {
            try {
                SSLSession sslSession = ((SslAuthenticationContext) context).session();
                X509Certificate[] chain = sslSession.getPeerCertificateChain();
                if (chain != null && chain.length > 0) {
                    String principal = "";
                    for (int index = chain.length - 1; index >= 0; index--) {
                        principal = appendEntry(principal, chain[index].getSubjectDN(), chain.length - index - 1);
                    }

                    LOG.info("SSLSession principal: {}", principal);
                    return new KafkaPrincipal(KafkaPrincipal.USER_TYPE, principal);
                }
            } catch (SSLPeerUnverifiedException e) {
                // Eat
            }
        }

        return KafkaPrincipal.ANONYMOUS;
    }

    private static String appendEntry(String entries, Principal entry, int index) {
        if (!entries.isEmpty()) {
            entries += ", ";
        }
        final String name = entry.getName().replaceAll("[\n\r\t]", "_");
        return entries + "[" + index + "] " + name;
    }
}
