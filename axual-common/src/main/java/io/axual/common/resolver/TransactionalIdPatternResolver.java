package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

/**
 * This class is able to resolve and unresolve patterns for Kafka producer transaction ids. It is
 * implemented in much the same way as Kafka (de)serializers, by using a default constructor first
 * and calling {@link #configure(Map)} later. The parameters passed in during configure() determine
 * the behaviour of the resolver. The class implements the {@link TransactionalIdResolver} interface.
 */
public class TransactionalIdPatternResolver implements TransactionalIdResolver {
    /**
     * The name of the property used to pass in the transaction id pattern.
     */
    public static final String TRANSACTIONAL_ID_PATTERN_CONFIG = "transactional.id.pattern";
    public static final String DEFAULT_PLACEHOLDER_VALUE = "transactional.id";

    private InternalPatternResolver resolver;


    @Override
    public void configure(Map<String, ?> configs) {
        if (resolver == null) {
            resolver = new InternalPatternResolver(TRANSACTIONAL_ID_PATTERN_CONFIG, DEFAULT_PLACEHOLDER_VALUE);
        }
        resolver.configure(new HashMap<>(configs));
    }

    @Override
    public String resolve(String transactionalId) {
        return resolver.resolve(transactionalId);
    }

    @Override
    public String unresolve(String transactionalId) {
        Map<String, String> context = unresolveContext(transactionalId);
        return context != null ? getNameFromContext(context) : null;
    }

    @Override
    public Map<String, String> unresolveContext(String transactionalId) {
        return resolver.unresolve(transactionalId);
    }

    @Override
    public String getNameFromContext(Map<String, String> context) {
        return context != null ? context.get(DEFAULT_PLACEHOLDER_VALUE) : null;
    }

    @Override
    public String resolveTransactionalId(String transactionalId) {
        return resolve(transactionalId);
    }

    @Override
    public String unresolveTransactionalId(String transactionalId) {
        return unresolve(transactionalId);
    }
}
