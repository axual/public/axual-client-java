package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.TopicPartition;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * This class is able to resolve and unresolve Kafka topic names. It is implemented in much the same
 * way as Kafka (de)serializers, by using a default constructor first and calling {@link
 * #configure(Map)} later. The parameters passed in during configure() determine the behaviour of
 * the resolver. The class implements the {@link TopicResolver} interface.
 */
public class TopicPatternResolver implements TopicResolver {
    /**
     * The name of the property used to pass in the topic name pattern.
     */
    public static final String TOPIC_PATTERN_CONFIG = "topic.pattern";
    private static final String DEFAULT_PLACEHOLDER_VALUE = "topic";

    private InternalPatternResolver resolver;

    @Override
    public void configure(Map<String, ?> configs) {
        if (resolver == null) {
            resolver = new InternalPatternResolver(TOPIC_PATTERN_CONFIG, DEFAULT_PLACEHOLDER_VALUE);
        }
        resolver.configure(new HashMap<>(configs));
    }

    @Override
    public String resolve(String topic) {
        return resolver.resolve(topic);
    }

    @Override
    public String unresolve(String topic) {
        Map<String, String> context = unresolveContext(topic);
        return context != null ? getNameFromContext(context) : null;
    }

    @Override
    public Map<String, String> unresolveContext(String topicName) {
        return resolver.unresolve(topicName);
    }

    @Override
    public String getNameFromContext(Map<String, String> context) {
        return context != null ? context.get(DEFAULT_PLACEHOLDER_VALUE) : null;
    }

    @Override
    public String resolveTopic(final String topic) {
        return resolve(topic);
    }

    @Override
    public TopicPartition resolveTopic(final TopicPartition topicPartition) {
        if (topicPartition == null) {
            return null;
        }
        return new TopicPartition(resolve(topicPartition.topic()), topicPartition.partition());
    }

    @Override
    public Pattern resolveTopicPattern(Pattern pattern) {
        // Wrap the pattern in brackets and resolve the resulting string as if it were a topic
        String resolvedRegex = resolve("(" + pattern.pattern() + ")");
        return Pattern.compile(resolvedRegex);
    }

    @Override
    public Set<String> resolveTopics(Collection<String> topics) {
        if (topics == null) {
            return new HashSet<>();
        }
        Set<String> result = new HashSet<>(topics.size());
        for (String topic : topics) {
            String resolvedTopic = resolve(topic);
            if (resolvedTopic != null) {
                result.add(resolvedTopic);
            }
        }
        return result;
    }

    @Override
    public Set<TopicPartition> resolveTopicPartitions(Collection<TopicPartition> topicPartitions) {
        if (topicPartitions == null) {
            return new HashSet<>();
        }
        Set<TopicPartition> result = new HashSet<>(topicPartitions.size());
        for (TopicPartition partition : topicPartitions) {
            result.add(resolveTopic(partition));
        }
        return result;
    }

    @Override
    public <V> Map<TopicPartition, V> resolveTopics(Map<TopicPartition, V> topicPartitionMap) {
        if (topicPartitionMap == null) {
            return new HashMap<>();
        }
        Map<TopicPartition, V> result = new HashMap<>(topicPartitionMap.size());
        for (Map.Entry<TopicPartition, V> entry : topicPartitionMap.entrySet()) {
            result.put(resolveTopic(entry.getKey()), entry.getValue());
        }
        return result;
    }

    @Override
    public String unresolveTopic(final String topic) {
        return unresolve(topic);
    }

    @Override
    public TopicPartition unresolveTopic(TopicPartition topicPartition) {
        if (topicPartition == null) {
            return null;
        }
        String unresolvedTopic = unresolveTopic(topicPartition.topic());
        if (unresolvedTopic == null) return null;
        return new TopicPartition(unresolvedTopic, topicPartition.partition());
    }

    @Override
    public Set<String> unresolveTopics(Collection<String> topics) {
        if (topics == null) {
            return new HashSet<>();
        }
        Set<String> result = new HashSet<>(topics.size());
        for (String topic : topics) {
            String unresolvedTopic = unresolveTopic(topic);
            if (unresolvedTopic != null) {
                result.add(unresolvedTopic);
            }
        }
        return result;
    }

    @Override
    public Set<TopicPartition> unresolveTopicPartitions(Collection<TopicPartition> topicPartitions) {
        if (topicPartitions == null) {
            return new HashSet<>();
        }
        Set<TopicPartition> result = new HashSet<>(topicPartitions.size());
        for (TopicPartition partition : topicPartitions) {
            TopicPartition unresolvedPartition = unresolveTopic(partition);
            if (unresolvedPartition != null) {
                result.add(unresolvedPartition);
            }
        }
        return result;
    }

    @Override
    public <V> Map<TopicPartition, V> unresolveTopics(Map<TopicPartition, V> topicPartitionMap) {
        if (topicPartitionMap == null) {
            return new HashMap<>();
        }
        Map<TopicPartition, V> result = new HashMap<>(topicPartitionMap.size());
        for (Map.Entry<TopicPartition, V> entry : topicPartitionMap.entrySet()) {
            TopicPartition unresolvedTopic = unresolveTopic(entry.getKey());
            if (unresolvedTopic != null) {
                result.put(unresolvedTopic, entry.getValue());
            }
        }
        return result;
    }
}
