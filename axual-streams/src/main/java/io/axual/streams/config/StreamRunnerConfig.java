package io.axual.streams.config;

/*-
 * ========================LICENSE_START=================================
 * axual-streams
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import io.axual.client.config.BaseProducerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.logging.core.LoggingConfig;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.tools.MapUtil;
import io.axual.streams.proxy.axual.AxualStreamsConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.streams.DefaultHandlerFactory;

@InterfaceStability.Evolving
public class StreamRunnerConfig {
    private static final Logger LOG = LoggerFactory.getLogger(StreamRunnerConfig.class);
    private static final Map<String, Object> config =
            MapUtil.<String, Object>builder()
                    .put(LoggingConfig.LOGLEVEL_CONFIG, "INFO")
                    .put(LoggingConfig.METHODS_CONFIG, "")
                    .build();
    public static final ProxyChain DEFAULT_PROXY_CHAIN = AxualStreamsConfig.DEFAULT_PROXY_CHAIN;

    private DeliveryStrategy deliveryStrategy;
    private TopologyFactory topologyFactory;
    private UncaughtExceptionHandlerFactory uncaughtExceptionHandlerFactory;
    private Object defaultKeySerde;
    private Object defaultValueSerde;
    private int batchSize;
    private long lingerMs;
    private int requestTimeoutMs;
    private boolean optimizeTopology;
    private ProxyChain proxyChain;

    private StreamRunnerConfig(Builder builder) {
        this.deliveryStrategy = builder.deliveryStrategy;
        this.topologyFactory = builder.topologyFactory;
        this.uncaughtExceptionHandlerFactory = builder.uncaughtExceptionHandlerFactory;
        this.defaultKeySerde = builder.defaultKeySerde;
        this.defaultValueSerde = builder.defaultValueSerde;
        this.batchSize = builder.batchSize;
        this.lingerMs = builder.lingerMs;
        this.requestTimeoutMs = builder.requestTimeoutMs;
        this.optimizeTopology = builder.optimizeTopology;
        this.proxyChain = builder.proxyChain;
    }

    public DeliveryStrategy getDeliveryStrategy() {
        return deliveryStrategy;
    }

    public TopologyFactory getTopologyFactory() {
        return topologyFactory;
    }

    public UncaughtExceptionHandlerFactory getUncaughtExceptionHandlerFactory() {
        return uncaughtExceptionHandlerFactory;
    }

    public Object getDefaultKeySerde() {
        return defaultKeySerde;
    }

    public Object getDefaultValueSerde() {
        return defaultValueSerde;
    }

    public Integer getBatchSize() {
        return batchSize;
    }

    public long getLingerMs() {
        return lingerMs;
    }

    public Integer getRequestTimeoutMs() {
        return requestTimeoutMs;
    }

    public boolean getOptimizeTopology() {
        return optimizeTopology;
    }

    public ProxyChain getProxyChain() {
        return proxyChain;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DeliveryStrategy deliveryStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        private TopologyFactory topologyFactory;
        private UncaughtExceptionHandlerFactory uncaughtExceptionHandlerFactory;
        private Object defaultKeySerde = Serdes.ByteArraySerde.class;
        private Object defaultValueSerde = Serdes.ByteArraySerde.class;
        private int batchSize = 262144;
        private long lingerMs = 0;
        private int requestTimeoutMs = 40000;
        private boolean optimizeTopology = true;
        private ProxyChain proxyChain = DEFAULT_PROXY_CHAIN;

        public Builder setDeliveryStrategy(final DeliveryStrategy deliveryStrategy) {
            this.deliveryStrategy = deliveryStrategy;
            return this;
        }

        public Builder setTopologyFactory(final TopologyFactory topologyFactory) {
            this.topologyFactory = topologyFactory;
            return this;
        }

        public Builder setUncaughtExceptionHandler(final UncaughtExceptionHandlerFactory uncaughtExceptionHandlerFactory) {
            this.uncaughtExceptionHandlerFactory = uncaughtExceptionHandlerFactory;
            return this;
        }

        public Builder setDefaultKeySerde(Serde serde) {
            this.defaultKeySerde = serde;
            return this;
        }

        public <T extends Serde> Builder setDefaultKeySerde(Class<T> serdeClass) {
            this.defaultKeySerde = serdeClass;
            return this;
        }

        public Builder setDefaultKeySerde(String serdeClassName) {
            this.defaultKeySerde = serdeClassName;
            return this;
        }

        public Builder setDefaultValueSerde(Serde serde) {
            this.defaultValueSerde = serde;
            return this;
        }

        public <T extends Serde> Builder setDefaultValueSerde(Class<T> serdeClass) {
            this.defaultValueSerde = serdeClass;
            return this;
        }

        public Builder setDefaultValueSerde(String serdeClassName) {
            this.defaultValueSerde = serdeClassName;
            return this;
        }

        /**
         * Sets the batchSize to influence the sending of messages to Kafka
         *
         * @param batchSize see Kafka's ProducerConfig.BATCH_SIZE_DOC
         * @return the {@link BaseProducerConfig.Builder} object to be used for further
         * configuration
         */
        public Builder setBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        /**
         * Sets the lingerMs to influence the sending of messages to Kafka
         *
         * @param lingerMs see Kafka's ProducerConfig.LINGER_MS_DOC for documentation
         * @return the {@link StreamRunnerConfig.Builder} object to be used for further
         * configuration
         */
        public Builder setLingerMs(long lingerMs) {
            this.lingerMs = lingerMs;
            return this;
        }

        /**
         * Sets the lingerMs to influence the sending of messages to the Axual Platform Cluster
         *
         * @param requestTimeoutMs see {@link CommonClientConfigs#REQUEST_TIMEOUT_MS_DOC}
         * @return the {@link StreamRunnerConfig.Builder} object to be used for further
         * configuration
         */
        public Builder setRequestTimeoutMs(int requestTimeoutMs) {
            this.requestTimeoutMs = requestTimeoutMs;
            return this;
        }

        public Builder setOptimizeTopology(boolean optimizeTopology) {
            this.optimizeTopology = optimizeTopology;
            return this;
        }

        public Builder setProxyChain(ProxyChain proxyChain) {
            this.proxyChain = proxyChain;
            return this;
        }

        public StreamRunnerConfig build() {
            if (deliveryStrategy == null) {
                throw new IllegalStateException("Error building configuration: DeliveryStrategy is missing");
            }
            if (topologyFactory == null) {
                throw new IllegalStateException("Error building configuration: TopologyFactory is missing");
            }
            if (uncaughtExceptionHandlerFactory == null) {
                LOG.warn("No UncaughtExceptionHandlerProvider was configured, using DefaultHandlerProvider");
                uncaughtExceptionHandlerFactory = new DefaultHandlerFactory();
            }

            return new StreamRunnerConfig(this);
        }
    }
}
