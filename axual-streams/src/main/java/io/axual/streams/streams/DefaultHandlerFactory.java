package io.axual.streams.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.common.annotation.InterfaceStability;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.generic.streams.Streams;

/**
 * Default handler provider, makes sure the restarter will stop, and isStreaming() will return the
 * correct value
 */
@InterfaceStability.Evolving
public class DefaultHandlerFactory implements UncaughtExceptionHandlerFactory {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultHandlerFactory.class);

    @Override
    public StreamsUncaughtExceptionHandler create(Streams streams) {
        return new StreamErrorHandler();
    }

    private static final class StreamErrorHandler implements StreamsUncaughtExceptionHandler {

        private StreamErrorHandler() {
        }

        @Override
        public StreamThreadExceptionResponse handle(Throwable throwable) {
            LOG.error("Uncaught exception while streaming: {}.\nShutting down Streams.", throwable.getMessage());
            return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
        }
    }
}
