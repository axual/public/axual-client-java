# Axual Streams

The Axual Streams Proxy implementation provides a simple way to migrate from a pure Kafka 
implementation to the Axual platform. 
It enables a full fledged Axual platform functionality with minimal code wise effort limited to 
replacing of some key Kafka components and imports with their Axual counterparts.
In order to achieve full Axual support, the developer needs to provide the necessary proxies 
through configuration.

## Packaging
The packages under `io.axual.streams.proxy` are separated based on functionality.
Each of them plugs the functionality into a finally constructed chain of proxies which defines the
behavior of the producer or consumer object.

## Proxies
The proxies currently available are:
 * `switching` adds the switching functionality, enabling users to facilitate the active-active 
 Axual configuration. 
 This covers cases where a cluster is unavailable and internally handles this case by creating a 
 new Producer or Consumer pointed at a cluster that is available.
 