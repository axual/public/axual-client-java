package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.test.Random;
import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class StreamsSwitchIT {

    private static final Logger LOG = LoggerFactory.getLogger(StreamsSwitchIT.class);

    private static final String SOURCE_STREAM = "general-source-StreamsSwitchIT";
    private static final String TARGET_STREAM = "general-target-StreamsSwitchIT";
    private static final long TTL = 10L * 1000; // ten seconds
    private static final long LONGER_THAN_TTL = Math.round(TTL * 1.5);
    private static final long SHORTER_THAN_TTL = Math.round(TTL * 0.5);
    private long lastStreamerTimestamp;

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit(true)
            .addStream(new StreamConfig()
                    .setName(SOURCE_STREAM)
                    .setKeySchema(Random.getClassSchema())
                    .setValueSchema(Random.getClassSchema())
                    .setPartitions(2))
            .addStream(new StreamConfig()
                    .setName(TARGET_STREAM)
                    .setKeySchema(Random.getClassSchema())
                    .setValueSchema(Random.getClassSchema())
                    .setPartitions(2));

    @Test
    public void switchSingleStreamer_switchStreamerThenProducer() throws Throwable {
        run(1, switchStreamerThenProducer);
    }

    @Test
    public void switchTwoStreamers_switchStreamerThenProducer() throws Throwable {
        run(2, switchStreamerThenProducer);
    }

    @Test
    public void switchThreeStreamers_switchStreamerThenProducer() throws Throwable {
        run(3, switchStreamerThenProducer);
    }

    @Test
    public void switchSingleStreamer_switchProducerThenStreamer() throws Throwable {
        run(1, switchProducerThenStreamer);
    }

    private void run(final int streamerInstances, final TestScenario scenario) throws Throwable {
        // Precondition:
        // - Producer is producing on SOURCE topic on cluster A
        // - Distributor copies all messages on SOURCE+TARGET topic to cluster B
        // - Stream app being tested consumes SOURCE topic and writes TARGET topic
        // - Stream app starts on cluster A
        // Test:
        // - Run the test scenario supplied
        // Expected behavior:
        // - No messages are missed by stream app
        // Verification:
        // - A verification consumer on cluster B should see all messages on TARGET topic

        // Initialize Axual Platform
        platform.platform().getInstance().getDiscoveryUnit().setTtl(TTL);

        // Verification consumer
        final Queue<ConsumerMessage<Random, Random>> receivedMessages = new LinkedBlockingQueue<>();
        try (final AxualClient verificationApp = new AxualClient(platform.platform().getInstance().getClientConfig("verificationApp", platform.clusterB()))) {
            SpecificAvroConsumerConfig<Random, Random> specificConsumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                    .setStream(TARGET_STREAM)
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .build();
            try (final Consumer verificationConsumer = verificationApp.buildConsumer(specificConsumerConfig,
                    new EnqueueProcessor<>("verificationConsumer", receivedMessages, true))) {
                verificationConsumer.startConsuming();

                // Start test producer
                try (TestProducer testProducer = new TestProducer(platform, SOURCE_STREAM, true)) {
                    testProducer.start();

                    // Simple streaming topology that reads from one topic and copies to another
                    final TopologyFactory testTopology = builder -> {
                        builder
                                .<Random, Random>stream(SOURCE_STREAM)
                                .filter((k, v) -> {
                                    LOG.info("Streaming message from source to target: # {}", k.getRandom());
                                    lastStreamerTimestamp = System.currentTimeMillis();
                                    return true;
                                })
                                .to(TARGET_STREAM);
                        return builder.build();
                    };

                    // Set up streaming application(s)
                    final String applicationId = "streamsunit";

                    final StreamRunner[] streamer = new StreamRunner[streamerInstances];

                    try (final AxualStreamsClient streamingAppInstance = new AxualStreamsClient(platform.instance().getClientConfig(applicationId, platform.clusterA()))) {
                        for (int index = 0; index < streamerInstances; index++) {
                            // Start a new application for every instance
                            StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                                    .setTopologyFactory(testTopology)
                                    .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                                    .setDefaultKeySerde(SpecificAvroSerde.class)
                                    .setDefaultValueSerde(SpecificAvroSerde.class)
                                    .build();
                            streamer[index] = streamingAppInstance.buildStreamRunner(streamRunnerConfig);
                            streamer[index].start();
                            if (index + 1 < streamerInstances) {
                                sleepUninterruptibly(Math.round(Math.random() * 1000 * 5), MILLISECONDS);
                            }
                        }

                        // Execute the test scenario
                        scenario.clientConfig = streamingAppInstance.getConfiguration();
                        scenario.producer = testProducer;
                        scenario.run();

                        // Stop the producer
                        testProducer.stopProducing();
                        // Wait until no further stream processing happens
                        lastStreamerTimestamp = System.currentTimeMillis();
                        await("Wait until streaming stops").atMost(60, SECONDS).until(() -> lastStreamerTimestamp + 15000 < System.currentTimeMillis());

                        // Stop the streaming app instances
                        for (int index = 0; index < streamerInstances; index++) {
                            streamer[index].close();
                        }
                    }

                    // Stop the verification consumer
                    ConsumeFailedException e = verificationConsumer.stopConsuming();
                    if (e != null) {
                        LOG.warn("Verification consumer exited with error: ", e);
                        throw e;
                    }

                    // Make sure all messages have reached the TO topic on Cluster B
                    boolean allOK = true;
                    final Map<Long, Integer> counts = MessageAggregator.aggregate(receivedMessages);
                    for (long index = 0; index < testProducer.getProduceCounter(); index++) {
                        int count = 0;
                        if (counts.containsKey(index)) {
                            count = counts.get(index);
                        }
                        // If not EXACTLY_ONCE
                        if (count != 1) {
                            LOG.info("Message {} was received {} times", index, count);
                        }
                        if (count == 0) {
                            //We missed messages!
                            allOK = false;
                        }
                    }
                    assertTrue("Messages missed", allOK);
                }
            }
        }

        final TestLogger logger = TestLoggerFactory.getTestLogger(StreamsSwitchIT.class);
        assertThat("No exceptions during switch", logger.getAllLoggingEvents().stream().noneMatch(loggingEvent -> loggingEvent.getMessage().contains("duplicates")), is(true));
    }

    private abstract class TestScenario implements Runnable {
        ClientConfig clientConfig;
        TestProducer producer;
    }

    private TestScenario switchStreamerThenProducer = new TestScenario() {
        @Override
        public void run() {
            // Test case:
            // - Stream app switches to cluster B
            // - Producer switches to cluster B
            LOG.info("Produce messages on cluster A for a while");

            // Produce longerThanTTL
            sleepUninterruptibly(LONGER_THAN_TTL, MILLISECONDS);

            LOG.info("Produced {} messages, switch streamer(s) to cluster B", producer.getProduceCounter());

            // Direct streamer(s) to cluster B
            platform.platform().getInstance().directApplicationTo(clientConfig, platform.clusterB());

            // Produce shorterThanTTL
            sleepUninterruptibly(SHORTER_THAN_TTL, MILLISECONDS);

            LOG.info("Produced {}  messages, switch producer to cluster B", producer.getProduceCounter());

            // Switch producer to cluster B
            producer.switchToCluster(platform.clusterB());

            // Sleep some more
            sleepUninterruptibly(LONGER_THAN_TTL, MILLISECONDS);

            LOG.info("Done");
        }
    };

    private TestScenario switchProducerThenStreamer = new TestScenario() {
        @Override
        public void run() {
            // Test case:
            // - Producer switches to cluster B
            // - Stream app switches to cluster B
            LOG.info("Produce messages on cluster A for a while");

            // Produce longerThanTTL
            sleepUninterruptibly(LONGER_THAN_TTL, MILLISECONDS);

            LOG.info("Produced {} messages, switch producer to cluster B", producer.getProduceCounter());

            // Switch producer to cluster B
            producer.switchToCluster(platform.clusterB());

            LOG.info("Produced {} messages, switch streamer(s) to cluster B", producer.getProduceCounter());

            // Direct streamer(s) to cluster B
            platform.platform().getInstance().directApplicationTo(clientConfig, platform.clusterB());

            // Sleep some more
            sleepUninterruptibly(LONGER_THAN_TTL, MILLISECONDS);

            LOG.info("Done");
        }
    };
}
