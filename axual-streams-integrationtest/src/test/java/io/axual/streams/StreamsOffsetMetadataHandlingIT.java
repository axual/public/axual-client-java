package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;
import lombok.SneakyThrows;

import static io.axual.client.config.DeliveryStrategy.AT_LEAST_ONCE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toSet;

/**
 * Verifies that Streams keeps working when Axual Offset Distribution information is stored in the metadata of an offset commit
 */

public class StreamsOffsetMetadataHandlingIT {
    public static final ProxyChain PROXY_CHAIN = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .build();
    private static final Logger LOG = LoggerFactory.getLogger(StreamsOffsetMetadataHandlingIT.class);
    private static final String STREAM_NAME_INPUT_STREAM = "source-stream-StreamsOffsetMetadataHandlingIT";
    private static final String STREAM_NAME_OUTPUT_STREAM = "target-stream-StreamsOffsetMetadataHandlingIT";
    private static final int PARTITIONS = 1;
    private static final String METADATA_DISTRIBUTION = "{\"copyFlags\":0}";
    private static final String METADATA_CLEAN = "";
    private static final String METADATA_OTHER = "{ShouldCrashStreams}";
    private static final String APP_ID_PRODUCER = "Test-Data-Producer";
    private static final String APP_ID_STREAMS_CLEAN = "Streams-Clean-Metadata";
    private static final String APP_ID_STREAMS_DISTRIBUTION = "Streams-Distribution-Metadata";
    private static final String APP_ID_STREAMS_OTHER = "Streams-Other-Metadata";

    @Rule
    public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit("{topic}", "{group}")
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_INPUT_STREAM).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_OUTPUT_STREAM).setPartitions(PARTITIONS));

    @SneakyThrows
    private long produceData(int nrOfRecords, int recordToReturnOffset, int messageStartNumber) {
        long offset = -1;

        Map<String, Object> props = KafkaUtil.getKafkaConfigs(platformUnit.instance().getClientConfig(APP_ID_PRODUCER, false));
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(AxualProducerConfig.CHAIN_CONFIG, PROXY_CHAIN);
        try (Producer<Integer, String> producer = new AxualProducer<>(props, new IntegerSerializer(), new StringSerializer())) {
            for (int i = 1; i <= nrOfRecords; i++) {
                Integer key = i + messageStartNumber;
                String value = "Sending message nr " + key + " to stream '" + STREAM_NAME_INPUT_STREAM + "'";
                ProducerRecord<Integer, String> record = new ProducerRecord<>(STREAM_NAME_INPUT_STREAM, key, value);
                RecordMetadata recordMetadata = producer.send(record).get();
                LOG.info("Produced record to stream {} - key {} - value {} to offset {}", STREAM_NAME_INPUT_STREAM, key, value, recordMetadata.offset());
                if (i == recordToReturnOffset) {
                    offset = recordMetadata.offset();
                }
            }
        }

        return offset;
    }

    @SneakyThrows
    private void commitOffset(String consumerGroup, long offset, String metadata) {
        final int partition = 0;
        Map<String, Object> props = KafkaUtil.getKafkaConfigs(platformUnit.instance().getClientConfig(APP_ID_PRODUCER, false));
        props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, platformUnit.cluster().getBootstrapServer());
        try (Admin admin = Admin.create(props)) {
            final TopicPartition topicPartition = new TopicPartition(STREAM_NAME_INPUT_STREAM, partition);
            final OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(offset, metadata);
            AlterConsumerGroupOffsetsResult result =
                    admin.alterConsumerGroupOffsets(consumerGroup, Collections.singletonMap(topicPartition, offsetAndMetadata));
            result.all().get();
        }
    }

    @Test
    public void streamStartsWithCleanOffsetMetadata() {
        runTest(APP_ID_STREAMS_CLEAN, 3000, METADATA_CLEAN);
    }

    @Test
    public void streamStartsWithDistributionFlagsInMetadata() {
        runTest(APP_ID_STREAMS_DISTRIBUTION, 2000, METADATA_DISTRIBUTION);
    }

    @Test
    public void streamStartsWithOtherContentsInMetadata() {
        runTest(APP_ID_STREAMS_OTHER, 1000, METADATA_OTHER);
    }

    private void runTest(String appId, int messageStartNumber, String metadata) {
        final int nrOfRecords = 10;
        final int recordOfOffset = 2;
        final long offset = produceData(nrOfRecords, recordOfOffset, messageStartNumber);
        commitOffset(appId, offset, metadata);

        Map<Integer, String> readRecords = new HashMap<>();
        final TopologyFactory topologyFactory = builder -> {
            builder
                    .stream(STREAM_NAME_INPUT_STREAM, Consumed.with(Serdes.Integer(), Serdes.String()))
                    .peek(readRecords::put)
                    .peek((key, value) -> LOG.info("Stream {} - Incoming message: key: {} value: {}", STREAM_NAME_INPUT_STREAM, key, value))
                    .to(STREAM_NAME_OUTPUT_STREAM, Produced.with(Serdes.Integer(), Serdes.String()));
            return builder.build();
        };

        final ClientConfig streamsConfig = platformUnit.instance().getClientConfig(appId, false);

        try (AxualStreamsClient streamsClient = new AxualStreamsClient(streamsConfig)) {
            // Prepare streamRunner
            final StreamRunnerConfig runnerConfig = StreamRunnerConfig.builder()
                    .setDeliveryStrategy(AT_LEAST_ONCE)
                    .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                    .setDefaultKeySerde(Serdes.String())
                    .setDefaultValueSerde(Serdes.String())
                    .setTopologyFactory(topologyFactory)
                    .build();

            final State expectedState = State.RUNNING;
            StreamRunner runner = streamsClient.buildStreamRunner(runnerConfig);
            runner.start();
            Awaitility.await("Wait for running state of Kafka Streams")
                    .atMost(15, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> runner.state() == expectedState);
            LOG.info("Runner reached state " + expectedState);

            // Determine the keys of the expected records
            Set<Integer> expectedKeys =
                    IntStream.rangeClosed(recordOfOffset + messageStartNumber, nrOfRecords + messageStartNumber)
                            .boxed()
                            .collect(toSet());
            LOG.info("Expecting keys {}", expectedKeys);
            Awaitility.await("Wait for all expected keys to be processed")
                    .atMost(20, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> {
                        for (Integer expected : expectedKeys) {
                            if (!readRecords.containsKey(expected)) {
                                return false;
                            }
                        }
                        return true;
                    });

            // Stop streams
            LOG.info("Stopping the runner");
            runner.stop();
        }
    }

}
