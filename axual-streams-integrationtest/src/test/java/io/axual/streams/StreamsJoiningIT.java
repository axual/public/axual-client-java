package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.awaitility.Awaitility;
import org.javatuples.Pair;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.test.Application;
import io.axual.client.test.MatchRule;
import io.axual.client.test.TimeAlert;
import io.axual.client.test.TimeEvent;
import io.axual.client.test.TimeEventAlert;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.OptimizedTopologyFactory;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StreamsJoiningIT {
    private static final Logger LOG = LoggerFactory.getLogger(StreamsJoiningIT.class);
    private static final String FROM_TOPIC = "general-time-StreamsJoiningIT";
    private static final String ALERT_SETTINGS_TOPIC = "general-timealertsettings-StreamsJoiningIT";
    private static final String ALERT_SETTINGS_STORE = "general-timealertsettings-StreamsJoiningIT-store";
    private static final String ALERT_SETTINGS_CHANGELOG = "streamapp-general-timealertsettings-StreamsJoiningIT-store-changelog";
    private static final String JOINED_ALERT_TOPIC = "general-timealertint-StreamsJoiningIT";
    private static final String TO_TOPIC = "general-timealert-StreamsJoiningIT";
    private static final Integer PARTITIONS = 2;

    private static final Application APP_1 = Application.newBuilder().setName("One").setVersion("1.1.1").build();
    private static final Application APP_2 = Application.newBuilder().setName("Two").setVersion("2.2.2").build();
    private static final Application APP_3 = Application.newBuilder().setName("Three").setVersion("3.3.3").build();
    private static final Application APP_4 = Application.newBuilder().setName("Four").setVersion("4.4.4").build();

    private static final long ALERT_TIME_1 = 20000010;
    private static final long ALERT_TIME_2 = 20000020;
    private static final long ALERT_TIME_3 = 10000000;

    private static final TimeAlert ALERT_1 = TimeAlert.newBuilder().setMatcher(MatchRule.BEFORE).setTimestamp(ALERT_TIME_1).build();
    private static final TimeAlert ALERT_2 = TimeAlert.newBuilder().setMatcher(MatchRule.AFTER).setTimestamp(ALERT_TIME_2).build();
    private static final TimeAlert ALERT_3 = TimeAlert.newBuilder().setMatcher(MatchRule.BEFORE).setTimestamp(ALERT_TIME_3).build();

    private static final long TIME_FOR_ALERT_1 = 20000009; // Alert 1 trigger time
    private static final long TIME_FOR_ALERT_2 = 20000021; // Alert 2 trigger time
    private static final long TIME_WITHOUT_ALERT = 20000015; // Alert for none

    private static final Pair<Application, TimeAlert> ALERT_SETTING_1 = new Pair<>(APP_1, ALERT_1);
    private static final Pair<Application, TimeAlert> ALERT_SETTING_2 = new Pair<>(APP_2, ALERT_2);
    private static final Pair<Application, TimeAlert> ALERT_SETTING_3 = new Pair<>(APP_3, ALERT_3);

    private static final Pair<Application, TimeEvent> EVENT_STREAMED_1 = new Pair<>(APP_1, TimeEvent.newBuilder().setSource(APP_1).setTimestamp(TIME_FOR_ALERT_1).build());
    private static final Pair<Application, TimeEvent> EVENT_STREAMED_2 = new Pair<>(APP_2, TimeEvent.newBuilder().setSource(APP_2).setTimestamp(TIME_FOR_ALERT_2).build());
    private static final Pair<Application, TimeEvent> EVENT_FILTERED_1 = new Pair<>(APP_1, TimeEvent.newBuilder().setSource(APP_1).setTimestamp(TIME_WITHOUT_ALERT).build());
    private static final Pair<Application, TimeEvent> EVENT_FILTERED_2 = new Pair<>(APP_2, TimeEvent.newBuilder().setSource(APP_2).setTimestamp(TIME_WITHOUT_ALERT).build());
    private static final Pair<Application, TimeEvent> EVENT_FILTERED_3 = new Pair<>(APP_3, TimeEvent.newBuilder().setSource(APP_3).setTimestamp(TIME_FOR_ALERT_2).build());
    private static final Pair<Application, TimeEvent> EVENT_FILTERED_4 = new Pair<>(APP_3, TimeEvent.newBuilder().setSource(APP_3).setTimestamp(TIME_FOR_ALERT_1).build());
    private static final Pair<Application, TimeEvent> EVENT_FILTERED_5 = new Pair<>(APP_4, TimeEvent.newBuilder().setSource(APP_4).setTimestamp(666).build());

    @Rule
    public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit()
            .addStream(new StreamConfig()
                    .setName(FROM_TOPIC).setKeySchema(Application.getClassSchema()).setValueSchema(TimeEvent.getClassSchema()).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(ALERT_SETTINGS_TOPIC).setKeySchema(Application.getClassSchema()).setValueSchema(TimeAlert.getClassSchema()).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(ALERT_SETTINGS_STORE).setKeySchema(Application.getClassSchema()).setValueSchema(TimeAlert.getClassSchema()).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(ALERT_SETTINGS_CHANGELOG).setKeySchema(Application.getClassSchema()).setValueSchema(TimeAlert.getClassSchema()).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(JOINED_ALERT_TOPIC).setKeySchema(Application.getClassSchema()).setValueSchema(TimeEventAlert.getClassSchema()).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(TO_TOPIC).setKeySchema(Application.getClassSchema()).setValueSchema(TimeEvent.getClassSchema()).setPartitions(PARTITIONS));

    @Test
    public void testKTableJoin() throws Exception {
        // Prepare validation consumer
        final LinkedList<ConsumerMessage<Application, TimeEvent>> alertList = new LinkedList<>();
        final EnqueueProcessor<Application, TimeEvent> timeEventProcessor = new EnqueueProcessor<>("ReceivedAlerts", alertList);

        final SpecificAvroConsumerConfig<Application, TimeEvent> timeEventConsumerConfig = SpecificAvroConsumerConfig.<Application, TimeEvent>builder()
                .setStream(TO_TOPIC)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();

        // Prepare test producer timeEvents
        final SpecificAvroProducerConfig<Application, TimeEvent> timeEventProducerConfig = SpecificAvroProducerConfig.<Application, TimeEvent>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .setBlocking(true)
                .build();

        // Prepare test producer timeAlerts
        final SpecificAvroProducerConfig<Application, TimeAlert> timeAlertProducerConfig = SpecificAvroProducerConfig.<Application, TimeAlert>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .setBlocking(true)
                .build();

//        Working simple streams app
//        final TopologyFactory testTopology = builder -> {
//            builder.<Application, TimeEvent>stream(FROM_TOPIC)
//                    .map((k, v) -> new KeyValue<>(k, TimeEvent.newBuilder().setTimestamp(1111).setSource(k).build()))
//                    .to(TO_TOPIC);
//            return builder.build();
//        };
//

//        TODO: implement streams to support this join topology
        final TopologyFactory testTopology = builder -> {
            //                    Commented out because table does it himself now
//                    KeyValueBytesStoreSupplier x = Stores.inMemoryKeyValueStore("ABC");
//
//                    BebSpecificSerde<Application> appSerde;
//                    BebSpecificSerde<TimeAlert> alertSerde;
//
//                    try {
//                        appSerde = new BebSpecificSerde<>(RESOLVED_ALERT);
//                        appSerde.configure(builder.getConfigs(), true);
//                        alertSerde = new BebSpecificSerde<>(RESOLVED_ALERT);
//                        alertSerde.configure(builder.getConfigs(), false);
//                    } catch (Exception e) {
//                        throw new RuntimeException(e);
//                    }
//
//                    Materialized<Application, TimeAlert, KeyValueStore<Bytes, byte[]>> xsl = Materialized.as(x);
//                    KTable<Application, TimeAlert> alertTable = builder.table(RESOLVED_ALERT, xsl.withLoggingDisabled().withCachingDisabled().withKeySerde(appSerde).withValueSerde(alertSerde));
            KTable<Application, TimeAlert> alertTable = builder.table(ALERT_SETTINGS_TOPIC, Materialized.as(ALERT_SETTINGS_STORE));
            KStream<Application, TimeEvent> sourceStream = builder.stream(FROM_TOPIC);


            sourceStream
                    .peek((k, v) -> {
                        LOG.info("Received Key {} with Value {}", k, v);
                    })
                    .leftJoin(alertTable, (v1, v2) -> {
                        if (v2 == null) {
                            LOG.info("Joined Event '{}' to null", v1);
                            return null;
                        }

                        boolean isMatch = false;

                        // Check timestamp
                        long eventTime = v1.getTimestamp();
                        long alertTime = v2.getTimestamp();
                        switch (v2.getMatcher()) {
                            case AFTER:
                                isMatch = (eventTime > alertTime);
                                break;
                            case BEFORE:
                                isMatch = (eventTime < alertTime);
                                break;
                        }


                        if (isMatch) {
                            TimeEventAlert eventAlert = TimeEventAlert.newBuilder()
                                    .setTimeevent(v1)
                                    .setAlert(v2)
                                    .build();
                            LOG.info("Joined to TimeEventAlert '{}'", eventAlert);
                            return eventAlert;
                        } else {
                            LOG.info("Not an alert match for TimeEvent '{}' and TimeAlert {}", v1, v2);
                            return null;
                        }
                    })
                    .through(JOINED_ALERT_TOPIC)
                    .filterNot((c, v) -> v == null)
                    .peek((k, v) -> {
                        LOG.info("Filtered Key {} with Value {}", k, v);
                    })
                    .map((k, v) -> new KeyValue<>(k, v.getTimeevent()))
                    .to(TO_TOPIC);

            return builder.build();
        };


        // Prepare streamRunner
        final StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                .setDefaultKeySerde(SpecificAvroSerde.class.getName())
                .setDefaultValueSerde(SpecificAvroSerde.class.getName())
                .setTopologyFactory(testTopology)
                .build();

        try (AxualClient client = new AxualClient(platformUnit.instance().getClientConfig("APP1"));
             AxualStreamsClient streamsClient = new AxualStreamsClient(platformUnit.instance().getClientConfig("streamapp", false));
             Producer<Application, TimeEvent> timeEventProducer = client.buildProducer(timeEventProducerConfig);
             Producer<Application, TimeAlert> timeAlertProducer = client.buildProducer(timeAlertProducerConfig);
             Consumer<Application, TimeEvent> timeEventConsumer = client.buildConsumer(timeEventConsumerConfig, timeEventProcessor);
        ) {

            // Start consumer
            timeEventConsumer.startConsuming();

            // Produce Alert Settings

            timeAlertProducer.produce(ProducerMessage.<Application, TimeAlert>newBuilder().setStream(ALERT_SETTINGS_TOPIC).setKey(ALERT_SETTING_1.getValue0()).setValue(ALERT_SETTING_1.getValue1()).build()).get();
            timeAlertProducer.produce(ProducerMessage.<Application, TimeAlert>newBuilder().setStream(ALERT_SETTINGS_TOPIC).setKey(ALERT_SETTING_2.getValue0()).setValue(ALERT_SETTING_2.getValue1()).build()).get();
            timeAlertProducer.produce(ProducerMessage.<Application, TimeAlert>newBuilder().setStream(ALERT_SETTINGS_TOPIC).setKey(ALERT_SETTING_3.getValue0()).setValue(ALERT_SETTING_3.getValue1()).build()).get();

            // Start streams
            StreamRunner streamRunner = streamsClient.buildStreamRunner(streamRunnerConfig);
            streamRunner.start();
            Awaitility.await("Wait for running state of Kafka Streams")
                    .atMost(15, TimeUnit.SECONDS)
                    .pollInterval(100, TimeUnit.MILLISECONDS)
                    .until(() -> streamRunner.state() == KafkaStreams.State.RUNNING);

            // Produce TimeEvent expected to stream to the end

            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_STREAMED_1.getValue0()).setValue(EVENT_STREAMED_1.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_STREAMED_2.getValue0()).setValue(EVENT_STREAMED_2.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_FILTERED_1.getValue0()).setValue(EVENT_FILTERED_1.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_FILTERED_2.getValue0()).setValue(EVENT_FILTERED_2.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_FILTERED_3.getValue0()).setValue(EVENT_FILTERED_3.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_FILTERED_4.getValue0()).setValue(EVENT_FILTERED_4.getValue1()).build()).get();
            timeEventProducer.produce(ProducerMessage.<Application, TimeEvent>newBuilder().setStream(FROM_TOPIC).setKey(EVENT_FILTERED_5.getValue0()).setValue(EVENT_FILTERED_5.getValue1()).build()).get();


            Awaitility.await("Wait for filtered alerts")
                    .atMost(60, TimeUnit.SECONDS)
                    .pollInterval(500, TimeUnit.MILLISECONDS)
                    .until(new Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            int size = alertList.size();
                            LOG.info("Alert list size = {}", size);
                            return size >= 2;
                        }
                    });

            timeEventConsumer.stopConsuming();
            streamRunner.stop();

        } catch (Exception e) {
            LOG.error("An exception occurred", e);
            throw e;
        }

        List<Pair<Application, TimeEvent>> receivedPairs = alertList.stream().map((msg -> new Pair<>(msg.getKey(), msg.getValue()))).collect(Collectors.toList());
        receivedPairs.forEach(c -> LOG.info("Received Key {} and Value {}", c.getValue0(), c.getValue1()));
        assertTrue(receivedPairs.contains(EVENT_STREAMED_1));
        assertTrue(receivedPairs.contains(EVENT_STREAMED_2));
        assertFalse(receivedPairs.contains(EVENT_FILTERED_1));
        assertFalse(receivedPairs.contains(EVENT_FILTERED_2));
        assertFalse(receivedPairs.contains(EVENT_FILTERED_3));
        assertFalse(receivedPairs.contains(EVENT_FILTERED_4));
        assertFalse(receivedPairs.contains(EVENT_FILTERED_5));

    }
}
