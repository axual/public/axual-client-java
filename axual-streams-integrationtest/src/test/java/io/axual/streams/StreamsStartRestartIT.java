package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.AxualClient;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.resolving.generic.ResolvingProxyConfig;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.axual.AxualSerde;
import io.axual.streams.proxy.axual.AxualSerdeConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.Consumed;
import org.awaitility.Awaitility;
import org.javatuples.KeyValue;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static io.axual.client.config.DeliveryStrategy.AT_LEAST_ONCE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class StreamsStartRestartIT {

    private static final Logger LOG = LoggerFactory.getLogger(StreamsStartRestartIT.class);

    private static final String FROM_TOPIC = "input-messages-StreamsAggregatorIT";
    private static final Integer PARTITIONS = 2;

    private static final String[][] INPUT_MESSAGES = {
            {"0", "account0"},
            {"1", "account1"},
            {"2", "account2"},
            {"3", "account3"},
            {"4", "account4"},
    };

    private static final String[][] INPUT_MESSAGES_2 = {
            {"5", "account5"},
            {"6", "account6"},
            {"7", "account7"},
            {"8", "account8"},
            {"9", "account9"},
    };


    @Rule
    public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit()
            .addStream(new StreamConfig()
                    .setName(FROM_TOPIC).setPartitions(PARTITIONS));

    @Test
    public void testRestartingStreams() throws Exception {
        // Prepare validation consumer
        final LinkedList<KeyValue<String, String>> resultList = new LinkedList<>();

        // Prepare producer for source messages
        final ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
                .setDeliveryStrategy(AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .setBlocking(true)
                .setKeySerializer(StringSerializer.class.getName())
                .setValueSerializer(StringSerializer.class.getName())
                .build();

        final ProxyChain chain = StreamRunnerConfig.DEFAULT_PROXY_CHAIN;
        final Map<String, Object> configs = new HashMap<>();
        configs.put(AxualSerdeConfig.KEY_SERDE_CHAIN_CONFIG, chain);
        configs.put(AxualSerdeConfig.VALUE_SERDE_CHAIN_CONFIG, chain);
        configs.put(AxualSerdeConfig.BACKING_KEY_SERDE_CONFIG, Serdes.String());
        configs.put(AxualSerdeConfig.BACKING_VALUE_SERDE_CONFIG, Serdes.String());
        configs.put(ResolvingProxyConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, "{tenant}-{instance}-{environment}-{topic}");

        final AxualSerde<String> keySerde = new AxualSerde<>(configs, true);
        final AxualSerde<String> valueSerde = new AxualSerde<>(configs, false);

        final TopologyFactory testTopology = builder -> {
            builder
                    .stream(FROM_TOPIC, Consumed.with(keySerde, valueSerde))
                    .peek((key, value) -> LOG.info("Incoming message: key: {} value: {}", key, value))
                    .foreach((key, value) -> resultList.push(KeyValue.with(key, value)));
            return builder.build();
        };

        // Prepare streamRunner
        final StreamRunnerConfig aggregatorConfig = StreamRunnerConfig.builder()
                .setDeliveryStrategy(AT_LEAST_ONCE)
                .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                .setDefaultKeySerde(Serdes.String())
                .setDefaultValueSerde(Serdes.String())
                .setTopologyFactory(testTopology)
                .build();

        try (AxualClient client = new AxualClient(platformUnit.instance().getClientConfig("APP1"));
             AxualStreamsClient streamsClient = new AxualStreamsClient(platformUnit.instance().getClientConfig("streamsclient", false));
             Producer<String, String> producer = client.buildProducer(producerConfig);
        ) {

            // Start aggregator
            StreamRunner aggregator = streamsClient.buildStreamRunner(aggregatorConfig);
            aggregator.start();
            Awaitility.await("Wait for running state of Kafka Streams")
                    .atMost(500, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> aggregator.state() == KafkaStreams.State.RUNNING);

            // Produce input messages
            for (String[] message : INPUT_MESSAGES) {
                ProducerMessage<String, String> msg = ProducerMessage.<String, String>newBuilder()
                        .setStream(FROM_TOPIC)
                        .setKey(message[0])
                        .setValue(message[1])
                        .build();
                producer.produce(msg);
            }

            Awaitility.await("Wait for output messages")
                    .atMost(60, SECONDS)
                    .pollInterval(500, MILLISECONDS)
                    .until(() -> {
                        int size = resultList.size();
                        LOG.info("Result list size = {}", size);
                        return size >= INPUT_MESSAGES.length;
                    });

            LOG.info("pinda closing");
            // Stop Stream
            aggregator.close(Duration.ofSeconds(15));
            LOG.info("pinda closed");

            // Restart Stream
            StreamRunner aggregatorTwo = streamsClient.buildStreamRunner(aggregatorConfig);
            aggregatorTwo.start();

            Awaitility.await("Wait for running state of Kafka Streams")
                    .atMost(500, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> aggregatorTwo.state() == KafkaStreams.State.RUNNING);

            // Produce input messages after restart
            for (String[] message : INPUT_MESSAGES_2) {
                ProducerMessage<String, String> msg = ProducerMessage.<String, String>newBuilder()
                        .setStream(FROM_TOPIC)
                        .setKey(message[0])
                        .setValue(message[1])
                        .build();
                producer.produce(msg);
            }

            Awaitility.await("Wait for output messages")
                    .atMost(60, SECONDS)
                    .pollInterval(500, MILLISECONDS)
                    .until(() -> {
                        int size = resultList.size();
                        LOG.info("Result list size = {}", size);
                        return size >= (INPUT_MESSAGES.length + INPUT_MESSAGES_2.length);
                    });
            aggregator.stop();
        } catch (Exception e) {
            LOG.error("An exception occurred", e);
            throw e;
        }


    }
}
