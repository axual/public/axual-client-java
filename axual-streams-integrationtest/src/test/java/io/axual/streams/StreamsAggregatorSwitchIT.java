package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.streams.state.KeyValueStore;
import org.awaitility.Awaitility;
import org.javatuples.Pair;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.ProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.resolving.generic.ResolvingProxyConfig;
import io.axual.common.config.ClientConfig;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.axual.AxualSerde;
import io.axual.streams.proxy.axual.AxualSerdeConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;

import static io.axual.client.config.DeliveryStrategy.AT_LEAST_ONCE;
import static io.axual.client.config.OrderingStrategy.KEEPING_ORDER;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertTrue;


/**
 * In this test the scenario is as follows:
 * Distribution is disabled.
 * One producerA application is pointed to A, producerB to B, both produce the same payloads.
 * The streaming application starts on cluster A.
 * Two consumers, one on each cluster consuming from the streaming apps output topic.
 *
 * Steps:
 *      - producerA produces all messages on cluster A
 *      - switch streaming application to cluster B
 *      - producerB produces all messages on cluster B
 *
 * Verification includes matching messages of consumed by both of the consumers.
 */
public class StreamsAggregatorSwitchIT {

    private static final Logger LOG = LoggerFactory.getLogger(StreamsAggregatorSwitchIT.class);

    private static final String FROM_TOPIC = "input-messages-StreamsAggregatorSwitchIT";
    private static final String TO_TOPIC = "output-messages-StreamsAggregatorSwitchIT";

    // tmp dirs are not cleaned it seems. This results in consecutive calls on same cluster
    // appending data. Other tests don't face this issue since the data itself is not being checked,
    // only message counts.
    private static final String STREAMS_ID = "aggregatorswitch" + UUID.randomUUID();
    private static final String STORE_NAME = "storename-StreamsAggregatorSwitchIT";
    private static final String REPARTITION_TOPIC = STREAMS_ID + "-storename-StreamsAggregatorSwitchIT-repartition";
    private static final String CHANGELOG_TOPIC = STREAMS_ID + "-storename-StreamsAggregatorSwitchIT-changelog";

    private static final Integer PARTITIONS = 1;

    private static final long TTL = 10L * 1000;

    private static final String[][] INPUT_MESSAGES = new String[][] {
                {"customer:1:account1", "account1"},
                {"customer:1:account2", "account2"},
                {"customer:2:account3", "account3"},
                {"customer:2:account4", "account4"},
                {"customer:3:account5", "account5"},
                {"customer:3:account6", "account6"},
                {"customer:4:account7", "account7"},
                {"customer:4:account8", "account8"},
                {"order:1:nr1", "nr1"},
                {"order:1:nr2", "nr2"},
                {"order:2:nr3", "nr3"},
                {"order:2:nr4", "nr4"},
                {"order:3:nr5", "nr5"},
                {"order:3:nr6", "nr6"},
                {"file:2:id1", "id1"},
                {"file:2:id2", "id2"},
                {"file:4:id3", "id3"},
                {"file:4:id4", "id4"},
                {"file:6:id5", "id5"},
                {"file:6:id6", "id6"},
    };

    private final List<Pair<String, String>> EXPECTED_OUTPUT = Arrays.asList(
            new Pair<>("customer:1", "account1:account2"),
            new Pair<>("customer:2", "account3:account4"),
            new Pair<>("customer:3", "account5:account6"),
            new Pair<>("customer:4", "account7:account8"),
            new Pair<>("order:1", "nr1:nr2"),
            new Pair<>("order:2", "nr3:nr4"),
            new Pair<>("order:3", "nr5:nr6"),
            new Pair<>("file:2", "id1:id2"),
            new Pair<>("file:4", "id3:id4"),
            new Pair<>("file:6", "id5:id6")
    );

    @Rule
    public DualClusterPlatformUnit platformUnit = new DualClusterPlatformUnit(false)
            .addStream(new StreamConfig()
                    .setName(FROM_TOPIC).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(REPARTITION_TOPIC).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(CHANGELOG_TOPIC).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(TO_TOPIC).setPartitions(PARTITIONS));

    private TopologyFactory buildTopologyFactory(final AxualSerde<String> keySerde,
                                                 final AxualSerde<String> valueSerde) {
        final Reducer<String> reducer = (value1, value2) -> value1 + ":" + value2;

        return builder -> {
            builder
                    .stream(FROM_TOPIC, Consumed.with(keySerde, valueSerde))
                    .peek((key, value) -> LOG.info("Incoming message: key: {} value: {}", key, value))
                    .selectKey((key, value) -> {
                        final String[] parts = key.split(":");
                        if (parts.length != 3) {
                            LOG.error("Found record with unexpected key: [{}]. Keeping record key as-is before group by", key);
                            return key;
                        }
                        return parts[0] + ":" + parts[1];
                    })
                    .groupByKey(Grouped.with(keySerde, valueSerde))
                    .reduce(reducer, Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(STORE_NAME)
                            .withKeySerde(keySerde)
                            .withValueSerde(valueSerde))
                    .toStream()
                    .peek((key, value) -> LOG.info("Outgoing message: key: {} value: {}", key, value))
                    .to(TO_TOPIC, Produced.with(keySerde, valueSerde));
            return builder.build();
        };
    }

    private void awaitStreamsStart(final StreamRunner runner) {
        Awaitility.await("Wait for running state of Kafka Streams")
                .atMost(15, SECONDS)
                .pollInterval(100, MILLISECONDS)
                .until(() -> runner.state() == KafkaStreams.State.RUNNING);
    }

    private void awaitStreamsSwitch(final StreamRunner runner) {
        Awaitility.await("Wait for stopping state of Kafka Streams")
                .atMost(TTL*2, MILLISECONDS)
                .pollInterval(100, MILLISECONDS)
                .until(() -> runner.state() != KafkaStreams.State.RUNNING);
        awaitStreamsStart(runner);
    }

    @Test
    public void testAggregatorSwitchTopology() throws Exception {
        platformUnit.platform().getInstance().getDiscoveryUnit().setTtl(TTL);

        // Prepare validation consumer
        final LinkedList<ConsumerMessage<String, String>> resultListA = new LinkedList<>();
        final LinkedList<ConsumerMessage<String, String>> resultListB = new LinkedList<>();
        final EnqueueProcessor<String, String> resultProcessorA = new EnqueueProcessor<>("resultCollectorA", resultListA);
        final EnqueueProcessor<String, String> resultProcessorB = new EnqueueProcessor<>("resultCollectorB", resultListB);

        final ProxyChain chain = StreamRunnerConfig.DEFAULT_PROXY_CHAIN;
        final Map<String, Object> configs = new HashMap<>();
        configs.put(AxualSerdeConfig.KEY_SERDE_CHAIN_CONFIG, chain);
        configs.put(AxualSerdeConfig.VALUE_SERDE_CHAIN_CONFIG, chain);
        configs.put(AxualSerdeConfig.BACKING_KEY_SERDE_CONFIG, Serdes.String());
        configs.put(AxualSerdeConfig.BACKING_VALUE_SERDE_CONFIG, Serdes.String());
        configs.put(ResolvingProxyConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, "{tenant}-{instance}-{environment}-{topic}");

        try (final AxualSerde<String> keySerde = new AxualSerde<>(configs, true);
            final AxualSerde<String> valueSerde = new AxualSerde<>(configs, false)) {

            final TopologyFactory testTopology = buildTopologyFactory(keySerde, valueSerde);

            // Prepare streamRunner
            final StreamRunnerConfig aggregatorConfig = StreamRunnerConfig.builder()
                    .setDeliveryStrategy(AT_LEAST_ONCE)
                    .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                    .setDefaultKeySerde(Serdes.String())
                    .setDefaultValueSerde(Serdes.String())
                    .setTopologyFactory(testTopology)
                    .build();
            // Prepare producer for source messages
            final ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
                    .setDeliveryStrategy(AT_LEAST_ONCE)
                    .setOrderingStrategy(KEEPING_ORDER)
                    .setBlocking(true)
                    .setKeySerializer(StringSerializer.class.getName())
                    .setValueSerializer(StringSerializer.class.getName())
                    .build();
            final ConsumerConfig<String, String> resultCollectorConfig = ConsumerConfig.<String, String>builder()
                    .setStream(TO_TOPIC)
                    .setDeliveryStrategy(AT_LEAST_ONCE)
                    .setKeyDeserializer(StringDeserializer.class.getName())
                    .setValueDeserializer(StringDeserializer.class.getName())
                    .build();

            // application configurations
            final ClientConfig clientConfigA = platformUnit.instance().getClientConfig("client.app.a", false, platformUnit.clusterA());
            final ClientConfig clientConfigB = platformUnit.instance().getClientConfig("client.app.b", false, platformUnit.clusterB());
            final ClientConfig streamsClientConfig = platformUnit.instance().getClientConfig(STREAMS_ID, false, platformUnit.clusterA());

            try (AxualClient clientA = new AxualClient(clientConfigA);
                 AxualClient clientB = new AxualClient(clientConfigB);
                 AxualStreamsClient streamsClient = new AxualStreamsClient(streamsClientConfig);
                 Producer<String, String> producerA = clientA.buildProducer(producerConfig);
                 Producer<String, String> producerB = clientB.buildProducer(producerConfig);
                 Consumer<String, String> resultCollectorA = clientA.buildConsumer(resultCollectorConfig, resultProcessorA);
                 Consumer<String, String> resultCollectorB = clientB.buildConsumer(resultCollectorConfig, resultProcessorB)
            ) {
                // Direct everything to clusterA
                platformUnit.instance().directApplicationTo(clientConfigA, platformUnit.clusterA());
                platformUnit.instance().directApplicationTo(clientConfigB, platformUnit.clusterB());
                platformUnit.instance().directApplicationTo(streamsClientConfig, platformUnit.clusterA());

                // Start consumers
                resultCollectorA.startConsuming();
                resultCollectorB.startConsuming();

                // Start aggregator
                StreamRunner aggregator = streamsClient.buildStreamRunner(aggregatorConfig);
                aggregator.start();
                awaitStreamsStart(aggregator);

                // Produce input messages on cluster A
                for (String[] message : INPUT_MESSAGES) {
                    ProducerMessage<String, String> msg = ProducerMessage.<String, String>newBuilder()
                            .setStream(FROM_TOPIC)
                            .setKey(message[0])
                            .setValue(message[1])
                            .build();
                    producerA.produce(msg).get();
                }

                LOG.info("Switching streaming app to cluster B.");
                platformUnit.instance().directApplicationTo(streamsClientConfig, platformUnit.clusterB());
                awaitStreamsSwitch(aggregator);

                // Produce input messages on cluster B
                for (String[] message : INPUT_MESSAGES) {
                    ProducerMessage<String, String> msg = ProducerMessage.<String, String>newBuilder()
                            .setStream(FROM_TOPIC)
                            .setKey(message[0])
                            .setValue(message[1])
                            .build();
                    producerB.produce(msg).get();
                }

                Awaitility.await("Wait for output messages")
                        .atMost(60, SECONDS)
                        .pollInterval(500, MILLISECONDS)
                        .until(() -> {
                            LOG.info("Result list size = {}", resultListB.size());
                            return resultListB.size() >= EXPECTED_OUTPUT.size();
                        });

                aggregator.stop();
                ConsumeFailedException consumeFailedException = resultCollectorA.stopConsuming();
                if (consumeFailedException != null) {
                    consumeFailedException = resultCollectorB.stopConsuming();
                }
                if (consumeFailedException != null) {
                    LOG.error("A ConsumeFailedException occurred", consumeFailedException);
                }
            } catch (Exception e) {
                LOG.error("An exception occurred", e);
                throw e;
            }
        }

        verifyResult(resultListA);
        verifyResult(resultListB);
    }

    private void verifyResult(Collection<ConsumerMessage<String, String>> results) {
        for (ConsumerMessage<String, String> message : results) {
            if (!EXPECTED_OUTPUT.contains(new Pair<>(message.getKey(), message.getValue()))) {
                assertTrue(
                        String.format("Unexpected message <%s,%s> received", message.getKey(), message.getValue()),
                        EXPECTED_OUTPUT.contains(new Pair<>(message.getKey(), message.getValue()))
                );
            }
        }
    }

}
