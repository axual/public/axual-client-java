# Axual Streams Integration Tests

This module contains the code that tests the streams client on some key end-to-end scenarios.
Those include: 
 * Aggregating two streams and producing to a third.
 * Stream joining
 * a cluster switch occurring on any of the above.
