package io.axual.client.proxy.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;

import static io.axual.client.proxy.helper.Helper.createAxualConsumer;
import static io.axual.client.proxy.helper.Helper.createAxualProducer;
import static io.axual.platform.test.core.PlatformUnit.generateClusterConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterName;
import static io.axual.platform.test.core.PlatformUnit.generateInstanceConfig;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AxualConsumerPKCS12IT {
    private static final Logger LOG = LoggerFactory.getLogger(AxualConsumerIT.class);
    private static final String STREAM = "general-random-AxualConsumerPKCS12IT";
    private static final String TEST_CONTENT = "Test Content";
    private static final long TTL = 0;

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit(
            generateInstanceConfig(2, false, SslConfig.KeystoreType.PKCS12),
            generateClusterConfig(generateClusterName(0), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true),
            generateClusterConfig(generateClusterName(1), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true))
            .addStream(new StreamConfig().setName(STREAM));

    private Runnable getProducerThread(final AxualProducer<String, String> producer, final String name, final int count, final CountDownLatch latch) {
        return () -> {
            Thread.currentThread().setName(String.format("Producer %s thread", name));
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(STREAM, TEST_CONTENT, TEST_CONTENT);
            int i = count - 1;
            do {
                producer.send(producerRecord,
                        (metadata, e) -> {
                            if (e != null) {
                                LOG.error("Exception: ", e);
                            } else {
                                LOG.info("Producer {} produced offset: {}", name, metadata.offset());
                            }
                        });
            } while (i-- > 0);
            latch.countDown();
        };
    }

    @Test
    public void shouldConsumeFromOtherClusterAfterSwitch() throws InterruptedException {
        final InstanceUnit instance = platform.instance();

        final int messagesPerCluster = 5;
        instance.getDiscoveryUnit().setTtl(TTL);

        // create consumer
        final ClientConfig consumerConfig = instance.getClientConfig("io.axual.test");
        instance.getDiscoveryUnit().directApplicationTo(consumerConfig, platform.clusterA());
        final AxualConsumer<String, String> consumer = createAxualConsumer(consumerConfig, instance.getDiscoveryUnit().getUrl());
        final Queue<ConsumerRecord<String, String>> queue = new LinkedBlockingQueue<>();

        // create producers
        final ClientConfig producerConfigA = instance.getClientConfig("io.axual.testa");
        instance.getDiscoveryUnit().directApplicationTo(producerConfigA, platform.clusterA());
        AxualProducer<String, String> producerA = createAxualProducer(producerConfigA, instance.getDiscoveryUnit().getUrl());

        final ClientConfig producerConfigB = instance.getClientConfig("io.axual.testb");
        instance.getDiscoveryUnit().directApplicationTo(producerConfigB, platform.clusterB());
        AxualProducer<String, String> producerB = createAxualProducer(producerConfigB, instance.getDiscoveryUnit().getUrl());

        assertEquals(SslConfig.KeystoreType.PKCS12, consumerConfig.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, consumerConfig.getSslConfig().getTruststoreType());

        assertEquals(SslConfig.KeystoreType.PKCS12, producerConfigA.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, producerConfigA.getSslConfig().getTruststoreType());

        assertEquals(SslConfig.KeystoreType.PKCS12, producerConfigB.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, producerConfigB.getSslConfig().getTruststoreType());

        // setup
        CountDownLatch latch = new CountDownLatch(3);
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        // register consumer on stream
        threadPool.submit(() -> {
            Thread.currentThread().setName("Consumer thread");
            consumer.subscribe(Collections.singletonList(STREAM));

            while (queue.size() < 2 * messagesPerCluster) {
                ConsumerRecords<String, String> read = consumer.poll(100);
                read.forEach(record -> {
                    LOG.info("Consumed offset: {}.", record.offset());
                    queue.add(record);
                });
                consumer.commitSync();

                if (queue.size() == messagesPerCluster) { // switch
                    LOG.info("Before switch command");
                    instance.directApplicationTo(consumerConfig, platform.clusterB());
                    LOG.info("After switch command");
                }
            }
            latch.countDown();
        });

        // producers produce X messages each
        threadPool.submit(getProducerThread(producerA, "A", messagesPerCluster, latch));
        threadPool.submit(getProducerThread(producerB, "B", messagesPerCluster, latch));

        assertTrue("Expected latch to be 0 in 30 seconds, Consumers or producers not done", latch.await(30, SECONDS));

        producerB.close();
        producerA.close();
        consumer.close();

        // assert consumer has consumed 2 x X messages
        assertEquals(2 * messagesPerCluster, queue.size());
    }
}
