package io.axual.client.proxy.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.CooperativeStickyAssignor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static io.axual.client.proxy.helper.Helper.createAxualConsumer;
import static io.axual.client.proxy.helper.Helper.createAxualProducer;
import static io.axual.client.proxy.helper.Helper.getGenericConsumerConfigs;
import static io.axual.client.proxy.helper.Helper.putProxyChainConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterName;
import static io.axual.platform.test.core.PlatformUnit.generateInstanceConfig;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AxualConsumerIT {
    private static final Logger LOG = LoggerFactory.getLogger(AxualConsumerIT.class);
    private static final String STREAM = "general-random-AxualConsumerIT";
    private static final String STREAM_REJOINING_CONSUMERS = "general-random-rejoin";
    private static final String TEST_CONTENT = "Test Content";
    private static final long TTL = 0;
    private static final int PARTITIONS = 6;
    private static final int NUMBER_OF_MESSAGES = 5;

    @Rule
    public DualClusterPlatformUnit dualClusterPlatform = new DualClusterPlatformUnit(
            generateInstanceConfig(2, false, SslConfig.KeystoreType.PKCS12),
            generateClusterConfig(generateClusterName(0), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true),
            generateClusterConfig(generateClusterName(1), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true))
            .addStream(new StreamConfig().setName(STREAM).setPartitions(PARTITIONS));

    @Rule
    public SingleClusterPlatformUnit singleClusterPlatform = new SingleClusterPlatformUnit(
            generateInstanceConfig(1, false, SslConfig.KeystoreType.PKCS12),
            generateClusterConfig(generateClusterName(0), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true))
            .addStream(new StreamConfig().setName(STREAM_REJOINING_CONSUMERS).setPartitions(PARTITIONS));

    private Runnable getProducerThread(final AxualProducer<String, String> producer,
                                       final String name,
                                       final int numberOfMessages,
                                       final CountDownLatch latch) {
        return () -> {
            Thread.currentThread().setName(String.format("Producer %s thread", name));
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(STREAM, TEST_CONTENT, TEST_CONTENT);
            int i = numberOfMessages - 1;
            do {
                produceRecord(producer, producerRecord);
            } while (i-- > 0);
            latch.countDown();
        };
    }

    @Test
    public void shouldConsumeFromOtherClusterAfterSwitch() throws InterruptedException {
        final InstanceUnit instance = dualClusterPlatform.instance();

        instance.getDiscoveryUnit().setTtl(TTL);

        // create consumer
        final ClientConfig consumerConfig = instance.getClientConfig("io.axual.test");
        instance.getDiscoveryUnit().directApplicationTo(consumerConfig, dualClusterPlatform.clusterA());
        final AxualConsumer<String, String> consumer = createAxualConsumer(consumerConfig, instance.getDiscoveryUnit().getUrl());
        final Queue<ConsumerRecord<String, String>> queue = new LinkedBlockingQueue<>();

        // create producers
        final ClientConfig producerConfigA = instance.getClientConfig("io.axual.testa");
        instance.getDiscoveryUnit().directApplicationTo(producerConfigA, dualClusterPlatform.clusterA());
        AxualProducer<String, String> producerA = createAxualProducer(producerConfigA, instance.getDiscoveryUnit().getUrl());

        final ClientConfig producerConfigB = instance.getClientConfig("io.axual.testb");
        instance.getDiscoveryUnit().directApplicationTo(producerConfigB, dualClusterPlatform.clusterB());
        AxualProducer<String, String> producerB = createAxualProducer(producerConfigB, instance.getDiscoveryUnit().getUrl());

        // setup
        CountDownLatch latch = new CountDownLatch(3);
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        // register consumer on stream
        threadPool.submit(() -> {
            Thread.currentThread().setName("Consumer thread");
            consumer.subscribe(Collections.singletonList(STREAM));

            while (queue.size() < 2 * NUMBER_OF_MESSAGES) {
                ConsumerRecords<String, String> read = consumer.poll(Duration.ofMillis(100));
                read.forEach(record -> {
                    LOG.info("Consumed offset: {}.", record.offset());
                    queue.add(record);
                });
                consumer.commitSync();

                if (queue.size() == NUMBER_OF_MESSAGES) { // switch
                    LOG.info("Before switch command");
                    instance.directApplicationTo(consumerConfig, dualClusterPlatform.clusterB());
                    LOG.info("After switch command");
                }
            }
            latch.countDown();
        });

        // producers produce X messages each
        threadPool.submit(getProducerThread(producerA, "A", NUMBER_OF_MESSAGES, latch));
        threadPool.submit(getProducerThread(producerB, "B", NUMBER_OF_MESSAGES, latch));

        assertTrue("Expected latch to be 0 in 30 seconds, Consumers or producers not done", latch.await(30, SECONDS));

        producerB.close();
        producerA.close();
        consumer.close();

        // assert consumer has consumed 2 x X messages
        assertEquals(2 * NUMBER_OF_MESSAGES, queue.size());
    }

    @Test
    public void cooperativeRebalancing() {
        final InstanceUnit instance = dualClusterPlatform.instance();

        // create consumers
        final ClientConfig consumerConfig = instance.getClientConfig("io.axual.test");
        instance.getDiscoveryUnit().directApplicationTo(consumerConfig, dualClusterPlatform.clusterA());
        final Map<String, Object> configs = getGenericConsumerConfigs(consumerConfig, instance.getDiscoveryUnit().getUrl(), "");
        putProxyChainConfig(configs);
        // Set the assignment strategy
        configs.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, CooperativeStickyAssignor.class.getName());
        final AxualConsumer<String, String> consumer1 = new AxualConsumer<>(configs);
        final AxualConsumer<String, String> consumer2 = new AxualConsumer<>(configs);
        // Subscribe to the same topic
        consumer1.subscribe(Collections.singletonList(STREAM));
        consumer2.subscribe(Collections.singletonList(STREAM));

        // Verify that CooperativeAssignment leaves the two consumers with an equal amount of partitions assigned
        Awaitility.await()
                .atMost(30, TimeUnit.SECONDS)
                .until(() ->
                        {
                            // Poll such that the consumers get assigned
                            consumer1.poll(Duration.ZERO);
                            consumer2.poll(Duration.ZERO);

                            return consumer1.assignment().size() == PARTITIONS / 2
                                    && consumer2.assignment().size() == consumer1.assignment().size();
                        }
                );
    }

    @Test
    public void shouldIgnoreAutoOffsetResetWhenACommittedOffsetExists() throws InterruptedException, ExecutionException {
        final InstanceUnit instance = singleClusterPlatform.instance();

        // create producer
        final ClientConfig producerConfig = instance.getClientConfig("io.axual.test");
        AxualProducer<String, String> producer = createAxualProducer(producerConfig, instance.getDiscoveryUnit().getUrl());

        produceMessagesToTopic(producer, "first_batch");

        // Create a consumer with AUTO_RESET_OFFSET="earliest" because we need to read in the first place:
        // If the first time, we start already with latest, we will miss the initial part of the stream, and we will not be able to read.
        // In this way, we will commit the offset that has been read with the first consumer, and when we will start the second consumer,
        // with the same group_id, the setting "latest" should be ignored, and we should read from the 6th message: this behavior is what
        // we are interested to verify in this test
        Map<String, Object> configMap = getConsumerConfiguration(instance);
        createAxualConsumerAndConsume(configMap);

        produceMessagesToTopic(producer, "second_batch");

        configMap = getConsumerConfiguration(instance);
        configMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        List<ConsumerRecord<String, String>> readRecords = createAxualConsumerAndConsume(configMap);

        producer.close();

        assertEquals(5, readRecords.size());
        readRecords.forEach(record -> assertTrue(record.value().contains("second_batch")));
    }

    private List<ConsumerRecord<String, String>> createAxualConsumerAndConsume(Map<String, Object> configMap) {
        AxualConsumer<String, String> consumer = new AxualConsumer<>(configMap);
        consumer.subscribe(Collections.singletonList(STREAM_REJOINING_CONSUMERS));
        List<ConsumerRecord<String, String>> readRecords = readRecords(consumer);
        consumer.close();
        return readRecords;
    }

    private Map<String, Object> getConsumerConfiguration(InstanceUnit instance) {
        // create consumer, randomizeAppId has to be set to false such that the 2 consumers have the same app id
        final ClientConfig consumerConfig = instance.getClientConfig("io.axual.test", false);
        final Map<String, Object> configMap = getGenericConsumerConfigs(consumerConfig, instance.getDiscoveryUnit().getUrl(), "");
        putProxyChainConfig(configMap);
        return configMap;
    }

    private List<ConsumerRecord<String, String>> readRecords(AxualConsumer<String, String> consumer) {
        LOG.info("Reading messages");
        AtomicInteger counter = new AtomicInteger();
        List<ConsumerRecord<String, String>> readRecords = new ArrayList<>();
        Awaitility.await()
                .atMost(15, TimeUnit.SECONDS)
                .until(() ->
                        {
                            while (counter.get() < NUMBER_OF_MESSAGES) {
                                ConsumerRecords<String, String> read = consumer.poll(Duration.ofMillis(100));
                                read.forEach(record -> {
                                    LOG.info("Consumed offset: {}.", record.offset());
                                    readRecords.add(record);
                                    counter.getAndIncrement();
                                });
                                consumer.commitSync();
                            }
                        }
                );
        return readRecords;
    }

    private void produceMessagesToTopic(AxualProducer<String, String> producer, String content) throws ExecutionException, InterruptedException {
        int i = 0;
        do {
            String currentContent = content + "_" + i;
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(STREAM_REJOINING_CONSUMERS, currentContent, currentContent);
            produceRecord(producer, producerRecord).get();
        } while (++i < NUMBER_OF_MESSAGES);
    }

    private static Future<RecordMetadata> produceRecord(AxualProducer<String, String> producer, ProducerRecord<String, String> producerRecord) {
        return producer.send(producerRecord,
                (metadata, e) -> {
                    if (e != null) {
                        LOG.error("Exception: ", e);
                    } else {
                        LOG.info("Produced content at partition: {}, offset: {}", metadata.partition(), metadata.offset());
                    }
                });
    }

}