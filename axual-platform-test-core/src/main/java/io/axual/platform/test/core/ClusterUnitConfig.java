package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.SslConfig;

@InterfaceStability.Evolving
public class ClusterUnitConfig {
    private String name;
    private String bindAddress;
    private String advertisedAddress;
    private SslConfig sslConfig;
    private String groupPattern;
    private String topicPattern;
    private String transactionalIdPattern;
    private Integer zookeeperPort;
    private Integer brokerPort;
    private Integer schemaRegistryPort;
    private boolean useAdvancedAcl;
    private boolean useValueHeaders;

    public String getName() {
        return name;
    }

    public ClusterUnitConfig setName(String name) {
        this.name = name;
        return this;
    }

    public String getAdvertisedAddress() {
        return advertisedAddress;
    }

    public ClusterUnitConfig setAdvertisedAddress(String advertisedAddress) {
        this.advertisedAddress = advertisedAddress;
        return this;
    }

    public String getBindAddress() {
        return bindAddress;
    }

    public ClusterUnitConfig setBindAddress(String bindAddress) {
        this.bindAddress = bindAddress;
        return this;
    }

    public SslConfig getSslConfig() {
        return sslConfig;
    }

    public ClusterUnitConfig setSslConfig(SslConfig sslConfig) {
        this.sslConfig = sslConfig;
        return this;
    }

    public String getGroupPattern() {
        return groupPattern;
    }

    public ClusterUnitConfig setGroupPattern(String groupPattern) {
        this.groupPattern = groupPattern;
        return this;
    }

    public String getTopicPattern() {
        return topicPattern;
    }

    public ClusterUnitConfig setTopicPattern(String topicPattern) {
        this.topicPattern = topicPattern;
        return this;
    }

    public String getTransactionalIdPattern() {
        return transactionalIdPattern;
    }

    public ClusterUnitConfig setTransactionalIdPattern(String pattern) {
        this.transactionalIdPattern = pattern;
        return this;
    }

    public Integer getZookeeperPort() {
        return zookeeperPort;
    }

    public ClusterUnitConfig setZookeeperPort(Integer zookeeperPort) {
        this.zookeeperPort = zookeeperPort;
        return this;
    }

    public Integer getBrokerPort() {
        return brokerPort;
    }

    public ClusterUnitConfig setBrokerPort(Integer brokerPort) {
        this.brokerPort = brokerPort;
        return this;
    }

    public Integer getSchemaRegistryPort() {
        return schemaRegistryPort;
    }

    public ClusterUnitConfig setSchemaRegistryPort(Integer schemaRegistryPort) {
        this.schemaRegistryPort = schemaRegistryPort;
        return this;
    }

    public boolean isUseAdvancedAcl() {
        return useAdvancedAcl;
    }

    public ClusterUnitConfig setUseAdvancedAcl(boolean useAdvancedAcl) {
        this.useAdvancedAcl = useAdvancedAcl;
        return this;
    }

    public boolean isUseValueHeaders() {
        return useValueHeaders;
    }

    public ClusterUnitConfig setUseValueHeaders(boolean useValueHeaders) {
        this.useValueHeaders = useValueHeaders;
        return this;
    }
}
