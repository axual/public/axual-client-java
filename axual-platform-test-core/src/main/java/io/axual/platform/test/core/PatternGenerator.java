package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Random;

public class PatternGenerator {
    private static final int MAX_PREFIXES = 5;
    private static final int MAX_SUFFIXES = 5;
    private static final int MAX_REPEATING_SEPARATORS = 4;
    @SuppressWarnings("java:S2245")
    private final Random random = new Random();

    private final String replacementName;
    private final String[] keywords;
    private final String[] separators;

    public PatternGenerator(String replacementName, String[] keywords, String[] separators) {
        this.replacementName = replacementName;
        this.keywords = keywords;
        this.separators = separators;
    }

    public String randomPattern() {
        StringBuilder bld = new StringBuilder();

        final int numKeywordsBefore = random.nextInt(MAX_PREFIXES);
        for (int i = 0; i < numKeywordsBefore; i++) {
            bld.append(randomKeyword());
            bld.append(randomSeparators());
        }

        bld.append(givenKeyword(replacementName));

        final int numKeywordsAfter = random.nextInt(MAX_SUFFIXES);
        for (int i = 0; i < numKeywordsAfter; i++) {
            bld.append(randomSeparators() + randomKeyword());
        }

        return bld.toString();
    }

    private String randomKeyword() {
        return givenKeyword(keywords[random.nextInt(keywords.length)]);
    }

    private String givenKeyword(String keyword) {
        return "{" + keyword + "}";
    }

    private String randomSeparators() {
        StringBuilder bld = new StringBuilder();

        final int numSeparators = random.nextInt(MAX_REPEATING_SEPARATORS);
        for (int i = 0; i < numSeparators + 1; i++) {
            bld.append(separators[random.nextInt(separators.length)]);
        }
        return bld.toString();
    }
}
