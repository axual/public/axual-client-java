package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import io.axual.common.config.CommonConfig;
import io.axual.common.exception.ClientException;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.tools.ExecutorUtil;
import io.axual.common.tools.KafkaUtil;
import io.axual.common.tools.SleepUtil;

public class DistributionUnit {
    private static final Logger LOG = LoggerFactory.getLogger(DistributionUnit.class);
    private static final String COPY_FLAG_HEADER = "DistributionUnitCopyFlag";

    private final Map<ClusterUnit, Producer<byte[], byte[]>> producers = new HashMap<>();
    private final List<ClusterUnit> clusters;
    private final List<Future<?>> futures = new ArrayList<>();
    private final Map<String, Object> context = new HashMap<>();
    private final ExecutorService executor = Executors.newCachedThreadPool();

    private boolean stop = false;
    private boolean paused = false;

    public DistributionUnit(List<ClusterUnit> clusters, Map<String, Object> context) {
        this.clusters = clusters;
        this.context.putAll(context);
    }

    public void start() {
        // Launch a consumer and producer on all clusters
        for (ClusterUnit cluster : clusters) {
            // Create producer that will produce copied records to the given cluster
            Map<String, Object> props = KafkaUtil.getKafkaConfigs(SslUnit.getDefaultSslConfig());
            props.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, cluster.getBootstrapServer());
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
            producers.put(cluster, new KafkaProducer<>(props));

            // Create and start the thread that subscribes and triggers distribution
            Set<ClusterUnit> targetClusters = new HashSet<>(clusters);
            targetClusters.remove(cluster);
            futures.add(executor.submit(new DistributionTask(cluster, targetClusters)));
        }
    }

    public void stop() {
        stop = true;

        for (Map.Entry<ClusterUnit, Producer<byte[], byte[]>> entry : producers.entrySet()) {
            entry.getValue().close();
        }
        producers.clear();

        for (Future<?> future : futures) {
            while (!future.isDone()) {
                future.cancel(false);
            }
        }

        // Allow threads to close by themselves for 10 seconds
        ExecutorUtil.terminateExecutor(executor, Duration.ofSeconds(10));
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

    private class DistributionTask implements Runnable {
        private final ClusterUnit sourceCluster;
        private Set<ClusterUnit> targetClusters;
        private final TopicResolver sourceResolver;
        private final String topicRegex;
        private final Map<ClusterUnit, TopicResolver> targetResolvers = new HashMap<>();

        DistributionTask(ClusterUnit sourceCluster, Set<ClusterUnit> targetClusters) {
            this.sourceCluster = sourceCluster;
            this.targetClusters = targetClusters;

            // Create a resolver that can (un)resolve topic names from the source cluster
            sourceResolver = new TopicPatternResolver();
            Map<String, Object> configs = new HashMap<>(context);
            configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, sourceCluster.getTopicPattern());

            // Create a pattern to subscribe to all tenant topics
            configs.put(CommonConfig.ENVIRONMENT, ".*");
            sourceResolver.configure(configs);
            topicRegex = sourceResolver.resolveTopic(".*");

            // Remove the wildcard for all other purposes
            configs.remove(CommonConfig.ENVIRONMENT);
            sourceResolver.configure(configs);

            // Since TopicResolvers are not threadsafe, and we use multiple distribution threads,
            // we use a private set of resolvers in this thread. In the block below we create one
            // topic resolver for every cluster.
            for (ClusterUnit target : targetClusters) {
                TopicResolver resolver = new TopicPatternResolver();
                targetResolvers.put(target, resolver);
            }
        }

        public void run() {
            try {
                // Create our consumer
                Map<String, Object> props = KafkaUtil.getKafkaConfigs(SslUnit.getDefaultSslConfig());
                props.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, sourceCluster.getBootstrapServer());
                props.put(ConsumerConfig.GROUP_ID_CONFIG, "distributor-unit-" + UUID.randomUUID().toString());
                props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
                props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
                props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
                try (final Consumer<byte[], byte[]> consumer = new KafkaConsumer<>(props)) {
                    consumer.subscribe(Pattern.compile(topicRegex), new ConsumerRebalanceListener() {
                        @Override
                        public void onPartitionsRevoked(Collection<TopicPartition> collection) {
                            LOG.info("Partitions revoked on Cluster {}:", sourceCluster.getName());
                            for (TopicPartition partition : collection) {
                                LOG.info("  {} / {} ", partition.topic(), partition.partition());
                            }
                        }

                        @Override
                        public void onPartitionsAssigned(Collection<TopicPartition> collection) {
                            LOG.info("Partitions assigned on Cluster {}:", sourceCluster.getName());
                            for (TopicPartition partition : collection) {
                                LOG.info("  {} / {} ", partition.topic(), partition.partition());
                            }
                        }
                    });
                    while (!stop) {
                        if (!paused) {
                            pollOnce(consumer);
                        } else {
                            SleepUtil.sleepInterruptibly(Duration.ofMillis(100), () -> stop);
                        }
                    }
                }
            } catch (InterruptException e) {
                LOG.info("Distribution interrupted");
            } catch (Exception e) {
                throw new ClientException("Distribution task exited with exception", e);
            }
        }

        private void pollOnce(Consumer<byte[], byte[]> consumer) {
            // Poll from our source cluster
            ConsumerRecords<byte[], byte[]> records = consumer.poll(Duration.ofMillis(100));

            //Iterate through all received records
            for (ConsumerRecord<byte[], byte[]> consumerRecord : records) {
                // If the record does not contain our copy flag header
                if (consumerRecord.headers() != null) {
                    if (consumerRecord.headers().lastHeader(COPY_FLAG_HEADER) == null) {
                        distributeMessage(consumerRecord);
                    } else {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Skipping message from {} (partition {}, offset {}) on {}",
                                    consumerRecord.topic(), consumerRecord.partition(), consumerRecord.offset(), sourceCluster.getName());
                        }
                    }
                }
            }
        }

        private void distributeMessage(ConsumerRecord<byte[], byte[]> consumerRecord) {
            //Unresolve the topic name into a context and name
            Map<String, Object> topicContext = new HashMap<>(sourceResolver.unresolveContext(consumerRecord.topic()));
            String unresolvedTopic = sourceResolver.unresolveTopic(consumerRecord.topic());

            // Then iterate through all clusters
            for (ClusterUnit targetCluster : targetClusters) {
                // Construct a copy producer record
                consumerRecord.headers().add(new RecordHeader(COPY_FLAG_HEADER, new byte[]{}));

                // Set up the target resolver with the right context
                TopicResolver targetResolver = targetResolvers.get(targetCluster);
                topicContext.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, targetCluster.getTopicPattern());
                targetResolver.configure(topicContext);
                ProducerRecord<byte[], byte[]> copiedRecord = new ProducerRecord<>(
                        targetResolver.resolveTopic(unresolvedTopic),
                        consumerRecord.partition(),
                        consumerRecord.timestamp(),
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.headers());

                // And produce a copied record to the target cluster
                producers.get(targetCluster).send(copiedRecord);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Copied message from {} (partition {}, offset {}) on {} to {}",
                            unresolvedTopic, consumerRecord.partition(), consumerRecord.offset(), sourceCluster.getName(), targetCluster.getName());
                }
            }
        }
    }
}
