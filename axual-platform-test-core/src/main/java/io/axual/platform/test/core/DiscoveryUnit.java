package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.ClientConfig;
import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.tools.DiscoveryConfigParserV2;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@InterfaceStability.Evolving
public class DiscoveryUnit {
    public static final String QUERY_NAME_APP_ID = "applicationId";
    public static final String QUERY_NAME_ENVIRONMENT = "env";
    public static final String QUERY_NAME_ENV_SUFFIX = "envSuffix";
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryUnit.class);
    private static final String DEFAULT_ADVERTISED = "localhost";
    private static final String DEFAULT_BIND_ALL = "0.0.0.0";
    private static final String RESPONSE_TEMPLATE_V2 = "{" +
            "\"tenant\": \"%s\"," +
            "\"environment\": \"%s\"," +
            "\"system\": \"%s\"," +
            "\"instance\": \"%s\"," +
            "\"cluster\": \"%s\"," +
            "\"bootstrap.servers\": \"%s\"," +
            "\"schema.registry.url\": \"%s\"," +
            "\"ttl\": \"%d\"," +
            "\"distributor.timeout\": \"%d\"," +
            "\"distributor.distance\": \"%d\"," +
            "\"enable.value.headers\": \"%s\"," +
            "\"group.id.resolver\": \"io.axual.common.resolver.GroupPatternResolver\"," +
            "\"group.id.pattern\": \"%s\"," +
            "\"topic.resolver\": \"io.axual.common.resolver.TopicPatternResolver\"," +
            "\"topic.pattern\": \"%s\"," +
            "\"transactional.id.resolver\": \"io.axual.common.resolver.TransactionalIdPatternResolver\"," +
            "\"transactional.id.pattern\": \"%s\"," +
            "\"acl.principal.builder\": \"%s\"" +
            "}";
    private static final String RESPONSE_TEMPLATE_V1 = "{" +
            "\"bootstrap.servers\": \"%s\"," +
            "\"schema.registry.url\": \"%s\"," +
            "\"tenant\": \"%s\"," +
            "\"environment\": \"%s\"," +
            "\"ttl\": \"%d\"" +
            "}";
    private final String tenant;
    private final String instanceName;
    private final String system;
    private final String bindAddress;
    private final String advertisedAddress;
    private final List<ClusterUnit> clusters = new ArrayList<>();
    private final Map<ClusterUnit, SchemaRegistryUnit> schemaRegistries = new HashMap<>();
    private final WireMockServer server;
    private ReentrantLock lock = new ReentrantLock();
    private long ttl = 5000L;
    private long distributorTimeout = 10000L;
    private long distributorDistance = 1L;
    private Map<DiscoveryClientId, ClusterUnit> applications = new HashMap<>();
    private Map<DiscoveryClientId, ClusterUnit> distributors = new HashMap<>();

    private DiscoveryUnit(Builder builder) {
        tenant = builder.tenant != null ? builder.tenant : "axual";
        instanceName = builder.instanceName != null ? builder.instanceName : "unit";
        system = builder.system != null ? builder.system : "unit";
        bindAddress = builder.bindAddress != null ? builder.bindAddress : DEFAULT_BIND_ALL;
        advertisedAddress = builder.advertisedAddress != null ? builder.advertisedAddress : DEFAULT_ADVERTISED;
        clusters.addAll(builder.clusters);
        schemaRegistries.putAll(builder.schemaRegistries);
        server = new WireMockServer(wireMockConfig().bindAddress(bindAddress).port(builder.port).disableRequestJournal());
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        lock.lock();
        try {
            this.ttl = ttl;
            refreshConfiguration();
        } finally {
            lock.unlock();
        }
    }

    public long getDistributorTimeout() {
        return distributorTimeout;
    }

    public void setDistributorTimeout(long distributorTimeout) {
        lock.lock();
        try {
            this.distributorTimeout = distributorTimeout;
            refreshConfiguration();
        } finally {
            lock.unlock();
        }
    }

    public long getDistributorDistance() {
        return distributorDistance;
    }

    public void setDistributorDistance(long distributorDistance) {
        lock.lock();
        try {
            this.distributorDistance = distributorDistance;
            refreshConfiguration();
        } finally {
            lock.unlock();
        }
    }

    public String getUrl() {
        return "http://" + bindAddress + ":" + getPort();
    }

    public int getPort() {
        return server.port();
    }

    public void directApplicationTo(final ClientConfig config, final ClusterUnit cluster) {
        updateDiscovery(applications, DiscoveryConfigParserV2.getDiscoveryConfig(config), cluster);
    }

    public void directDistributorTo(final ClusterUnit source, final int level, final ClusterUnit target) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("cluster", source.getName());
        parameters.put("level", "" + level);
        DiscoveryConfig config = DiscoveryConfig.newBuilder()
                .setApplicationId("io.axual.distributor-" + source.getName() + "-" + level)
                .setParameters(parameters)
                .build();
        updateDiscovery(distributors, config, target);
    }

    private void updateDiscovery(final Map<DiscoveryClientId, ClusterUnit> map, final DiscoveryConfig config, final ClusterUnit cluster) {
        lock.lock();
        try {
            map.put(new DiscoveryClientId(config), cluster);
            refreshConfiguration();
        } finally {
            lock.unlock();
        }
    }

    public ClusterUnit getClusterFor(final DiscoveryConfig discoveryConfig) {
        lock.lock();
        try {
            return applications.get(new DiscoveryClientId(discoveryConfig));
        } finally {
            lock.unlock();
        }
    }

    private void refreshConfiguration() {
        refreshApplicationDiscoveriesForLastCluster(null);
        for (ClusterUnit cluster : clusters) {
            refreshApplicationDiscoveriesForLastCluster(cluster.getName());
        }

        refreshDistributorDiscoveriesForLastCluster(null);
        for (ClusterUnit cluster : clusters) {
            refreshDistributorDiscoveriesForLastCluster(cluster.getName());
        }
    }

    private void refreshApplicationDiscoveriesForLastCluster(String lastCluster) {
        for (Map.Entry<DiscoveryClientId, ClusterUnit> entry : applications.entrySet()) {
            final String environment = entry.getKey().parameters.get(QUERY_NAME_ENVIRONMENT);
            final String appId = entry.getKey().applicationId;
            final String v1_ext = "^\\/?(v1)?\\/?";
            final String v2_ext = "^\\/?v2\\/?";

            if (environment == null || environment.trim().isEmpty()) {
                LOG.error("Null or empty environment: {}", entry.getKey());
            }

            String bodyV1 = String.format(RESPONSE_TEMPLATE_V1,
                    entry.getValue().getBootstrapServer(),
                    schemaRegistries.get(entry.getValue()).getBaseURL(),
                    tenant,
                    environment,
                    ttl
            );

            server.stubFor(get(urlPathMatching(v1_ext)).atPriority(5)
                    .withQueryParam(QUERY_NAME_APP_ID, matching(entry.getKey().applicationId))
                    .willReturn(aResponse().withStatus(200).withBody(bodyV1)));

            LOG.info("Stubbing DiscoveryApi V1 request http://{}:{} for application id {} with environment {} to {}", advertisedAddress, server.port(), appId, environment, bodyV1);

            if (environment != null
                    && environment.startsWith(instanceName)
                    && !environment.equals(instanceName)) {
                String envSuffix = StringUtils.removeStart(environment, instanceName);
                server.stubFor(get(urlPathMatching(v1_ext)).atPriority(1)
                        .withQueryParam(QUERY_NAME_APP_ID, matching(entry.getKey().applicationId))
                        .withQueryParam(QUERY_NAME_ENV_SUFFIX, matching(envSuffix))
                        .willReturn(aResponse().withStatus(200).withBody(bodyV1)));
                LOG.info("Stubbing DiscoveryApi V1 request http://{}:{} for application id {} with envSuffix {} to {}", advertisedAddress, server.port(), appId, envSuffix, bodyV1);
            }

            String bodyV2 = String.format(RESPONSE_TEMPLATE_V2,
                    tenant,
                    environment,
                    system,
                    instanceName,
                    entry.getValue().getName(),
                    entry.getValue().getBootstrapServer(),
                    schemaRegistries.get(entry.getValue()).getBaseURL(),
                    ttl,
                    distributorTimeout,
                    distributorDistance,
                    entry.getValue().valueHeadersEnabled(),
                    entry.getValue().getGroupPattern(),
                    entry.getValue().getTopicPattern(),
                    entry.getValue().getTransactionalIdPattern(),
                    entry.getValue().getAclPrincipalBuilder());

            server.stubFor(get(urlPathMatching(v2_ext))
                    .atPriority(5)
                    .withQueryParam(QUERY_NAME_APP_ID, matching(appId))
                    .willReturn(aResponse().withStatus(200).withBody(bodyV2)));

            server.stubFor(get(urlPathMatching(v2_ext))
                    .atPriority(1)
                    .withQueryParam(QUERY_NAME_APP_ID, matching(appId))
                    .withQueryParam(QUERY_NAME_ENVIRONMENT, matching(environment))
                    .willReturn(aResponse().withStatus(200).withBody(bodyV2)));
            LOG.info("Stubbing DiscoveryApi V2 request http://{}:{}/v2 for application id {} and environment to {}", advertisedAddress, server.port(), appId, environment);

        }
    }

    private void refreshDistributorDiscoveriesForLastCluster(String lastCluster) {
        final String distributorPathRegex = "^\\/?distributor\\/?";
        for (Map.Entry<DiscoveryClientId, ClusterUnit> entry : distributors.entrySet()) {
            final String appId = entry.getKey().applicationId;

            MappingBuilder mappingBuilder = get(urlPathMatching(distributorPathRegex));
            mappingBuilder.withQueryParam(QUERY_NAME_APP_ID, matching(appId));
            if (lastCluster != null) {
                mappingBuilder.withQueryParam("lastCluster", matching(lastCluster));
            }
            for (Map.Entry<String, String> parameter : entry.getKey().parameters.entrySet()) {
                mappingBuilder.withQueryParam(parameter.getKey(), matching(parameter.getValue()));
            }

            String body = String.format(RESPONSE_TEMPLATE_V2,
                    tenant,
                    entry.getKey().parameters.get(QUERY_NAME_ENVIRONMENT),
                    system,
                    instanceName,
                    entry.getValue().getName(),
                    entry.getValue().getBootstrapServer(),
                    schemaRegistries.get(entry.getValue()).getBaseURL(),
                    ttl,
                    distributorTimeout,
                    distributorDistance,
                    entry.getValue().valueHeadersEnabled(),
                    entry.getValue().getGroupPattern(),
                    entry.getValue().getTopicPattern(),
                    entry.getValue().getTransactionalIdPattern(),
                    entry.getValue().getAclPrincipalBuilder());

            LOG.info("Stubbing DiscoveryApi request to http://{}:{}/distributor for applicationId {}  to {}", advertisedAddress, server.port(), appId, body);

            server.stubFor(mappingBuilder
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(body)));
        }
    }

    void start() {
        server.start();
    }

    void stop() {
        server.stop();
    }

    public boolean isRunning() {
        return server.isRunning();
    }

    private static class DiscoveryClientId {
        private final String applicationId;
        private final String applicationVersion;
        private final Map<String, String> parameters = new HashMap<>();

        private DiscoveryClientId(DiscoveryConfig config) {
            applicationId = config.getApplicationId();
            applicationVersion = config.getApplicationVersion();
            parameters.putAll(config.getParameters());
        }

        @Override
        public int hashCode() {
            int result = applicationId != null ? applicationId.hashCode() : 0;
            result = 31 * result + (applicationVersion != null ? applicationVersion.hashCode() : 0);
            result = 31 * result + parameters.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            DiscoveryClientId other = (DiscoveryClientId) o;

            if (!Objects.equals(applicationId, other.applicationId)) {
                return false;
            }
            if (!Objects.equals(applicationVersion, other.applicationVersion)) {
                return false;
            }
            if (!Objects.equals(parameters, other.parameters)) {
                return false;
            }

            return true;
        }
    }

    public static class Builder {
        private String tenant;
        private String instanceName;
        private String system;
        private String bindAddress = DEFAULT_BIND_ALL;
        private String advertisedAddress = DEFAULT_ADVERTISED;
        private int port = 0;

        private List<ClusterUnit> clusters;
        private Map<ClusterUnit, SchemaRegistryUnit> schemaRegistries;

        public String getTenant() {
            return tenant;
        }

        public Builder setTenant(String tenant) {
            this.tenant = tenant;
            return this;
        }

        public String getInstanceName() {
            return instanceName;
        }

        public Builder setInstanceName(String instanceName) {
            this.instanceName = instanceName;
            return this;
        }

        public String getSystem() {
            return system;
        }

        public Builder setSystem(String system) {
            this.system = system;
            return this;
        }

        public String getBindAddress() {
            return bindAddress;
        }

        public Builder setBindAddress(String bindAddress) {
            this.bindAddress = bindAddress;
            return this;
        }

        public String getAdvertisedAddress() {
            return advertisedAddress;
        }

        public Builder setAdvertisedAddress(String advertisedAddress) {
            this.bindAddress = advertisedAddress;
            return this;
        }

        public int getPort() {
            return port;
        }

        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public List<ClusterUnit> getClusters() {
            return clusters;
        }

        public Builder setClusters(List<ClusterUnit> clusters) {
            this.clusters = clusters;
            return this;
        }

        public Map<ClusterUnit, SchemaRegistryUnit> getSchemaRegistries() {
            return schemaRegistries;
        }

        public Builder setSchemaRegistries(Map<ClusterUnit, SchemaRegistryUnit> schemaRegistries) {
            this.schemaRegistries = schemaRegistries;
            return this;
        }

        public DiscoveryUnit build() {
            return new DiscoveryUnit(this);
        }

    }
}
