package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.Json;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;

import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.common.annotation.InterfaceStability;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@InterfaceStability.Evolving
public class SchemaRegistryUnit {

    private static final Logger LOG = LoggerFactory.getLogger(SchemaRegistryUnit.class);
    private static final String SCHEMAS_ENDPOINT_REGEX = "^\\/schemas\\/ids\\/";

    private static final String LOCALHOST = "localhost";
    private static final String BIND_ALL = "0.0.0.0";
    private static final String POST_SUBJECT_VERSIONS_RESPONSE_TEMPLATE = "{\"id\":%d}";
    private static final String GET_SUBJECT_VERSIONS_RESPONSE_TEMPLATE = "[ 1 ]";
    private static final String GET_SCHEMA_BY_ID_RESPONSE_TEMPLATE = "{\"schema\": \"%s\"}";
    private static final String GET_SPECIFIC_SUBJECT_VERSION_RESPONSE_TEMPLATE = "{\"subject\":\"%s\",\"version\":1,\"id\":%d,\"schema\": \"%s\"}";

    private final WireMockServer server;
    private final String bindAddress;
    private final String advertisedAddress;
    private final int port;

    // Subject and schema store
    private final AtomicInteger idCounter = new AtomicInteger(0);
    private final Map<String, Integer> subjectToSchemaIdMap = new HashMap<>();
    private final Map<String, Integer> schemaIdMap = new HashMap<>();

    public SchemaRegistryUnit() {
        this(null, null, 0);
    }

    public SchemaRegistryUnit(String bindAddress, String advertisedAddress, int port) {
        this.bindAddress = bindAddress != null ? bindAddress : BIND_ALL;
        this.advertisedAddress = advertisedAddress != null ? advertisedAddress : LOCALHOST;
        this.port = port;
        this.server = new WireMockServer(
                wireMockConfig().bindAddress(this.bindAddress).port(this.port).disableRequestJournal());
    }

    int getPort() {
        return server.port();
    }

    void registerSchema(final String topic, final Schema keySchema, final Schema valueSchema) {
        if (keySchema != null) {
            registerSubject(topic + "-key", keySchema);
        }
        if (valueSchema != null) {
            registerSubject(topic + "-value", valueSchema);
        }
    }

    private String makeJsonFromCollection(Collection<? extends Object> list) {
        StringBuilder subjectResponseBuilder = new StringBuilder();
        subjectResponseBuilder.append(String.format("[%n"));
        Iterator<?> iterator = list.iterator();
        while (iterator.hasNext()) {
            String next = String.format("\"%s\"", iterator.next());
            if (iterator.hasNext()) {
                subjectResponseBuilder.append(String.format("%s,%n", next));
            } else {
                subjectResponseBuilder.append(String.format("%s%n", next));
            }
        }
        subjectResponseBuilder.append(String.format("]%n"));
        return subjectResponseBuilder.toString();
    }

    private synchronized void registerSubject(final String subject, final Schema schema) {
        if (schema != null) {
            if (subjectToSchemaIdMap.containsKey(subject)) {
                LOG.warn("Subject '{}' already exists, skipping registration", subject);
                return;
            }

            final String normalizedSchemaString = getNormalizedSchemaString(schema);
            final int schemaId = schemaIdMap.computeIfAbsent(normalizedSchemaString, s -> idCounter.getAndIncrement());

            subjectToSchemaIdMap.put(subject, schemaId);

            final String SUBJECTS_ENDPOINT_REGEX = "^\\/subjects\\/";

            final int STATUS_CODE_NOT_FOUND = 404;
            final int ERROR_CODE_SUBJECT_NOT_FOUND = 40401;
            final String ERROR_MESSAGE_SUBJECT_NOT_FOUND = "Subject not found";

            final int ERROR_CODE_SCHEMA_NOT_FOUND = 40403;
            final String ERROR_MESSAGE_SCHEMA_NOT_FOUND = "Schema not found";

            final int STATUS_CODE_INTERNAL_ERROR = 500;
            final int ERROR_CODE_BACKEND_STORE_FAIL = 50001;
            final String ERROR_MESSAGE_BACKEND_STORE_FAIL = "Error in the backend datastore";

            // Schemas endpoint https://docs.confluent.io/5.2.2/schema-registry/develop/api.html#schemas

            // Fail all gets for unknown schema ids
            // GET /schemas/ids/{int: id}
            server.stubFor(get(urlMatching(SCHEMAS_ENDPOINT_REGEX + ".*")).atPriority(5)
                    .willReturn(errorResponse(STATUS_CODE_NOT_FOUND, ERROR_CODE_SCHEMA_NOT_FOUND, ERROR_MESSAGE_SCHEMA_NOT_FOUND)));


            // GET /schemas/ids/{int: id}
            // Used by Consumers to fetch their deserialize Schema
            server.stubFor(get(urlMatching(SCHEMAS_ENDPOINT_REGEX + schemaId + "\\/?.*")).atPriority(1)
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(String
                                    .format(GET_SCHEMA_BY_ID_RESPONSE_TEMPLATE, jsonEscape(normalizedSchemaString)))));

            // Helper, not in API, all IDs
            // GET /schemas/ids
            server.stubFor(get(urlMatching(SCHEMAS_ENDPOINT_REGEX + "?"))
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(makeJsonFromCollection(schemaIdMap.values()))));

            // Subjects endpoint https://docs.confluent.io/5.2.2/schema-registry/develop/api.html#subjects

            final String SUBJECT_ENDPOINT = SUBJECTS_ENDPOINT_REGEX + subject + "\\/";
            final String SUBJECT_VERSION_ENDPOINT = SUBJECT_ENDPOINT + "versions\\/";

            // Fail all gets for unknown subjects
            // GET /subjects
            server.stubFor(get(urlMatching(SUBJECTS_ENDPOINT_REGEX + ".*")).atPriority(5)
                    .willReturn(errorResponse(STATUS_CODE_NOT_FOUND, ERROR_CODE_SUBJECT_NOT_FOUND, ERROR_MESSAGE_SUBJECT_NOT_FOUND)));

            // Get all the registered subjects
            // GET /subjects
            server.stubFor(get(urlMatching(SUBJECTS_ENDPOINT_REGEX + "?")).atPriority(1)
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(makeJsonFromCollection(subjectToSchemaIdMap.keySet()))));

            // Get all versions for a registered subject
            // GET /subjects/(string: subject)/versions
            server.stubFor(get(urlMatching(SUBJECT_VERSION_ENDPOINT + "?")).atPriority(1)
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(GET_SUBJECT_VERSIONS_RESPONSE_TEMPLATE)));

            // DELETE /subjects/(string: subject) Not Implemented, not supported. Returns SR error
            server.stubFor(delete(urlMatching(SUBJECT_ENDPOINT + "?"))
                    .willReturn(errorResponse(STATUS_CODE_INTERNAL_ERROR, ERROR_CODE_BACKEND_STORE_FAIL, ERROR_MESSAGE_BACKEND_STORE_FAIL)));

            // Get a specific version of a registered subject, or the latest
            // GET /subjects/(string: subject)/versions/(versionId: version)

            // Used by Consumers to fetch their deserialize Schema
            server.stubFor(get(urlMatching(SUBJECT_VERSION_ENDPOINT + "(1|latest)\\/?.*")).atPriority(1)
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(String.format(GET_SPECIFIC_SUBJECT_VERSION_RESPONSE_TEMPLATE, subject, schemaId,
                                    jsonEscape(normalizedSchemaString)))));


            // get the Schema for a specific version of a subject
            // GET /subjects/(string: subject)/versions/(versionId: version)/schema
            server.stubFor(get(urlMatching(SUBJECT_VERSION_ENDPOINT + "(1|latest)\\/schema")).atPriority(1)
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(normalizedSchemaString)));

            // Register a new schema for the subject
            // POST /subjects/(string: subject)/versions
            server.stubFor(post(urlMatching(SUBJECT_VERSION_ENDPOINT + "?.*")).atPriority(1)
                    .withRequestBody(
                            new EqualToSchemaPattern(normalizedSchemaString))
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(String.format(POST_SUBJECT_VERSIONS_RESPONSE_TEMPLATE, schemaId))));

            // Used to check if schema exists for subject
            // POST /subjects/(string: subject)
            server.stubFor(post(urlMatching(SUBJECT_ENDPOINT + "?.*")).atPriority(2)
                    .withRequestBody(
                            new EqualToSchemaPattern(normalizedSchemaString))
                    .willReturn(aResponse()
                            .withStatus(200)
                            .withBody(foundSchema(subject, 1, schemaId, normalizedSchemaString))));

            // Failure for all unknown post registrations to unknown subjects endpoint
            server.stubFor(post(urlMatching("^\\/subjects\\/.*")).atPriority(5)
                    .willReturn(errorResponse(STATUS_CODE_INTERNAL_ERROR, ERROR_CODE_BACKEND_STORE_FAIL, ERROR_MESSAGE_BACKEND_STORE_FAIL)));

            // Delete a specific version for a subject
            // DELETE /subjects/(string: subject)/versions/(versionId: version) Not Implemented, not supported. Returns SR error
            server.stubFor(delete(urlMatching(SUBJECT_VERSION_ENDPOINT + "?"))
                    .willReturn(errorResponse(STATUS_CODE_INTERNAL_ERROR, ERROR_CODE_BACKEND_STORE_FAIL, ERROR_MESSAGE_BACKEND_STORE_FAIL)));

        }
    }

    private ResponseDefinitionBuilder errorResponse(int statusCode, int errorCode, String errorMessage) {
        return aResponse().withStatus(statusCode).withBody(errorString(errorCode, errorMessage));
    }

    private String foundSchema(String subject, int version, int id, String schema) {
        JsonNodeFactory factory = JsonNodeFactory.instance;
        ObjectNode found = factory.objectNode();
        found.set("subject", factory.textNode(subject));
        found.set("id", factory.numberNode(id));
        found.set("version", factory.numberNode(version));
        found.set("schema", factory.textNode(schema));

        return Json.write(found);
    }

    private String errorString(int code, String message) {
        JsonNodeFactory factory = JsonNodeFactory.instance;
        ObjectNode error = factory.objectNode();
        error.set("error_code", factory.numberNode(code));
        error.set("message", factory.textNode(message));
        return Json.write(error);
    }

    public String getAdvertisedAddress() {
        return advertisedAddress;
    }

    public String getBaseURL() {
        return String.format("http://%s:%d", this.advertisedAddress, this.server.port());
    }

    void start() {
        server.start();
    }

    void stop() {
        server.stop();
    }

    public boolean isRunning() {
        return server.isRunning();
    }

    private String jsonEscape(final String schema) {
        return schema.replace("\"", "\\\"");
    }

    String getNormalizedSchemaString(Schema schema) {
        return SchemaNormalizer.normalizeSchema(schema).toString(false);
    }

    Integer getSchemaId(Schema schema) {
        return schemaIdMap.get(getNormalizedSchemaString(schema));
    }

    private static class EqualToSchemaPattern extends StringValuePattern {
        private static final String FIELDNAME_SCHEMA = "schema";
        private final String expectedSchemaString;

        private EqualToSchemaPattern(@JsonProperty("equalToSchema") String expectedSchemaString) {
            super(createExpected(expectedSchemaString));
            this.expectedSchemaString = expectedSchemaString;
        }

        private static String createExpected(String expectedSchemaString) {
            Objects.requireNonNull(expectedSchemaString);
            JsonNode expectedNode = JsonNodeFactory.instance.objectNode().set(FIELDNAME_SCHEMA, JsonNodeFactory.instance.textNode(expectedSchemaString));
            return Json.write(expectedNode);
        }

        @Override
        public MatchResult match(String value) {
            if (value == null || value.isEmpty()) {
                return MatchResult.noMatch();
            }

            final JsonNode rawRootNode = Json.read(value, JsonNode.class);
            if (!(rawRootNode instanceof ObjectNode)) {
                return MatchResult.noMatch();
            }

            ObjectNode rootNode = (ObjectNode) rawRootNode;
            if (!(rootNode.has(FIELDNAME_SCHEMA))) {
                return MatchResult.noMatch();
            }
            JsonNode rawSchemaNode = rootNode.get(FIELDNAME_SCHEMA);
            try {
                final Schema requestedSchema = SchemaNormalizer.normalizeSchema(new Schema.Parser().parse(rawSchemaNode.textValue()));
                return MatchResult.of(expectedSchemaString.equals(requestedSchema.toString(false)));
            } catch (Exception e) {
                LOG.error("An exception occurred while matching schema request", e);
            }

            return MatchResult.noMatch();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof EqualToSchemaPattern)) return false;
            if (!super.equals(o)) return false;
            EqualToSchemaPattern that = (EqualToSchemaPattern) o;
            return expectedSchemaString.equals(that.expectedSchemaString);
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), expectedSchemaString);
        }
    }

}
