package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.ClientException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.curator.test.InstanceSpec;
import org.apache.curator.test.TestingServer;

@InterfaceStability.Stable
public class ZookeeperUnit {

  private int port;
  private TestingServer server;
  private AtomicBoolean isRunning = new AtomicBoolean(false);

  public ZookeeperUnit(int port) {
    this.port = port;
  }

  public void startup() {
    InstanceSpec spec = new InstanceSpec(null, port, -1, -1, true, -1, 2000, 10);
    System.setProperty("zookeeper.serverCnxnFactory",
        "org.apache.zookeeper.server.NettyServerCnxnFactory");
    try {
      server = new TestingServer(spec, true);
      isRunning.set(true);
    } catch (Exception e) {
      throw new ClientException("Unable to start ZooKeeper", e);
    }
  }

  public void shutdown() {
    try {
      if (server != null) {
        server.stop();
        isRunning.set(false);
        server.close();
        server = null;
      }
    } catch (Exception e) {
      throw new ClientException("Unable to stop ZooKeeper", e);
    }
  }

  public boolean isRunning(){
    return isRunning.get();
  }
}
