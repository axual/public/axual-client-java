package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.SslConfig;
import io.axual.discovery.client.DiscoveryClientRegistry;
import io.axual.discovery.client.tools.DiscoveryConfigParserV2;

@InterfaceStability.Evolving
public class InstanceUnit {
    public static final String DEFAULT_ENVIRONMENT = "unittest";

    private static final Logger LOG = LoggerFactory.getLogger(InstanceUnit.class);
    private static final int DEFAULT_PARTITIONS = 12;

    private boolean started = false;
    private final String tenant;
    private final String name;
    private final String system;

    private final int discoveryPort;
    private final String discoveryBindAddress;
    private final String discoveryAdvertisedAddress;
    private final boolean enableDistribution;
    private final SslConfig sslConfig;
    private final List<ClusterUnit> clusters = new ArrayList<>();
    private final Map<ClusterUnit, SchemaRegistryUnit> schemaRegistries = new HashMap<>();
    private DiscoveryUnit discoveryUnit;
    private DistributionUnit distributorUnit;
    private final Map<String, StreamConfig> streams = new HashMap<>();
    private final Map<String, StreamConfig> rawTopics = new HashMap<>();
    private final List<ClientConfig> applications = new ArrayList<>();


    public InstanceUnit(InstanceUnitConfig config, Map<String, ClusterUnit> clustersByName) {
        this.tenant = config.getTenant();
        this.name = config.getName();
        this.system = config.getSystem() != null ? config.getSystem() : "unit";
        this.discoveryBindAddress = config.getDiscoveryBindAddress() != null ? config.getDiscoveryBindAddress() : "0.0.0.0";
        this.discoveryAdvertisedAddress = config.getDiscoveryAdvertisedAddress() != null ? config.getDiscoveryAdvertisedAddress() : "localhost";
        this.discoveryPort = config.getDiscoveryPort();
        enableDistribution = config.getEnableDistribution();
        this.sslConfig = config.getSslConfig() != null ? config.getSslConfig() : SslUnit.getDefaultSslConfig();

        for (String clusterName : config.getClusters()) {
            clusters.add(clustersByName.get(clusterName));
        }
    }

    public String getTenant() {
        return tenant;
    }

    public String getName() {
        return name;
    }

    public Map<String, StreamConfig> getStreams() {
        return streams;
    }

    public boolean addStream(StreamConfig streamConfig) {
        String streamName = streamConfig.getEnvironment() + "-" + streamConfig.getName();
        if (streams.containsKey(streamName)) {
            return false;
        }
        streams.put(streamName, streamConfig);
        if (started) {
            deployStream(streamConfig);
        }
        return true;
    }

    public Map<String, StreamConfig> getRawTopics() {
        return rawTopics;
    }

    public boolean addRawTopic(StreamConfig topicConfig) {
        if (rawTopics.containsKey(topicConfig.getName())) {
            return false;
        }
        rawTopics.put(topicConfig.getName(), topicConfig);
        if (started) {
            deployRawTopic(topicConfig);
        }
        return true;
    }

    public void start() {
        LOG.info("Starting instance {}", name);
        for (ClusterUnit cluster : clusters) {
            schemaRegistries.put(cluster, cluster.getSchemaRegistryUnit());
        }

        discoveryUnit = DiscoveryUnit.builder()
                .setTenant(tenant)
                .setInstanceName(this.name)
                .setSystem(system)
                .setAdvertisedAddress(this.discoveryAdvertisedAddress)
                .setBindAddress(this.discoveryBindAddress)
                .setPort(this.discoveryPort)
                .setClusters(clusters)
                .setSchemaRegistries(schemaRegistries)
                .build();

        for (ClusterUnit cluster : clusters) {
            schemaRegistries.get(cluster).start();
        }

        discoveryUnit.start();

        for (StreamConfig stream : streams.values()) {
            deployStream(stream);
        }

        for (ClientConfig app : applications) {
            directApplicationTo(app, clusters.get(0));
        }

        if (enableDistribution) {
            Map<String, Object> context = new HashMap<>();
            context.put(CommonConfig.TENANT, tenant);
            context.put(CommonConfig.INSTANCE, name);
            distributorUnit = new DistributionUnit(clusters, context);
            distributorUnit.start();
        }

        started = true;
    }

    public void stop() {
        LOG.info("Stopping instance {}", name);
        started = false;

        if (distributorUnit != null) {
            distributorUnit.stop();
        }

        discoveryUnit.stop();

        for (ClusterUnit cluster : clusters) {
            schemaRegistries.get(cluster).stop();
        }
    }

    public void pauseDistribution() {
        if (distributorUnit != null) {
            distributorUnit.pause();
        }
    }

    public void resumeDistribution() {
        if (distributorUnit != null) {
            distributorUnit.resume();
        }
    }

    public DiscoveryUnit getDiscoveryUnit() {
        return discoveryUnit;
    }

    public int getDiscoveryPort() {
        return discoveryUnit.getPort();
    }

    /**
     * Point an application to a specific cluster.
     *
     * This method should only be called *after* the Axual Test Platform Unit is started.
     * @param clientConfig the configuration used by the client to be redirected
     * @param cluster the cluster to assign the application to
     */
    public void directApplicationTo(ClientConfig clientConfig, ClusterUnit cluster) {
        directApplicationTo(clientConfig, cluster, false);
    }

    /**
     * Point an application to a specific cluster.
     *
     * This method should only be called *after* the Axual Test Platform Unit is started.
     * @param clientConfig the configuration used by the client to be redirected
     * @param cluster the cluster to assign the application to
     * @param immediate load the new configuration immediately
     */
    public void directApplicationTo(ClientConfig clientConfig, ClusterUnit cluster, boolean immediate) {
        if (!applications.contains(clientConfig)) {
            applications.add(clientConfig);
        }
        discoveryUnit.directApplicationTo(clientConfig, cluster != null ? cluster : clusters.get(0));
        if (immediate) {
            DiscoveryClientRegistry.invalidate(DiscoveryConfigParserV2.getDiscoveryConfig(clientConfig));
        }
    }

    public void directDistributorTo(ClusterUnit source, int level, ClusterUnit target) {
        discoveryUnit.directDistributorTo(source, level, target != null ? target : clusters.get(0));
    }

    /**
     * Get the cluster assignment for the given client configuration
     * @param clientConfig the configuration of the client
     * @return the Cluster to which the client is assigned
     */
    public ClusterUnit getClusterFor(ClientConfig clientConfig) {
        return discoveryUnit.getClusterFor(DiscoveryConfigParserV2.getDiscoveryConfig(clientConfig));
    }

    /**
     * Deploy a so called "resolved" topic, or strea,. This means the topic name will be resolved using the topic resolving pattern set for the cluster.
     * @param stream The stream configuration to apply.
     */
    public void deployStream(final StreamConfig stream) {
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, tenant);
        context.put(CommonConfig.INSTANCE, name);
        context.put(CommonConfig.ENVIRONMENT, stream.getEnvironment() != null ? stream.getEnvironment() : DEFAULT_ENVIRONMENT);
        for (ClusterUnit cluster : clusters) {
            // Register the topic on the Kafka cluster
            final int partitions = stream.getPartitions() != null ? stream.getPartitions() : DEFAULT_PARTITIONS;
            cluster.registerStream(stream.getName(), partitions, context);
            // Register the topic in the schema registry attached to the Kafka cluster
            schemaRegistries.get(cluster).registerSchema(cluster.resolveTopic(stream.getName(), context), stream.getKeySchema(), stream.getValueSchema());
        }
    }

    /**
     * Deploy a so called "raw" topic. This means the topic name will not use the topic resolving pattern set for the cluster.
     * The literal name will be used as topic name
     * @param topic The stream configuration for the raw topic. The environment setting will be ignored
     */
    public void deployRawTopic(final StreamConfig topic) {
        for (ClusterUnit cluster : clusters) {
            // Register the topic on the Kafka cluster
            final int partitions = topic.getPartitions() != null ? topic.getPartitions() : DEFAULT_PARTITIONS;
            cluster.registerRawTopic(topic.getName(), partitions);
            // Register the topic in the schema registry attached to the Kafka cluster
            schemaRegistries.get(cluster).registerSchema(topic.getName(), topic.getKeySchema(), topic.getValueSchema());
        }
    }

    /**
     * Registers a new Client Configuration for the application id and assigns it to the first cluster.
     * The application id will have a random component added.
     * @param appId The application id to be used by the client
     * @return The ClientConfig to be used to create a client for the platform
     */
    public ClientConfig getClientConfig(final String appId) {
        return getClientConfig(appId, true, null, clusters.get(0));
    }

    /**
     * Registers a new Client Configuration for the application id and assigns it to the first cluster.
     * The application id will have a random component added.
     * @param appId The application id to be used by the client
     * @param initialClusterAssignment the cluster to assign the client to
     * @return The ClientConfig to be used to create a client for the platform
     */
    public ClientConfig getClientConfig(final String appId, ClusterUnit initialClusterAssignment) {
        return getClientConfig(appId, true, null, initialClusterAssignment);
    }

    /**
     * Registers a new Client Configuration for the application id and assigns it to the first cluster.
     * The application id will have a random component added.
     * @param appId The application id to be used by the client
     * @param randomizeAppId when set to true the application id will be appended with a random component
     * @return The ClientConfig to be used to create a client for the platform
     */
    public ClientConfig getClientConfig(final String appId, final boolean randomizeAppId) {
        return getClientConfig(appId, randomizeAppId, null, clusters.get(0));
    }

    /**
     * Registers a new Client Configuration for the application id and assigns it to the first cluster.
     * The application id will have a random component added.
     * @param appId The application id to be used by the client
     * @param randomizeAppId when set to true the application id will be appended with a random component
     * @param initialClusterAssignment the cluster to assign the client to
     * @return The ClientConfig to be used to create a client for the platform
     */
    public ClientConfig getClientConfig(final String appId, final boolean randomizeAppId, ClusterUnit initialClusterAssignment) {
        return getClientConfig(appId, randomizeAppId, null, initialClusterAssignment);
    }

    public ClientConfig getClientConfig(final String appId, final boolean randomizeAppId, String environment, ClusterUnit initialClusterAssignment) {
        return getClientConfig(appId, randomizeAppId, environment, initialClusterAssignment, null);
    }

    /**
     * Registers a new Client Configuration for the application id and assigns it to the first cluster.
     * The application id will have a random component added.
     * @param appId The application id to be used by the client
     * @param randomizeAppId when set to true the application id will be appended with a random component
     * @param environment the environment this client will use
     * @param initialClusterAssignment the cluster to assign the client to
     * @param keystoreType type of keystore client will use
     * @return The ClientConfig to be used to create a client for the platform
     */
    public ClientConfig getClientConfig(final String appId, final boolean randomizeAppId, String environment, ClusterUnit initialClusterAssignment, SslConfig.KeystoreType keystoreType) {
        SslConfig tempsSslConfig = sslConfig;

        if (keystoreType != null) {
            tempsSslConfig = SslConfig.KeystoreType.PKCS12.equals(keystoreType) ?
                    SslUnit.getDefaultSslPKCS12Config() :
                    SslUnit.getDefaultSslConfig();
        }

        ClientConfig result = ClientConfig.newBuilder()
                .setTenant(tenant)
                .setEnvironment(environment != null ? environment : DEFAULT_ENVIRONMENT)
                .setApplicationId(
                        randomizeAppId ? appId + "-" + UUID.randomUUID().toString().toLowerCase()
                                : appId)
                .setApplicationVersion("0.1-test")
                .setEndpoint(discoveryUnit.getUrl())
                .setSslConfig(tempsSslConfig)
                .build();
        directApplicationTo(result, initialClusterAssignment);
        return result;
    }

    /**
     * Get the list of registered applications
     * @return a list of ClientConfig objects which are registered with this test instance
     */
    public List<ClientConfig> getApplications() {
        return applications;
    }
}
