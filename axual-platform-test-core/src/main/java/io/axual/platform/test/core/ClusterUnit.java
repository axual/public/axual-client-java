package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.SslConfig;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.GroupResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.resolver.TransactionalIdPatternResolver;
import io.axual.common.resolver.TransactionalIdResolver;
import io.axual.common.tools.KafkaUtil;

import static java.lang.String.format;

@InterfaceStability.Evolving
public class ClusterUnit {
    private static final Logger LOG = LoggerFactory.getLogger(ClusterUnit.class);
    private static final String LOCALHOST = "localhost";
    private static final String BIND_ALL = "0.0.0.0";

    private static final String DEFAULT_GROUP_PATTERN = "{tenant}-{instance}-{environment}-{group}";
    private static final String DEFAULT_TOPIC_PATTERN = "{tenant}-{instance}-{environment}-{topic}";
    private static final String DEFAULT_TRANSACTIONAL_ID_PATTERN = "{tenant}-{environment}-{app.id}-{transactional.id}";
    private static final String DEFAULT_BIND_ADDRESS = BIND_ALL;
    private static final String DEFAULT_ADVERTISED_ADDRESS = LOCALHOST;
    private static final int DEFAULT_SCHEMA_REGISTRY_PORT = 0; // Force to resolve

    private static final String ACL_PRINCIPAL_BUILDER_ADVANCED = "io.axual.security.principal.AdvancedAclPrincipalBuilder";
    private static final String ACL_PRINCIPAL_BUILDER_BASIC = "io.axual.security.principal.BasicAclPrincipalBuilder";


    private final ClusterUnitConfig config;
    private final String name;
    private final String bindAddress;
    private final String advertisedAddress;
    private final SslConfig sslConfig;
    private final String groupPattern;
    private final GroupResolver groupResolver = new GroupPatternResolver();
    private final String topicPattern;
    private final TopicResolver topicResolver = new TopicPatternResolver();
    private final String transactionalIdPattern;
    private final TransactionalIdResolver transactionalIdResolver = new TransactionalIdPatternResolver();
    private final String aclPrincipalBuilder;
    private final boolean useValueHeaders;

    private final KafkaUnit kafkaUnit;
    private final SchemaRegistryUnit schemaRegistryUnit;

    public ClusterUnit(final ClusterUnitConfig config) {
        this.config = config;
        name = config.getName();
        bindAddress = config.getBindAddress() != null ? config.getBindAddress() : DEFAULT_BIND_ADDRESS;
        advertisedAddress = config.getAdvertisedAddress() != null ? config.getAdvertisedAddress() : DEFAULT_ADVERTISED_ADDRESS;
        sslConfig = config.getSslConfig() != null ? config.getSslConfig() : SslUnit.getDefaultSslConfig();
        groupPattern = config.getGroupPattern() != null ? config.getGroupPattern() : DEFAULT_GROUP_PATTERN;
        topicPattern = config.getTopicPattern() != null ? config.getTopicPattern() : DEFAULT_TOPIC_PATTERN;
        transactionalIdPattern = config.getTransactionalIdPattern() != null ? config.getTransactionalIdPattern() : DEFAULT_TRANSACTIONAL_ID_PATTERN;
        aclPrincipalBuilder = config.isUseAdvancedAcl() ? ACL_PRINCIPAL_BUILDER_ADVANCED : ACL_PRINCIPAL_BUILDER_BASIC;
        useValueHeaders = config.isUseValueHeaders();

        if (config.getZookeeperPort() != null && config.getBrokerPort() != null) {
            kafkaUnit = new KafkaUnit(bindAddress, advertisedAddress, config.getZookeeperPort(), config.getBrokerPort());
        } else {
            kafkaUnit = new KafkaUnit();
        }

        final Map<String, Object> brokerProperties = getBrokerProperties(bindAddress, advertisedAddress, kafkaUnit.getBrokerPort(), config.isUseAdvancedAcl());
        for (Map.Entry<String, Object> entry : brokerProperties.entrySet()) {
            kafkaUnit.setKafkaBrokerConfig(entry.getKey(), entry.getValue());
        }

        schemaRegistryUnit = new SchemaRegistryUnit(config.getBindAddress(), config.getAdvertisedAddress(), config.getSchemaRegistryPort() != null ? config.getSchemaRegistryPort() : DEFAULT_SCHEMA_REGISTRY_PORT);
    }

    public String getBootstrapServer() {
        return kafkaUnit.getBootstrapServer();
    }

    public int getBrokerPort() {
        return kafkaUnit.getBrokerPort();
    }

    public int getZookeeperPort() {
        return kafkaUnit.getZkPort();
    }

    public String getGroupPattern() {
        return groupPattern;
    }

    public String getTopicPattern() {
        return topicPattern;
    }

    public String getTransactionalIdPattern() {
        return transactionalIdPattern;
    }

    public void start() {
        LOG.info("Starting cluster {}", name);
        kafkaUnit.startup();
    }

    public void stop() {
        LOG.info("Stopping cluster {}", name);
        kafkaUnit.shutdown();
    }

    private Map<String, Object> getBrokerProperties(final String bindAddress, final String advertisedAddress, final int brokerPort, boolean useAdvancedAcl) {
        final Map<String, Object> configs = new HashMap<>();
        configs.put("listeners", format("SSL://%s:%d", bindAddress, brokerPort));
        configs.put("advertised.listeners", format("SSL://%s:%d", advertisedAddress, brokerPort));
        configs.put("ssl.client.auth", "requested");
        configs.put("security.inter.broker.protocol", "SSL");
        configs.put("authorizer.class.name", "kafka.security.authorizer.AclAuthorizer");
        configs.put("allow.everyone.if.no.acl.found", "true");
        configs.put("auto.create.topics.enable", "false");
        if (useAdvancedAcl) {
            configs.put("principal.builder.class", "io.axual.security.auth.SslPrincipalBuilder");
        }

        KafkaUtil.getKafkaConfigs(sslConfig, configs);
        return configs;
    }

    public String resolveGroup(String group, Map<String, Object> context) {
        context.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, groupPattern);
        groupResolver.configure(context);
        return groupResolver.resolveGroup(group);
    }

    public void registerStream(final String stream, final int numPartitions, final Map<String, Object> context) {
        kafkaUnit.createTopic(resolveTopic(stream, context), numPartitions);
    }

    public void registerRawTopic(final String topic, final int numPartitions) {
        kafkaUnit.createTopic(topic, numPartitions);
    }

    public String resolveTopic(String topic, Map<String, Object> context) {
        context.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, topicPattern);
        topicResolver.configure(context);
        return topicResolver.resolveTopic(topic);
    }

    public String resolveTransactionalId(String transactionalId, Map<String, Object> context) {
        context.put(TransactionalIdPatternResolver.TRANSACTIONAL_ID_PATTERN_CONFIG, transactionalIdPattern);
        transactionalIdResolver.configure(context);
        return transactionalIdResolver.resolveTransactionalId(transactionalId);
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public SchemaRegistryUnit getSchemaRegistryUnit() {
        return schemaRegistryUnit;
    }

    public KafkaUnit getKafkaUnit() {
        return kafkaUnit;
    }

    public String getAclPrincipalBuilder() {
        return aclPrincipalBuilder;
    }

    public ClusterUnitConfig getConfig() {
        return config;
    }

    public boolean valueHeadersEnabled() {
        return useValueHeaders;
    }
}
