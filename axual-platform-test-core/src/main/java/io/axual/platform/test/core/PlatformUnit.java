package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.SleepUtil;

@InterfaceStability.Evolving
public class PlatformUnit implements AutoCloseable {
    public static final String DEFAULT_TENANT = "axual";
    public static final String DEFAULT_INSTANCE = "unit";
    public static final String DEFAULT_SYSTEM = "axualunit";
    public static final String DEFAULT_ADVERTISED_ADDRESS = "localhost";
    public static final String DEFAULT_BIND_ADDRESS = "127.0.0.1";
    final AtomicBoolean isStarted = new AtomicBoolean(false);
    private final List<ClusterUnit> clusters = new ArrayList<>();
    private final Map<String, ClusterUnit> clustersByName = new HashMap<>();
    private final Map<String, ClusterUnitConfig> clusterConfigsByName = new HashMap<>();
    private final InstanceUnitConfig instanceUnitConfig;
    private final InstanceUnit instance;
    private final ExecutorService executorService;
    private final Collection<Callable<Boolean>> starters;
    private final Collection<Callable<Boolean>> stoppers;


    public PlatformUnit(int numberOfClusters, boolean enableDistribution, SslConfig.KeystoreType keystoreType) {
        this(generateInstanceConfig(numberOfClusters, enableDistribution, keystoreType), generateClusterConfigs(numberOfClusters, keystoreType));
    }

    public PlatformUnit(int numberOfClusters, boolean enableDistribution) {
        this(generateInstanceConfig(numberOfClusters, enableDistribution), generateClusterConfigs(numberOfClusters));
    }

    public PlatformUnit(String[] topicPatterns, String[] groupPatterns) {
        this(generateInstanceConfig(topicPatterns.length, false), generateClusterConfigs(topicPatterns, groupPatterns));
    }

    public PlatformUnit(InstanceUnitConfig instanceConfig, ClusterUnitConfig... clusterConfigs) {
        this(instanceConfig, Arrays.asList(clusterConfigs));
    }

    public PlatformUnit(InstanceUnitConfig instanceConfig, List<ClusterUnitConfig> clusterConfigs) {
        starters = new ArrayList<>(clusterConfigs.size());
        stoppers = new ArrayList<>(clusterConfigs.size());
        for (ClusterUnitConfig clusterUnitConfig : clusterConfigs) {
            addCluster(clusterUnitConfig);
        }
        instanceUnitConfig = instanceConfig;
        instance = new InstanceUnit(instanceConfig, clustersByName);
        executorService = Executors.newFixedThreadPool(Integer.max(clusterConfigs.size(), 1));
    }

    public static String generateClusterName(int clusterNr) {
        return "CLUSTER" + clusterNr;
    }

    public static InstanceUnitConfig generateInstanceConfig(int numberOfClusters, boolean enableDistribution) {
        return generateInstanceConfig(numberOfClusters, enableDistribution, null);
    }

    public static InstanceUnitConfig generateInstanceConfig(int numberOfClusters, boolean enableDistribution, SslConfig.KeystoreType keystoreType) {
        final InstanceUnitConfig result =
                new InstanceUnitConfig()
                        .setName(DEFAULT_INSTANCE)
                        .setTenant(DEFAULT_TENANT)
                        .setSystem(DEFAULT_SYSTEM)
                        .setEnableDistribution(enableDistribution)
                        .setDiscoveryBindAddress(DEFAULT_BIND_ADDRESS)
                        .setDiscoveryAdvertisedAddress(DEFAULT_ADVERTISED_ADDRESS)
                        .setSslConfig(SslConfig.KeystoreType.PKCS12.equals(keystoreType) ? SslUnit.getDefaultSslPKCS12Config() : SslUnit.getDefaultSslConfig());
        for (int i = 0; i < numberOfClusters; i++) {
            result.addCluster(generateClusterName(i));
        }
        return result;
    }

    private static ClusterUnitConfig[] generateClusterConfigs(int numberOfClusters) {
        return generateClusterConfigs(numberOfClusters, null);
    }

    private static ClusterUnitConfig[] generateClusterConfigs(int numberOfClusters, SslConfig.KeystoreType keystoreType) {
        ClusterUnitConfig[] result = new ClusterUnitConfig[numberOfClusters];
        for (int i = 0; i < numberOfClusters; i++) {
            result[i] = generateClusterConfig(generateClusterName(i), keystoreType);
        }
        return result;
    }

    public static ClusterUnitConfig generateClusterConfig(String name) {
        return generateClusterConfig(name, null);
    }

    public static ClusterUnitConfig generateClusterConfig(String name, SslConfig.KeystoreType keystoreType) {
        return new ClusterUnitConfig()
                .setName(name)
                .setBindAddress(DEFAULT_BIND_ADDRESS)
                .setAdvertisedAddress(DEFAULT_ADVERTISED_ADDRESS)
                .setSslConfig(SslConfig.KeystoreType.PKCS12.equals(keystoreType) ? SslUnit.getDefaultSslPKCS12Config() : SslUnit.getDefaultSslConfig());
    }

    private static ClusterUnitConfig[] generateClusterConfigs(String[] topicPatterns, String[] groupPatterns) {
        if (topicPatterns.length != groupPatterns.length) {
            throw new RuntimeException("Wrong number of patterns passed in");
        }
        ClusterUnitConfig[] result = generateClusterConfigs(topicPatterns.length);
        for (int i = 0; i < topicPatterns.length; i++) {
            result[i].setTopicPattern(topicPatterns[i]);
            result[i].setGroupPattern(groupPatterns[i]);
        }
        return result;
    }

    public int getClusterCount() {
        return clusters.size();
    }

    public ClusterUnit getCluster(String clusterName) {
        return clustersByName.get(clusterName);
    }

    public ClusterUnit getCluster(int clusterNr) {
        return clusters.get(clusterNr);
    }

    public InstanceUnit getInstance() {
        return instance;
    }

    public PlatformUnit addStream(StreamConfig streamConfig) {
        instance.addStream(streamConfig);
        return this;
    }

    private void addCluster(ClusterUnitConfig config) {
        final ClusterUnit result = new ClusterUnit(config);
        clusters.add(result);
        clusterConfigsByName.put(config.getName(), config);
        clustersByName.put(result.getName(), result);
        starters.add(() -> {
            result.start();
            return true;
        });
        stoppers.add(() -> {
            result.stop();
            return true;
        });
    }

    public void start() {
        if (clustersByName.isEmpty()) {
            throw new IllegalStateException("No clusters added on start");
        }

        if (isStarted.get()) {
            throw new IllegalStateException("Already started");
        }

        if (executorService.isShutdown() || executorService.isTerminated()) {
            throw new IllegalStateException("Cannot restart a closed PlatformUnit");
        }

        List<Future<Boolean>> futures = starters
                .parallelStream()
                .map(executorService::submit)
                .collect(Collectors.toList());
        while (!futures.isEmpty()) {
            futures.removeIf(Future::isDone);
            SleepUtil.sleep(Duration.ofMillis(100));
        }
        instance.start();
        isStarted.set(true);
    }

    public void stop() {
        if (clustersByName.isEmpty()) {
            throw new IllegalStateException("No clusters added on stop");
        }

        if (!isStarted.get()) {
            // Already stopped
            return;
        }

        instance.stop();

        List<Future<Boolean>> futures = stoppers
                .parallelStream()
                .map(executorService::submit)
                .collect(Collectors.toList());
        while (!futures.isEmpty()) {
            futures.removeIf(Future::isDone);
            SleepUtil.sleep(Duration.ofMillis(100));
        }
    }

    public boolean isStarted() {
        return isStarted.get();
    }

    @Override
    public void close() {
        stop();
        executorService.shutdown();
    }

    public InstanceUnitConfig getInstanceUnitConfig() {
        return instanceUnitConfig;
    }

    public Map<String, ClusterUnitConfig> getClusterUnitConfigs() {
        return clusterConfigsByName;
    }
}
