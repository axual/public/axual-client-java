package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.confluent.kafka.schemaregistry.client.rest.RestService;
import io.confluent.kafka.schemaregistry.client.rest.entities.SchemaString;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

import static org.junit.Assert.*;

public class SchemaRegistryUnitTest {
    private static final int EXPECTED_STATUS_INTERNAL_ERROR = 500;
    private static final int EXPECTED_ERROR_CODE_INTERNAL_ERROR = 50001;
    private final String TOPIC = "testing-topic";
    private final Schema KEY_SCHEMA = SchemaBuilder.builder().record("TestRecord1").fields()
            .name("content").doc("Some documentation for the Key").type().optional().stringType()
            .endRecord();
    private final Schema VALUE_SCHEMA = SchemaBuilder.builder().record("TestRecord2").fields()
            .name("content").doc("Some documentation for the value").type().optional().longType()
            .endRecord();
    private final int EXPECTED_NR_OF_SUBJECTS = 2;
    private final String EXPECTED_SUBJECT_KEY = TOPIC + "-key";
    private final String EXPECTED_SUBJECT_VALUE = TOPIC + "-value";
    private final Integer EXPECTED_VERSION = 1;
    private final List<Integer> EXPECTED_VERSION_LIST = Collections.singletonList(EXPECTED_VERSION);
    private final String EXPECTED_KEY_SCHEMA = SchemaNormalizer.normalizeSchema(KEY_SCHEMA).toString(false);
    private final String EXPECTED_VALUE_SCHEMA = SchemaNormalizer.normalizeSchema(VALUE_SCHEMA).toString(false);

    private final String SUBJECT_NOT_EXISTS = "This should not exist";
    private final Integer SCHEMA_ID_NOT_EXISTS = Integer.MAX_VALUE;
    private final int EXPECTED_STATUS_NOT_FOUND = 404;
    private final int EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND = 40401;
    private final int EXPECTED_ERROR_CODE_SCHEMA_NOT_FOUND = 40403;

    @Test
    public void testStartStop() {
        SchemaRegistryUnit toTest = new SchemaRegistryUnit();
        try {
            toTest.start();
            assertTrue("Server should be running", toTest.isRunning());
            toTest.stop();
            assertFalse("Server should be stopped", toTest.isRunning());
        } finally {
            if (toTest.isRunning()) {
                toTest.stop();
            }
        }
    }

    @Test
    public void testEndpoint_GetAllSubjects() {
        runEndpointTest(client -> {
            try {
                Collection<String> result = client.getAllSubjects();
                assertNotNull(result);
                assertEquals(EXPECTED_NR_OF_SUBJECTS, result.size());
                assertTrue("Subject '" + EXPECTED_SUBJECT_KEY + "' not found", result.contains(EXPECTED_SUBJECT_KEY));
                assertTrue("Subject '" + EXPECTED_SUBJECT_VALUE + "' not found", result.contains(EXPECTED_SUBJECT_VALUE));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetAllVersions() {
        runEndpointTest(client -> {
            try {
                List<Integer> result = client.getAllVersions(EXPECTED_SUBJECT_KEY);
                assertNotNull(result);
                assertEquals(EXPECTED_VERSION_LIST, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }


    @Test
    public void testEndpoint_GetAllVersions_SubjectNotFound() {
        runEndpointTest(client -> {
            try {
                client.getAllVersions(SUBJECT_NOT_EXISTS);
                fail("Get All Versions should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetVersion() {
        runEndpointTest(client -> {
            try {
                io.confluent.kafka.schemaregistry.client.rest.entities.Schema result = client.getVersion(EXPECTED_SUBJECT_KEY, EXPECTED_VERSION);
                assertNotNull(result);
                assertEquals(EXPECTED_SUBJECT_KEY, result.getSubject());
                assertEquals(EXPECTED_VERSION, result.getVersion());
                assertEquals(EXPECTED_KEY_SCHEMA, result.getSchema());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetVersion_SubjectNotFound() {
        runEndpointTest(client -> {
            try {
                client.getVersion(SUBJECT_NOT_EXISTS, EXPECTED_VERSION);
                fail("Get Version should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetLatestVersion() {
        runEndpointTest(client -> {
            try {
                io.confluent.kafka.schemaregistry.client.rest.entities.Schema result = client.getLatestVersion(EXPECTED_SUBJECT_KEY);
                assertNotNull(result);
                assertEquals(EXPECTED_SUBJECT_KEY, result.getSubject());
                assertEquals(EXPECTED_VERSION, result.getVersion());
                assertEquals(EXPECTED_KEY_SCHEMA, result.getSchema());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetLatestVersion_SubjectNotFound() {
        runEndpointTest(client -> {
            try {
                client.getLatestVersion(SUBJECT_NOT_EXISTS);
                fail("Get Latest Version should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetVersionSchemaOnly() {
        runEndpointTest(client -> {
            try {
                String result = client.getVersionSchemaOnly(EXPECTED_SUBJECT_VALUE, EXPECTED_VERSION);
                assertNotNull(result);
                assertEquals(EXPECTED_VALUE_SCHEMA, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetVersionSchemaOnly_SubjectNotFound() {
        runEndpointTest(client -> {
            try {
                client.getVersionSchemaOnly(SUBJECT_NOT_EXISTS, EXPECTED_VERSION);
                fail("Get Version Schema Only should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetLatestVersionSchemaOnly() {
        runEndpointTest(client -> {
            try {
                String result = client.getLatestVersionSchemaOnly(EXPECTED_SUBJECT_VALUE);
                assertNotNull(result);
                assertEquals(EXPECTED_VALUE_SCHEMA, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }


    @Test
    public void testEndpoint_GetLatestSchemaOnly_SubjectNotFound() {
        runEndpointTest(client -> {
            try {
                client.getLatestVersionSchemaOnly(SUBJECT_NOT_EXISTS);
                fail("Get Latest Version Schema Only should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SUBJECT_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_DeleteSubject() {
        runEndpointTest(client -> {
            try {
                client.deleteSubject(Collections.emptyMap(), EXPECTED_SUBJECT_KEY);
                fail("Delete should throw an exception");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_INTERNAL_ERROR, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_INTERNAL_ERROR, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_RegisterSubjectVersion_OriginalSchema() {
        runEndpointTest((client, srUnit) -> {
            try {
                final Integer expected = srUnit.getSchemaId(KEY_SCHEMA);
                Integer result = client.registerSchema(KEY_SCHEMA.toString(true), EXPECTED_SUBJECT_KEY);
                assertEquals(expected, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_RegisterSubjectVersion_NormalizedSchema() {
        runEndpointTest((client, srUnit) -> {
            try {
                final Integer expected = srUnit.getSchemaId(KEY_SCHEMA);
                Integer result = client.registerSchema(SchemaNormalizer.normalizeSchema(KEY_SCHEMA).toString(true), EXPECTED_SUBJECT_KEY);
                assertEquals(expected, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_RegisterSubjectVersion_IncorrectSchema() {
        runEndpointTest((client, srUnit) -> {
            try {
                client.registerSchema(SchemaNormalizer.normalizeSchema(VALUE_SCHEMA).toString(true), EXPECTED_SUBJECT_KEY);
                fail("Register Subject Version should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_INTERNAL_ERROR, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_INTERNAL_ERROR, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_RegisterSubjectVersion_UnknownSubject() {
        runEndpointTest((client, srUnit) -> {
            try {
                client.registerSchema(SchemaNormalizer.normalizeSchema(KEY_SCHEMA).toString(true), SUBJECT_NOT_EXISTS);
                fail("Register Subject Version should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_INTERNAL_ERROR, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_INTERNAL_ERROR, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetSchemaById_KnownId() {
        runEndpointTest((client, srUnit) -> {
            try {
                final Integer schemaId = srUnit.getSchemaId(KEY_SCHEMA);
                SchemaString result = client.getId(schemaId);
                assertNotNull(result);
                assertEquals(EXPECTED_KEY_SCHEMA, result.getSchemaString());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_GetSchemaById_UnknownId() {
        runEndpointTest((client, srUnit) -> {
            try {
                client.getId(SCHEMA_ID_NOT_EXISTS);
                fail("Get Schema By ID should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_NOT_FOUND, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_SCHEMA_NOT_FOUND, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_lookUpSubjectVersion_OriginalSchema() {
        runEndpointTest(client -> {
            try {
                io.confluent.kafka.schemaregistry.client.rest.entities.Schema result = client.lookUpSubjectVersion(KEY_SCHEMA.toString(true), EXPECTED_SUBJECT_KEY);
                assertNotNull(result);
                assertEquals(EXPECTED_SUBJECT_KEY, result.getSubject());
                assertEquals(EXPECTED_VERSION, result.getVersion());
                assertEquals(EXPECTED_KEY_SCHEMA, result.getSchema());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_lookUpSubjectVersion_NormalizedSchema() {
        runEndpointTest(client -> {
            try {
                io.confluent.kafka.schemaregistry.client.rest.entities.Schema result = client.lookUpSubjectVersion(SchemaNormalizer.normalizeSchema(KEY_SCHEMA).toString(true), EXPECTED_SUBJECT_KEY);
                assertNotNull(result);
                assertEquals(EXPECTED_SUBJECT_KEY, result.getSubject());
                assertEquals(EXPECTED_VERSION, result.getVersion());
                assertEquals(EXPECTED_KEY_SCHEMA, result.getSchema());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testEndpoint_lookUpSubjectVersion_IncorrectSchema() {
        runEndpointTest(client -> {
            try {
                client.lookUpSubjectVersion(SchemaNormalizer.normalizeSchema(VALUE_SCHEMA).toString(true), EXPECTED_SUBJECT_KEY);
                fail("Get Schema By ID should throw RestClientException");
            } catch (RestClientException rce) {
                assertEquals(EXPECTED_STATUS_INTERNAL_ERROR, rce.getStatus());
                assertEquals(EXPECTED_ERROR_CODE_INTERNAL_ERROR, rce.getErrorCode());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void runEndpointTest(Consumer<RestService> testExecution) {
        runEndpointTest((a, b) -> testExecution.accept(a));
    }

    public void runEndpointTest(BiConsumer<RestService, SchemaRegistryUnit> testExecution) {
        SchemaRegistryUnit toTest = new SchemaRegistryUnit();
        toTest.registerSchema(TOPIC, KEY_SCHEMA, VALUE_SCHEMA);
        try {
            toTest.start();
            String endpoint = toTest.getBaseURL();

            RestService restService = new RestService(endpoint);
            testExecution.accept(restService, toTest);
        } finally {
            if (toTest.isRunning()) {
                toTest.stop();
            }
        }
    }

}
