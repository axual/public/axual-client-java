package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.ClientConfig;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({
        ClusterUnit.class,
        DiscoveryUnit.class,
        DistributionUnit.class,
        InstanceUnit.class
})
public class InstanceUnitTest {

    private static final String TENANT = "tenant";
    private static final String NAME = "name";
    private static final String SYSTEM = "unit";

    private static final String CLUSTER = "C1";

    private static final String STREAM = "stream";
    private static final String RESOLVED_STREAM = "resolved-stream";
    private static final int PARTITIONS = 1;

    private static final DiscoveryUnit mockDiscoveryUnit = PowerMockito.mock(DiscoveryUnit.class);
    private static final DistributionUnit mockDistributionUnit = PowerMockito.mock(DistributionUnit.class);
    private static final ClusterUnit mockClusterUnit = PowerMockito.mock(ClusterUnit.class);
    private static final SchemaRegistryUnit mockSchemaRegistryUnit = PowerMockito.mock(SchemaRegistryUnit.class);

    private InstanceUnit target;


    private InstanceUnitConfig validConfig() {
        InstanceUnitConfig config = new InstanceUnitConfig();

        config.setTenant(TENANT);
        config.setName(NAME);
        config.setSystem(SYSTEM);
        config.setDiscoveryPort(1234);
        config.setEnableDistribution(true);
        config.addCluster(CLUSTER);
        return config;
    }

    @Before
    public void init() throws Exception {
        initMocks(this);

        whenNew(DistributionUnit.class).withAnyArguments().thenReturn(mockDistributionUnit);
        whenNew(DiscoveryUnit.class).withAnyArguments().thenReturn(mockDiscoveryUnit);
        when(mockClusterUnit.getSchemaRegistryUnit()).thenReturn(mockSchemaRegistryUnit);

        Map<String, ClusterUnit> clustersByName = new HashMap<>();
        clustersByName.put(CLUSTER, mockClusterUnit);

        target = new InstanceUnit(validConfig(), clustersByName);
        target.start();
    }

    @Test
    public void testGetTenant() {
        assertEquals(TENANT, target.getTenant());
    }

    @Test
    public void testGetName() {
        assertEquals(NAME, target.getName());
    }

    @Test
    public void testStart() {
        target.start();
        verify(mockDiscoveryUnit, atLeastOnce()).start();
        verify(mockDistributionUnit, atLeastOnce()).start();
    }

    @Test
    public void testStop() {
        target.stop();

        verify(mockDistributionUnit, times(1)).stop();
    }

    @Test
    public void testPauseResumeDistribution() {
        target.pauseDistribution();
        target.resumeDistribution();

        verify(mockDistributionUnit, times(1)).pause();
        verify(mockDistributionUnit, times(1)).resume();
    }

    @Test
    public void testDirectApplicationTo() {
        when(mockClusterUnit.resolveTopic(eq(STREAM), any())).thenReturn(RESOLVED_STREAM);
        final ClientConfig clientConfig = ClientConfig.newBuilder().build();

        target.directApplicationTo(clientConfig, mockClusterUnit);

        verify(mockDiscoveryUnit, times(1)).directApplicationTo(eq(clientConfig), eq(mockClusterUnit));
    }

    @Test
    public void testDeployStream() {
        when(mockClusterUnit.resolveTopic(eq(STREAM), any())).thenReturn(RESOLVED_STREAM);
        final StreamConfig streamConfig = new StreamConfig()
                .setName(STREAM)
                .setKeySchema(null)
                .setValueSchema(null)
                .setPartitions(PARTITIONS);

        target.deployStream(streamConfig);

        verify(mockClusterUnit, times(1)).registerStream(eq(STREAM), eq(PARTITIONS), any());
        verify(mockSchemaRegistryUnit, times(1)).registerSchema(eq(RESOLVED_STREAM), eq(null), eq(null));
    }

    @Test
    public void testDeployRawTopic() {
        final StreamConfig streamConfig = new StreamConfig()
                .setName(STREAM)
                .setKeySchema(null)
                .setValueSchema(null)
                .setPartitions(PARTITIONS);

        target.deployRawTopic(streamConfig);

        verify(mockClusterUnit, times(1)).registerRawTopic(STREAM, PARTITIONS);
        verify(mockSchemaRegistryUnit, times(1)).registerSchema(eq(STREAM), eq(null), eq(null));
    }
}
