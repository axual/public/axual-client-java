package io.axual.client.janitor;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.axual.common.concurrent.LockedObject;

/**
 * This class is responsible for a registration of AutoCloseable objects, which it will auto-close
 * upon application shutdown.
 */
public class Janitor implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(Janitor.class);

    // Set up a concurrently accessible map, then convert it into a concurrently accessible set
    private static final Map<Janitor, Boolean> janitorMap = new ConcurrentHashMap<>();
    private static final Set<Janitor> janitors = Collections.newSetFromMap(janitorMap);

    // Static thread singleton, which gets called when at least one Janitor is active upon shutdown
    private static LockedObject<Thread> shutDownThread = new LockedObject<>(null);

    // Each janitor holds its own registry of managed objects, contained in this map. The objects
    // themselves are in the key, the value is a Boolean that indicates whether the registering
    // caller expects the user of the object to close it or not. If true and Janitor.close() sees
    // the object was not closed, it generates a warning on the execProc output.
    private final Map<ManagedCloseable, Boolean> objectMap = new ConcurrentHashMap<>();

    public static class ManagedCloseable implements AutoCloseable {
        Janitor responsibleJanitor;

        @Override
        public void close() {
            if (responsibleJanitor != null) {
                if (responsibleJanitor.objectMap.remove(this) == null) {
                    LOG.warn("Closing an unregistered {}, potential double close", getClass().getSimpleName());
                } else {
                    LOG.debug("Unregistered {}", getClass().getSimpleName());
                }
            }
        }
    }

    public Janitor() {
        // Make sure only one the first janitor registers a shutdown hook
        try (LockedObject<Thread>.WriteLock lock = shutDownThread.getWriteLock()) {
            if (lock.getObject() == null) {
                // Create a thread that gets executed upon application shutdown to gracefully stop
                // and close all managed objects
                lock.setObject(new Thread(new ShutDownHook(), "Janitor shutdown thread"));
                Runtime.getRuntime().addShutdownHook(lock.getObject());
            }
        }

        janitors.add(this);
    }

    public void close() {
        // Close all unclosed objects managed by this janitor
        for (Map.Entry<ManagedCloseable, Boolean> objectEntry : objectMap.entrySet()) {
            final String type = objectEntry.getKey().getClass().getSimpleName();
            if (objectEntry.getValue().equals(Boolean.TRUE)) {
                LOG.warn("Cleaning up unclosed {}", type);
            }
            try {
                objectEntry.getKey().close();
            } catch (Exception swallow) {
                LOG.warn("Could not close {}", type, swallow);
            }
        }

        // Unregister from the registry of janitors
        janitors.remove(this);
    }

    public <T extends ManagedCloseable> T register(T object) {
        return register(object, true);
    }

    public <T extends ManagedCloseable> T register(final T object, boolean expectProperClose) {
        if (object.responsibleJanitor != null) {
            object.responsibleJanitor.objectMap.remove(object);
        }
        LOG.debug("Registering {}", object.getClass().getSimpleName());
        object.responsibleJanitor = this;
        objectMap.put(object, expectProperClose);
        return object;
    }

    private static final class ShutDownHook implements Runnable {
        @Override
        public void run() {
            LOG.info("Janitor shutdown activated, cleaning up");
            for (Janitor janitor : janitors) {
                janitor.close();
            }
        }
    }
}
