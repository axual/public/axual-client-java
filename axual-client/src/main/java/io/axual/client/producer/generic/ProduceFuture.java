package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;

/**
 * Implementation class for the {@link Future} containing the {@link ProducedMessage} This class is
 * the actual class returned by {@link Producer#produce(ProducerMessage)} {@link
 * Producer#produce(ProducerMessage)}
 */
public final class ProduceFuture<K, V> implements Future<ProducedMessage<K, V>> {
    private static final int SLEEP_TIME_MILLISECONDS = 10;
    private static final long MULTIPLIER_DEFAULT = 0L;
    private static final long MULTIPLIER_MILLISECONDS = 1L;
    private static final long MULTIPLIER_SECONDS = 1000L;
    private static final long MULTIPLIER_MINUTES = 60000L;
    private static final long MULTIPLIER_HOURS = 3600000L;
    private static final long MULTIPLIER_DAYS = 86400000L;

    private ProducedMessage<K, V> producedMessage = null;
    private ExecutionException executionException = null;
    private boolean isCancelled = false;

    public synchronized void complete(final ProducedMessage<K, V> producedMessage) {
        this.producedMessage = producedMessage;
    }

    public synchronized void complete(final ExecutionException executionException) {
        this.executionException = executionException;
    }

    public ProducedMessage<K, V> getProducedMessage() {
        return producedMessage;
    }

    @Override
    public synchronized boolean cancel(boolean mayInterruptIfRunning) {
        isCancelled = true;
        executionException = new ExecutionException(new CancellationException("Execution cancelled"));
        return true;
    }

    @Override
    public synchronized boolean isCancelled() {
        return this.isCancelled;
    }

    @Override
    public synchronized boolean isDone() {
        return (producedMessage != null || executionException != null);
    }

    @Override
    public ProducedMessage<K, V> get() throws InterruptedException, ExecutionException {
        while (!isDone()) {
            Thread.sleep(SLEEP_TIME_MILLISECONDS);
        }

        synchronized (this) {
            if (executionException != null) {
                throw executionException;
            }

            return producedMessage;
        }
    }

    @Override
    public ProducedMessage<K, V> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        final long currentTime = System.currentTimeMillis();
        final long multiplier;
        switch (unit) {
            case MILLISECONDS:
                multiplier = MULTIPLIER_MILLISECONDS;
                break;
            case SECONDS:
                multiplier = MULTIPLIER_SECONDS;
                break;
            case MINUTES:
                multiplier = MULTIPLIER_MINUTES;
                break;
            case HOURS:
                multiplier = MULTIPLIER_HOURS;
                break;
            case DAYS:
                multiplier = MULTIPLIER_DAYS;
                break;
            default:
                multiplier = MULTIPLIER_DEFAULT;
        }

        final long timeoutTime = (currentTime + (multiplier * timeout));

        while (true) {
            if (isDone()) {
                synchronized (this) {
                    if (executionException != null) {
                        throw executionException;
                    }
                    return producedMessage;
                }
            } else if (System.currentTimeMillis() > timeoutTime) {
                throw new TimeoutException();
            } else {
                Thread.sleep(SLEEP_TIME_MILLISECONDS);
            }
        }
    }
}
