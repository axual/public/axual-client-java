package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.producer.ExtendedProducerRecord;
import io.axual.client.proxy.lineage.LineageHeaders;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.serde.utils.HeaderUtils;

public class GenericProducedMessage<K, V> implements ProducedMessage<K, V> {
    private final ProducerMessage<K, V> message;
    private final RecordMetadata metadata;
    private final Map<String, Object> context;

    public GenericProducedMessage(final ProducerMessage<K, V> message,
                                  final RecordMetadata metadata) {
        this.message = message;
        this.metadata = metadata;

        context = Collections.unmodifiableMap(
                message.getProducerRecord() instanceof ExtendedProducerRecord
                        ? ((ExtendedProducerRecord<K, V>) message.getProducerRecord()).context()
                        : new HashMap<>());
    }

    @Override
    public ProducerMessage<K, V> getMessage() {
        return message;
    }

    @Override
    public String getSystem() {
        return getContextString(DiscoveryResult.SYSTEM_PROPERTY);
    }

    @Override
    public String getInstance() {
        return getContextString(DiscoveryResult.INSTANCE_PROPERTY);
    }

    @Override
    public String getCluster() {
        return getContextString(DiscoveryResult.CLUSTER_PROPERTY);
    }

    @Override
    public String getStream() {
        return metadata.topic();
    }

    @Override
    public int getPartition() {
        return metadata.partition();
    }

    @Override
    public long getOffset() {
        return metadata.offset();
    }

    // The timestamp is the timestamp in the corresponding ProducerMessage<K, V> if the user provided
    // one. If not it will be the producer local time when the producer record was handed to the
    // producer.
    @Override
    public Long getTimestamp() {
        return metadata.hasTimestamp() ? metadata.timestamp() : null;
    }

    @Override
    public Long getSerializationTimestamp() {
        Header header = message.getProducerRecord().headers().lastHeader(LineageHeaders.SERIALIZATION_TIME_HEADER);
        return header != null ? HeaderUtils.decodeLongHeader(header) : null;
    }

    private String getContextString(String key) {
        Object value = context.get(key);
        return value instanceof String ? (String) value : null;
    }
}
