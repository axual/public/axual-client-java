package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.concurrent.Future;

import io.axual.client.producer.generic.GenericProducedMessage;

/**
 * Common interface used for all Axual Client Producers
 *
 * @param <K> the key of produced messages
 * @param <V> the value of produced messages
 */
public interface Producer<K, V> extends AutoCloseable {
    /**
     * Method used to produce records, the implementation is dependent on the strategy
     *
     * @param message the message to be produced.
     * @return a Future containing a {@link GenericProducedMessage} or a {@link
     * java.util.concurrent.ExecutionException} if the produce execution failed
     */
    Future<ProducedMessage<K, V>> produce(ProducerMessage<K, V> message);

    /**
     * Method used to produce records, the implementation is dependent on the strategy
     *
     * @param message  the message to be produced
     * @param callback the callback implementation to be used to notify the producing application
     * @return a Future containing a {@link ProducedMessage} or a {@link
     * java.util.concurrent.ExecutionException} if the produce execution failed
     */
    Future<ProducedMessage<K, V>> produce(ProducerMessage<K, V> message, ProduceCallback<K, V> callback);

    /**
     * Closes the connection, the connection is re-opened after produce is called, or if after a
     * failed attempt to produce a retry is called.
     */
    void close();
}
