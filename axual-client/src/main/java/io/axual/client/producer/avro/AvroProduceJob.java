package io.axual.client.producer.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericContainer;
import org.apache.kafka.common.KafkaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.exception.NoExistingSchemaException;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.generic.ProduceFuture;
import io.axual.client.producer.generic.ProduceJob;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;

public class AvroProduceJob<K extends GenericContainer, V extends GenericContainer> extends ProduceJob<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(AvroProduceJob.class);

    AvroProduceJob(ProducerMessage<K, V> message, ProduceCallback<K, V> produceCallback) {
        super(message, produceCallback);
    }

    @Override
    public ProduceFuture<K, V> execute(ProducerProxy<K, V> producer) {
        try {
            return super.execute(producer);
        } catch (KafkaException e) {
            final String srUrls = producer.getStringConfig(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG);
            NoExistingSchemaException.handleException(e, getMessage().getStream(), srUrls);
            throw e;
        } catch (NoExistingSchemaException e) {
            if (getMessage().getKey() != null) {
                LOG.error("  Key schema: {}", getMessage().getKey().getSchema());
            }
            if (getMessage().getValue() != null) {
                LOG.error("  Value schema: {}", getMessage().getValue().getSchema());
            }

            throw e;
        }
    }
}
