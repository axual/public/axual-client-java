package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.client.config.BaseProducerConfig;
import io.axual.common.config.ClientConfig;

/**
 * Manages the {@link ProducerWorker} registry. Producers are registered with {@link
 * ProducerWorkerManager#claimWorker(ClientConfig, GenericProducer, BaseProducerConfig)} and a
 * ProducerWorker is retrieved from the registry. If the producer is not yet registered it will
 * create a new ProducerWorker and start the thread execution.
 * <p>
 * The {@link ProducerWorker} thread can be stopped with {@link ProducerWorkerManager#releaseWorker(GenericProducer)}
 * This will cancel all messages still waiting in the message queue.
 */
public final class ProducerWorkerManager implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerWorkerManager.class);
    private static final Duration JOIN_TIMOUT = Duration.ofSeconds(10);
    private final Map<GenericProducer, ProducerWorker> registry = new ConcurrentHashMap<>();
    private final AtomicBoolean isClosed = new AtomicBoolean(false);

    /**
     * Checks if the producer exists in the registry. It it is found, it will check if the worker
     * thread is still running and restart it if it's not running. When not found, a new {@link
     * ProducerWorker} instance will be created, the thread started and the worker returned.
     *
     * @param <K>            the key type of produced messages
     * @param <V>            the value type of produced messages
     * @param producer       The producer to be registered
     * @param clientConfig   The configuration of the client application
     * @param producerConfig The configuration for the producer
     * @return The worker instance created or retrieved from the registry
     */
    public synchronized <K, V> ProducerWorker<K, V> claimWorker(final ClientConfig clientConfig,
                                                                final GenericProducer<K, V> producer,
                                                                final BaseProducerConfig<K, V> producerConfig) {
        if (isClosed.get()) {
            throw new IllegalStateException("The ProducerWorkerManager is already closed");
        }

        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Claiming worker" +
                            "\tProducer                 : %s" +
                            "\tSink                     : %s" +
                            "\tDeliveryStrategy         : %s" +
                            "\tOrderingStrategy         : %s" +
                            "\tMessageBufferSize        : %d" +
                            "\tmessageBufferWaitTimeout : %d" +
                            "\tblockedMessageInsert     : %s",
                    producer, producerConfig, producerConfig.getDeliveryStrategy(),
                    producerConfig.getOrderingStrategy(), producerConfig.getMessageBufferSize(),
                    producerConfig.getMessageBufferWaitTimeout(), producerConfig.isBlocking()));
        }

        if (registry.containsKey(producer)) {
            LOG.debug("Producer found in registry");
            return registry.get(producer);
        }

        LOG.debug("Producer not found in registry, starting new ProducerWorker");
        ProducerWorker<K, V> worker = new ProducerWorker<>(clientConfig, producerConfig);
        worker.start();

        // Keep track of producer/worker combination in the registry
        registry.put(producer, worker);

        return worker;
    }

    /**
     * Stops the running producer worker thread
     *
     * @param <K>      the key type of the producer
     * @param <V>      the value type of the producer
     * @param producer the producer
     */
    public synchronized <K, V> void releaseWorker(final GenericProducer<K, V> producer) {
        LOG.info("Releasing producer {}", producer);
        if (registry.containsKey(producer)) {
            LOG.debug("Send ProducerWorker cancel command");
            stopWorker(registry.get(producer));
            registry.remove(producer);
        } else {
            LOG.debug("Producer not found in registry");
        }
    }

    /**
     * Gracefully shutdown the ProducerWorkerManager.
     */
    @Override
    public void close() {
        if (isClosed.compareAndSet(false, true)) {
            LOG.info("Shutting down ProducerWorkerManager, cancelling all workers");
            for (ProducerWorker worker : registry.values()) {
                stopWorker(worker);
            }
        } else {
            LOG.warn("ProducerWorkerManager already closed, ignoring...");
        }
    }

    private void stopWorker(ProducerWorker worker) {
        worker.cancel();
        long startTime = System.currentTimeMillis();
        try {
            worker.join(JOIN_TIMOUT.toMillis());
        } catch (InterruptedException e) {
            LOG.warn("Interrupted while waiting for ProducerWorker to stop, ignoring");
            Thread.currentThread().interrupt();
        }
        Duration duration = Duration.ofMillis(System.currentTimeMillis() - startTime);
        LOG.info("Stopping producer worker took {}", duration);
    }
}
