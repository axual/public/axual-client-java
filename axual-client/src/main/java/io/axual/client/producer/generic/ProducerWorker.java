package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.client.config.BaseProducerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.exception.BufferFullException;
import io.axual.client.exception.ProducerWorkerCancelledException;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.common.tools.SleepUtil;

import static org.apache.kafka.clients.producer.ProducerConfig.*;

/**
 * Implementation of the worker thread for the producer. It will accept ProduceTracker instances to
 * be sent to a buffer from outside the thread. The buffer is emptied by the thread one message at a
 * time.
 */
public class ProducerWorker<K, V> extends Thread {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerWorker.class);

    private static final AtomicInteger WORKERCOUNT = new AtomicInteger(0);
    private static final String THREE_MINUTES = "180000";

    private final AtomicBoolean isCancelled = new AtomicBoolean(false);
    private final ClientConfig clientConfig;
    private final BaseProducerConfig producerConfig;
    private final boolean blockedMessageInsert;
    private final int messageBufferSize;
    private final long messageBufferWaitTimeout;
    private final ArrayBlockingQueue<ProduceJob<K, V>> messageBuffer;
    private final ProducerProxy<K, V> producer;
    private final AtomicBoolean isRunning;

    public ProducerWorker(final ClientConfig clientConfig, final BaseProducerConfig producerConfig) {
        LOG.info("Creating new ProducerWorker");
        this.clientConfig = clientConfig;
        this.producerConfig = producerConfig;
        this.messageBufferSize = producerConfig.getMessageBufferSize();
        this.messageBufferWaitTimeout = producerConfig.getMessageBufferWaitTimeout();
        this.blockedMessageInsert = producerConfig.isBlocking();
        this.messageBuffer = new ArrayBlockingQueue<>(this.messageBufferSize);
        this.producer = createNewKafkaProducer();
        this.isRunning = new AtomicBoolean(false);
    }

    private ProducerProxy<K, V> createNewKafkaProducer() {
        final Map<String, Object> configs = KafkaUtil.getKafkaConfigs(clientConfig);
        final Long RETRIES=2L;
        final Long REQUEST_TIMEOUT_MS=30000L;
        final Long DELIVERY_TIMEOUT_MS = RETRIES * (REQUEST_TIMEOUT_MS + producerConfig.getLingerMs());

        //used to serialize the key to bytes
        configs.put(KEY_SERIALIZER_CLASS_CONFIG, producerConfig.getKeySerializer());
        //used to serialize the value to bytes
        configs.put(VALUE_SERIALIZER_CLASS_CONFIG, producerConfig.getValueSerializer());

        /*
         * The produce is only considered a success if it is received by the number of brokers
         * specified by the min.insync.replicas values, typically this means if one broker fails,
         * the record is still retrievable from the cluster.
         */
        configs.put(ACKS_CONFIG,
                producerConfig.getDeliveryStrategy() == DeliveryStrategy.AT_LEAST_ONCE
                        ? "-1"
                        : "0");

        /*
         * The Retry policy should be the only way retries are initiated, to the user of the library is in control
         */
        configs.put(RETRIES_CONFIG, Long.toString(RETRIES));

        /*
         * The amount of time to wait before attempting to reconnect to a given host. This avoids
         * repeatedly connecting to a host in a tight loop.
         */
        configs.put(RECONNECT_BACKOFF_MS_CONFIG, "1000");

        /*
         * The amount of time to wait before attempting to retry a failed fetch request to a given
         * topic partition.
         */
        configs.put(RETRY_BACKOFF_MS_CONFIG, "1000");

        /*
         * When we allow ordering to be lost, we allow multiple requests in parallel to the Kafka
         * brokers. When we want to keep order, we only allow one request, ensuring serialization
         * of all messages sent.
         */
        configs.put(MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION,
                producerConfig.getOrderingStrategy() == OrderingStrategy.LOSING_ORDER
                        ? "10"
                        : "1");

        /*
         * Add the linger.ms if it's present in the provider properties
         */
        configs.put(LINGER_MS_CONFIG, producerConfig.getLingerMs());

        /*
         * Add the batch size if it's present in the provider properties
         */
        configs.put(BATCH_SIZE_CONFIG, producerConfig.getBatchSize());


        // Recommended Azure configurations from: https://docs.microsoft.com/en-us/azure/event-hubs/apache-kafka-configurations
        // Azure closes inbound TCP idle > 240_000 ms (4min), Kafka defaults at 540_000ms (9min).
        configs.put(CONNECTIONS_MAX_IDLE_MS_CONFIG, THREE_MINUTES);
        // Refreshes metadata, Kafka defaults at 300_000ms (5min).
        configs.put(METADATA_MAX_AGE_CONFIG, THREE_MINUTES);
        // Controls how long the producer will cache metadata for a topic that's idle. Kafka defaults at 300_000ms (5min)
        configs.put(METADATA_MAX_IDLE_CONFIG, THREE_MINUTES);
        // Azure will close connections if requests larger than 1_046_528 bytes (Kafka default 1_048_576) are sent.
        configs.put(MAX_REQUEST_SIZE_CONFIG, "1000000");
        
        configs.put(REQUEST_TIMEOUT_MS_CONFIG, REQUEST_TIMEOUT_MS.toString());
        configs.put(DELIVERY_TIMEOUT_MS_CONFIG, DELIVERY_TIMEOUT_MS.toString());
        
        // Enable the default proxy layers
        configs.put(AxualProducerConfig.CHAIN_CONFIG, producerConfig.getProxyChain());

        LOG.info("Creating a new Axual producer with properties: {}", configs);
        ProducerProxy<K, V> result = new AxualProducer<>(configs);
        LOG.info("Created a new Axual producer");
        return result;
    }

    @Override
    public synchronized void start() {
        this.isRunning.set(true);
        super.start();
    }

    @Override
    public void run() {
        final int producerWorkerNr = WORKERCOUNT.getAndIncrement();
        Thread.currentThread().setName(ProducerWorker.class.getSimpleName() + Integer.toString(producerWorkerNr));
        LOG.info("Starting ProducerWorker thread {}", Thread.currentThread().getName());

        try {
            // Main loop to handle produce jobs
            while (!isCancelled.get()) {
                takeJob(producer);
            }

            // Flush the remainder of the messages that haven't been produced to Kafka yet
            LOG.info("ProducerWorker cancelled, flushing {} messages in buffer", messageBuffer.size());

            for (ProduceJob<K, V> job : messageBuffer) {
                job.execute(producer);
            }
            messageBuffer.clear();
        } catch (Exception e) {
            LOG.error("ProducerWorker caught exception", e);
        } finally {
            this.isRunning.set(false);
            producer.close();
        }

        LOG.info("Exiting ProducerWorker thread {}", Thread.currentThread().getName());
    }

    void cancel() {
        LOG.info("ProducerWorker cancel requested");
        isCancelled.set(true);
    }

    private void takeJob(ProducerProxy<K, V> producer) {
        LOG.debug("Taking new job from queue");
        try {
            ProduceJob<K, V> job = messageBuffer.poll(100, TimeUnit.MILLISECONDS);
            if (job != null) {
                executeJob(producer, job);
            }
        } catch (InterruptedException e) {
            if (isCancelled.get()) {
                LOG.info("The ProducerWorker got cancelled and the take from messageBuffer was interrupted");
            } else {
                LOG.warn("Take from messageBuffer was interrupted while the ProducerWorker is not cancelled", e);
            }
            Thread.currentThread().interrupt();
        }
    }

    private void executeJob(ProducerProxy<K, V> producer, ProduceJob<K, V> job) {
        try {
            LOG.debug("Producing message");
            ProduceFuture future = job.execute(producer);
            // When we enforce ordering, wait until the produce is done
            if (producerConfig.getOrderingStrategy() == OrderingStrategy.KEEPING_ORDER) {
                while (!future.isDone()) {
                    SleepUtil.sleep(Duration.ofMillis(25));
                }
            }
        } catch (Exception e) {
            LOG.error("Produce message failed", e);
            job.completeProduce("Produce message failed", e);
        }
    }

    /**
     * Puts a ProduceJob on the message queue. Any exceptions or errors will be processed by the job
     * itself.
     *
     * @param produceJob the message tracker containing the message key, value and callback data.
     * @return an object that contains the result of the asynchronous job sometime in the future
     */
    public Future<ProducedMessage<K, V>> queueJob(final ProduceJob<K, V> produceJob) {
        if (isCancelled.get()) {
            LOG.error("Trying to produce while the ProducerWorker is cancelled");
            produceJob.completeProduce(new ProducerWorkerCancelledException("Cannot produce messages on cancelled ProducerWorker"));
            return produceJob.getFuture();
        }

        if (!isRunning.get()) {
            LOG.error("Trying to produce while the ProducerWorker is stopped or failed");
            produceJob.completeProduce(new ProducerWorkerCancelledException("Cannot produce messages on stopped ProducerWorker"));
            return produceJob.getFuture();
        }

        try {
            if (blockedMessageInsert) {
                // Insert to buffer, wait indefinitely for room in buffer
                messageBuffer.put(produceJob);
                return produceJob.getFuture();
            }

            if (messageBufferWaitTimeout < 1) {
                // Insert into buffer without waiting
                if (!messageBuffer.offer(produceJob)) {
                    produceJob.completeProduce(new BufferFullException(String.format("Could not add message to buffer. Buffer maximum size is %d", messageBufferSize)));
                }
                return produceJob.getFuture();
            }

            // Insert into buffer, block when queue is full for messageBufferWaitTimeout
            if (!messageBuffer.offer(produceJob, messageBufferWaitTimeout, TimeUnit.MILLISECONDS)) {
                produceJob.completeProduce(new BufferFullException(String.format("Could not add message to buffer. Buffer wait timeout is %d. Buffer maximum size is %d", messageBufferWaitTimeout, messageBufferSize)));
            }
            return produceJob.getFuture();
        } catch (InterruptedException e) {
            LOG.warn("Put message into buffer was interrupted");
            produceJob.completeProduce("Put message into buffer was interrupted", e);
            Thread.currentThread().interrupt();
            return produceJob.getFuture();
        }
    }
}
