package io.axual.client.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * Interface used for the class which does the processing
 *
 * @param <K> key of messages produced
 * @param <V> value of messages produced
 */
public interface Processor<K, V> {

    /**
     * This method will be called on each message retrieved from the Axual Platform. To control possible
     * next steps, it can:
     *   Return without an Exception: acknowledge processing and move to the next message.
     *   Throw a RetriableException:  signal an error that might be solved soon, so the framework
     *                                should retry processing after a given timeout.
     *   Throw a SkippableException:  signal an error, that should be logged before moving on to
     *                                the next message.
     *   Throw any other Exception:   signal a fatal error. The consumer loop will execProc the error
     *                                and exit.
     *
     * @param message   a Message retrieved from Axual Platform
     */
    void processMessage(ConsumerMessage<K, V> message);
}
