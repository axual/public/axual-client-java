package io.axual.client.consumer.base;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;

import java.util.UUID;

import io.axual.client.consumer.ConsumerMessage;
import io.axual.serde.utils.HeaderUtils;

import static io.axual.client.proxy.lineage.LineageHeaders.DESERIALIZATION_TIME_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.INTERMEDIATE_ID_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.INTERMEDIATE_VERSION_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.MESSAGE_ID_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.PRODUCER_ID_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.PRODUCER_VERSION_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.SERIALIZATION_TIME_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.CLUSTER_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.ENVIRONMENT_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.INSTANCE_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.SYSTEM_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.TENANT_HEADER;

public class BaseMessage<K, V> implements ConsumerMessage<K, V> {
    private final ConsumerRecord<K, V> record;

    protected BaseMessage(ConsumerRecord<K, V> record) {
        this.record = record;
    }

    public ConsumerRecord<K, V> getRecord() {
        return record;
    }

    @Override
    public String getProducerId() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(PRODUCER_ID_HEADER)) : null;
    }

    @Override
    public String getProducerVersion() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(PRODUCER_VERSION_HEADER)) : null;
    }

    @Override
    public String getIntermediateId() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(INTERMEDIATE_ID_HEADER)) : null;
    }

    @Override
    public String getIntermediateVersion() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(INTERMEDIATE_VERSION_HEADER)) : null;
    }

    @Override
    public String getSystem() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(SYSTEM_HEADER)) : null;
    }

    @Override
    public String getComponent() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(CLUSTER_HEADER)) : null;
    }

    @Override
    public String getInstance() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(INSTANCE_HEADER)) : null;
    }

    @Override
    public String getTenant() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(TENANT_HEADER)) : null;
    }

    @Override
    public String getEnvironment() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeStringHeader(headers.lastHeader(ENVIRONMENT_HEADER)) : null;
    }

    @Override
    public UUID getId() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeUuidHeader(headers.lastHeader(MESSAGE_ID_HEADER)) : null;
    }

    @Override
    public Long getSerializationTime() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeLongHeader(headers.lastHeader(SERIALIZATION_TIME_HEADER)) : null;
    }

    @Override
    public Long getDeserializationTime() {
        Headers headers = record.headers();
        return headers != null ? HeaderUtils.decodeLongHeader(headers.lastHeader(DESERIALIZATION_TIME_HEADER)) : null;
    }

    @Override
    public String getStream() {
        return record.topic();
    }

    @Override
    public int getPartition() {
        return record.partition();
    }

    @Override
    public long getOffset() {
        return record.offset();
    }

    @Override
    public long getTimestamp() {
        return record.timestamp();
    }

    @Override
    public K getKey() {
        return record.key();
    }

    @Override
    public V getValue() {
        return record.value();
    }
}
