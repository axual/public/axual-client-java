package io.axual.client.consumer.base;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import io.axual.client.config.BaseConsumerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.exception.NoExistingStreamException;
import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.switching.consumer.SwitchingConsumerConfig;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;

import static org.apache.kafka.clients.consumer.ConsumerConfig.*;

/**
 * Generic implementation of a message source, makes a connection to a stream and polls for
 * messages
 */
public abstract class BaseMessageSource<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(BaseMessageSource.class);

    private ConsumerProxy<K, V> consumer = null;

    private final ClientConfig clientConfig;
    private final BaseConsumerConfig<K, V> consumerConfig;
    private final CommitStrategy<K, V> commitStrategy;

    public BaseMessageSource(ClientConfig clientConfig, final BaseConsumerConfig<K, V> consumerConfig) {
        this.clientConfig = clientConfig;
        this.consumerConfig = consumerConfig;

        final Committer<K, V> committer = new Committer<K, V>() {
            private final AtomicLong lastCommitId = new AtomicLong(0);
            private Map<TopicPartition, OffsetAndMetadata> processed = new HashMap<>();

            @Override
            public void markAsProcessed(BaseMessage<K, V> message) {
                // Add offset to list of offsets to commit in the near future
                processed.put(
                        new TopicPartition(message.getRecord().topic(), message.getRecord().partition()),
                        new OffsetAndMetadata(message.getRecord().offset() + 1, "{\"strategy\":\"" + consumerConfig.getDeliveryStrategy().name() + "\"}"));
            }

            @Override
            public void commitProcessedOffsets(boolean synchronous) {
                if (!processed.isEmpty() && consumer != null) {
                    final long commitId = lastCommitId.incrementAndGet();
                    if (!synchronous) {
                        LOG.debug("Commit async: id={}, partition_count={}", commitId, processed.size());
                        consumer.commitAsync(processed, (map, e) -> {
                            if (e == null) {
                                LOG.debug("Commit successful: id={}", commitId);
                            } else {
                                LOG.debug("Commit not successful: id={}" ,commitId, e);
                            }
                        });
                    } else {
                        LOG.debug("Commit sync: id={}", commitId);
                        consumer.commitSync(processed);
                    }
                    processed.clear();
                }
            }
        };

        if (consumerConfig.getDeliveryStrategy() == DeliveryStrategy.AT_MOST_ONCE) {
            this.commitStrategy = new CommitStrategyAMO<>(committer);
        } else {
            this.commitStrategy = new CommitStrategyALO<>(committer);
        }
    }

    public BaseConsumerConfig<K, V> getConsumerConfig() {
        return consumerConfig;
    }

    public String getInfo() {
        return "stream = " + consumerConfig.getStream();
    }

    public List<BaseMessage<K, V>> getMessages() {
        if (consumer == null) {
            final Map<String, Object> configs = getConsumerConfigs();
            configs.put(AxualConsumerConfig.CHAIN_CONFIG, consumerConfig.getProxyChain());
            LOG.debug("Creating a new Axual consumer with properties: {}", configs);
            consumer = new AxualConsumer<>(configs);
            LOG.debug("Created a new Axual consumer");

            // Check if stream exist, throw Exception if not
            List<PartitionInfo> partitionInfo = consumer.partitionsFor(consumerConfig.getStream());
            if (partitionInfo == null || partitionInfo.isEmpty()) {
                throw new NoExistingStreamException("No partitions found for stream", consumerConfig.getStream());
            }

            consumer.subscribe(Collections.singletonList(consumerConfig.getStream()));
            LOG.debug("Subscribed consumer to stream: {}", consumerConfig.getStream());
        }

        ConsumerRecords<K, V> records = consumer.poll(Duration.ofMillis(100));

        List<BaseMessage<K, V>> result = new ArrayList<>(records.count());
        LOG.debug("Poll retrieved {} messages", records.count());
        for (ConsumerRecord<K, V> record : records) {
            result.add(new BaseMessage<>(record));
        }

        commitStrategy.onAfterFetchBatch(result);
        return result;
    }

    public void close() {
        commitStrategy.close();

        LOG.info("Closing the Kafka consumer");
        if (consumer != null) {
            consumer.close();
            consumer = null;
            LOG.info("Closed the Kafka consumer");
        } else {
            LOG.info("Kafka consumer was already closed");
        }
    }

    protected Map<String, Object> getConsumerConfigs() {
        Map<String, Object> result = KafkaUtil.getKafkaConfigs(clientConfig);

        //needed to convert the bytes of the key to a GenericRecord
        result.put(KEY_DESERIALIZER_CLASS_CONFIG, consumerConfig.getKeyDeserializer());
        //needed to convert the bytes of the value to a GenericRecord
        result.put(VALUE_DESERIALIZER_CLASS_CONFIG, consumerConfig.getValueDeserializer());
        //needed to be able to apply the correct strategy, instead of a random one
        result.put(ENABLE_AUTO_COMMIT_CONFIG, "false");

        if (consumerConfig.getDeliveryStrategy() == DeliveryStrategy.AT_LEAST_ONCE) {
            // Jump to the earliest offset to prevent missing message(s) in case an error occurs.
            result.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
            result.put(SwitchingConsumerConfig.DELIVERY_STRATEGY_ON_SWITCH, DeliveryStrategy.AT_LEAST_ONCE.toString());
        } else {
            result.put(AUTO_OFFSET_RESET_CONFIG, "latest");
            result.put(SwitchingConsumerConfig.DELIVERY_STRATEGY_ON_SWITCH, DeliveryStrategy.AT_MOST_ONCE.toString());
        }

        //set a max on the records returned, lower value means more overhead, but also more time to
        // process, and less duplication in case of error.
        // If maximumPollSize is not set, use consumerStrategy default
        result.put(MAX_POLL_RECORDS_CONFIG, consumerConfig.getMaximumPollSize().toString());
        // Recommended Azure configurations from: https://docs.microsoft.com/en-us/azure/event-hubs/apache-kafka-configurations
        // Azure closes inbound TCP idle > 240_000 ms (4min), Kafka defaults at 540_000ms (9min).
        result.put(CONNECTIONS_MAX_IDLE_MS_CONFIG, "180000");
        result.put(METADATA_MAX_AGE_CONFIG, "180000");

        return result;
    }

    public void onAfterProcessBatch() {
        commitStrategy.onAfterProcessBatch();
    }

    public void onAfterProcessMessage(BaseMessage<K, V> message, Throwable error) {
        commitStrategy.onAfterProcessMessage(message, error);
    }
}
