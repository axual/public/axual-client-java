package io.axual.client.consumer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.config.BaseConsumerConfig;
import io.axual.client.consumer.base.BaseMessageSource;
import io.axual.common.config.ClientConfig;

public class GenericMessageSource<K, V> extends BaseMessageSource<K, V> {
    public GenericMessageSource(ClientConfig clientConfig, BaseConsumerConfig<K, V> consumerConfig) {
        super(clientConfig, consumerConfig);
    }
}
