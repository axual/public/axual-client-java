package io.axual.client.consumer.base;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.client.config.BaseConsumerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.Processor;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.exception.RetriableException;
import io.axual.client.exception.SkippableException;
import io.axual.client.janitor.Janitor;
import io.axual.common.tools.ExecutorUtil;
import io.axual.common.tools.SleepUtil;

/**
 * Generic consumer class, the strategy is implemented in the used source
 */
public abstract class BaseConsumer<K, V> extends Janitor.ManagedCloseable implements Consumer<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(BaseConsumer.class);
    private static final String NEWLINE = "\n";
    private static final String LOG_ID = "ID: {}" + NEWLINE;
    private static final String LOG_KEY = "KEY: {}" + NEWLINE;
    private static final String LOG_VALUE = "VALUE: {}" + NEWLINE;

    private final BaseMessageSource<K, V> messageSource;
    private final Processor<K, V> processor;
    private final AtomicBoolean consumeThreadRunning = new AtomicBoolean(false);
    private final AtomicBoolean stopConsumer = new AtomicBoolean(false);
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Future<ConsumeFailedException> consumeResult = null;

    public BaseConsumer(BaseMessageSource<K, V> messageSource, Processor<K, V> processor) {
        this.messageSource = messageSource;
        this.processor = processor;
        LOG.info("Created consumer with source of class: {}.", messageSource.getClass().getName());
    }

    @Override
    public BaseConsumerConfig<K, V> getConfig() {
        return messageSource.getConsumerConfig();
    }

    /**
     * This method starts the actual consumeThreadRunning, it can be stopped by calling
     * stopconsuming()
     *
     * @return whether or not it was successfully started
     */
    @Override
    public Future<ConsumeFailedException> startConsuming() {
        if (consumeThreadRunning.compareAndSet(false, true)) {
            consumeResult = executorService.submit(() -> {
                try {
                    return consumeLoop();
                } catch (ConsumeFailedException e) {
                    return e;
                } finally {
                    consumeThreadRunning.set(false);
                }
            });
            return consumeResult;
        } else {
            LOG.warn("There is already a process running, no new process is started.");
            return null;
        }
    }

    public ConsumeFailedException stopConsuming() {
        stopConsumer.set(true);

        if (consumeResult != null && consumeThreadRunning.get()) {
            return getConsumeResult();
        }
        return null;
    }

    @Override
    public boolean isConsuming() {
        return consumeThreadRunning.get();
    }

    @Override
    public ConsumeFailedException getConsumeResult() {
        if (consumeResult != null) {
            try {
                return consumeResult.get();
            } catch (CancellationException e) {
                return null;
            } catch (Exception e) {
                return new ConsumeFailedException(e);
            }
        }
        return null;
    }

    public ConsumeFailedException consumeLoop() {
        // Wait for consumeResult to be initialized
        while (consumeResult == null) {
            SleepUtil.sleep(Duration.ofMillis(50));
        }

        while (!stopConsumer.get()) {
            try {
                List<BaseMessage<K, V>> messages = messageSource.getMessages();
                LOG.trace("Fetched {} messages", messages.size());

                try {
                    for (BaseMessage<K, V> message : messages) {
                        handleMessage(message);
                        if (stopConsumer.get()) {
                            LOG.warn("Consumer requested to stop, exiting batch processing loop");
                            break;
                        }
                    }
                } finally {
                    messageSource.onAfterProcessBatch();
                    LOG.trace("Processed message batch");
                }
            } catch (Exception e) {
                LOG.error("Error processing message batch", e);
                return new ConsumeFailedException(e, messageSource.getInfo());
            }
        }

        return null;
    }

    private void handleMessage(BaseMessage<K, V> message) {
        int tryCount = 0;

        boolean success = false;
        while (!success && !stopConsumer.get()) {
            try {
                tryCount++;
                LOG.trace("Processing message (try #{}):\n" + LOG_ID + LOG_KEY + LOG_VALUE,
                        tryCount,
                        message.getId(),
                        message.getKey(),
                        message.getValue());
                processor.processMessage(message);
                LOG.debug("Message successfully processed");
                messageSource.onAfterProcessMessage(message, null);
                success = true;
            } catch (RetriableException e) {
                if(LOG.isTraceEnabled()) {
                    LOG.warn("Message could not be processed, retrying in {} ms.\n" + LOG_ID + LOG_KEY + LOG_VALUE,
                            e.getSleepTime(),
                            message.getId(),
                            message.getKey(),
                            message.getValue());
                }else{
                    LOG.warn("Message could not be processed, retrying in {} ms.\n" + LOG_ID,
                            e.getSleepTime(),
                            message.getId());
                }
                messageSource.onAfterProcessMessage(message, e);
                SleepUtil.sleep(e.getSleepTime());
            } catch (SkippableException e) {
                if(LOG.isTraceEnabled()) {
                    LOG.warn("Skipping unprocessable message:\n" + LOG_ID + LOG_KEY + LOG_VALUE,
                            message.getId(),
                            message.getKey(),
                            message.getValue());
                }else{
                    LOG.warn("Skipping unprocessable message:\n" + LOG_ID,
                            message.getId());
                }
                messageSource.onAfterProcessMessage(message, e);
                success = true;
            } catch (Exception e) {
                if(LOG.isTraceEnabled()) {
                    LOG.error("Message could not be processed:\n" + LOG_ID + LOG_KEY + LOG_VALUE + "EXCEPTION: {}",
                            message.getId(),
                            message.getKey(),
                            message.getValue(),
                            e);
                }else{
                    LOG.error("Message could not be processed:\n" + LOG_ID + "EXCEPTION: {}",
                            message.getId(),
                            e);
                }
                messageSource.onAfterProcessMessage(message, e);
                throw new ConsumeFailedException(e);
            }
        }
    }

    @Override
    public void close() {
        ConsumeFailedException consumeException = stopConsuming();
        ExecutorUtil.terminateExecutor(executorService, Duration.ofSeconds(10));
        try {
            messageSource.close();
        } catch (Exception e) {
            LOG.error("Error during closing of the message source", e);
        }
        super.close();
        if (consumeException != null) {
            throw consumeException;
        }
    }
}
