package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.exception.ClientException;

/**
 * Exception that signals processing a message failed, and it should not be retried.
 */
public class SkippableException extends ClientException {
    /**
     * Creates a new RetriableException with specified sleeptime
     *
     * @param cause the original error
     */
    public SkippableException(Throwable cause) {
        super(String.format("Error during processing of message, won't retry.%nOriginal error: %s", cause.getMessage()), cause);
    }
}
