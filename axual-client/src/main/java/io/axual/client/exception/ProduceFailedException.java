package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.exception.ClientException;

/**
 * Error specifically used when there is an error sending an record
 */
public class ProduceFailedException extends ClientException {
    private static final String MESSAGE_FORMAT =
            "An error occurred trying to send record. Time in millis until error occurred: %d";

    private final Object key;
    private final Object value;

    /**
     * Creates a new ProduceFailedException
     *
     * @param key               the key of the produced message
     * @param value             the value of the produced message
     * @param produceTime       the produce time
     * @param originalException the original exception
     */
    public ProduceFailedException(Object key, Object value, long produceTime, Exception originalException) {
        super(String.format(MESSAGE_FORMAT, System.currentTimeMillis() - produceTime),
                originalException);
        this.key = key;
        this.value = value;
    }

    public Object getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
