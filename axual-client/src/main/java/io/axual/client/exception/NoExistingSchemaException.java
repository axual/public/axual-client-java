package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.exception.ClientException;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

/**
 * Error specifically used when there is an trying to connect to a master schema registry, because
 * we disallow access to the master by design this causes the error message to be more informative.
 */
public final class NoExistingSchemaException extends ClientException {

    public static final int REQUEST_FORWARDING_FAILED_ERROR_CODE = 50003;
    public static final int UNKNOWN_MASTER_ERROR_CODE = 50004;
    private static final String KEY_FORMAT = "%s/subjects/%s-key/versions/latest";
    private static final String VALUE_FORMAT = "%s/subjects/%s-value/versions/latest";

    /**
     * Creates a new NoExistingSchemaException
     */
    private NoExistingSchemaException(String urlKey, String urlValue, Throwable cause) {
        super(String.format("The used schema was not found on the stream, please check at %s (for the key) or %s (for the value)if you use the latest version of the schema.", urlKey, urlValue), cause);
    }

    public static void handleException(Exception e, String stream, String srUrls) {
        Throwable cause = e.getCause();
        if (cause instanceof RestClientException) {
            RestClientException restClientException = (RestClientException) cause;
            int errorCode = restClientException.getErrorCode();
            if (errorCode == REQUEST_FORWARDING_FAILED_ERROR_CODE || errorCode == UNKNOWN_MASTER_ERROR_CODE) {
                String keyUrl = getUrl(srUrls, stream, KEY_FORMAT);
                String valueUrl = getUrl(srUrls, stream, VALUE_FORMAT);
                throw new NoExistingSchemaException(keyUrl, valueUrl, e);
            }
        }
    }

    private static String getUrl(String srUrls, String stream, String format) {
        if (srUrls == null) {
            return null;
        }

        int index = srUrls.indexOf(',');
        String baseUrl = index < 0 ? srUrls : srUrls.substring(0, index);
        return String.format(format, baseUrl, stream);
    }
}
