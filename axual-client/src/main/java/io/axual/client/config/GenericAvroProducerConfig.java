package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.common.serialization.Serializer;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.ClientException;
import io.axual.serde.avro.GenericAvroSerializer;

/**
 * Use this ProducerConfig to produce Generic Avro objects to a stream. This config is ideal for
 * generic producing solutions where specific Avro object are not required.
 */
@InterfaceStability.Evolving
public class GenericAvroProducerConfig extends BaseAvroProducerConfig<GenericRecord, GenericRecord> {
    private GenericAvroProducerConfig(Builder<?> builder) {
        super(builder);
    }

    /**
     * The Builder can be used to set the configuration options, except for the serializers.
     *
     * @return a new instance of builder
     */
    public static Builder<?> builder() {
        return new Builder<>();
    }

    /**
     * The Builder is used to set the configuration options.
     */
    public static class Builder<T extends Builder<T>> extends BaseAvroProducerConfig.Builder<GenericRecord, GenericRecord, T> {
        /**
         * Instantiates a new Builder.
         */
        Builder() {
            super.setKeySerializer(GenericAvroSerializer.class.getName());
            super.setValueSerializer(GenericAvroSerializer.class.getName());
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keySerializerClassName The configured key serializer
         * @return the {@link GenericAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeySerializer(String keySerializerClassName) {
            throw new ClientException("Not allowed to override key serializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keySerializer The configured key serializer
         * @return the {@link GenericAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeySerializer(Serializer<GenericRecord> keySerializer) {
            throw new ClientException("Not allowed to override key serializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueSerializerClassName The configured value serializer class name
         * @return the {@link GenericAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueSerializer(String valueSerializerClassName) {
            throw new ClientException("Not allowed to override value serializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueSerializer The configured value serializer
         * @return the {@link GenericAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueSerializer(Serializer<GenericRecord> valueSerializer) {
            throw new ClientException("Not allowed to override value serializer");
        }

        @Override
        public GenericAvroProducerConfig build() {
            validate();
            return new GenericAvroProducerConfig(this);
        }
    }
}
