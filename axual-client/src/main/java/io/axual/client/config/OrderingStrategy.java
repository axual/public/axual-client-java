package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

public enum OrderingStrategy {
    /**
     * The LOSING_ORDER strategy allows multiple message batches to be sent to the server in
     * parallel, causing them to overtake each other.
     */
    LOSING_ORDER,
    /**
     * The KEEPING_ORDER strategy only allows one message batch to be sent to the server at the same
     * time, preventing messages from overtaking each other. This strategy mandates producers to
     * wait DistributorTimeout when switching to another cluster.
     */
    KEEPING_ORDER
}
