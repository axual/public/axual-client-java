package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.common.serialization.Deserializer;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.ClientException;
import io.axual.serde.avro.GenericAvroDeserializer;

/**
 * Use this ConsumerConfig to consume Generic Avro objects from a stream. This config is ideal for
 * generic consuming solutions where specific Avro object are not required.
 */
@InterfaceStability.Evolving
public class GenericAvroConsumerConfig extends BaseAvroConsumerConfig<GenericRecord, GenericRecord> {
    private GenericAvroConsumerConfig(Builder<?> builder) {
        super(builder);
    }

    /**
     * The Builder can be used to set the configuration options, except for the deserializers.
     *
     * @return a new instance of builder
     */
    public static Builder<?> builder() {
        return new Builder();
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<T extends Builder<T>> extends BaseAvroConsumerConfig.Builder<GenericRecord, GenericRecord, T> {
        public Builder() {
            super.setKeyDeserializer(GenericAvroDeserializer.class.getName());
            super.setValueDeserializer(GenericAvroDeserializer.class.getName());
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keyDeserializerClassName The configured key deserializer class name
         * @return the {@link GenericAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeyDeserializer(String keyDeserializerClassName) {
            throw new ClientException("Not allowed to override key deserializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keyDeserializer The configured key deserializer
         * @return the {@link GenericAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeyDeserializer(Deserializer<GenericRecord> keyDeserializer) {
            throw new ClientException("Not allowed to override key deserializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueDeserializerClassName The configured value deserializer class name
         * @return the {@link GenericAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueDeserializer(String valueDeserializerClassName) {
            throw new ClientException("Not allowed to override value deserializer");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueDeserializer The configured value deserializer
         * @return the {@link GenericAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueDeserializer(Deserializer<GenericRecord> valueDeserializer) {
            throw new ClientException("Not allowed to override value deserializer");
        }

        @Override
        public GenericAvroConsumerConfig build() {
            validate();
            return new GenericAvroConsumerConfig(this);
        }
    }
}
