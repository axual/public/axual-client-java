package io.axual.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.GenericAvroConsumerConfig;
import io.axual.client.config.GenericAvroProducerConfig;
import io.axual.client.config.ProducerConfig;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.Processor;
import io.axual.client.consumer.avro.AvroConsumer;
import io.axual.client.consumer.avro.AvroMessageSource;
import io.axual.client.consumer.generic.GenericConsumer;
import io.axual.client.consumer.generic.GenericMessageSource;
import io.axual.client.janitor.Janitor;
import io.axual.client.janitor.TemporaryFile;
import io.axual.client.producer.Producer;
import io.axual.client.producer.avro.GenericAvroProducer;
import io.axual.client.producer.generic.GenericProducer;
import io.axual.client.producer.generic.ProducerWorkerManager;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.ResourceUtil;

/**
 * The AxualClient class represents a client application's connection to Axual Platform. Through
 * this class the application can create Producers and Consumers to send/receive messages. The
 * general assumption is that an application only requires one instance of this class, which is
 * configured by passing a {@link ClientConfig} object to the constructor. The configuration passed
 * in is automatically applied to all created Producers and Consumers.
 */
public class AxualClient implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(AxualClient.class);

    private final ClientConfig config;
    private final Janitor janitor = new Janitor();
    private final ProducerWorkerManager producerWorkerManager = new ProducerWorkerManager();

    /**
     * Instantiates a new Axual client with given configuration.
     *
     * @param config the configuration of the application's connection to Axual Platform.
     */
    public AxualClient(final ClientConfig config) {
        final SslConfig.Builder sslConfigBuilder = SslConfig.newBuilder(config.getSslConfig());
        if (!config.getDisableTemporarySecurityFile()) {
            if (sslConfigBuilder.getKeystoreLocation() != null) {
                LOG.debug("Creating temporary keystore file.");
                sslConfigBuilder.setKeystoreLocation(createTmpKeystore(config));
            }
            if (sslConfigBuilder.getTruststoreLocation() != null) {
                LOG.debug("Creating temporary truststore file.");
                sslConfigBuilder.setTruststoreLocation(createTmpTruststore(config));
            }
        }

        this.config = ClientConfig.newBuilder(config)
                .setSslConfig(sslConfigBuilder.build())
                .build();
    }

    /**
     * Gets the client configuration.
     *
     * @return the configuration
     */
    public ClientConfig getConfig() {
        return config;
    }

    /**
     * Build a new Consumer with any key/value type.
     *
     * @param <K>            the key type of consumed messages
     * @param <V>            the value type of consumed messages
     * @param consumerConfig the consumer configuration
     * @param processor      the processor that receives callbacks for every message received
     * @return the consumer
     */
    public <K, V> Consumer<K, V> buildConsumer(ConsumerConfig<K, V> consumerConfig, Processor<K, V> processor) {
        return janitor.register(new GenericConsumer<>(new GenericMessageSource<>(config, consumerConfig), processor));
    }

    /**
     * Build a new Consumer that consumes generic Avro messages.
     *
     * @param consumerConfig the consumer configuration
     * @param processor      the processor that receives callbacks for every message received
     * @return the consumer
     */
    public Consumer<GenericRecord, GenericRecord> buildConsumer(GenericAvroConsumerConfig consumerConfig, Processor<GenericRecord, GenericRecord> processor) {
        return janitor.register(new AvroConsumer<>(new AvroMessageSource<>(config, consumerConfig), processor));
    }

    /**
     * Build a new Consumer that consumes specific Avro messages.
     *
     * @param <K>            the specific key type of consumed Avro messages
     * @param <V>            the specific value type of consumed Avro messages
     * @param consumerConfig the consumer configuration
     * @param processor      the processor that receives callbacks for every message received
     * @return the consumer
     */
    public <K extends SpecificRecord, V extends SpecificRecord> Consumer<K, V> buildConsumer(SpecificAvroConsumerConfig<K, V> consumerConfig, Processor<K, V> processor) {
        return janitor.register(new AvroConsumer<>(new AvroMessageSource<>(config, consumerConfig), processor));
    }

    /**
     * Build a new Producer with any key/value type.
     *
     * @param <K>            the key type of produced messages
     * @param <V>            the value type of produced messages
     * @param producerConfig An instance of {@link ProducerConfig} containing the necessary
     *                       configuration to create this producer
     * @return the producer
     */
    public <K, V> Producer<K, V> buildProducer(final ProducerConfig<K, V> producerConfig) {
        return janitor.register(new GenericProducer<>(config, producerConfig, producerWorkerManager));
    }

    /**
     * Build a new Producer that produces generic Avro messages.
     *
     * @param producerConfig An instance of {@link GenericAvroProducerConfig} containing the
     *                       necessary configuration to create this producer
     * @return the producer
     */
    public Producer<GenericRecord, GenericRecord> buildProducer(final GenericAvroProducerConfig producerConfig) {
        return janitor.register(new GenericAvroProducer<>(config, producerConfig, producerWorkerManager));
    }

    /**
     * Build a new Producer that produces specific Avro messages.
     *
     * @param <K>            the specific key type of produced Avro messages
     * @param <V>            the specific value type of produced Avro messages
     * @param producerConfig An instance of {@link SpecificAvroProducerConfig} containing the
     *                       necessary configuration to create this producer
     * @return {@link Producer}
     */
    public <K extends SpecificRecord, V extends SpecificRecord> Producer<K, V> buildProducer(final SpecificAvroProducerConfig<K, V> producerConfig) {
        return janitor.register(new GenericAvroProducer<>(config, producerConfig, producerWorkerManager));
    }

    @Override
    public void close() {
        LOG.info("Closing AxualClient {}", config.getApplicationId());
        producerWorkerManager.close();
        janitor.close();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of AxualClient
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private String createTmpKeystore(final ClientConfig original) {
        TemporaryFile tmpKeystore = new TemporaryFile(
                ResourceUtil.getResourceAsFile(original.getSslConfig().getKeystoreLocation(), original.getTempDir())
        );
        return janitor.register(tmpKeystore, false).getFilename();
    }

    private String createTmpTruststore(final ClientConfig original) {
        TemporaryFile tmpTruststore = new TemporaryFile(
                ResourceUtil.getResourceAsFile(original.getSslConfig().getTruststoreLocation(), original.getTempDir())
        );
        return janitor.register(tmpTruststore, false).getFilename();
    }
}
