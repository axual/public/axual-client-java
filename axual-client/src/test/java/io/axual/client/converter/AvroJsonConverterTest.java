package io.axual.client.converter;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.Test;

import io.axual.client.exception.SchemaResolveException;
import io.axual.client.exception.SerializationException;
import io.axual.client.test.AccountId;
import io.axual.client.test.IBAN;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AvroJsonConverterTest {
    private static final String accountIdJson = "{\"id\":{\"io.axual.client.test.IBAN\":{\"accountNumber\":\"NL11ASNB0708054404\"}}}";
    private static final AccountId accountIdObject = AccountId.newBuilder()
            .setId(IBAN.newBuilder()
                    .setAccountNumber("NL11ASNB0708054404")
                    .build())
            .build();

    @Test
    public void fromRecord_correctValue() throws SerializationException {
        final AvroJsonConverter jsonConverter = new AvroJsonConverter(accountIdObject.getSchema());
        assertThat(jsonConverter.convertFrom(accountIdObject), is(accountIdJson));
    }

    @Test
    public void toSpecific_correctValue() throws SerializationException {
        final AvroJsonConverter jsonConverter = new AvroJsonConverter(accountIdObject.getSchema());
        assertThat(jsonConverter.toSpecific(accountIdJson, AccountId.class), is(accountIdObject));
    }

    @Test(expected = SerializationException.class)
    public void toSpecific_should_return_exception_on_invalid_json() throws SerializationException {
        final AvroJsonConverter jsonConverter = new AvroJsonConverter(accountIdObject.getSchema());
        jsonConverter.toSpecific("{", AccountId.class);
    }

    @Test
    public void toGeneric_correctValue() throws SchemaResolveException, SerializationException {
        final AvroJsonConverter jsonConverter = new AvroJsonConverter(accountIdObject.getSchema());

        final GenericRecord accountIdGeneric = new GenericData.Record(AccountId.SCHEMA$);
        final GenericRecord iban = new GenericData.Record(IBAN.SCHEMA$);
        iban.put("accountNumber", "NL11ASNB0708054404");
        accountIdGeneric.put("id", iban);

        assertThat(jsonConverter.convertTo(accountIdJson), is(accountIdGeneric));
    }

    @Test(expected = SerializationException.class)
    public void toGeneric_should_return_exception_on_invalid_json() throws SchemaResolveException, SerializationException {
        final AvroJsonConverter jsonConverter = new AvroJsonConverter(accountIdObject.getSchema());

        jsonConverter.convertTo("{");
    }
}
