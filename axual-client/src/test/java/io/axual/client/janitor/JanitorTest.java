package io.axual.client.janitor;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import java.io.IOException;

import io.axual.client.consumer.base.BaseConsumer;
import io.axual.client.producer.generic.GenericProducer;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class JanitorTest {
    @Test
    public void testClose_shouldCallCloseOnAllManagedObjects() throws Exception {
        Janitor janitor = new Janitor();
        BaseConsumer<String, String> consumer = mock(BaseConsumer.class);
        GenericProducer<String, String> producer = mock(GenericProducer.class);

        janitor.register(consumer);
        janitor.register(producer);
        janitor.close();
        verify(consumer, times(1)).close();
        verify(producer, times(1)).close();
    }

    @Test
    public void testRegisterWithExceptionThrowingObject() throws Exception {
        Janitor janitor = new Janitor();
        BaseConsumer<String, String> consumer = mock(BaseConsumer.class);

        doThrow(IOException.class).when(consumer).close();
        janitor.register(consumer);

        janitor.close();

        verify(consumer, times(1)).close();
    }
}
