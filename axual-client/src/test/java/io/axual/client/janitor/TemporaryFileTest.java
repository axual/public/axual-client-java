package io.axual.client.janitor;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import io.axual.common.test.TestUtils;

public class TemporaryFileTest {

    private final Logger LOG = LoggerFactory.getLogger(TemporaryFileTest.class);

    @Test
    public void testTemporaryFile() throws IOException{
        File file = null;
        String tempFileName = "temporaryFile.txt";
        try {
            //create temporary
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir", "axual-temp-file.txt");
            int fileListCount = file.listFiles().length;
            TemporaryFile temporaryFile = null;
            try {
                temporaryFile = new TemporaryFile(TestUtils.createFile(file.getAbsolutePath(), tempFileName).getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Error in test of TemporaryFile; Reason: " + e.getMessage(), e);
            }
            File f = new File(temporaryFile.getFilename());
            Assert.assertTrue(temporaryFile != null && f.exists());
            Assert.assertEquals("Temporary file name should be same", tempFileName, f.getName());
            temporaryFile.close();
            Assert.assertFalse("Temporary file should delete after resource cleanup", f.exists());
            Assert.assertEquals(fileListCount, file.listFiles().length);
        } finally {
            // delete temp dir
            if (file != null) {
                try {
                    TestUtils.deleteDirectory(file);
                } catch (IOException e) {
                    LOG.error("Error while deleting directory: {}",file.getAbsolutePath(), e);
                }
            }
        }


    }

}
