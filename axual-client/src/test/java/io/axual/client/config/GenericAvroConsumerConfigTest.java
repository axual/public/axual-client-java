package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import io.axual.common.exception.ClientException;

import static org.junit.Assert.assertEquals;

public class GenericAvroConsumerConfigTest {

    @Test(expected = ClientException.class)
    public void testOverridingKeyDeserializerClass_ShouldThrowAnException() {
        GenericAvroConsumerConfig.Builder builder = new GenericAvroConsumerConfig.Builder();
        builder
                .setKeyDeserializer("dummy-key")
                .build();
    }

    @Test(expected = ClientException.class)
    public void testOverridingValueDeserializerClass_ShouldThrowAnException() {
        GenericAvroConsumerConfig.Builder builder = new GenericAvroConsumerConfig.Builder();
        builder
                .setValueDeserializer("dummy-value")
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildConsumerConfigWithoutSettingRequiredProperties_ShouldThrowAnException() {
        GenericAvroConsumerConfig.builder()
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildConsumerConfigWithoutSettingStream_ShouldThrowAnException() {
        GenericAvroConsumerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();
    }

    @Test
    public void testBuildConsumerConfig_ShouldReturnSetOptions() {
        DeliveryStrategy expectedStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        String expectedStream = "general-stream";
        GenericAvroConsumerConfig consumerConfig = GenericAvroConsumerConfig.builder()
                .setStream(expectedStream)
                .setDeliveryStrategy(expectedStrategy)
                .build();
        assertEquals(expectedStrategy, consumerConfig.getDeliveryStrategy());
        assertEquals(expectedStream, consumerConfig.getStream());
    }
}
