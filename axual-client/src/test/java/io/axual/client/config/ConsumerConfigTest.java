package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConsumerConfigTest {
    @Test(expected = IllegalStateException.class)
    public void testBuildGenericConsumerConfigWithoutSettingStrategy_ShouldThrowAnException() {
        ConsumerConfig.builder().build();
    }

    @Test
    public void testBuildGenericConsumerConfigWithoutSettingStream_ShouldReturnSetOptions() {
        final String dummyClass = "io.axual.dummy.class";
        final String dummyStream = "generic-dummy";

        DeliveryStrategy expectedStrategy = DeliveryStrategy.AT_LEAST_ONCE;

        ConsumerConfig consumerConfig = ConsumerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setKeyDeserializer(dummyClass)
                .setValueDeserializer(dummyClass)
                .setStream(dummyStream)
                .build();
        assertEquals(expectedStrategy, consumerConfig.getDeliveryStrategy());
        assertEquals(dummyClass, consumerConfig.getKeyDeserializer());
        assertEquals(dummyClass, consumerConfig.getValueDeserializer());
        assertEquals(dummyStream, consumerConfig.getStream());
    }

    @Test
    public void testBuildGenericConsumerConfig_ShouldReturnSetOptions() {
        final String dummyKey = "io.axual.dummy.class.key";
        final String dummyValue = "io.axual.dummy.class.value";
        final String dummyStream = "generic-dummy";

        final Integer dummyMaxPollSize = 10;

        ConsumerConfig consumerConfig = ConsumerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setKeyDeserializer(dummyKey)
                .setValueDeserializer(dummyValue)
                .setStream(dummyStream)
                .setMaximumPollSize(dummyMaxPollSize)
                .build();

        assertEquals(dummyMaxPollSize, consumerConfig.getMaximumPollSize());
    }
}
