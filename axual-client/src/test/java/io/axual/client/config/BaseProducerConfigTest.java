package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serializer;
import org.junit.Test;

public class BaseProducerConfigTest {

    @Test(expected = IllegalStateException.class)
    public void testBuildBaseProducerConfigWithoutSettingKey_ShouldThrowAnException() {
        BaseProducerConfig.Builder<String, String, ?> builder = new BaseProducerConfig.Builder<>();
        builder
                .setDeliveryStrategy(DeliveryStrategy.AT_MOST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .setKeySerializer((Serializer<String>) null)
                .validate();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildBaseProducerConfigWithoutSettingValue_ShouldThrowAnException() {
        BaseProducerConfig.Builder<String, String, ?> builder = new BaseProducerConfig.Builder<>();
        builder
                .setDeliveryStrategy(DeliveryStrategy.AT_MOST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .setKeySerializer("io.axual.dummy.key")
                .setValueSerializer((Serializer<String>) null)
                .validate();
    }
}
