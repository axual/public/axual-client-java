package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

public class BaseConsumerConfigTest {

    @Test(expected = IllegalStateException.class)
    public void testBuildBaseConsumerConfigWithoutSettingKey_ShouldThrowAnException() {
        BaseConsumerConfig.Builder builder = new BaseConsumerConfig.Builder();
        builder
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setStream("general-dummy")
                .setKeyDeserializer("")
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildBaseConsumerConfigWithoutSettingValue_ShouldThrowAnException() {
        BaseConsumerConfig.Builder builder = new BaseConsumerConfig.Builder();
        builder.setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setStream("general-dummy")
                .setKeyDeserializer("io.axual.dummy.key")
                .setValueDeserializer("")
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildConfigWithWrongMaximumPollSize_ShouldThrowAnException() {
        BaseConsumerConfig.Builder builder = new BaseConsumerConfig.Builder();
        builder.setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setStream("general-dummy")
                .setKeyDeserializer("io.axual.dummy.key")
                .setValueDeserializer("")
                .setMaximumPollSize(501)
                .build();
    }
}
