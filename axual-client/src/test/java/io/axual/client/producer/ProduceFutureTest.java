package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.axual.client.producer.generic.GenericProducedMessage;
import io.axual.client.producer.generic.ProduceFuture;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RecordMetadata.class)
public class ProduceFutureTest {
    @Test
    public void testDefaultStatus() {
        ProduceFuture produceFuture = new ProduceFuture();
        assertFalse(produceFuture.isCancelled());
        assertFalse(produceFuture.isDone());
        assertNull(produceFuture.getProducedMessage());
    }

    @Test(expected = TimeoutException.class)
    public void testTimeoutException() throws InterruptedException, ExecutionException, TimeoutException {
        ProduceFuture produceFuture = new ProduceFuture();
        produceFuture.get(100, TimeUnit.MILLISECONDS);
    }

    @Test(expected = ExecutionException.class)
    public void testProduceException() throws ExecutionException, InterruptedException {
        final ProduceFuture<String, String> produceFuture = new ProduceFuture<>();

        final ProducerMessage<String, String> producerMessage = ProducerMessage.<String, String>newBuilder()
                .setKey("Key")
                .setMessageId(UUID.randomUUID())
                .setTimestamp(System.currentTimeMillis())
                .setStream("Stream")
                .setValue("Value").build();

        final TestRunnerProduceFuture<String, String> runner = new TestRunnerProduceFuture<>(produceFuture, producerMessage);

        final Thread runnerThread = new Thread(runner);

        final Thread commandThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runner.setCommand(Command.CREATE_EXCEPTION);
        });

        runnerThread.start();
        commandThread.start();

        ProducedMessage messageMetadata = produceFuture.get();
        assertNotNull(messageMetadata);

        runner.setCommand(Command.STOP);
        runnerThread.join();
        commandThread.join();
    }

    @Test
    public void testProduceComplete() throws ExecutionException, InterruptedException {
        final ProduceFuture produceFuture = new ProduceFuture();
        final ProducerMessage<String, String> producerMessage = ProducerMessage.<String, String>newBuilder()
                .setKey("Key")
                .setMessageId(UUID.randomUUID())
                .setTimestamp(System.currentTimeMillis())
                .setStream("Stream")
                .setValue("Value").build();

        final TestRunnerProduceFuture<String, String> runner = new TestRunnerProduceFuture<>(produceFuture, producerMessage);

        final Thread runnerThread = new Thread(runner);

        final Thread commandThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runner.setCommand(Command.CREATE_SUCCESS);
        });

        runnerThread.start();
        commandThread.start();

        ProducedMessage messageMetadata = produceFuture.get();
        assertNotNull(messageMetadata);

        runner.setCommand(Command.STOP);
        runnerThread.join();
        commandThread.join();
    }

    public enum Command {
        STOP,
        CREATE_EXCEPTION,
        CREATE_SUCCESS,
        CONTINUE
    }

    class TestRunnerProduceFuture<K, V> implements Runnable {
        protected ProduceFuture<K, V> produceFuture;
        protected Command command = Command.CONTINUE;
        protected RecordMetadata mockedMetadata;
        protected ProducerMessage<K, V> message;

        public void setCommand(Command newCommand) {
            synchronized (this) {
                this.command = newCommand;
            }
        }

        TestRunnerProduceFuture(ProduceFuture<K, V> produceFuture, ProducerMessage<K, V> message) {
            this.produceFuture = produceFuture;
            this.message = message;
            this.mockedMetadata = PowerMockito.mock(RecordMetadata.class);
            when(mockedMetadata.offset()).thenReturn(1L);
            when(mockedMetadata.partition()).thenReturn(0);
            when(mockedMetadata.topic()).thenReturn("geen");
            when(mockedMetadata.timestamp()).thenReturn(System.currentTimeMillis());
            when(mockedMetadata.toString()).thenReturn("cs");
            when(mockedMetadata.serializedKeySize()).thenReturn(1234);
            when(mockedMetadata.serializedValueSize()).thenReturn(12345);
        }

        public void run() {
            boolean keepRunning = true;

            while (keepRunning) {
                Command currentCommand = null;
                synchronized (this) {
                    currentCommand = command;
                }
                switch (currentCommand) {
                    case STOP:
                        keepRunning = false;
                        break;
                    case CREATE_EXCEPTION:
                        produceFuture.complete(new ExecutionException(new Exception("Thrown something")));
                        keepRunning = false;
                        break;
                    case CREATE_SUCCESS:
                        ProducedMessage<K, V> produced = new GenericProducedMessage<K, V>(message, mockedMetadata);
                        produceFuture.complete(produced);
                        keepRunning = false;
                        break;
                    case CONTINUE:
                        break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
