package io.axual.platform.test.jupiter;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-jupiter
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;

/**
 * Test with an Axual Test Platform using one cluster
 */
@InterfaceStability.Evolving
public class SingleClusterPlatformUnit extends
    BasePlatformUnit<SingleClusterPlatformUnit> {

    /**
     * Constructs a default test platform with a single cluster
     */
    public SingleClusterPlatformUnit() {
        super(new PlatformUnit(1, false));
    }

    /**
     * Constructs a test platform with a single cluster according to the provided instance and cluster
     * configuration
     *
     * @param instanceConfig The instance configuration to use
     * @param clusterConfig  The configuration for the first cluster
     */
    public SingleClusterPlatformUnit(InstanceUnitConfig instanceConfig,
        ClusterUnitConfig clusterConfig) {
        super(new PlatformUnit(instanceConfig, clusterConfig));
    }

    /**
     * Constructs a default test platform with a single cluster using the provided topic and group
     * patterns
     *
     * @param topicPattern the Topic pattern to use for the first cluster
     * @param groupPattern the Group pattern to use for the first cluster
     */
    public SingleClusterPlatformUnit(String topicPattern, String groupPattern) {
        super(new PlatformUnit(new String[]{topicPattern}, new String[]{groupPattern}));
    }

    /**
     * Get the cluster controller
     *
     * @return the cluster controller
     */
    public ClusterUnit cluster() {
        return platform().getCluster(0);
    }
}
