package io.axual.platform.test.jupiter;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-jupiter
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import io.axual.common.annotation.InterfaceStability;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;

/**
 * Test with an Axual Test Platform using three clusters
 */
@InterfaceStability.Evolving
public class TripleClusterPlatformUnit extends
    BasePlatformUnit<TripleClusterPlatformUnit> {

    /**
     * Constructs a default test platform with three clusters and distribution disabled
     */
    public TripleClusterPlatformUnit() {
        super(new PlatformUnit(3, false));
    }


    /**
     * Constructs a default test platform with two clusters
     *
     * @param enableDistribution enable/disable distribution
     */
    public TripleClusterPlatformUnit(boolean enableDistribution) {
        super(new PlatformUnit(3, enableDistribution));
    }

    /**
     * Constructs a test platform with three clusters according to the provided instance and cluster
     * configurations
     *
     * @param instanceConfig The instance configuration to use
     * @param clusterA       The configuration for the first cluster
     * @param clusterB       The configuration for the second cluster
     * @param clusterC       The configuration for the third cluster
     */
    public TripleClusterPlatformUnit(InstanceUnitConfig instanceConfig,
        ClusterUnitConfig clusterA, ClusterUnitConfig clusterB, ClusterUnitConfig clusterC) {
        super(new PlatformUnit(instanceConfig, clusterA, clusterB, clusterC));
    }

    /**
     * Constructs a default test platform with two clusters using the provided topic and group
     * patterns
     *
     * @param topicPatternA the Topic pattern to use for the first cluster
     * @param topicPatternB the Topic pattern to use for the second cluster
     * @param topicPatternC the Topic pattern to use for the third cluster
     * @param groupPatternA the Group pattern to use for the first cluster
     * @param groupPatternB the Group pattern to use for the second cluster
     * @param groupPatternC the Group pattern to use for the third cluster
     */
    public TripleClusterPlatformUnit(String topicPatternA, String topicPatternB,
        String topicPatternC, String groupPatternA, String groupPatternB, String groupPatternC) {
        super(new PlatformUnit(new String[]{topicPatternA, topicPatternB, topicPatternC},
            new String[]{groupPatternA, groupPatternB, groupPatternC}));
    }

    /**
     * Get the cluster controller for the first cluster
     *
     * @return the cluster controller
     */
    public ClusterUnit clusterA() {
        return platform().getCluster(0);
    }

    /**
     * Get the cluster controller for the second cluster
     *
     * @return the cluster controller
     */
    public ClusterUnit clusterB() {
        return platform().getCluster(1);
    }

    /**
     * Get the cluster controller for the third cluster
     *
     * @return the cluster controller
     */
    public ClusterUnit clusterC() {
        return platform().getCluster(2);
    }
}
