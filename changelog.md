# Changelog
All notable changes to this project will be documented in this file.

## [master]

## [6.0.6] - 2022-11-18
* Release new version of standalone

## [6.0.5] - 2022-11-16
* Fix an issue that might have caused skipping messages when auto.offset.reset was set to latest
* Introduced a delivery.strategy.on.switch configuration to configure the behavior of the consumer when switching cluster

## [6.0.4] - 2022-10-21
* org.apache.commons:commons-text CVE 2022-42889 mitigation

## [6.0.3] - 2022-10-13
* [Issue #40](https://gitlab.com/axual/public/axual-client-java/-/issues/40) - Upgrade to Kafka 3.2.3
* Upgraded Dependencies:
  * `spring-boot - 2.7.4`
  * `wiremock-jre8 - 2.34.0`
  * `tomcat-embed-core - 9.0.65`
  * `jackson.databind - 2.13.4`
  * `jetty - 9.4.48.v20220622`

## [6.0.2] - 2022-08-24
* [Issue #39](https://gitlab.com/axual/public/axual-client-java/-/issues/39) - Discovery client and Schema Registry client performing SSL hostname validation even with `hostnameVerification` set to false

## [6.0.1] - 2022-07-11
* [Issue #38](https://gitlab.com/axual/public/axual-client-java/-/issues/38) - Error producing/consuming Avro messages while using client v6.0.0

## [6.0.0] - 2022-06-28
* No changes from 6.0.0-RC1

## [6.0.0-RC1] - 2022-06-15
* [Issue #23](https://gitlab.com/axual-public/axual-client-java/-/issues/23) -
  Upgrade to Kafka 3.2.0
* [Issue #35](https://gitlab.com/axual-public/axual-client-java/-/issues/35) - 
Avro and Confluent Schema Registry dependencies are out of date, causing runtime exceptions after upgrades 

## [5.8.1]
* [Issue #33](https://gitlab.com/axual-public/axual-client-java/-/issues/33) -
  Kafka Streams crashes when encountering Axual Offset Distribution Metadata
* [Issue #31](https://gitlab.com/axual-public/axual-client-java/-/issues/31) - Standalone fails to start

## [5.8.0] - 2021-10-29
* Fixed dependency issues with test-core and junit4 / jupiter test frameworks
* Updated enforcer policies to allow RC and SNAPSHOT versions
* Upgraded Dependencies:
  * `Principal Builder - 2.5.0`
  * `apache.avro - 1.10.2`
  * `org.scala-lang - 2.13.5`
  * `slf4j - 1.7.32`
  * `io.netty - 4.1.69.Final`
  * `com.github.jknack - 4.3.0`

## [5.8.0-RC1] - 2021-10-11
* [Issue #12](https://gitlab.com/axual-public/axual-client-java/-/issues/12) -
  Update Schema Registry Mock to be compatible with other languages and uses normalisation
* [Issue #21](https://gitlab.com/axual-public/axual-client-java/-/issues/21) -
  Support for custom `StreamsUncaughtExceptionHandler` in streams
* [Issue #22](https://gitlab.com/axual-public/axual-client-java/-/issues/22) -
  Upgrade to Kafka 2.8.1

## [5.7.3] - 2021-09-28
* [Issue #19](https://gitlab.com/axual-public/axual-client-java/-/issues/19) -
  Set timeouts to prevent applications failing on Azure closing idle connections

## [5.7.2] - 2021-09-13
* [Issue #17](https://gitlab.com/axual-public/axual-client-java/-/issues/17) -
  Kafka Streams fails when starting multiple instances

## [5.7.1] - 2021-07-07
* [Issue #14](https://gitlab.com/axual-public/axual-client-java/-/issues/14) -
  ResolvingAdmin does not unresolve a DescribeTopicsResult object properly

## [5.5.3] - 2021-09-28
* [Issue #17](https://gitlab.com/axual-public/axual-client-java/-/issues/17) -
  Kafka Streams fail when running multiple instances and with Cooperative Rebalancing enabled
* [Issue #19](https://gitlab.com/axual-public/axual-client-java/-/issues/19) -
  Set timeouts to prevent applications failing on Azure closing idle connections

## [5.5.2] - 2021-03-17
* [Issue #10](https://gitlab.com/axual-public/axual-client-java/-/issues/10) -
  Axual Streams fails with IllegalStateException finding partitions

## [5.5.0] - 2020-11-12
* [Issue #1](https://gitlab.com/axual-public/axual-client-java/-/issues/1) -
  Standalone Docker does not load classes in /config and /schemaJars
* [Issue #3](https://gitlab.com/axual-public/axual-client-java/-/issues/3) -
  Update to Kafka 2.6.0
* [Issue #5](https://gitlab.com/axual-public/axual-client-java/-/issues/5) -
  Update Unit Test Platform to provide jUnit Jupiter Extensions
* [Issue #6](https://gitlab.com/axual-public/axual-client-java/-/issues/6) -
  Proxy client fails on missing key password config

## [5.4.4] - 2020-06-13
* Initial Open Source release after repository relocation


[master]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.6...master
[6.0.6]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.5...6.0.6
[6.0.5]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.4...6.0.5
[6.0.4]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.3...6.0.4
[6.0.3]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.2...6.0.3
[6.0.2]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.1...6.0.2
[6.0.1]: https://gitlab.com/axual/public/axual-client-java/-/compare/6.0.0...6.0.1
[6.0.0]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.8.1...6.0.0
[6.0.0-RC1]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.8.1...6.0.0-RC1
[5.8.1]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.8.0...5.8.1
[5.8.0]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.7.3...5.8.0
[5.8.0-RC1]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.7.3...5.8.0-RC1
[5.7.3]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.7.2...5.7.3
[5.7.2]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.7.1...5.7.2
[5.7.1]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.5.3...5.7.1
[5.5.3]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.5.2...5.5.3
[5.5.2]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.5.0...5.5.2
[5.5.0]: https://gitlab.com/axual/public/axual-client-java/-/compare/5.4.4...5.5.0
[5.4.4]: https://gitlab.com/axual/public/axual-client-java/-/tree/release/5.4.x
