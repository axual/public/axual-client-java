package io.axual.client.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.awaitility.Awaitility;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.exception.NoExistingStreamException;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.test.Random;
import io.axual.common.tools.SleepUtil;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ConsumerIT {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerIT.class);
    private static final String STREAM = "general-random-ConsumerIT";
    private static final String UNKNOWN_STREAM = "UNKNOWN-random-ConsumerIT";

    private static final long TTL = 5000; // five seconds
    private static final long DISTRIBUTOR_TIMEOUT = TTL;
    private static final long DISTRIBUTOR_DISTANCE = 1L;

    private static final long longerThanTTL = Math.round(Math.max(DISTRIBUTOR_TIMEOUT * DISTRIBUTOR_DISTANCE, TTL) * 1.5);
    private static final long shorterThanTTL = Math.round(TTL * 0.5);

    @Rule
    public SingleClusterPlatformUnit platform = new SingleClusterPlatformUnit()
            .addStream(new StreamConfig()
                    .setName(STREAM)
                    .setKeySchema(Random.getClassSchema())
                    .setValueSchema(Random.getClassSchema())
                    .setPartitions(1));

    private static Random createRandom(final int id) {
        return Random.newBuilder().setRandom(Integer.toString(id)).build();
    }

    @Before
    public void prepare() {
        platform.instance().getDiscoveryUnit().setTtl(TTL);
        platform.instance().getDiscoveryUnit().setDistributorTimeout(DISTRIBUTOR_TIMEOUT);
        platform.instance().getDiscoveryUnit().setDistributorDistance(DISTRIBUTOR_DISTANCE);
    }

    private List<Random> produce(int start, int count, Producer<Random, Random> producer) {
        List<Random> result = new ArrayList<>(count);
        try {
            for (int id = start; id < start + count; id++) {
                Random newValue = createRandom(id);
                ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                        .setStream(STREAM)
                        .setMessageId(UUID.randomUUID())
                        .setKey(newValue)
                        .setValue(newValue)
                        .build();
                result.add(newValue);
                Future<ProducedMessage<Random, Random>> future = producer.produce(message);
                if (future != null) {
                    ProducedMessage<Random, Random> produceResult = future.get();
                    LOG.info("Produced message " + produceResult.getMessage().getKey().getRandom() + " on cluster " + produceResult.getCluster());
                } else {
                    LOG.info("Could not produce (already closed producer?)");
                }
            }
        } catch (ExecutionException | InterruptedException e) {
            LOG.error("Caught exception while producing", e);
        }

        return result;
    }

    @Test
    public void testConsumer_AtLeastOnce() throws ExecutionException, InterruptedException {
        // Test that a consumer with AT_LEAST_ONCE strategy Works correctlt
        // when directed to do so by the Discovery API
        // Scenario:
        // - Set up consumer on Cluster A
        // - Produce 100 messages on Cluster A
        // - Verify that all messages were received by the consumer

        final InstanceUnit instance = platform.instance();
        final SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();
        final SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setMessageBufferSize(1)
                .setBlocking(false)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();

        final List<Random> producedIDs = new ArrayList<>(200);
        final List<Random> consumedIDs = new ArrayList<>(200);

        try (final AxualClient consumerClient = new AxualClient(instance.getClientConfig("aloConsumer", platform.cluster()));
             final AxualClient producerClient = new AxualClient(instance.getClientConfig("producer1", platform.cluster()))) {
            try (final Producer<Random, Random> producer = producerClient.buildProducer(producerConfig);
                 final Consumer<Random, Random> consumer = consumerClient.buildConsumer(consumerConfig, message -> {
                     LOG.info("Consumed {}", message.getValue());
                     consumedIDs.add(message.getValue());
                     LOG.info(String.format("Received item %d random %s", consumedIDs.size(), message.getValue()));
                 })) {

                consumer.startConsuming();

                // Producer 100 messages on Cluster A
                producedIDs.addAll(produce(0, 100, producer));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 100;
                });

                // Stop consuming
                consumer.stopConsuming();
            }
        }
        // Verify all messages were received
        for (Random produced : producedIDs) {
            assertThat(consumedIDs, hasItem(produced));
        }
    }

    @Test
    public void testConsumer_AtMostOnce() throws ExecutionException, InterruptedException {
        // Test that an AT_MOST_ONCE consumes only messages produced after the first start
        // Scenario:
        // - Produce 100 messages on Cluster
        // - Set up consumer on Cluster A
        // - Produce next 100 messages on Cluster
        // - Verify that all messages 100-199 were consumed, because fo atMostOnce

        final InstanceUnit instance = platform.instance();
        SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_MOST_ONCE)
                .build();
        SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setMessageBufferSize(1)
                .setBlocking(false)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();

        final List<Random> producedIDsBeforeConsumerStart = new ArrayList<>(100);
        final List<Random> producedIDsAfterConsumerStart = new ArrayList<>(100);
        final List<Random> consumedIDs = new ArrayList<>(100);

        try (AxualClient consumerClient = new AxualClient(instance.getClientConfig("amoConsumer", platform.cluster()));
             AxualClient producerClient = new AxualClient(instance.getClientConfig("producer1", platform.cluster()))) {
            try (final Producer<Random, Random> producer = producerClient.buildProducer(producerConfig);
                 final Consumer<Random, Random> consumer = consumerClient.buildConsumer(consumerConfig, message -> {
                     consumedIDs.add(message.getValue());
                     LOG.info("Received item {} random {}", consumedIDs.size(), message.getValue());
                 })) {

                // Produce the first 100 messages, which should not be read by consumer
                producedIDsBeforeConsumerStart.addAll(produce(0, 100, producer));

                consumer.startConsuming();
                // Wait for consumer to join group
                SleepUtil.sleep(Duration.ofSeconds(5));

                // Produce the second 100 messages, which should be read by consumer
                producedIDsAfterConsumerStart.addAll(produce(100, 100, producer));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(200, TimeUnit.MILLISECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 100;
                });

                consumer.stopConsuming();
            }
        }

        for (Random consumedID : consumedIDs) {
            assertThat(producedIDsBeforeConsumerStart, not(hasItem(consumedID)));
            assertThat(producedIDsAfterConsumerStart, hasItem(consumedID));
        }
    }

    @Test
    public void testConsumer_NoTopic() throws ExecutionException, InterruptedException {
        final InstanceUnit instance = platform.instance();
        SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(UNKNOWN_STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_MOST_ONCE)
                .build();

        try (AxualClient consumerClient = new AxualClient(instance.getClientConfig("noTopicConsumer", platform.cluster()));
             Consumer<Random, Random> consumer = consumerClient.buildConsumer(consumerConfig, message -> {
                 LOG.info("Received unexpected message on unknown stream, random {}", message.getValue());
             })) {
            final Future<ConsumeFailedException> consumerResult = consumer.startConsuming();
            Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(200, TimeUnit.MILLISECONDS).until(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return consumerResult.isDone();
                }
            });
            ConsumeFailedException consumeFailedException = consumerResult.get();
            assertNotNull(consumeFailedException);
            assertTrue(consumeFailedException.getCause() instanceof NoExistingStreamException);
        }
    }

    @Test
    public void testConsumer_StopConsumingNoFailure() throws ExecutionException, InterruptedException {
        final InstanceUnit instance = platform.instance();
        SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setMessageBufferSize(1)
                .setBlocking(false)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();
        SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();

        final List<Random> producedIds = new ArrayList<>(100);
        final List<Random> consumedIds = new ArrayList<>(100);

        try (final AxualClient axualClient = new AxualClient(instance.getClientConfig("noTopicConsumer", platform.cluster()));
             final Producer<Random, Random> producer = axualClient.buildProducer(producerConfig);
             final Consumer<Random, Random> consumer = axualClient.buildConsumer(consumerConfig, message -> {
                 consumedIds.add(message.getValue());
                 LOG.info("Received message {} on stream, random {}", consumedIds.size(), message.getValue());
             })) {

            final Future<ConsumeFailedException> consumerResult = consumer.startConsuming();
            producedIds.addAll(produce(0, 100, producer));
            Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(200, TimeUnit.MILLISECONDS).until(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    LOG.warn("Consumed Ids size = {}", consumedIds.size());
                    return consumedIds.size() >= 100;
                }
            });

            ConsumeFailedException consumeFailedExceptionFromStop = consumer.stopConsuming();
            assertNull(consumeFailedExceptionFromStop);

            ConsumeFailedException consumeFailedExceptionFromFuture = consumerResult.get();
            assertNull(consumeFailedExceptionFromFuture);
        }
        // Verify all messages were received
        for (Random produced : producedIds) {
            assertThat(consumedIds, hasItem(produced));
        }

    }
}
