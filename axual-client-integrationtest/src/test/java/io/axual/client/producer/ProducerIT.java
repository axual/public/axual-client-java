package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.common.acl.AccessControlEntry;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclOperation;
import org.apache.kafka.common.acl.AclPermissionType;
import org.apache.kafka.common.errors.TopicAuthorizationException;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePattern;
import org.apache.kafka.common.resource.ResourceType;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.exception.ProduceFailedException;
import io.axual.client.test.Random;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.*;

public class ProducerIT {

    private static final Logger LOG = LoggerFactory.getLogger(ProducerIT.class);

    private static final String STREAM = "general-random-ProducerIT";
    private static final String TEST_CONTENT = "Test Content";
    private static final long TTL = 0;

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit()
            .addStream(new StreamConfig().setName(STREAM));

    private void produceMessages(final Producer<String, String> producer, final int count, final CountDownLatch latch) {
        ProducerMessage<String, String> producerMessage = ProducerMessage.<String, String>newBuilder()
                .setStream(STREAM)
                .setKey(TEST_CONTENT)
                .setValue(TEST_CONTENT)
                .build();

        int i = count - 1;
        do {
            producer.produce(producerMessage, new ProduceCallback<String, String>() {
                @Override
                public void onComplete(ProducedMessage<String, String> message) {
                    LOG.info("Message " + message.getMessage().getKey() + " was produced on cluster " + message.getCluster() + " on offset " + message.getOffset());

                    // Ensure that the context gets filled by downstream proxies. Test this
                    // by validating that serialziation timestamp is set in the context
                    assertNotNull(message.getSerializationTimestamp());

                    latch.countDown();
                }

                @Override
                public void onError(ProducerMessage<String, String> message, ExecutionException exception) {
                    LOG.error("Exception: \"" + message + "\"", exception);
                    latch.countDown();
                }
            });
        } while (i-- > 0);
    }

    private class MsgProcessor implements Processor<String, String> {
        private final String name;
        private final Queue<ConsumerMessage<String, String>> queue;
        private final CountDownLatch latch;

        MsgProcessor(String name, Queue<ConsumerMessage<String, String>> queue, CountDownLatch latch) {
            this.name = name;
            this.queue = queue;
            this.latch = latch;
        }

        @Override
        public void processMessage(ConsumerMessage<String, String> message) {
            LOG.info("Consumer {} consumed. Offset: {}.", name, message.getOffset());
            queue.add(message);
            if (queue.size() == 5) {
                latch.countDown();
            }
        }
    }

    @Test
    public void produceNullData() throws InterruptedException, ExecutionException {
        final InstanceUnit instance = platform.instance();

        instance.getDiscoveryUnit().setTtl(TTL);

        SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .build();

        SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();

        LinkedList<ConsumerMessage<Random, Random>> consumerMessages = new LinkedList<>();
        Processor<Random, Random> processor = msg -> consumerMessages.add(msg);

        try (final AxualClient client = new AxualClient(instance.getClientConfig("io.axual.test", platform.clusterA()));
             final Producer<Random, Random> producer = client.buildProducer(producerConfig);
             final Consumer<Random, Random> consumer = client.buildConsumer(consumerConfig, processor)) {
            Future<ConsumeFailedException> consumeFuture = consumer.startConsuming();
            assertNotNull(consumeFuture);
            assertFalse(consumeFuture.isDone());

            ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                    .setStream(STREAM)
                    .setKey(null)
                    .setValue(null)
                    .build();
            Future<ProducedMessage<Random, Random>> produceFuture = producer.produce(message, new ProduceCallback<Random, Random>() {
                @Override
                public void onComplete(ProducedMessage<Random, Random> message) {
                    LOG.info("Produce message OK");
                }

                @Override
                public void onError(ProducerMessage<Random, Random> message, ExecutionException exception) {
                    LOG.error("Produce message FAIL", exception);
                }
            });

            assertNotNull(produceFuture);
            ProducedMessage<Random, Random> produced = produceFuture.get();
            assertNotNull(produced);

            Awaitility.await("Wait for consumer to catch up")
                    .atMost(15, TimeUnit.SECONDS)
                    .until(() -> consumerMessages.size() > 0);
            consumer.stopConsuming();

            ConsumerMessage<Random,Random> consumed = consumerMessages.getFirst();
            assertNotNull(consumed);

            assertEquals(produced.getMessage().getKey(), consumed.getKey());
            assertEquals(produced.getMessage().getValue(), consumed.getValue());
        }
    }

    @Test
    public void produceNullDataIncorrectAcl() throws InterruptedException, ExecutionException {
        final InstanceUnit instance = platform.instance();

        instance.getDiscoveryUnit().setTtl(TTL);

        // AdminClient Config
        ClientConfig adminClientConfig = instance.getClientConfig("io.axual.test.admin");
        Map<String, Object> adminConfigMap = KafkaUtil.getKafkaConfigs(adminClientConfig);
        adminConfigMap.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, platform.clusterA().getBootstrapServer());

        // create a prefixex write ACL for all topics with the tenant prefix
        try (AdminClient client = AdminClient.create(adminConfigMap)) {
            AclBinding aclBinding = new AclBinding(new ResourcePattern(ResourceType.TOPIC, platform.instance().getTenant(), PatternType.PREFIXED),
                    new AccessControlEntry("user:NotMatching", "*", AclOperation.WRITE, AclPermissionType.ALLOW));
            client.createAcls(Collections.singletonList(aclBinding)).all().get();
        }

        SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .build();

        try (final AxualClient producerClient = new AxualClient(instance.getClientConfig("io.axual.testproducer", platform.clusterA()));
             final Producer<Random, Random> producer = producerClient.buildProducer(producerConfig)) {
            ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                    .setStream(STREAM)
                    .setKey(null)
                    .setValue(null)
                    .build();
            Future<ProducedMessage<Random, Random>> futureProduce = producer.produce(message, new ProduceCallback<Random, Random>() {
                @Override
                public void onComplete(ProducedMessage<Random, Random> message) {
                    LOG.info("Produce message OK");
                }

                @Override
                public void onError(ProducerMessage<Random, Random> message, ExecutionException exception) {
                    LOG.error("Produce message FAIL", exception);
                }
            });

            assertNotNull(futureProduce);
            ExecutionException executionException = null;
            try {
                futureProduce.get();
            } catch (ExecutionException e) {
                executionException = e;
            } catch (Exception e) {
                LOG.error("Getting future exception");
                throw e;
            }

            assertNotNull(executionException);
            assertNotNull(executionException.getCause());
            assertTrue(executionException.getCause() instanceof ProduceFailedException);
            assertNotNull(executionException.getCause().getCause());
            assertTrue(executionException.getCause().getCause() instanceof TopicAuthorizationException);
        }
    }

    @Test
    public void shouldProduceToOtherClusterAfterSwitch() throws InterruptedException {
        final InstanceUnit instance = platform.instance();

        final int messagesPerCluster = 5;
        instance.getDiscoveryUnit().setTtl(TTL);

        try (final AxualClient producerClient = new AxualClient(instance.getClientConfig("io.axual.testproducer", platform.clusterA()));
             final AxualClient consumerClientA = new AxualClient(instance.getClientConfig("io.axual.testconsumerA", platform.clusterA()));
             final AxualClient consumerClientB = new AxualClient(instance.getClientConfig("io.axual.testconsumerB", platform.clusterB()))) {
            final CountDownLatch consumerLatch = new CountDownLatch(2);
            final ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                    .setKeySerializer(StringSerializer.class.getName())
                    .setValueSerializer(StringSerializer.class.getName())
                    .build();
            final ConsumerConfig<String, String> consumerConfig = ConsumerConfig.<String, String>builder()
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .setStream(STREAM)
                    .setKeyDeserializer(StringDeserializer.class.getName())
                    .setValueDeserializer(StringDeserializer.class.getName())
                    .build();
            final Queue<ConsumerMessage<String, String>> queueA = new LinkedBlockingQueue<>();
            final Queue<ConsumerMessage<String, String>> queueB = new LinkedBlockingQueue<>();

            try (final Producer<String, String> kafkaProducer = producerClient.buildProducer(producerConfig);
                 final Consumer<String, String> kafkaConsumerA = consumerClientA.buildConsumer(consumerConfig, new MsgProcessor("A", queueA, consumerLatch));
                 final Consumer<String, String> kafkaConsumerB = consumerClientB.buildConsumer(consumerConfig, new MsgProcessor("B", queueB, consumerLatch))) {

                kafkaConsumerA.startConsuming();
                kafkaConsumerB.startConsuming();

                CountDownLatch produceOnClusterALatch = new CountDownLatch(messagesPerCluster);
                LOG.info("Producing.");
                produceMessages(kafkaProducer, messagesPerCluster, produceOnClusterALatch);
                assertTrue("Expected produceOnClusterALatch to be 0 in 30 seconds", produceOnClusterALatch.await(30, SECONDS));

                LOG.info("Before switch command");
                instance.directApplicationTo(producerClient.getConfig(), platform.clusterB(), true);
                LOG.info("After switch command");

                CountDownLatch produceOnClusterBLatch = new CountDownLatch(messagesPerCluster);
                LOG.info("Producing.");
                produceMessages(kafkaProducer, messagesPerCluster, produceOnClusterBLatch);
                assertTrue("Expected produceOnClusterBLatch to be 0 in 30 seconds", produceOnClusterBLatch.await(30, SECONDS));

                assertTrue("Expected consumerLatch to be 0 in 30 seconds", consumerLatch.await(30, SECONDS));
            }

            assertEquals(queueA.size(), queueB.size());
            assertEquals(messagesPerCluster, queueA.size());
        }
    }
}
