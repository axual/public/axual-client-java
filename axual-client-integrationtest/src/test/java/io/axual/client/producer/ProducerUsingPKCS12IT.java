package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static org.junit.Assert.*;

public class ProducerUsingPKCS12IT {

    private static final Logger LOG = LoggerFactory.getLogger(ProducerUsingPKCS12IT.class);
    private static final String STREAM = "general-test-stream-ProducerUsingPKCS12IT";
    private static final long TTL = 1000;

    @Rule
    public SingleClusterPlatformUnit platform = new SingleClusterPlatformUnit(SslConfig.KeystoreType.PKCS12)
            .addStream(new StreamConfig().setName(STREAM));

    @Test
    public void produceAndConsumeData() throws InterruptedException, ExecutionException {
        final InstanceUnit instance = platform.instance();
        instance.getDiscoveryUnit().setTtl(TTL);

        final ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .setKeySerializer(StringSerializer.class.getName())
                .setValueSerializer(StringSerializer.class.getName())
                .build();
        final ConsumerConfig<String, String> consumerConfig = ConsumerConfig.<String, String>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setStream(STREAM)
                .setKeyDeserializer(StringDeserializer.class.getName())
                .setValueDeserializer(StringDeserializer.class.getName())
                .build();

        LinkedList<ConsumerMessage<String, String>> consumerMessages = new LinkedList<>();
        Processor<String, String> processor = message -> {
            consumerMessages.add(message);
            LOG.info("Message Consumed: {}", message.getValue());
        };

        try (final AxualClient producerClient = new AxualClient(instance.getClientConfig("io.axual.producerapp", platform.cluster()));
             final AxualClient consumerClient = new AxualClient(instance.getClientConfig("io.axual.consumerapp", platform.cluster()));
             final Producer<String, String> producer = producerClient.buildProducer(producerConfig);
             final Consumer<String, String> consumer = consumerClient.buildConsumer(consumerConfig, processor)) {

            assertEquals(SslConfig.KeystoreType.PKCS12,producerClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PKCS12,producerClient.getConfig().getSslConfig().getTruststoreType());

            assertEquals(SslConfig.KeystoreType.PKCS12,consumerClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PKCS12,consumerClient.getConfig().getSslConfig().getTruststoreType());

            Future<ConsumeFailedException> consumeFuture = consumer.startConsuming();

            assertNotNull(consumeFuture);
            assertFalse(consumeFuture.isDone());

            ProducerMessage<String, String> message = ProducerMessage.<String, String>newBuilder()
                    .setStream(STREAM)
                    .setKey("Test Key")
                    .setValue("Test Value")
                    .build();
            Future<ProducedMessage<String, String>> produceFuture = producer.produce(message, new ProduceCallback<String, String>() {
                @Override
                public void onComplete(ProducedMessage<String, String> message) {
                    LOG.info("Message produced successfully.");
                }

                @Override
                public void onError(ProducerMessage<String, String> message, ExecutionException exception) {
                    LOG.error("Produce message FAILED", exception);
                }
            });

            assertNotNull(produceFuture);
            ProducedMessage<String, String> produced = produceFuture.get();
            assertNotNull(produced);

            Awaitility.await("Wait for consumer to catch up")
                    .atMost(15, TimeUnit.SECONDS)
                    .until(() -> consumerMessages.size() > 0);
            consumer.stopConsuming();

            ConsumerMessage<String, String> consumed = consumerMessages.getFirst();
            assertNotNull(consumed);

            assertEquals(produced.getMessage().getKey(), consumed.getKey());
            assertEquals(produced.getMessage().getValue(), consumed.getValue());
        }
    }
}
