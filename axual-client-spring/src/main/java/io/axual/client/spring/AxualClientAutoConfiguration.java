package io.axual.client.spring;

/*-
 * ========================LICENSE_START=================================
 * axual-client-spring
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import io.axual.client.AxualClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ComponentScan
@ConditionalOnProperty("axual.client.enable")
@Deprecated
public class AxualClientAutoConfiguration {
    private final AxualClientProperties config;

    @Autowired
    public AxualClientAutoConfiguration(AxualClientProperties config) {
        log.warn("Axual Client Spring is deprecated and is replaced with Spring for Apache Kafka with a custom Producer/Consumer/Streams factory");
        this.config = config;
    }

    @Bean
    @ConditionalOnMissingBean
    public AxualClient axualClient() {
        return new AxualClient(config.asClientConfig());
    }
}
