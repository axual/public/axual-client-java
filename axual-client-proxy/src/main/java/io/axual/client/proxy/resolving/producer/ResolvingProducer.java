package io.axual.client.proxy.resolving.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.generic.producer.ExtendedProducerRecord;
import io.axual.client.proxy.generic.producer.StaticProducerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.tools.MapUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.Serializer;

public class ResolvingProducer<K, V> extends StaticProducerProxy<K, V, ResolvingProducerConfig<K, V>> {
    public ResolvingProducer(Map<String, Object> configs) {
        super(new ResolvingProducerConfig<>(configs));
    }

    public ResolvingProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public ResolvingProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public ResolvingProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    private static RecordMetadata convertRecordMetadata(RecordMetadata input, String topic) {
        return new RecordMetadata(
                new TopicPartition(topic, input.partition()),
                input.offset(),
                0,
                input.timestamp(),
                null,
                input.serializedKeySize(),
                input.serializedValueSize());
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
        String consumerGroupId) {
        Map<TopicPartition, OffsetAndMetadata> newOffsets = new HashMap<>();
        for (Map.Entry<TopicPartition, OffsetAndMetadata> entry : offsets.entrySet()) {
            newOffsets
                .put(config.getTopicResolver().resolveTopic(entry.getKey()), entry.getValue());
        }
        super.sendOffsetsToTransaction(newOffsets,
            config.getGroupResolver().resolveGroup(consumerGroupId));
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
        ConsumerGroupMetadata groupMetadata) throws ProducerFencedException {
        Map<TopicPartition, OffsetAndMetadata> newOffsets = new HashMap<>();
        for (Map.Entry<TopicPartition, OffsetAndMetadata> entry : offsets.entrySet()) {
            newOffsets
                .put(config.getTopicResolver().resolveTopic(entry.getKey()), entry.getValue());
        }

        super.sendOffsetsToTransaction(newOffsets, new ConsumerGroupMetadata(
            config.getGroupResolver().resolveGroup(groupMetadata.groupId()),
            groupMetadata.generationId(), groupMetadata.memberId(),
            groupMetadata.groupInstanceId()));
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord) {
        ProducerRecord<K, V> sentRecord = convertProducerRecord(producerRecord);
        Future<RecordMetadata> future = super.send(sentRecord);
        applyHeadersToOriginalRecord(producerRecord, sentRecord);
        return new ProxyFuture(future, producerRecord.topic());
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord, Callback callback) {
        if (callback == null) {
            return send(producerRecord);
        }

        ProducerRecord<K, V> sentRecord = convertProducerRecord(producerRecord);
        Future<RecordMetadata> future = super.send(sentRecord, new ProxyCallback(callback, producerRecord.topic()));
        applyHeadersToOriginalRecord(producerRecord, sentRecord);
        return new ProxyFuture(future, producerRecord.topic());
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        List<PartitionInfo> rawResult = super.partitionsFor(config.getTopicResolver().resolveTopic(topic));
        List<PartitionInfo> result = new ArrayList<>(rawResult.size());
        for (PartitionInfo info : rawResult) {
            result.add(new PartitionInfo(
                    config.getTopicResolver().unresolveTopic(info.topic()),
                    info.partition(),
                    info.leader(),
                    info.replicas(),
                    info.inSyncReplicas()));
        }
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of KafkaProducer
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private ProducerRecord<K, V> convertProducerRecord(ProducerRecord<K, V> producerRecord) {
        final TopicResolver resolver = config.getTopicResolver();

        // Check if we received dynamic context passed in through an ExtendedProducerRecord
        if (producerRecord instanceof ExtendedProducerRecord) {
            ExtendedProducerRecord<K, V> extendedRecord = (ExtendedProducerRecord<K, V>) producerRecord;

            // Apply any dynamic context by (re)configuring the resolver
            resolver.configure(extendedRecord.context());

            // Return an ExtendedProducerRecord with same contents but with a resolved topic
            return new ExtendedProducerRecord<>(
                    resolver.resolveTopic(producerRecord.topic()),
                    producerRecord.partition(),
                    producerRecord.timestamp(),
                    producerRecord.key(),
                    producerRecord.value(),
                    producerRecord.headers(),
                    extendedRecord.context());
        }

        // Return a regular ProducerRecord with a resolved topic
        return new ProducerRecord<>(
                resolver.resolveTopic(producerRecord.topic()),
                producerRecord.partition(),
                producerRecord.timestamp(),
                producerRecord.key(),
                producerRecord.value(),
                producerRecord.headers());
    }

    private void applyHeadersToOriginalRecord(ProducerRecord<K, V> original, ProducerRecord<K, V> sent) {
        if (original != sent && original instanceof ExtendedProducerRecord) {
            ((ExtendedProducerRecord<K, V>) original).setHeaders(sent.headers());
        }
    }

    private static final class ProxyCallback implements Callback {
        private final Callback callback;
        private final String topic;

        ProxyCallback(Callback callback, String topic) {
            this.callback = callback;
            this.topic = topic;
        }

        @Override
        public void onCompletion(RecordMetadata input, Exception e) {
            if (input != null) {
                callback.onCompletion(convertRecordMetadata(input, topic), e);
            } else {
                callback.onCompletion(null, e);
            }
        }
    }

    private static class ProxyFuture implements Future<RecordMetadata> {
        private final Future<RecordMetadata> future;
        private final String topic;

        ProxyFuture(final Future<RecordMetadata> future, final String topic) {
            this.future = future;
            this.topic = topic;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return future.cancel(mayInterruptIfRunning);
        }

        @Override
        public boolean isCancelled() {
            return future.isCancelled();
        }

        @Override
        public boolean isDone() {
            return future.isDone();
        }

        @Override
        public RecordMetadata get() throws InterruptedException, ExecutionException {
            return convertRecordMetadata(future.get(), topic);
        }

        @Override
        public RecordMetadata get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return convertRecordMetadata(future.get(timeout, unit), topic);
        }
    }
}
