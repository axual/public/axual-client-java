package io.axual.client.proxy.resolving.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;

import io.axual.common.exception.NotSupportedException;

public class ResolvingPartitionInfo extends PartitionInfo {
    private static final Node[] emptyNodes = {};

    private static final String UNSUPPORTED = "<Not Supported>";

    public ResolvingPartitionInfo(String topic, int partition) {
        super(topic, partition, null, emptyNodes, emptyNodes);
    }

    @Override
    public Node leader() {
        throw new NotSupportedException("Retrieving Leader is not supported");
    }

    @Override
    public Node[] replicas() {
        throw new NotSupportedException("Retrieving Replica's is not supported");
    }

    @Override
    public Node[] inSyncReplicas() {
        throw new NotSupportedException("Retrieving In Sync Replica's is not supported");
    }

    @Override
    public String toString() {
        return String.format("Partition(topic = %s, partition = %d, leader = %s, replicas = %s, isr = %s)", this.topic(), this.partition(), UNSUPPORTED, UNSUPPORTED, UNSUPPORTED);
    }
}
