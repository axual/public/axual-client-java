package io.axual.client.proxy.switching.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;

import java.util.Collection;

public class TopicSetSubscription<K, V> extends Subscription<K, V> {
    private final Collection<String> topics;

    public TopicSetSubscription(Collection<String> topics, ConsumerRebalanceListener listener) {
        super(listener);
        this.topics = topics;
    }

    public Collection<String> getTopics() {
        return topics;
    }

    @Override
    public void subscribe(final Consumer<K, V> consumer, final boolean seekToEnd) {
        consumer.subscribe(topics, getListener(consumer, seekToEnd));
    }
}
