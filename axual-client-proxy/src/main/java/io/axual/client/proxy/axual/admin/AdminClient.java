package io.axual.client.proxy.axual.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;
import java.util.Properties;

/**
 * Plugin replace for Kafka AdminClient
 */
public class AdminClient extends AxualAdminClient {
    // The configuration for the proxy consumer

    public AdminClient(Map<String, Object> configs) {
        super(configs);
    }

    public AdminClient(Properties properties) {
        super(properties);
    }

    public static AdminClient create(Properties props) {
        return new AdminClient(props);
    }

    public static AdminClient create(Map<String, Object> conf) {
        return new AdminClient(conf);
    }
}
