package io.axual.client.proxy.resolving.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.resolving.generic.ResolvingProxyConfig;
import io.axual.common.config.BaseConfig;
import io.axual.common.resolver.TopicResolver;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerPartitionAssignor;

public class ResolvingConsumerPartitionAssignorConfig extends BaseConfig {

    public static final String BACKING_ASSIGNOR_CONFIG = "resolvingconsumerpartitionassignor.backing.assignor";
    public static final String TOPIC_RESOLVER_CONFIG =
        "resolvingconsumerpartitionassignor." + ResolvingProxyConfig.TOPIC_RESOLVER_CONFIG;

    private ConsumerPartitionAssignor backingAssignor;
    private TopicResolver topicResolver;

    public ResolvingConsumerPartitionAssignorConfig(Map<String, Object> configs) {
        super(configs);
        filterDownstream(BACKING_ASSIGNOR_CONFIG, TOPIC_RESOLVER_CONFIG);
        backingAssignor = getConfiguredInstance(BACKING_ASSIGNOR_CONFIG,
            ConsumerPartitionAssignor.class);
        topicResolver = getConfiguredInstance(TOPIC_RESOLVER_CONFIG, TopicResolver.class);
    }

    public ConsumerPartitionAssignor getBackingAssignor() {
        return backingAssignor;
    }

    public TopicResolver getTopicResolver() {
        return topicResolver;
    }
}
