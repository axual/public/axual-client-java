package io.axual.client.proxy.resolving.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.ExtendableDescribeTopicsResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.Uuid;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.resolver.TopicResolver;

public class ResolvingDescribeTopicsResult extends ExtendableDescribeTopicsResult {
    private final TopicResolver resolver;

    public ResolvingDescribeTopicsResult(Map<Uuid, KafkaFuture<TopicDescription>> topicIdFutures,
                                         Map<String, KafkaFuture<TopicDescription>> nameFutures,
                                         TopicResolver resolver) {
        super(topicIdFutures, nameFutures);
        this.resolver = resolver;
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    public ResolvingDescribeTopicsResult(Map<String, KafkaFuture<TopicDescription>> futures, TopicResolver resolver) {
        super(ResolverUtil.unresolve(futures, resolver));
        this.resolver = resolver;
    }


    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public Map<String, KafkaFuture<TopicDescription>> values() {
        return convertFuture(super.values());
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public KafkaFuture<Map<String, TopicDescription>> all() {
        return super.all().thenApply(this::convertResult);
    }

    @Override
    public KafkaFuture<Map<String, TopicDescription>> allTopicNames() {
        return super.allTopicNames().thenApply(this::convertResult);
    }

    @Override
    public KafkaFuture<Map<Uuid, TopicDescription>> allTopicIds() {
        return super.allTopicIds().thenApply(this::convertResult);
    }

    @Override
    public Map<String, KafkaFuture<TopicDescription>> topicNameValues() {
        return convertFuture(super.topicNameValues());
    }

    @Override
    public Map<Uuid, KafkaFuture<TopicDescription>> topicIdValues() {
        return convertFuture(super.topicIdValues());
    }

    private <T> Map<T, KafkaFuture<TopicDescription>> convertFuture(Map<T, KafkaFuture<TopicDescription>> values) {
        Map<T, KafkaFuture<TopicDescription>> result = new HashMap<>();
        for (Map.Entry<T, KafkaFuture<TopicDescription>> entry : values.entrySet()) {
            result.put(entry.getKey(), entry.getValue().thenApply(this::unresolveTopicDescription));
        }

        return result;
    }

    private <T> Map<T, TopicDescription> convertResult(Map<T, TopicDescription> values) {
        Map<T, TopicDescription> result = new HashMap<>();
        for (Map.Entry<T, TopicDescription> entry : values.entrySet()) {
            result.put(entry.getKey(), unresolveTopicDescription(entry.getValue()));
        }

        return result;
    }

    private TopicDescription unresolveTopicDescription(TopicDescription td) {
        return new TopicDescription(resolver.unresolveTopic(td.name()), td.isInternal(), td.partitions(), td.authorizedOperations());
    }
}
