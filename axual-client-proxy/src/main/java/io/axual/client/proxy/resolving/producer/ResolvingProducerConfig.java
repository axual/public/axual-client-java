package io.axual.client.proxy.resolving.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.proxy.resolving.consumer.ResolvingConsumerConfig;
import io.axual.common.resolver.GroupResolver;
import java.util.Map;

import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.resolving.generic.ResolvingClientProxyConfig;
import io.axual.common.resolver.TransactionalIdResolver;

public class ResolvingProducerConfig<K, V> extends ResolvingClientProxyConfig<ProducerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "resolvingproducer.backing.factory";
    public static final String GROUP_ID_RESOLVER_CONFIG = ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG;
    public static final String TRANSACTIONAL_ID_RESOLVER_CONFIG = "transactional.id.resolver";
    public static final String TRANSACTIONAL_ID = "transactional.id";

    private static final Logger log = LoggerFactory.getLogger(ResolvingProducerConfig.class);

    private final GroupResolver groupResolver;

    private TransactionalIdResolver transactionalIdResolver;

    public ResolvingProducerConfig(Map<String, Object> configs) {
        super(configs, BACKING_FACTORY_CONFIG);
        groupResolver = getConfiguredInstance(GROUP_ID_RESOLVER_CONFIG, GroupResolver.class);
        groupResolver.configure(configs);

        final String transactionalId = (String) configs.get(TRANSACTIONAL_ID);
        if (transactionalId != null) {
            transactionalIdResolver = getConfiguredInstance(TRANSACTIONAL_ID_RESOLVER_CONFIG, TransactionalIdResolver.class, false);
            putDownstream(TRANSACTIONAL_ID, resolveTransactionalId(transactionalId));
        }
    }

    public GroupResolver getGroupResolver() {
        return groupResolver;
    }

    /**
     * Return the resolved version of a given transactional id, or the original id if resolving failed.
     * @param transactionalId a configured transactional.id.
     * @return the resolved transactional.id.
     */
    private String resolveTransactionalId(String transactionalId) {
        String result = transactionalIdResolver.resolveTransactionalId(transactionalId);
        if (result == null) {
            log.warn("Failed to resolve transactional.id={}, leaving as is", transactionalId);
            return transactionalId;
        }
        return result;
    }
}
