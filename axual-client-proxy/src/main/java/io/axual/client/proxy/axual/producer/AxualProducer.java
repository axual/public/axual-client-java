package io.axual.client.proxy.axual.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.generic.producer.StaticProducerProxy;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.wrapped.producer.WrappedProducer;
import io.axual.common.tools.MapUtil;

/**
 * The type Axual producer. This is a general purpose proxy, that sets up a "proxy chain" using
 * other proxy types. Each proxy adds a specific piece of functionality transparent to the user of
 * this class. Proxy chains are parsed in the AxualProducerConfig and initialized by the
 * constructor. A typical chain can be configured as follows:
 * <p>
 * properties.put(AxualProducerConfig.CHAIN_CONFIG, "SWITCHING;RESOLVING;HEADER");
 * <p>
 * This configuration sets up a chain containing the SwitchingProducer --&gt; ResolvingProducer
 * --&gt; LineageProducer --&gt; HeaderProducer --&gt; KafkaProducer.
 * <p>
 * Parameters can also be passed to each individual proxy as follows:
 * <p>
 * properties.put(AxualProducerConfig.CHAIN_CONFIG, "SWITCHING;LOGGING:name=MyLoggingProxy,level=WARN;RESOLVING;LINEAGE;HEADER");
 * <p>
 * In this example the LoggingProducer gets two extra parameters passed in, namely "name" and
 * "level".
 *
 * @param <K> the key type of messages
 * @param <V> the value type of messages
 */
public class AxualProducer<K, V> extends StaticProducerProxy<K, V, AxualProducerConfig<K, V>> {
    public AxualProducer(Map<String, Object> configs) {
        super(AxualProducer.createChain(configs, null));
    }

    public AxualProducer(Map<String, Object> configs, Producer<K, V> producer) {
        super(AxualProducer.createChain(configs, producer));
    }

    public AxualProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public AxualProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public AxualProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    private static <K, V> ClientProxyInitializer<ProducerProxy<K, V>, AxualProducerConfig<K, V>> createChain(Map<String, Object> configs, Producer<K, V> producer) {
        AxualProducerConfig<K, V> config = new AxualProducerConfig<>(configs);

        if (producer != null) {
            return new ClientProxyInitializer<>(config, new WrappedProducer<>(producer, configs));
        }

        ClientProxyFactory<ProducerProxy<K, V>> factory = ProxyChainUtil.setupProducerFactoryChain(config.getProxyChain(), config.getBackingFactory());
        return new ClientProxyInitializer<>(config, factory.create(config.getDownstreamConfigs()));
    }
}
