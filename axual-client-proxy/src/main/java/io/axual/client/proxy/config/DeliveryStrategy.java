package io.axual.client.proxy.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */
public enum DeliveryStrategy {
    /**
     * The AT_MOST_ONCE strategy will try to deliver messages once (or less) and continue processing
     * in all failure cases. In case something fails the message will be lost. For Producers this
     * means that messages will be sent to Kafka only once. For Consumers this means skipping to the
     * end of a new PartitionOffset upon subscribing.
     */
    AT_MOST_ONCE,
    /**
     * The AT_LEAST_ONCE strategy will retry to deliver messages upon technical failures,
     * potentially leading to message duplication in certain situations. Duplication is considered
     * to be a lesser evil by applications that adopt this strategy. Be sure to be idempotent in
     * your application when adopting this strategy. For Producers this strategy ensures successful
     * delivery to Kafka and the message is replicated to other node in the cluster. For Consumers
     * this strategy jumps to the beginning of the stream if the consumer's group id did not commit
     * any offset beforehand.
     */
    AT_LEAST_ONCE
}
