package io.axual.client.proxy.lineage.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeaders;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import io.axual.serde.utils.HeaderUtils;

import static io.axual.client.proxy.lineage.LineageHeaders.*;

public class LineageAppender {
    private static final Set<String> LINEAGE_HEADERS = new HashSet<>();

    static {
        LINEAGE_HEADERS.add(MESSAGE_ID_HEADER);
        LINEAGE_HEADERS.add(SERIALIZATION_TIME_HEADER);
        LINEAGE_HEADERS.add(DESERIALIZATION_TIME_HEADER);
        LINEAGE_HEADERS.add(COPY_FLAGS_HEADER);
        LINEAGE_HEADERS.add(PRODUCER_ID_HEADER);
        LINEAGE_HEADERS.add(PRODUCER_VERSION_HEADER);
        LINEAGE_HEADERS.add(INTERMEDIATE_ID_HEADER);
        LINEAGE_HEADERS.add(INTERMEDIATE_VERSION_HEADER);
        LINEAGE_HEADERS.add(SYSTEM_HEADER);
        LINEAGE_HEADERS.add(INSTANCE_HEADER);
        LINEAGE_HEADERS.add(CLUSTER_HEADER);
        LINEAGE_HEADERS.add(TENANT_HEADER);
        LINEAGE_HEADERS.add(ENVIRONMENT_HEADER);
    }

    private LineageAppender() {
    }

    public static Headers clearLineageHeaders(Headers headers) {
        Headers result = new RecordHeaders();
        for (Header header : headers) {
            if (!LINEAGE_HEADERS.contains(header.key())) {
                result.add(header.key(), header.value());
            }
        }
        return result;
    }

    public static void appendLineageForDeserialization(Headers headers, LineageConfig config) {
        if (headers != null) {
            // Reset the Deserializer time
            append(headers, DESERIALIZATION_TIME_HEADER, System.currentTimeMillis(), true);

            //Set the functional lineage context
            append(headers, TENANT_HEADER, config.getTenant(), false);
            append(headers, ENVIRONMENT_HEADER, config.getEnvironment(), false);

            // Set the technical lineage context
            append(headers, INTERMEDIATE_ID_HEADER, config.getApplicationId(), true);
            append(headers, INTERMEDIATE_VERSION_HEADER, config.getApplicationVersion(), true);
            append(headers, SYSTEM_HEADER, config.getSystem(), false);
            append(headers, INSTANCE_HEADER, config.getInstance(), false);
            append(headers, CLUSTER_HEADER, config.getCluster(), false);
        }
    }

    public static void appendLineageForSerialization(Headers headers, LineageConfig config) {
        if (headers != null) {
            // Unset the deserialization time if set
            headers.remove(DESERIALIZATION_TIME_HEADER);

            if (!config.isSystemProduceEnabled()) {
                // Remove the copy flag header if needed
                headers.remove(COPY_FLAGS_HEADER);
                // Remove the message id header if needed
                headers.remove(MESSAGE_ID_HEADER);
            }

            // Set a random unique message id if not set already
            if (headers.lastHeader(MESSAGE_ID_HEADER) == null) {
                HeaderUtils.addUuidHeader(headers, MESSAGE_ID_HEADER, UUID.randomUUID());
            }

            // Set the producer id and version
            if (headers.lastHeader(PRODUCER_ID_HEADER) == null && config.getApplicationId() != null) {
                // Set the producer id and version lineage headers
                headers.remove(PRODUCER_VERSION_HEADER);
                HeaderUtils.addStringHeader(headers, PRODUCER_ID_HEADER, config.getApplicationId());
                HeaderUtils.addStringHeader(headers, PRODUCER_VERSION_HEADER, config.getApplicationVersion());
            }

            headers.remove(INTERMEDIATE_ID_HEADER);
            headers.remove(INTERMEDIATE_VERSION_HEADER);
            HeaderUtils.addStringHeader(headers, INTERMEDIATE_ID_HEADER, config.getApplicationId());
            HeaderUtils.addStringHeader(headers, INTERMEDIATE_VERSION_HEADER, config.getApplicationVersion());

            // Set the serialization time
            headers.remove(SERIALIZATION_TIME_HEADER);
            HeaderUtils.addLongHeader(headers, SERIALIZATION_TIME_HEADER, System.currentTimeMillis());

            //Set the functional lineage context
            append(headers, TENANT_HEADER, config.getTenant(), true);
            append(headers, ENVIRONMENT_HEADER, config.getEnvironment(), true);

            // Set the technical lineage context
            append(headers, INTERMEDIATE_ID_HEADER, config.getApplicationId(), true);
            append(headers, INTERMEDIATE_VERSION_HEADER, config.getApplicationVersion(), true);
            append(headers, SYSTEM_HEADER, config.getSystem(), true);
            append(headers, INSTANCE_HEADER, config.getInstance(), true);
            append(headers, CLUSTER_HEADER, config.getCluster(), true);
        }
    }

    private static void append(Headers headers, String headerKey, String headerValue, boolean overwrite) {
        if (headers != null) {
            if (overwrite) {
                headers.remove(headerKey);
            }
            if (headers.lastHeader(headerKey) == null) {
                HeaderUtils.addStringHeader(headers, headerKey, headerValue);
            }
        }
    }

    private static void append(Headers headers, String headerKey, Long headerValue, boolean overwrite) {
        if (headers != null) {
            if (overwrite) {
                headers.remove(headerKey);
            }
            if (headers.lastHeader(headerKey) == null) {
                HeaderUtils.addLongHeader(headers, headerKey, headerValue);
            }
        }
    }
}
