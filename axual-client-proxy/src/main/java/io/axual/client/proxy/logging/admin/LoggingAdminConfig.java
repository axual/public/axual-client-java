package io.axual.client.proxy.logging.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.callback.admin.CallbackAdminConfig;
import io.axual.client.proxy.callback.admin.CallbackAdminFactory;
import io.axual.client.proxy.callback.core.CallbackConfig;
import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.logging.core.LoggingConfig;
import io.axual.client.proxy.logging.core.LoggingMethodCallFactory;

public class LoggingAdminConfig extends BaseClientProxyConfig<AdminProxy> {
    public static final String BACKING_FACTORY_CONFIG = "loggingadmin.backing.factory";

    public LoggingAdminConfig(Map<String, Object> configs, Class<?> logClass) {
        // The LoggingAdminConfig injects the CallbackAdminClient in the proxy chain, making use
        // of its callback hooks to perform intended logging on all methods called.
        super(convertConfig(configs, logClass), BACKING_FACTORY_CONFIG);
    }

    private static Map<String, Object> convertConfig(Map<String, Object> configs, Class<?> logClass) {
        // This method converts the configs into configs accepted by the CallbackAdminClient.
        Map<String, Object> result = new HashMap<>(configs);

        // Inject the CallbackAdminClient in the chain by configuring it as backing factory. The
        // original backing factory becomes is shifted to the injected one.
        result.put(CallbackAdminConfig.BACKING_FACTORY_CONFIG, parseAndRemoveConfig(result, BACKING_FACTORY_CONFIG, true, null));
        result.put(BACKING_FACTORY_CONFIG, CallbackAdminFactory.class);

        // Set the proper configuration for the CallbackAdminClient
        result.put(CallbackConfig.CALL_FACTORY_CONFIG, new LoggingMethodCallFactory(LoggingConfig.parse(result), logClass));
        return result;
    }
}
