package io.axual.client.proxy.lineage.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.common.config.BaseConfig;
import io.axual.common.config.CommonConfig;

import static io.axual.discovery.client.DiscoveryResult.*;

public class LineageConfig extends BaseConfig {
    public static final String ENABLE_SYSTEM_PRODUCE = "enable.system.produce";
    private final String applicationId;
    private final String applicationVersion;
    private final String system;
    private final String instance;
    private final String cluster;
    private final String tenant;
    private final String environment;
    private final boolean systemProduceEnabled;

    public LineageConfig(Map<String, Object> configs) {
        super(configs);

        // Parse the application id and version
        applicationId = parseStringConfig(CommonConfig.APPLICATION_ID, false, null);
        applicationVersion = parseStringConfig(CommonConfig.APPLICATION_VERSION, false, "unknown");

        // Parse functional (application) context
        tenant = parseStringConfig(TENANT_PROPERTY, false, null);
        environment = parseStringConfig(ENVIRONMENT_PROPERTY, false, null);

        // Parse technical (server-side) context
        system = parseStringConfig(SYSTEM_PROPERTY, false, null);
        instance = parseStringConfig(INSTANCE_PROPERTY, false, null);
        cluster = parseStringConfig(CLUSTER_PROPERTY, false, null);

        systemProduceEnabled = parseConfig(configs, ENABLE_SYSTEM_PRODUCE, false, false);
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getSystem() {
        return system;
    }

    public String getInstance() {
        return instance;
    }

    public String getCluster() {
        return cluster;
    }

    public String getTenant() {
        return tenant;
    }

    public String getEnvironment() {
        return environment;
    }

    public boolean isSystemProduceEnabled() {
        return systemProduceEnabled;
    }
}
