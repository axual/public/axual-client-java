package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

import io.axual.common.exception.ClientException;

public class WrappedDeserializerInstance<T> implements Deserializer<T> {
    public static final String KEY_DESERIALIZER_INSTANCE_CONFIG = "wrappeddeserializerinstance.key.deserializer";
    public static final String VALUE_DESERIALIZER_INSTANCE_CONFIG = "wrappeddeserializerinstance.value.deserializer";

    private Deserializer<T> deserializerInstance;

    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        Object deserializer = configs.get(isKey ? KEY_DESERIALIZER_INSTANCE_CONFIG : VALUE_DESERIALIZER_INSTANCE_CONFIG);
        if (!(deserializer instanceof Deserializer)) {
            throw new ClientException("Illegal deserializer passed as instance argument");
        }
        deserializerInstance = (Deserializer<T>) deserializer;
        deserializerInstance.configure(configs, isKey);
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        return deserializerInstance.deserialize(topic, data);
    }

    @Override
    public void close() {
        if (deserializerInstance != null) {
            deserializerInstance.close();
            deserializerInstance = null;
        }
    }
}
