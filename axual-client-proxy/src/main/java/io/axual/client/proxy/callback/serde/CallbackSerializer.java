package io.axual.client.proxy.callback.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.callback.core.MethodInterceptor;
import io.axual.client.proxy.callback.core.CallbackConfig;
import io.axual.client.proxy.generic.serde.BaseSerializerProxy;

public class CallbackSerializer<T> extends BaseSerializerProxy<T, CallbackSerializerConfig<T>> {
    protected MethodInterceptor<CallbackConfig> interceptor;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(new CallbackSerializerConfig<>(new HashMap<>(configs), isKey));
        interceptor = new MethodInterceptor<>(config.getCallbackConfig());
    }

    @Override
    public byte[] serialize(final String topic, final Headers headers, final T object) {
        return interceptor.exec(this, "serialize", () -> backingSerializer.serialize(topic, headers, object), new String[]{"topic", "headers", "object"}, topic, headers, object);
    }
}
