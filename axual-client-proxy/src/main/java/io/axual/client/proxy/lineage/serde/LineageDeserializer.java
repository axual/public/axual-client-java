package io.axual.client.proxy.lineage.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.serde.BaseDeserializerProxy;
import io.axual.client.proxy.lineage.core.LineageAppender;

public class LineageDeserializer<T> extends BaseDeserializerProxy<T, LineageDeserializerConfig<T>> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(new LineageDeserializerConfig<>(new HashMap<>(configs), isKey));
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        // Append the lineage headers
        if (config.shouldAddLineage()) {
            LineageAppender.appendLineageForDeserialization(headers, config.getLineageConfig());
        }

        // Deserialize the object using the backing deserializer
        return backingDeserializer.deserialize(topic, headers, data);
    }
}
