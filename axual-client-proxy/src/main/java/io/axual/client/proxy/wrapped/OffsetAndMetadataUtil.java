package io.axual.client.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * Contains helpers to clean Kafka OffsetAndMetadata objects
 */
public class OffsetAndMetadataUtil {
    private static final Logger LOG = LoggerFactory.getLogger(OffsetAndMetadataUtil.class);
    private static final String FIELD_NAME_COPY_FLAGS = "copyFlags";
    public static final OffsetAndMetadataUtil INSTANCE = new OffsetAndMetadataUtil();
    private final ObjectMapper objectMapper;

    private OffsetAndMetadataUtil() {
        // Default private constructor
        objectMapper = new ObjectMapper();
    }

    /**
     * Clean the metadata of the OffsetAndMetadata object if an Axual Offset Distribution data model is found. If any other data or model is found, that will be returned
     *
     * @param offsetAndMetadata the object to clean
     * @return a cleaned OffsetAndMetadata instance, or null if the parameter was null
     */
    public OffsetAndMetadata cleanMetadata(OffsetAndMetadata offsetAndMetadata) {
        if (offsetAndMetadata == null) {
            return null;
        }

        String metadata = offsetAndMetadata.metadata().trim();
        if (metadata.startsWith("{")) {
            try {
                JsonNode rootNode = objectMapper.readTree(metadata);
                if (rootNode.has(FIELD_NAME_COPY_FLAGS)) {
                    // It's the Axual Offset Distribution wrapper, 
                    return new OffsetAndMetadata(offsetAndMetadata.offset(), offsetAndMetadata.leaderEpoch(), null);
                }
            } catch (JsonProcessingException e) {
                LOG.trace("Could not parse JSON Metadata for offset", e);
            }
        }

        // The metadata is not an Axual Distributor value, the original values can be used
        return offsetAndMetadata;
    }

    /**
     * Cleans the metadata of the OffsetAndMetadata values in the supplied map if an Axual Offset Distribution data model is found. If any other data or model is found, that will be returned
     *
     * @param offsetAndMetadataMap the map with the OffsetAndMetadata to clean
     * @return a cleaned map using the original key and a cleaned value
     */
    public <K> Map<K, OffsetAndMetadata> cleanMetadata(Map<K, OffsetAndMetadata> offsetAndMetadataMap) {
        Objects.requireNonNull(offsetAndMetadataMap);

        Map<K, OffsetAndMetadata> cleaned = new HashMap<>(offsetAndMetadataMap.size());
        offsetAndMetadataMap.forEach((k, v) -> cleaned.put(k, cleanMetadata(v)));
        return cleaned;
    }
}
