package io.axual.client.proxy.switching.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;

import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.switching.generic.BaseClientProxySwitcher;
import io.axual.discovery.client.DiscoveryResult;

/**
 * Creates a new proxied object from the configured proxy factory.
 * @param <K> key type of the producer proxy.
 * @param <V> value type of the producer proxy.
 */
public class ProducerSwitcher<K, V> extends BaseClientProxySwitcher<ProducerProxy<K, V>, SwitchingProducerConfig<K, V>> {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerSwitcher.class);

    @Override
    @SuppressWarnings("unchecked")
    protected ProducerProxy<K, V> createProxyObject(SwitchingProducerConfig config, DiscoveryResult discoveryResult) {
        Map<String, Object> properties = config.getDownstreamConfigs();

        // Add all discovery properties
        properties.putAll(discoveryResult.getConfigs());

        LOG.info("Creating a new {} with properties: {}", config.getProxyType(), properties);
        ClientProxyFactory<ProducerProxy<K, V>> factory = config.getBackingFactory();
        return factory.create(properties);
    }

    @Override
    protected Duration getSwitchTimeout(SwitchingProducerConfig config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        if (config.awaitDistributorOnSwitch()) {
            // When waiting for Distributor to complete its work, we take into account the
            // relative distance between the old and new cluster by multiplying the distance with
            // the Distributor Timeout.
            return super.getSwitchTimeout(config, oldResult, newResult);
        }

        return Duration.ZERO;
    }
}
