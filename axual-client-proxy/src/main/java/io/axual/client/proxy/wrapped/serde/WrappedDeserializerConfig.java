package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.proxy.BaseSerdeConfig;
import io.axual.client.proxy.generic.serde.DeserializerProxyFactory;

public class WrappedDeserializerConfig<T> extends BaseSerdeConfig {
    public static final String DESERIALIZER_CONFIG = "wrappeddeserializer.deserializer";

    private final Deserializer<T> deserializer;

    public WrappedDeserializerConfig(Map<String, ?> configs, boolean isKey) {
        super(configs, isKey);
        filterDownstream(DESERIALIZER_CONFIG);

        Object deserializerConfig = configs.get(WrappedDeserializerConfig.DESERIALIZER_CONFIG);
        if (deserializerConfig instanceof DeserializerProxyFactory) {
            deserializer = ((DeserializerProxyFactory<T>) deserializerConfig).create(new HashMap<>(getDownstreamConfigs()), isKey);
        } else {
            deserializer = getConfiguredInstance(WrappedDeserializerConfig.DESERIALIZER_CONFIG, Deserializer.class);
            deserializer.configure(getDownstreamConfigs(), isKey);
        }
    }

    public Deserializer<T> getDeserializer() {
        return deserializer;
    }
}
