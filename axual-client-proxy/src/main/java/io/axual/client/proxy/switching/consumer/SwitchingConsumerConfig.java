package io.axual.client.proxy.switching.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Map;

import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.switching.generic.SwitchingProxyConfig;

import static io.axual.client.proxy.config.DeliveryStrategy.AT_LEAST_ONCE;
import static io.axual.client.proxy.config.DeliveryStrategy.AT_MOST_ONCE;

public class SwitchingConsumerConfig<K, V> extends SwitchingProxyConfig<ConsumerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "switchingconsumer.backing.factory";
    public static final String DELIVERY_STRATEGY_ON_SWITCH = "delivery.strategy.on.switch";

    private static final String LATEST = "latest";

    private final boolean isAtLeastOnce;
    private final boolean istAtMostOnceOnSwitch;

    SwitchingConsumerConfig(Map<String, Object> configs) {
        super(configs, "consumer", BACKING_FACTORY_CONFIG);

        // Figure out if the consumption strategy is ALO
        isAtLeastOnce = !LATEST.equals(parseStringConfig(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, false, LATEST));
        istAtMostOnceOnSwitch = AT_MOST_ONCE.toString().equals(parseStringConfig(DELIVERY_STRATEGY_ON_SWITCH, false, AT_LEAST_ONCE.toString()));
    }

    boolean isAtLeastOnce() {
        return isAtLeastOnce;
    }

    public boolean isIstAtMostOnceOnSwitch() {
        return istAtMostOnceOnSwitch;
    }
}
