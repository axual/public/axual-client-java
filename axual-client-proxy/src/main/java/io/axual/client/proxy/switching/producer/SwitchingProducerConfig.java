package io.axual.client.proxy.switching.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Map;

import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.switching.generic.SwitchingProxyConfig;

/**
 * Holds configuration for a switching producer proxy.
 * @param <K> key type of the proxied producer.
 * @param <V> value type of the proxied producer.
 */
public class SwitchingProducerConfig<K, V> extends SwitchingProxyConfig<ProducerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "switchingproducer.backing.factory";

    private final boolean awaitDistributorOnSwitch;

    public SwitchingProducerConfig(Map<String, Object> configs) {
        super(configs, "producer", BACKING_FACTORY_CONFIG);

        // Parse producer settings
        awaitDistributorOnSwitch = isMaxInFlightRequestsEqualToOne();
    }

    private boolean isMaxInFlightRequestsEqualToOne() {
        // Parse the parameter, the "5" comes from the KafkaProducer documentation, which indicates
        // that if not set then 5 is the default.
        String maxInFlightRequests = parseStringConfig(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, false, "5");
        try {
            return Integer.parseInt(maxInFlightRequests) == 1;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    public boolean awaitDistributorOnSwitch() {
        return awaitDistributorOnSwitch;
    }
}
