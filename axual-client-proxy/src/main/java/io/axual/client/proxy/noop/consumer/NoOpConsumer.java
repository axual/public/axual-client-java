package io.axual.client.proxy.noop.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.generic.consumer.StaticConsumerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.common.tools.MapUtil;

public class NoOpConsumer<K, V> extends StaticConsumerProxy<K, V, NoOpConsumerConfig<K, V>> {
    public NoOpConsumer(Map<String, Object> configs) {
        super(new NoOpConsumerConfig<>(configs));
    }

    public NoOpConsumer(Map<String, Object> configs, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(SerdeUtil.addDeserializersToConfigs(configs, keyDeserializer, valueDeserializer));
    }

    public NoOpConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public NoOpConsumer(Properties properties, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(MapUtil.objectToStringMap(properties), keyDeserializer, valueDeserializer);
    }
}
