package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

import io.axual.common.exception.ClientException;

public class WrappedSerializerInstance<T> implements Serializer<T> {
    public static final String KEY_SERIALIZER_INSTANCE_CONFIG = "wrappedserializerinstance.key.serializer";
    public static final String VALUE_SERIALIZER_INSTANCE_CONFIG = "wrappedserializerinstance.value.serializer";

    private Serializer<T> serializerInstance;

    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        Object serializer = configs.get(isKey ? KEY_SERIALIZER_INSTANCE_CONFIG : VALUE_SERIALIZER_INSTANCE_CONFIG);
        if (!(serializer instanceof Serializer)) {
            throw new ClientException("Illegal serializer passed as instance argument");
        }
        serializerInstance = (Serializer<T>) serializer;
        serializerInstance.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, T object) {
        return serializerInstance.serialize(topic, object);
    }

    @Override
    public void close() {
        if (serializerInstance != null) {
            serializerInstance.close();
            serializerInstance = null;
        }
    }
}
