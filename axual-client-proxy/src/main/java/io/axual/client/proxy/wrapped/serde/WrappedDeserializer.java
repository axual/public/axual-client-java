package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.proxy.BaseProxy;
import io.axual.client.proxy.generic.serde.DeserializerProxy;

public class WrappedDeserializer<T> extends BaseProxy<WrappedDeserializerConfig> implements DeserializerProxy<T> {
    private Deserializer<T> deserializer;

    public WrappedDeserializer() {
        super(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        config = new WrappedDeserializerConfig(new HashMap<>(configs), isKey);
        deserializer = config.getDeserializer();
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        return deserializer.deserialize(topic, data);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        return deserializer.deserialize(topic, headers, data);
    }

    @Override
    public void close(Duration timeout) {
        if (deserializer != null) {
            deserializer.close();
            deserializer = null;
        }
        super.close(timeout);
    }
}
