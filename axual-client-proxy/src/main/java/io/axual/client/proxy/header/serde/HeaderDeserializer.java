package io.axual.client.proxy.header.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.serde.BaseDeserializerProxy;
import io.axual.serde.utils.HeaderUtils;
import io.axual.serde.utils.SerdeUtils;
import io.axual.serde.valueheader.ValueHeader;
import io.axual.serde.valueheader.ValueHeaderDeserializer;

import static io.axual.client.proxy.lineage.LineageHeaders.COPY_FLAGS_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.MESSAGE_ID_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.SERIALIZATION_TIME_HEADER;
import static io.axual.serde.utils.SerdeUtils.getValueHeaderSize;

public class HeaderDeserializer<T> extends BaseDeserializerProxy<T, HeaderDeserializerConfig<T>> {
    private ValueHeaderDeserializer valueHeaderDeserializer;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(new HeaderDeserializerConfig<>(new HashMap<>(configs), isKey));

        // Instantiate our value header deserializer
        valueHeaderDeserializer = new ValueHeaderDeserializer();
        valueHeaderDeserializer.configure(config.getDownstreamConfigs(), isKey);
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        return deserialize(topic, null, data);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        // Check if we need to decode a value header
        if (!config.isKey() && SerdeUtils.containsValueHeader(data)) {
            int headerSize = getValueHeaderSize(data);
            byte[] body = data.length > headerSize ? Arrays.copyOfRange(data, headerSize, data.length) : null;
            if (headers != null) {
                copyValueHeaderToKafkaHeaders(valueHeaderDeserializer.deserialize(topic, headers, data), headers);
            }
            return backingDeserializer.deserialize(topic, headers, body);
        }

        return backingDeserializer.deserialize(topic, headers, data);
    }

    private static void copyValueHeaderToKafkaHeaders(ValueHeader valueHeader, Headers kafkaHeaders) {
        // Copy ValueHeader field to the Kafka headers, if they are not already present. If they are
        // then Kafka headers always prevail the ValueHeader.

        if (kafkaHeaders.lastHeader(MESSAGE_ID_HEADER) == null) {
            // Copy the message id from value header to Kafka header
            HeaderUtils.addUuidHeader(kafkaHeaders, MESSAGE_ID_HEADER, valueHeader.getMessageId());
        }

        if (kafkaHeaders.lastHeader(SERIALIZATION_TIME_HEADER) == null) {
            // Copy the serialization time from value header to Kafka header
            HeaderUtils.addLongHeader(kafkaHeaders, SERIALIZATION_TIME_HEADER, valueHeader.getSerializationTimestamp());
        }

        if (kafkaHeaders.lastHeader(COPY_FLAGS_HEADER) == null) {
            // Copy the copy flags from value header to Kafka header
            HeaderUtils.addIntegerHeader(kafkaHeaders, COPY_FLAGS_HEADER, (int) valueHeader.getCopyFlags() & 0xff);
        }
    }

    @Override
    public void close(Duration duration) {
        valueHeaderDeserializer.close();
        super.close(duration);
    }
}
