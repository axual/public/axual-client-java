package io.axual.client.proxy.switching.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.client.proxy.generic.client.DynamicClientProxy;
import io.axual.common.concurrent.LockedObject;
import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.exception.NoClusterAvailableException;
import io.axual.discovery.client.DiscoveryResult;

/**
 * Base class for proxies which have the ability to switch the underlying proxy.
 * @param <T> proxy type contained in this class.
 * @param <C> configuration for the switching proxy.
 */
public abstract class SwitchingProxy<T extends ClientProxy, C extends SwitchingProxyConfig> extends DynamicClientProxy<T, C> {
    private final DiscoverySubscriber<T, C> subscriber;

    protected SwitchingProxy(C config, final DiscoverySubscriber<T, C> subscriber) {
        super(config, subscriber);
        this.subscriber = subscriber;
    }

    protected ClientProxySwitcher<T, C> getSwitcher() {
        return subscriber.getSwitcher();
    }

    @Override
    protected synchronized boolean maybeReplaceProxiedObject(boolean force) {
        subscriber.checkDiscovery();

        // If we replaced the proxied object, then also throw away the cached properties
        return super.maybeReplaceProxiedObject(force);
    }


    protected boolean maybeReplaceProxiedObject(boolean force, boolean isTransactional) {
        subscriber.checkDiscovery();

        return super.maybeReplaceProxiedObject(force, isTransactional);
    }

    @Override
    protected LockedObject<T>.ReadLock getReadLock() {
        LockedObject<T>.ReadLock result = super.getReadLock();
        if (result.object == null) {
            result.close();
            throw new NoClusterAvailableException();
        }

        return result;
    }

    protected DiscoveryResult getCurrentDiscoveryResult() {
        return subscriber.getCurrentDiscoveryResult();
    }

    protected String getDiscoveryProperty(String key) {
        return subscriber.getCurrentDiscoveryProperty(key);
    }
}
