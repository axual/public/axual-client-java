package io.axual.client.proxy.callback.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.client.proxy.callback.client.CallbackClientProxyConfig;
import io.axual.client.proxy.generic.admin.AdminProxy;

public class CallbackAdminConfig extends CallbackClientProxyConfig<AdminProxy> {
    public static final String BACKING_FACTORY_CONFIG = "callbackadmin.backing.factory";

    public CallbackAdminConfig(Map<String, Object> configs) {
        super(configs, BACKING_FACTORY_CONFIG);
    }
}
