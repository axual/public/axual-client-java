package io.axual.client.proxy.generic.registry;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

public class ProxyChainElement {
    private final String proxyId;
    private final Map<String, Object> configs;

    private ProxyChainElement(Builder builder) {
        this.proxyId = builder.proxyId;
        this.configs = builder.configs;
    }

    public String getProxyId() {
        return proxyId;
    }

    public Map<String, Object> getConfigs() {
        return configs;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private String proxyId;
        private Map<String, Object> configs;

        public Builder setProxyId(String proxyId) {
            this.proxyId = proxyId;
            return this;
        }

        public Builder setConfigs(ProxyConfigs configs) {
            this.configs = new HashMap<>(configs);
            return this;
        }

        public Builder setConfigs(Map<String, Object> configs) {
            this.configs = configs;
            return this;
        }

        public ProxyChainElement build() {
            return new ProxyChainElement(this);
        }
    }
}
