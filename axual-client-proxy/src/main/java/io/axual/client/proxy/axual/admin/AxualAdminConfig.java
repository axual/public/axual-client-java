package io.axual.client.proxy.axual.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.client.proxy.axual.generic.AxualProxyConfig;
import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.wrapped.admin.WrappedAdminClientFactory;

public class AxualAdminConfig extends AxualProxyConfig<AdminProxy> {
    public static final String BACKING_FACTORY_CONFIG = "axualadmin.backing.factory";
    public static final String CHAIN_CONFIG = "axualadmin.chain";

    public AxualAdminConfig(Map<String, Object> configs) {
        super(addDefaultFactory(configs, BACKING_FACTORY_CONFIG, WrappedAdminClientFactory.class),
                "admin",
                BACKING_FACTORY_CONFIG,
                CHAIN_CONFIG);
    }
}
