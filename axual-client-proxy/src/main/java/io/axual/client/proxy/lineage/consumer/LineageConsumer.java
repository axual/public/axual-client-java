package io.axual.client.proxy.lineage.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.Deserializer;

import java.time.Duration;
import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.generic.consumer.StaticConsumerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.lineage.core.LineageAppender;
import io.axual.client.proxy.lineage.core.LineageConfig;
import io.axual.common.tools.MapUtil;

public class LineageConsumer<K, V> extends StaticConsumerProxy<K, V, LineageConsumerConfig<K, V>> {
    private final LineageConfig lineageConfig;

    public LineageConsumer(Map<String, Object> configs) {
        super(new LineageConsumerConfig(configs));
        lineageConfig = new LineageConfig(configs);
    }

    public LineageConsumer(Map<String, Object> configs, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(SerdeUtil.addDeserializersToConfigs(configs, keyDeserializer, valueDeserializer));
    }

    public LineageConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public LineageConsumer(Properties properties, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(MapUtil.objectToStringMap(properties), keyDeserializer, valueDeserializer);
    }

    /**
     * Poll Kafka for records.
     *
     * @param timeout Timeout in milliseconds
     * @return List of ConsumerRecords polled from Kafka
     * @deprecated
     */
    @Deprecated
    @Override
    public ConsumerRecords<K, V> poll(long timeout) {
        return attachLineage(super.poll(timeout));
    }

    @Override
    public ConsumerRecords<K, V> poll(Duration timeout) {
        return attachLineage(super.poll(timeout));
    }

    private ConsumerRecords<K, V> attachLineage(ConsumerRecords<K, V> records) {
        for (ConsumerRecord<K, V> record : records) {
            LineageAppender.appendLineageForDeserialization(record.headers(), lineageConfig);
        }
        return records;
    }
}
