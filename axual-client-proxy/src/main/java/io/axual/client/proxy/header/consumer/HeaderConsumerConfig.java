package io.axual.client.proxy.header.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;

import java.util.Map;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;

public class HeaderConsumerConfig<K, V> extends BaseClientProxyConfig<ConsumerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "headerconsumer.backing.factory";

    HeaderConsumerConfig(Map<String, Object> configs) {
        super(configs,BACKING_FACTORY_CONFIG);

        // Parse deserializers
        Object keyDeserializer = configs.getOrDefault(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        Object valueDeserializer = configs.getOrDefault(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());

        // Insert the HeaderConsumerDeserializer in the deserialization chain
        putDownstream(HeaderConsumerDeserializer.BACKING_KEY_DESERIALIZER_CONFIG, keyDeserializer);
        putDownstream(HeaderConsumerDeserializer.BACKING_VALUE_DESERIALIZER_CONFIG, valueDeserializer);
        putDownstream(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, HeaderConsumerDeserializer.class);
        putDownstream(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, HeaderConsumerDeserializer.class);
    }
}
