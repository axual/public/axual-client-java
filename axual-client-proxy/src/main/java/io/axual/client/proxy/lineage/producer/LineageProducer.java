package io.axual.client.proxy.lineage.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import io.axual.client.proxy.generic.producer.StaticProducerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.lineage.core.LineageAppender;
import io.axual.client.proxy.lineage.core.LineageConfig;
import io.axual.common.tools.MapUtil;

public class LineageProducer<K, V> extends StaticProducerProxy<K, V, LineageProducerConfig<K, V>> {
    private final LineageConfig lineageConfig;

    public LineageProducer(Map<String, Object> configs) {
        super(new LineageProducerConfig<>(configs));
        lineageConfig = new LineageConfig(configs);
    }

    public LineageProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public LineageProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public LineageProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord) {
        LineageAppender.appendLineageForSerialization(producerRecord.headers(), lineageConfig);
        return super.send(producerRecord);
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord, Callback callback) {
        LineageAppender.appendLineageForSerialization(producerRecord.headers(), lineageConfig);
        return super.send(producerRecord, callback);
    }
}
