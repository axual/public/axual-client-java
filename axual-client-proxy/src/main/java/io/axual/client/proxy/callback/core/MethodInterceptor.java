package io.axual.client.proxy.callback.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.UUID;

public class MethodInterceptor<C extends CallbackConfig> {
    private final MethodCallFactory callFactory;

    public interface Procedure {
        void call();
    }

    public interface Function<T> {
        T call();
    }

    public MethodInterceptor(C config) {
        callFactory = config.getCallFactory();
    }

    public void execProc(final Object object, final String method, final Procedure proc) {
        Function<Void> function = () -> {
            proc.call();
            return null;
        };
        execute(object, method, function, false, null, (Object) null);
    }

    public void execProc(final Object object, final String method, final Procedure proc, final String[] paramNames, final Object... paramValues) {
        Function<Void> function = () -> {
            proc.call();
            return null;
        };
        execute(object, method, function, false, paramNames, paramValues);
    }

    public <T> T exec(final Object object, final String method, final Function<T> function) {
        return execute(object, method, function, true, null, (Object) null);
    }

    public <T> T exec(final Object object, final String method, final Function<T> function, final String[] paramNames, final Object... paramValues) {
        return execute(object, method, function, true, paramNames, paramValues);
    }

    private <T> T execute(final Object object, final String method, final Function<T> function, final boolean hasReturnValue, final String[] paramNames, final Object... paramValues) {
        try (final MethodCall call = callFactory.create(UUID.randomUUID(), object, method)) {
            call.onEnter(paramNames, paramValues);

            try {
                try {
                    T result = function.call();
                    if (hasReturnValue) {
                        call.onResult(result);
                    }
                    return result;
                } catch (Exception e) {
                    call.onException(e);
                    throw e;
                }
            } finally {
                call.onExit();
            }
        }
    }
}
