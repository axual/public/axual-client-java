package io.axual.client.proxy.generic.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import io.axual.client.proxy.generic.client.StaticClientProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import org.apache.kafka.common.errors.ProducerFencedException;

public class StaticProducerProxy<K, V, C extends BaseClientProxyConfig<ProducerProxy<K, V>>> extends StaticClientProxy<ProducerProxy<K, V>, C> implements ProducerProxy<K, V> {
    public StaticProducerProxy(C config) {
        super(config);
    }

    public StaticProducerProxy(ClientProxyInitializer<ProducerProxy<K, V>, C> initializer) {
        super(initializer);
    }

    @Override
    public void initTransactions() {
        proxiedObject.initTransactions();
    }

    @Override
    public void beginTransaction() {
        proxiedObject.beginTransaction();
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
                                         String consumerGroupId) {
        proxiedObject.sendOffsetsToTransaction(offsets, consumerGroupId);
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
        ConsumerGroupMetadata groupMetadata) throws ProducerFencedException {
        proxiedObject.sendOffsetsToTransaction(offsets,groupMetadata);
    }

    @Override
    public void commitTransaction() {
        proxiedObject.commitTransaction();
    }

    @Override
    public void abortTransaction() {
        proxiedObject.abortTransaction();
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> record) {
        return proxiedObject.send(record);
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> record, Callback callback) {
        return proxiedObject.send(record, callback);
    }

    @Override
    public void flush() {
        proxiedObject.flush();
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        return proxiedObject.partitionsFor(topic);
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return proxiedObject.metrics();
    }
}
