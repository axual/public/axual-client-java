package io.axual.client.proxy.callback.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AbortTransactionOptions;
import org.apache.kafka.clients.admin.AbortTransactionResult;
import org.apache.kafka.clients.admin.AbortTransactionSpec;
import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterClientQuotasResult;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsResult;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.CreateDelegationTokenOptions;
import org.apache.kafka.clients.admin.CreateDelegationTokenResult;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasResult;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeDelegationTokenOptions;
import org.apache.kafka.clients.admin.DescribeDelegationTokenResult;
import org.apache.kafka.clients.admin.DescribeFeaturesOptions;
import org.apache.kafka.clients.admin.DescribeFeaturesResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.DescribeProducersOptions;
import org.apache.kafka.clients.admin.DescribeProducersResult;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.DescribeTransactionsOptions;
import org.apache.kafka.clients.admin.DescribeTransactionsResult;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsResult;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ElectLeadersResult;
import org.apache.kafka.clients.admin.ExpireDelegationTokenOptions;
import org.apache.kafka.clients.admin.ExpireDelegationTokenResult;
import org.apache.kafka.clients.admin.FeatureUpdate;
import org.apache.kafka.clients.admin.FenceProducersOptions;
import org.apache.kafka.clients.admin.FenceProducersResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.ListTransactionsOptions;
import org.apache.kafka.clients.admin.ListTransactionsResult;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.RenewDelegationTokenOptions;
import org.apache.kafka.clients.admin.RenewDelegationTokenResult;
import org.apache.kafka.clients.admin.UnregisterBrokerOptions;
import org.apache.kafka.clients.admin.UnregisterBrokerResult;
import org.apache.kafka.clients.admin.UpdateFeaturesOptions;
import org.apache.kafka.clients.admin.UpdateFeaturesResult;
import org.apache.kafka.clients.admin.UserScramCredentialAlteration;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.TopicCollection;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import io.axual.client.proxy.callback.client.CallbackClientProxy;
import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.common.tools.MapUtil;

public class CallbackAdminClient extends
    CallbackClientProxy<AdminProxy, CallbackAdminConfig> implements AdminProxy {

  private static final String GROUP_ID = "groupId";
  private static final String OPTIONS = "options";
  private static final String PARTITIONS = "partitions";
  private static final String COLLECTION = "collection";

  public CallbackAdminClient(Map<String, Object> configs) {
    super(new CallbackAdminConfig(configs));
  }

  public static CallbackAdminClient create(Properties properties) {
    return new CallbackAdminClient(MapUtil.objectToStringMap(properties));
  }

  public static CallbackAdminClient create(Map<String, Object> configs) {
    return new CallbackAdminClient(configs);
  }

  @Override
  public void close(Duration timeout) {
    interceptor
        .execProc(this, "close", () -> super.close(timeout), new String[]{"timeout"}, timeout);
  }

  @Override
  public CreateTopicsResult createTopics(final Collection<NewTopic> newTopics,
      final CreateTopicsOptions options) {
    return interceptor
        .exec(this, "createTopics", () -> proxiedObject.createTopics(newTopics, options),
            new String[]{"newTopics", OPTIONS}, newTopics, options);
  }

  @Override
  public DeleteTopicsResult deleteTopics(final Collection<String> topics,
      final DeleteTopicsOptions options) {
    return interceptor.exec(this, "deleteTopics", () -> proxiedObject.deleteTopics(topics, options),
        new String[]{"topics", OPTIONS}, topics, options);
  }

  @Override
  public DeleteTopicsResult deleteTopics(TopicCollection topicCollection, DeleteTopicsOptions deleteTopicsOptions) {
    return interceptor.exec(this, "deleteTopics", () -> proxiedObject.deleteTopics(topicCollection, deleteTopicsOptions),
            new String[]{"topics", OPTIONS}, topicCollection, deleteTopicsOptions);
  }

  @Override
  public ListTopicsResult listTopics(final ListTopicsOptions options) {
    return interceptor
        .exec(this, "listTopics", () -> proxiedObject.listTopics(options), new String[]{OPTIONS},
            options);
  }

  @Override
  public DescribeTopicsResult describeTopics(final Collection<String> topicNames,
      final DescribeTopicsOptions options) {
    return interceptor
        .exec(this, "describeTopics", () -> proxiedObject.describeTopics(topicNames, options),
            new String[]{"topicNames", OPTIONS}, topicNames, options);
  }

  @Override
  public DescribeTopicsResult describeTopics(TopicCollection topicCollection, DescribeTopicsOptions describeTopicsOptions) {
    return interceptor
            .exec(this, "describeTopics", () -> proxiedObject.describeTopics(topicCollection, describeTopicsOptions),
                    new String[]{"topicNames", OPTIONS}, topicCollection, describeTopicsOptions);
  }

  @Override
  public DescribeClusterResult describeCluster(final DescribeClusterOptions options) {
    return interceptor.exec(this, "describeCluster", () -> proxiedObject.describeCluster(options),
        new String[]{OPTIONS}, options);
  }

  @Override
  public DescribeAclsResult describeAcls(final AclBindingFilter filter,
      final DescribeAclsOptions options) {
    return interceptor.exec(this, "describeAcls", () -> proxiedObject.describeAcls(filter, options),
        new String[]{"filter", OPTIONS}, filter, options);
  }

  @Override
  public CreateAclsResult createAcls(final Collection<AclBinding> acls,
      final CreateAclsOptions options) {
    return interceptor.exec(this, "createAcls", () -> proxiedObject.createAcls(acls, options),
        new String[]{"acls", OPTIONS}, acls, options);
  }

  @Override
  public DeleteAclsResult deleteAcls(final Collection<AclBindingFilter> filters,
      final DeleteAclsOptions options) {
    return interceptor.exec(this, "deleteAcls", () -> proxiedObject.deleteAcls(filters, options),
        new String[]{"filters", OPTIONS}, filters, options);
  }

  @Override
  public DescribeConfigsResult describeConfigs(final Collection<ConfigResource> resources,
      final DescribeConfigsOptions options) {
    return interceptor
        .exec(this, "describeConfigs", () -> proxiedObject.describeConfigs(resources, options),
            new String[]{"resources", OPTIONS}, resources, options);
  }

  @Override
  public synchronized AlterConfigsResult alterConfigs(final Map<ConfigResource, Config> configs,
      final AlterConfigsOptions options) {
    return interceptor
        .exec(this, "alterConfigs", () -> proxiedObject.alterConfigs(configs, options),
            new String[]{"configs", OPTIONS}, configs, options);
  }

  @Override
  public AlterConfigsResult incrementalAlterConfigs(
      Map<ConfigResource, Collection<AlterConfigOp>> configs, AlterConfigsOptions options) {
    return interceptor.exec(this, "incrementalAlterConfigs",
        () -> proxiedObject.incrementalAlterConfigs(configs, options),
        new String[]{"configs", OPTIONS}, configs, options);
  }

  @Override
  public AlterReplicaLogDirsResult alterReplicaLogDirs(
      final Map<TopicPartitionReplica, String> replicaAssignment,
      final AlterReplicaLogDirsOptions options) {
    return interceptor.exec(this, "alterReplicaLogDirs",
        () -> proxiedObject.alterReplicaLogDirs(replicaAssignment, options),
        new String[]{"replicaAssignment", OPTIONS}, replicaAssignment, options);
  }

  @Override
  public DescribeLogDirsResult describeLogDirs(final Collection<Integer> brokers,
      final DescribeLogDirsOptions options) {
    return interceptor
        .exec(this, "describeLogDirs", () -> proxiedObject.describeLogDirs(brokers, options),
            new String[]{"brokers", OPTIONS}, brokers, options);
  }

  @Override
  public DescribeReplicaLogDirsResult describeReplicaLogDirs(
      final Collection<TopicPartitionReplica> replicas,
      final DescribeReplicaLogDirsOptions options) {
    return interceptor.exec(this, "describeReplicaLogDirs",
        () -> proxiedObject.describeReplicaLogDirs(replicas, options),
        new String[]{"replicas", OPTIONS}, replicas, options);
  }

  @Override
  public CreatePartitionsResult createPartitions(final Map<String, NewPartitions> newPartitions,
      final CreatePartitionsOptions options) {
    return interceptor.exec(this, "createPartitions",
        () -> proxiedObject.createPartitions(newPartitions, options),
        new String[]{"newPartitions", OPTIONS}, newPartitions, options);
  }

  @Override
  public DeleteRecordsResult deleteRecords(
      final Map<TopicPartition, RecordsToDelete> recordsToDelete,
      final DeleteRecordsOptions options) {
    return interceptor
        .exec(this, "deleteRecords", () -> proxiedObject.deleteRecords(recordsToDelete, options),
            new String[]{"recordsToDelete", OPTIONS}, recordsToDelete, options);
  }

  @Override
  public CreateDelegationTokenResult createDelegationToken(CreateDelegationTokenOptions options) {
    return interceptor
        .exec(this, "createDelegationToken", () -> proxiedObject.createDelegationToken(options),
            new String[]{OPTIONS}, options);
  }

  @Override
  public RenewDelegationTokenResult renewDelegationToken(byte[] hmac,
      RenewDelegationTokenOptions options) {
    return interceptor
        .exec(this, "renewDelegationToken", () -> proxiedObject.renewDelegationToken(hmac),
            new String[]{"hmac", OPTIONS}, hmac, options);
  }

  @Override
  public ExpireDelegationTokenResult expireDelegationToken(byte[] hmac,
      ExpireDelegationTokenOptions options) {
    return interceptor.exec(this, "expireDelegationToken",
        () -> proxiedObject.expireDelegationToken(hmac, options), new String[]{"hmac", OPTIONS},
        hmac, options);
  }

  @Override
  public DescribeDelegationTokenResult describeDelegationToken(
      DescribeDelegationTokenOptions options) {
    return interceptor
        .exec(this, "describeDelegationToken", () -> proxiedObject.describeDelegationToken(options),
            new String[]{OPTIONS}, options);
  }

  @Override
  public DescribeConsumerGroupsResult describeConsumerGroups(Collection<String> groupIds,
      DescribeConsumerGroupsOptions options) {
    return interceptor
        .exec(this, "describeConsumerGroups", () -> proxiedObject.describeConsumerGroups(groupIds),
            new String[]{"groupIds", OPTIONS}, groupIds, options);
  }

  @Override
  public ListConsumerGroupsResult listConsumerGroups(ListConsumerGroupsOptions options) {
    return interceptor
        .exec(this, "listConsumerGroups", () -> proxiedObject.listConsumerGroups(options),
            new String[]{OPTIONS}, options);
  }

  @Override
  public ListConsumerGroupOffsetsResult listConsumerGroupOffsets(String groupId,
      ListConsumerGroupOffsetsOptions options) {
    return interceptor.exec(this, "listConsumerGroupOffsets",
        () -> proxiedObject.listConsumerGroupOffsets(groupId), new String[]{GROUP_ID, OPTIONS},
        groupId, options);
  }

  @Override
  public DeleteConsumerGroupsResult deleteConsumerGroups(Collection<String> groupIds,
      DeleteConsumerGroupsOptions options) {
    return interceptor
        .exec(this, "deleteConsumerGroups", () -> proxiedObject.deleteConsumerGroups(groupIds),
            new String[]{"groupIds", OPTIONS}, groupIds, options);
  }

  @Override
  public DeleteConsumerGroupOffsetsResult deleteConsumerGroupOffsets(String groupId,
      Set<TopicPartition> partitions, DeleteConsumerGroupOffsetsOptions options) {
    return interceptor.exec(this, "deleteConsumerGroupOffsets",
        () -> proxiedObject.deleteConsumerGroupOffsets(groupId, partitions, options),
        new String[]{GROUP_ID, PARTITIONS, OPTIONS}, groupId, partitions, options);
  }

  @Override
  public ElectLeadersResult electLeaders(ElectionType electionType,
      Set<TopicPartition> partitions,
      ElectLeadersOptions options) {
    return interceptor.exec(this, "electLeaders",
        () -> proxiedObject.electLeaders(electionType, partitions, options),
        new String[]{"electionType", PARTITIONS, OPTIONS}, electionType, partitions, options);
  }

  @Override
  public AlterPartitionReassignmentsResult alterPartitionReassignments(
      Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments,
      AlterPartitionReassignmentsOptions options) {
    return interceptor.exec(this, "alterPartitionReassignments",
        () -> proxiedObject.alterPartitionReassignments(reassignments, options),
        new String[]{"reassignments", OPTIONS}, reassignments, options);
  }

  @Override
  public ListPartitionReassignmentsResult listPartitionReassignments(
      Optional<Set<TopicPartition>> partitions, ListPartitionReassignmentsOptions options) {
    return interceptor.exec(this, "listPartitionReassignments",
        () -> proxiedObject.listPartitionReassignments(partitions, options),
        new String[]{PARTITIONS, OPTIONS}, partitions, options);
  }

  @Override
  public RemoveMembersFromConsumerGroupResult removeMembersFromConsumerGroup(String groupId,
      RemoveMembersFromConsumerGroupOptions options) {
    return interceptor.exec(this, "removeMembersFromConsumerGroup",
        () -> proxiedObject.removeMembersFromConsumerGroup(groupId, options),
        new String[]{GROUP_ID, OPTIONS}, groupId, options);
  }

  @Override
  public AlterConsumerGroupOffsetsResult alterConsumerGroupOffsets(String groupId,
      Map<TopicPartition, OffsetAndMetadata> offsets, AlterConsumerGroupOffsetsOptions options) {
    return interceptor.exec(this, "alterConsumerGroupOffsets",
        () -> proxiedObject.alterConsumerGroupOffsets(groupId, offsets, options),
        new String[]{GROUP_ID, "offsets", OPTIONS}, groupId, offsets, options);
  }

  @Override
  public ListOffsetsResult listOffsets(Map<TopicPartition, OffsetSpec> topicPartitionOffsets,
      ListOffsetsOptions options) {
    return interceptor.exec(this, "listOffsets",
        () -> proxiedObject.listOffsets(topicPartitionOffsets, options),
        new String[]{"topicPartitionOffsets", OPTIONS}, topicPartitionOffsets, options);
  }

  @Override
  public DescribeClientQuotasResult describeClientQuotas(ClientQuotaFilter filter,
      DescribeClientQuotasOptions options) {
    return interceptor.exec(this, "describeClientQuotas",
        () -> proxiedObject.describeClientQuotas(filter, options),
        new String[]{"filter", OPTIONS}, filter, options);
  }

  @Override
  public AlterClientQuotasResult alterClientQuotas(Collection<ClientQuotaAlteration> entries,
      AlterClientQuotasOptions options) {
    return interceptor.exec(this, "alterClientQuotas",
        () -> proxiedObject.alterClientQuotas(entries, options),
        new String[]{"entries", OPTIONS}, entries, options);
  }

  @Override
  public DescribeUserScramCredentialsResult describeUserScramCredentials(List<String> list,
                                                                         DescribeUserScramCredentialsOptions options) {
    return interceptor.exec(this, "describeUserScramCredentials",
            () -> proxiedObject.describeUserScramCredentials(list, options),
            new String[]{"list", OPTIONS}, list, options);
  }

  @Override
  public AlterUserScramCredentialsResult alterUserScramCredentials(List<UserScramCredentialAlteration> list,
                                                                   AlterUserScramCredentialsOptions options) {
    return interceptor.exec(this, "alterUserScramCredentials",
            () -> proxiedObject.alterUserScramCredentials(list, options),
            new String[]{"list", OPTIONS}, list, options);
  }

  @Override
  public DescribeFeaturesResult describeFeatures(DescribeFeaturesOptions options) {
    return interceptor.exec(this, "describeFeatures",
            () -> proxiedObject.describeFeatures(options),
            new String[]{OPTIONS}, options);
  }

  @Override
  public UpdateFeaturesResult updateFeatures(Map<String, FeatureUpdate> map,
                                             UpdateFeaturesOptions options) {
    return interceptor.exec(this, "updateFeatures",
            () -> proxiedObject.updateFeatures(map, options),
            new String[]{"map", OPTIONS}, map, options);
  }

  @Override
  public UnregisterBrokerResult unregisterBroker(int brokerId, UnregisterBrokerOptions options) {
    return interceptor.exec(this, "unregisterBroker",
            () -> proxiedObject.unregisterBroker(brokerId, options),
            new String[]{"brokerId", OPTIONS}, brokerId, options);
  }

  @Override
  public DescribeProducersResult describeProducers(Collection<TopicPartition> collection, DescribeProducersOptions describeProducersOptions) {
    return interceptor.exec(this, "describeProducers", () -> proxiedObject.describeProducers(collection, describeProducersOptions),
            new String[]{COLLECTION, "describeProducersOptions"}, collection, describeProducersOptions);
  }

  @Override
  public DescribeTransactionsResult describeTransactions(Collection<String> collection, DescribeTransactionsOptions describeTransactionsOptions) {
    return interceptor.exec(this, "describeTransactions", () -> proxiedObject.describeTransactions(collection, describeTransactionsOptions),
            new String[]{COLLECTION, "describeTransactionsOptions"}, collection, describeTransactionsOptions);
  }

  @Override
  public AbortTransactionResult abortTransaction(AbortTransactionSpec abortTransactionSpec, AbortTransactionOptions abortTransactionOptions) {
    return interceptor.exec(this, "abortTransaction", () -> proxiedObject.abortTransaction(abortTransactionSpec, abortTransactionOptions),
            new String[]{"abortTransactionSpec", "abortTransactionOptions"}, abortTransactionSpec, abortTransactionOptions);
  }

  @Override
  public ListTransactionsResult listTransactions(ListTransactionsOptions listTransactionsOptions) {
    return interceptor.exec(this, "listTransactions", () -> proxiedObject.listTransactions(listTransactionsOptions),
            new String[]{"listTransactionsOptions"}, listTransactionsOptions);
  }

  @Override
  public FenceProducersResult fenceProducers(Collection<String> collection, FenceProducersOptions fenceProducersOptions) {
    return interceptor.exec(this, "fenceProducers", () -> proxiedObject.fenceProducers(collection, fenceProducersOptions),
            new String[]{COLLECTION, "fenceProducersOptions"}, collection, fenceProducersOptions);
  }

  @Override
  public Map<MetricName, ? extends Metric> metrics() {
    return interceptor.exec(this, "metrics", proxiedObject::metrics);
  }
}
