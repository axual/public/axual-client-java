package io.axual.client.proxy.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.client.proxy.generic.serde.BaseDeserializerProxyConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;

public class AxualDeserializerConfig<T> extends BaseDeserializerProxyConfig<T> {
    public static final String CHAIN_CONFIG = "axualdeserializer.chain";
    public static final String DEFAULT_DESERIALIZER_CONFIG = "axualdeserializer.default.deserializer";
    private final ProxyChain proxyChain;

    public AxualDeserializerConfig(Map<String, Object> configs, boolean isKey) {
        super(configs, isKey, null);

        // Parse the chain of proxies to instantiate, passed as either String or ProxyChain object
        proxyChain = ProxyChainUtil.parseProxyChain(configs, CHAIN_CONFIG);
    }

    public ProxyChain getProxyChain() {
        return proxyChain;
    }
}
