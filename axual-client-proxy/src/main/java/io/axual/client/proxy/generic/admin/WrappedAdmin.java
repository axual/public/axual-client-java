package io.axual.client.proxy.generic.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AbortTransactionOptions;
import org.apache.kafka.clients.admin.AbortTransactionResult;
import org.apache.kafka.clients.admin.AbortTransactionSpec;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterClientQuotasResult;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsResult;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.CreateDelegationTokenOptions;
import org.apache.kafka.clients.admin.CreateDelegationTokenResult;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasResult;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeDelegationTokenOptions;
import org.apache.kafka.clients.admin.DescribeDelegationTokenResult;
import org.apache.kafka.clients.admin.DescribeFeaturesOptions;
import org.apache.kafka.clients.admin.DescribeFeaturesResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.DescribeProducersOptions;
import org.apache.kafka.clients.admin.DescribeProducersResult;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.DescribeTransactionsOptions;
import org.apache.kafka.clients.admin.DescribeTransactionsResult;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsResult;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ElectLeadersResult;
import org.apache.kafka.clients.admin.ExpireDelegationTokenOptions;
import org.apache.kafka.clients.admin.ExpireDelegationTokenResult;
import org.apache.kafka.clients.admin.FeatureUpdate;
import org.apache.kafka.clients.admin.FenceProducersOptions;
import org.apache.kafka.clients.admin.FenceProducersResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.ListTransactionsOptions;
import org.apache.kafka.clients.admin.ListTransactionsResult;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.RenewDelegationTokenOptions;
import org.apache.kafka.clients.admin.RenewDelegationTokenResult;
import org.apache.kafka.clients.admin.UnregisterBrokerOptions;
import org.apache.kafka.clients.admin.UnregisterBrokerResult;
import org.apache.kafka.clients.admin.UpdateFeaturesOptions;
import org.apache.kafka.clients.admin.UpdateFeaturesResult;
import org.apache.kafka.clients.admin.UserScramCredentialAlteration;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.TopicCollection;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class WrappedAdmin extends AdminClient implements Admin {

  private final Admin admin;

  public WrappedAdmin(final Admin admin) {
    this.admin = admin;
  }

  @Override
  public void close() {
    admin.close();
  }

  @Override
  public void close(Duration timeout) {
    admin.close(timeout);
  }

  @Override
  public CreateTopicsResult createTopics(Collection<NewTopic> newTopics,
      CreateTopicsOptions createTopicsOptions) {
    return admin.createTopics(newTopics, createTopicsOptions);
  }

  @Override
  public DeleteTopicsResult deleteTopics(Collection<String> topics, DeleteTopicsOptions options) {
    return admin.deleteTopics(topics, options);
  }

  @Override
  public DeleteTopicsResult deleteTopics(TopicCollection topicCollection, DeleteTopicsOptions deleteTopicsOptions) {
    return admin.deleteTopics(topicCollection, deleteTopicsOptions);
  }

  @Override
  public ListTopicsResult listTopics(ListTopicsOptions options) {
    return admin.listTopics(options);
  }

  @Override
  public DescribeTopicsResult describeTopics(Collection<String> topicNames,
      DescribeTopicsOptions options) {
    return admin.describeTopics(topicNames, options);
  }

  @Override
  public DescribeTopicsResult describeTopics(TopicCollection topicCollection, DescribeTopicsOptions describeTopicsOptions) {
    return admin.describeTopics(topicCollection, describeTopicsOptions);
  }

  @Override
  public DescribeClusterResult describeCluster(DescribeClusterOptions options) {
    return admin.describeCluster(options);
  }

  @Override
  public DescribeAclsResult describeAcls(AclBindingFilter filter, DescribeAclsOptions options) {
    return admin.describeAcls(filter, options);
  }

  @Override
  public CreateAclsResult createAcls(Collection<AclBinding> acls, CreateAclsOptions options) {
    return admin.createAcls(acls, options);
  }

  @Override
  public DeleteAclsResult deleteAcls(Collection<AclBindingFilter> filters,
      DeleteAclsOptions options) {
    return admin.deleteAcls(filters, options);
  }

  @Override
  public DescribeConfigsResult describeConfigs(Collection<ConfigResource> resources,
      DescribeConfigsOptions options) {
    return admin.describeConfigs(resources, options);
  }

  /**
   * @deprecated
   * @param configs
   * @param options
   * @return
   */
  @Deprecated
  @Override
  public AlterConfigsResult alterConfigs(Map<ConfigResource, Config> configs,
      AlterConfigsOptions options) {
    return admin.alterConfigs(configs, options);
  }

  @Override
  public AlterConfigsResult incrementalAlterConfigs(
      Map<ConfigResource, Collection<AlterConfigOp>> configs, AlterConfigsOptions options) {
    return admin.incrementalAlterConfigs(configs, options);
  }

  @Override
  public AlterReplicaLogDirsResult alterReplicaLogDirs(
      Map<TopicPartitionReplica, String> replicaAssignment, AlterReplicaLogDirsOptions options) {
    return admin.alterReplicaLogDirs(replicaAssignment, options);
  }

  @Override
  public DescribeLogDirsResult describeLogDirs(Collection<Integer> brokers,
      DescribeLogDirsOptions options) {
    return admin.describeLogDirs(brokers, options);
  }

  @Override
  public DescribeReplicaLogDirsResult describeReplicaLogDirs(
      Collection<TopicPartitionReplica> replicas, DescribeReplicaLogDirsOptions options) {
    return admin.describeReplicaLogDirs(replicas, options);
  }

  @Override
  public CreatePartitionsResult createPartitions(Map<String, NewPartitions> newPartitions,
      CreatePartitionsOptions options) {
    return admin.createPartitions(newPartitions, options);
  }

  @Override
  public DeleteRecordsResult deleteRecords(Map<TopicPartition, RecordsToDelete> recordsToDelete,
      DeleteRecordsOptions options) {
    return admin.deleteRecords(recordsToDelete, options);
  }

  @Override
  public CreateDelegationTokenResult createDelegationToken(CreateDelegationTokenOptions options) {
    return admin.createDelegationToken(options);
  }

  @Override
  public RenewDelegationTokenResult renewDelegationToken(byte[] hmac,
      RenewDelegationTokenOptions options) {
    return admin.renewDelegationToken(hmac, options);
  }

  @Override
  public ExpireDelegationTokenResult expireDelegationToken(byte[] hmac,
      ExpireDelegationTokenOptions options) {
    return admin.expireDelegationToken(hmac, options);
  }

  @Override
  public DescribeDelegationTokenResult describeDelegationToken(
      DescribeDelegationTokenOptions options) {
    return admin.describeDelegationToken(options);
  }

  @Override
  public DescribeConsumerGroupsResult describeConsumerGroups(Collection<String> groupIds,
      DescribeConsumerGroupsOptions options) {
    return admin.describeConsumerGroups(groupIds, options);
  }

  @Override
  public ListConsumerGroupsResult listConsumerGroups(ListConsumerGroupsOptions options) {
    return admin.listConsumerGroups(options);
  }

  @Override
  public ListConsumerGroupOffsetsResult listConsumerGroupOffsets(String groupId,
      ListConsumerGroupOffsetsOptions options) {
    return admin.listConsumerGroupOffsets(groupId, options);
  }

  @Override
  public DeleteConsumerGroupsResult deleteConsumerGroups(Collection<String> groupIds,
      DeleteConsumerGroupsOptions options) {
    return admin.deleteConsumerGroups(groupIds, options);
  }

  @Override
  public DeleteConsumerGroupOffsetsResult deleteConsumerGroupOffsets(String groupId,
      Set<TopicPartition> partitions, DeleteConsumerGroupOffsetsOptions options) {
    return admin.deleteConsumerGroupOffsets(groupId, partitions, options);
  }

  @Override
  public ElectLeadersResult electLeaders(ElectionType electionType, Set<TopicPartition> partitions,
      ElectLeadersOptions options) {
    return admin.electLeaders(electionType, partitions, options);
  }

  @Override
  public AlterPartitionReassignmentsResult alterPartitionReassignments(
      Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments,
      AlterPartitionReassignmentsOptions options) {
    return admin.alterPartitionReassignments(reassignments, options);
  }

  @Override
  public ListPartitionReassignmentsResult listPartitionReassignments(
      Optional<Set<TopicPartition>> partitions, ListPartitionReassignmentsOptions options) {
    return admin.listPartitionReassignments(partitions, options);
  }

  @Override
  public RemoveMembersFromConsumerGroupResult removeMembersFromConsumerGroup(String groupId,
      RemoveMembersFromConsumerGroupOptions options) {
    return admin.removeMembersFromConsumerGroup(groupId, options);
  }

  @Override
  public AlterConsumerGroupOffsetsResult alterConsumerGroupOffsets(String groupId,
      Map<TopicPartition, OffsetAndMetadata> offsets, AlterConsumerGroupOffsetsOptions options) {
    return admin.alterConsumerGroupOffsets(groupId, offsets, options);
  }

  @Override
  public ListOffsetsResult listOffsets(Map<TopicPartition, OffsetSpec> topicPartitionOffsets,
      ListOffsetsOptions options) {
    return admin.listOffsets(topicPartitionOffsets, options);
  }

  @Override
  public DescribeClientQuotasResult describeClientQuotas(ClientQuotaFilter filter,
      DescribeClientQuotasOptions options) {
    return admin.describeClientQuotas(filter, options);
  }

  @Override
  public AlterClientQuotasResult alterClientQuotas(Collection<ClientQuotaAlteration> entries,
      AlterClientQuotasOptions options) {
    return admin.alterClientQuotas(entries, options);
  }

  @Override
  public DescribeUserScramCredentialsResult describeUserScramCredentials(List<String> list,
                                                                         DescribeUserScramCredentialsOptions options) {
    return admin.describeUserScramCredentials(list, options);
  }

  @Override
  public AlterUserScramCredentialsResult alterUserScramCredentials(List<UserScramCredentialAlteration> list,
                                                                   AlterUserScramCredentialsOptions options) {
    return admin.alterUserScramCredentials(list, options);
  }

  @Override
  public DescribeFeaturesResult describeFeatures(DescribeFeaturesOptions options) {
    return admin.describeFeatures(options);
  }

  @Override
  public UpdateFeaturesResult updateFeatures(Map<String, FeatureUpdate> map,
                                             UpdateFeaturesOptions options) {
    return admin.updateFeatures(map, options);
  }

  @Override
  public UnregisterBrokerResult unregisterBroker(int brokerId, UnregisterBrokerOptions options) {
    return admin.unregisterBroker(brokerId, options);
  }

  @Override
  public DescribeProducersResult describeProducers(Collection<TopicPartition> collection, DescribeProducersOptions describeProducersOptions) {
    return admin.describeProducers(collection, describeProducersOptions);
  }

  @Override
  public DescribeTransactionsResult describeTransactions(Collection<String> collection, DescribeTransactionsOptions describeTransactionsOptions) {
    return admin.describeTransactions(collection, describeTransactionsOptions);
  }

  @Override
  public AbortTransactionResult abortTransaction(AbortTransactionSpec abortTransactionSpec, AbortTransactionOptions abortTransactionOptions) {
    return admin.abortTransaction(abortTransactionSpec, abortTransactionOptions);
  }

  @Override
  public ListTransactionsResult listTransactions(ListTransactionsOptions listTransactionsOptions) {
    return admin.listTransactions(listTransactionsOptions);
  }

  @Override
  public FenceProducersResult fenceProducers(Collection<String> collection, FenceProducersOptions fenceProducersOptions) {
    return admin.fenceProducers(collection, fenceProducersOptions);
  }

  @Override
  public Map<MetricName, ? extends Metric> metrics() {
    return admin.metrics();
  }
}
