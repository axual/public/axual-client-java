package io.axual.client.proxy.switching.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;

import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.switching.generic.BaseClientProxySwitcher;
import io.axual.discovery.client.DiscoveryResult;

public class ConsumerSwitcher<K, V> extends BaseClientProxySwitcher<ConsumerProxy<K, V>, SwitchingConsumerConfig<K, V>> {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerSwitcher.class);

    private Assignment<K, V> assignment = null;
    private Subscription<K, V> subscription = null;

    public Assignment<K, V> getAssignment() {
        return assignment;
    }

    public void setAssignment(Consumer<K, V> consumer, SwitchingConsumerConfig config, Assignment<K, V> assignment) {
        updateAssignment(consumer, config, assignment, false);
    }

    public Subscription<K, V> getSubscription() {
        return subscription;
    }

    public void setSubscription(Consumer<K, V> consumer, SwitchingConsumerConfig config, Subscription<K, V> subscription) {
        updateSubscription(consumer, config, subscription, false);
    }

    public void unsubscribe(Consumer<K, V> consumer, SwitchingConsumerConfig config) {
        updateAssignment(consumer, config, null, false);
        updateSubscription(consumer, config, null, false);
    }

    @Override
    public ConsumerProxy<K, V> switchProxy(ConsumerProxy<K, V> oldProxy, SwitchingConsumerConfig<K, V> config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        ConsumerProxy<K, V> result = super.switchProxy(oldProxy, config, oldResult, newResult);
        LOG.info("Consumer switched, applying assignments and subscriptions");
        updateAssignment(result, config, assignment, true);
        updateSubscription(result, config, subscription, true);
        LOG.info("Consumer switch finished");
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ConsumerProxy<K, V> createProxyObject(SwitchingConsumerConfig config, DiscoveryResult discoveryResult) {
        Map<String, Object> properties = config.getDownstreamConfigs();

        // Add all discovery properties
        properties.putAll(discoveryResult.getConfigs());

        LOG.info("Creating a new {} with properties: {}", config.getProxyType(), properties);
        ClientProxyFactory<ConsumerProxy<K, V>> factory = config.getBackingFactory();
        return factory.create(properties);
    }


    @Override
    protected Duration getSwitchTimeout(SwitchingConsumerConfig config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        // Consumers are tricky to switch, since multiple instances of the same consuming application
        // must be switched over at the same time to prevent parallel consumption from two clusters
        // at the same time. This means we have to wait at least TTL time before actually switching
        // to allow fellow consumer instances to also detect the switch and stop consumption on the
        // old cluster.

        // If there is no subscription or assignment, we can switch directly
        if (subscription == null && assignment == null) {
            return Duration.ZERO;
        }

        if (config.isAtLeastOnce()) {
            // "AT_LEAST_ONCE" consumers should wait TTL in order to make sure that
            // all instances of the same consumer have detected the switch
            return Duration.ofMillis(newResult.getTtl());
        }

        // For AT_MOST_ONCE we need to wait enough time to ensure all "just produced" messages on
        // the old cluster are distributed to the new cluster. That takes a maximum of
        // distance * DistributorTimeout time, as calculated in the super::getSwitchTimeout.
        // At the same time we need to wait at least TTL time (see above) so we wait the maximum
        // of switch timeout and TTL.
        return Duration.ofMillis(
                Math.max(
                        super.getSwitchTimeout(config, oldResult, newResult).toMillis(),
                        newResult.getTtl()));
    }

    private void updateAssignment(Consumer<K, V> consumer, SwitchingConsumerConfig config,
                                  Assignment<K, V> newAssignment, boolean isSwitching) {
        if (newAssignment != null) {

            // never seek to the end if the new assignment is not caused by a cluster switch
            boolean seekToEnd = isSwitching && config.isIstAtMostOnceOnSwitch();

            // Calling assign overwrites the old assignment set
            newAssignment.assign(consumer, seekToEnd);
        } else if (assignment != null) {
            // If there was an older assignment, then call unsubscribe to unassign
            consumer.unsubscribe();
        }
        assignment = newAssignment;
    }

    private void updateSubscription(Consumer<K, V> consumer, SwitchingConsumerConfig config,
                                    Subscription<K, V> newSubscription, boolean isSwitching) {
        if (newSubscription != null) {
            // If there is a new type of subscription (from pattern to topics or vice versa) then unsubscribe first
            if (subscription != null && subscription.getClass() != newSubscription.getClass()) {
                consumer.unsubscribe();
            }

            // never seek to the end if the new subscription is not caused by a cluster switch
            boolean seekToEnd = isSwitching && config.isIstAtMostOnceOnSwitch();

            newSubscription.subscribe(consumer, seekToEnd);
        } else if (subscription != null) {
            consumer.unsubscribe();
        }
        subscription = newSubscription;
    }
}
