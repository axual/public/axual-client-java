package io.axual.client.proxy.header.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.test.TestProducerProxy;
import io.axual.client.proxy.test.TestProducerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.fetcher.DiscoveryLoader;

import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({HeaderProducer.class, TestProducerProxyFactory.class, TestProducerProxy.class, DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class HeaderProducerTest {
    private static final String TOPIC = "general-applicationlog";
    private static final String APPLICATION_ID = "io.axual.test";
    private static final String APPLICATION_VERSION = "1.0";
    public static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    public static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    public static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

    @Mock
    TestProducerProxy<String, String> mockedProducer1, mockedProducer2;

    @Mock
    DiscoveryLoader mockedDiscoveryLoader;

    protected DiscoveryResult discoveryResult;

    protected Properties getValidProperties() {
        final Properties producerProps = new Properties();
        producerProps.putAll(getValidMap());
        return producerProps;
    }

    protected Map<String, Object> getValidMap() {
        final Map<String, Object> producerMap = new HashMap<>();

        producerMap.put(HeaderProducerConfig.BACKING_FACTORY_CONFIG, TestProducerProxyFactory.class.getName());
        producerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        producerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

        producerMap.put(ProducerConfig.ACKS_CONFIG, "-1");
        producerMap.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
        producerMap.put(ProducerConfig.RETRIES_CONFIG, "" + Integer.MAX_VALUE);
        producerMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeyPassword(SSL_PASSWORD)
                .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                .setKeystorePassword(SSL_PASSWORD)
                .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                .setTruststorePassword(SSL_PASSWORD)
                .setEnableHostnameVerification(false)
                .build();
        KafkaUtil.getKafkaConfigs(sslConfig, producerMap);

        return producerMap;
    }

    protected void validConstructorTest(final HeaderProducer producer) throws Exception {
        final HeaderProducerConfig config = (HeaderProducerConfig) Whitebox.getInternalState(producer, "config");

        Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
        Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

        // Verify initialisation of Apache KafkaProducer with mocked properties from the mocked provider
        verifyNew(TestProducerProxy.class, Mockito.times(1));
    }

    @Before
    public void setup() throws Exception {
        whenNew(TestProducerProxy.class).withAnyArguments().thenReturn(mockedProducer1, mockedProducer2);

        when(mockedDiscoveryLoader.getDiscoveryResult()).thenReturn(discoveryResult);
        when(mockedDiscoveryLoader.discoveryChanged()).thenReturn(Boolean.TRUE).thenReturn(Boolean.FALSE);

        spy(DiscoveryClientRegistry.class);
        PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments().thenReturn(mockedDiscoveryLoader);
    }

    @Test
    public void construction_ValidProperties() throws Exception {
        final HeaderProducer kafkaProducer = spy(new HeaderProducer(getValidProperties()));
        validConstructorTest(kafkaProducer);
    }

    @Test
    public void construction_ValidMap() throws Exception {
        final HeaderProducer kafkaProducer = spy(new HeaderProducer(getValidMap()));
        validConstructorTest(kafkaProducer);
    }

    @Test
    public void initTransactions() {
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.initTransactions();
        Mockito.verify(mockedProducer1, Mockito.times(1)).initTransactions();
    }

    @Test
    public void beginTransaction() {
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.beginTransaction();
        Mockito.verify(mockedProducer1, Mockito.times(1)).beginTransaction();
    }

    @Test
    public void sendOffsetsToTransaction_GroupId() {
        final HashMap<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
        final String groupId = "groupId";
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.sendOffsetsToTransaction(offsets, groupId);
        Mockito.verify(mockedProducer1, Mockito.times(1)).sendOffsetsToTransaction(eq(offsets), eq(groupId));
    }

    @Test
    public void sendOffsetsToTransaction_ConsumerGroupMetadata() {
      final HashMap<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
        ConsumerGroupMetadata groupMetadata =new ConsumerGroupMetadata("groupId");
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.sendOffsetsToTransaction(offsets, groupMetadata);
        Mockito.verify(mockedProducer1, Mockito.times(1)).sendOffsetsToTransaction(eq(offsets), eq(groupMetadata));
    }

    @Test
    public void commitTransaction() {
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.commitTransaction();
        Mockito.verify(mockedProducer1, Mockito.times(1)).commitTransaction();
    }

    @Test
    public void abortTransaction() {
        final HeaderProducer kafkaProducer = new HeaderProducer(getValidProperties());
        kafkaProducer.abortTransaction();
        Mockito.verify(mockedProducer1, Mockito.times(1)).abortTransaction();
    }

    @Test
    public void send() throws Exception {
        final HeaderProducer<String, String> kafkaProducer = spy(new HeaderProducer<String, String>(getValidProperties()));

        final String key = "MyKey";
        final String value = "MyValue";
        final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, key, value);
        kafkaProducer.send(record);

        final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor.forClass(ProducerRecord.class);
        Mockito.verify(mockedProducer1, Mockito.times(1)).send(producerRecordCaptor.capture());

        final ProducerRecord capturedProducerRecord = producerRecordCaptor.getValue();
        Assert.assertNotNull(capturedProducerRecord);
    }

    @Test
    public void sendWithCallback() {
        final HeaderProducer<String, String> kafkaProducer = spy(new HeaderProducer<String, String>(getValidProperties()));

        final String key = "MyKey";
        final String value = "MyValue";
        final ProducerRecord record = new ProducerRecord(TOPIC, key, value);

        // Created to compare to mock input
        final Callback produceCallback = new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                // never called;
            }
        };

        kafkaProducer.send(record, produceCallback);

        final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor.forClass(ProducerRecord.class);
        final ArgumentCaptor<Callback> callbackCaptor = ArgumentCaptor.forClass(Callback.class);
        Mockito.verify(mockedProducer1, Mockito.times(1)).send(producerRecordCaptor.capture(), callbackCaptor.capture());

        final ProducerRecord capturedProducerRecord = producerRecordCaptor.getValue();
        Assert.assertNotNull(capturedProducerRecord);
    }

    @Test
    public void flush() {
        final HeaderProducer<String, String> kafkaProducer = spy(new HeaderProducer<String, String>(getValidProperties()));

        kafkaProducer.flush();

        Mockito.verify(mockedProducer1, Mockito.times(1)).flush();
    }

    @Test
    public void partitionsFor() {
        final HeaderProducer<String, String> kafkaProducer = spy(new HeaderProducer<String, String>(getValidProperties()));

        kafkaProducer.partitionsFor(TOPIC);

        final ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(mockedProducer1, Mockito.times(1)).partitionsFor(topicCaptor.capture());

        final String capturedTopic = topicCaptor.getValue();
        Assert.assertEquals(TOPIC, capturedTopic);
    }

    @Test
    public void metrics() {
        final HeaderProducer<String, String> kafkaProducer = spy(new HeaderProducer<String, String>(getValidProperties()));

        kafkaProducer.metrics();

        Mockito.verify(mockedProducer1, Mockito.times(1)).metrics();
    }
}
