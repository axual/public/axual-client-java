package io.axual.client.proxy.wrapped.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AbortTransactionOptions;
import org.apache.kafka.clients.admin.AbortTransactionResult;
import org.apache.kafka.clients.admin.AbortTransactionSpec;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterClientQuotasResult;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsResult;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.CreateDelegationTokenOptions;
import org.apache.kafka.clients.admin.CreateDelegationTokenResult;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasResult;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeDelegationTokenOptions;
import org.apache.kafka.clients.admin.DescribeDelegationTokenResult;
import org.apache.kafka.clients.admin.DescribeFeaturesOptions;
import org.apache.kafka.clients.admin.DescribeFeaturesResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.DescribeProducersOptions;
import org.apache.kafka.clients.admin.DescribeProducersResult;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.DescribeTransactionsOptions;
import org.apache.kafka.clients.admin.DescribeTransactionsResult;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsResult;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ElectLeadersResult;
import org.apache.kafka.clients.admin.ExpireDelegationTokenOptions;
import org.apache.kafka.clients.admin.ExpireDelegationTokenResult;
import org.apache.kafka.clients.admin.FeatureUpdate;
import org.apache.kafka.clients.admin.FenceProducersOptions;
import org.apache.kafka.clients.admin.FenceProducersResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.ListTransactionsOptions;
import org.apache.kafka.clients.admin.ListTransactionsResult;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.RenewDelegationTokenOptions;
import org.apache.kafka.clients.admin.RenewDelegationTokenResult;
import org.apache.kafka.clients.admin.UnregisterBrokerOptions;
import org.apache.kafka.clients.admin.UnregisterBrokerResult;
import org.apache.kafka.clients.admin.UpdateFeaturesOptions;
import org.apache.kafka.clients.admin.UpdateFeaturesResult;
import org.apache.kafka.clients.admin.UserScramCredentialAlteration;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.TopicCollection;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.internals.KafkaFutureImpl;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WrappedAdminClientTest {

    @Mock
    public AdminClient mockedAdmin;

    public WrappedAdminClient wrappedAdminClient;

    @Before
    public void setup() {
        wrappedAdminClient = new WrappedAdminClient(mockedAdmin, Collections.emptyMap());
    }

    @Test
    public void close() {
        final Duration duration = Duration.ofMillis(100);
        wrappedAdminClient.close(duration);

        verify(mockedAdmin, times(1)).close(duration);
    }

    @Test
    public void createTopics() {
        Collection<NewTopic> topics = Collections.singleton(mock(NewTopic.class));
        CreateTopicsOptions options = new CreateTopicsOptions();
        CreateTopicsResult toReturn = mock(CreateTopicsResult.class);
        doReturn(toReturn).when(mockedAdmin).createTopics(topics, options);

        CreateTopicsResult result = wrappedAdminClient.createTopics(topics, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).createTopics(topics, options);
    }

    @Test
    public void deleteTopics() {
        Collection<String> topics = Collections.singleton("test");
        DeleteTopicsOptions options = new DeleteTopicsOptions();
        DeleteTopicsResult toReturn = mock(DeleteTopicsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteTopics(topics, options);

        DeleteTopicsResult result = wrappedAdminClient.deleteTopics(topics, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteTopics(topics, options);
    }

    @Test public void deleteTopicTopicCollection() {
        Collection<String> topics = Collections.singleton("test");
        DeleteTopicsOptions options = new DeleteTopicsOptions();
        TopicCollection topicCollection = TopicCollection.ofTopicNames(topics);

        DeleteTopicsResult toReturn = mock(DeleteTopicsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteTopics(topicCollection, options);

        DeleteTopicsResult result = wrappedAdminClient.deleteTopics(topicCollection, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteTopics(topicCollection, options);
    }

    @Test
    public void listTopics() {
        ListTopicsResult toReturn = mock(ListTopicsResult.class);
        ListTopicsOptions options = new ListTopicsOptions();
        doReturn(toReturn).when(mockedAdmin).listTopics(options);

        ListTopicsResult result = wrappedAdminClient.listTopics(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).listTopics(options);
    }

    @Test
    public void describeTopics() {
        DescribeTopicsResult toReturn = mock(DescribeTopicsResult.class);
        Collection<String> topics = Collections.singleton("test");
        DescribeTopicsOptions options = new DescribeTopicsOptions();
        doReturn(toReturn).when(mockedAdmin).describeTopics(topics, options);

        DescribeTopicsResult result = wrappedAdminClient.describeTopics(topics, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeTopics(topics, options);
    }

    @Test
    public void describeTopicsTopicCollection() {
        DescribeTopicsResult toReturn = mock(DescribeTopicsResult.class);
        Collection<String> topics = Collections.singleton("test");
        TopicCollection topicCollection = TopicCollection.ofTopicNames(topics);

        DescribeTopicsOptions options = new DescribeTopicsOptions();
        doReturn(toReturn).when(mockedAdmin).describeTopics(topicCollection, options);

        DescribeTopicsResult result = wrappedAdminClient.describeTopics(topicCollection, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeTopics(topicCollection, options);
    }

    @Test
    public void describeCluster() {
        DescribeClusterResult toReturn = mock(DescribeClusterResult.class);
        DescribeClusterOptions options = new DescribeClusterOptions();
        doReturn(toReturn).when(mockedAdmin).describeCluster(options);

        DescribeClusterResult result = wrappedAdminClient.describeCluster(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeCluster(options);
    }

    @Test
    public void describeAcls() {
        AclBindingFilter filter = mock(AclBindingFilter.class);
        DescribeAclsOptions options = new DescribeAclsOptions();
        DescribeAclsResult toReturn = mock(DescribeAclsResult.class);
        doReturn(toReturn).when(mockedAdmin).describeAcls(filter, options);

        DescribeAclsResult result = wrappedAdminClient.describeAcls(filter, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeAcls(filter, options);
    }

    @Test
    public void createAcls() {
        Collection<AclBinding> acls = Collections.singleton(mock(AclBinding.class));
        CreateAclsOptions options = new CreateAclsOptions();
        CreateAclsResult toReturn = mock(CreateAclsResult.class);
        doReturn(toReturn).when(mockedAdmin).createAcls(acls, options);

        CreateAclsResult result = wrappedAdminClient.createAcls(acls, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).createAcls(acls, options);
    }

    @Test
    public void deleteAcls() {
        Collection<AclBindingFilter> acls = Collections.singleton(mock(AclBindingFilter.class));
        DeleteAclsOptions options = new DeleteAclsOptions();
        DeleteAclsResult toReturn = mock(DeleteAclsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteAcls(acls, options);

        DeleteAclsResult result = wrappedAdminClient.deleteAcls(acls, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteAcls(acls, options);
    }

    @Test
    public void describeConfigs() {
        Collection<ConfigResource> resources = Collections.singleton(new ConfigResource(ConfigResource.Type.BROKER, "some.name"));
        DescribeConfigsOptions options = new DescribeConfigsOptions();
        DescribeConfigsResult toReturn = mock(DescribeConfigsResult.class);
        doReturn(toReturn).when(mockedAdmin).describeConfigs(resources, options);

        DescribeConfigsResult result = wrappedAdminClient.describeConfigs(resources, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeConfigs(resources, options);
    }

    @Test
    public void alterConfigs() {
        Map<ConfigResource, Config> resources = Collections.emptyMap();
        AlterConfigsOptions options = new AlterConfigsOptions();
        AlterConfigsResult toReturn = mock(AlterConfigsResult.class);
        doReturn(toReturn).when(mockedAdmin).alterConfigs(resources, options);

        AlterConfigsResult result = wrappedAdminClient.alterConfigs(resources, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterConfigs(resources, options);
    }

    @Test
    public void incrementalAlterConfigs() {
        Map<ConfigResource, Collection<AlterConfigOp>> resources = Collections.emptyMap();
        AlterConfigsOptions options = new AlterConfigsOptions();
        AlterConfigsResult toReturn = mock(AlterConfigsResult.class);
        doReturn(toReturn).when(mockedAdmin).incrementalAlterConfigs(resources, options);

        AlterConfigsResult result = wrappedAdminClient.incrementalAlterConfigs(resources, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).incrementalAlterConfigs(resources, options);
    }

    @Test
    public void alterReplicaLogDirs() {
        Map<TopicPartitionReplica, String> replicas = Collections.emptyMap();
        AlterReplicaLogDirsOptions options = new AlterReplicaLogDirsOptions();
        AlterReplicaLogDirsResult toReturn = mock(AlterReplicaLogDirsResult.class);
        doReturn(toReturn).when(mockedAdmin).alterReplicaLogDirs(replicas, options);

        AlterReplicaLogDirsResult result = wrappedAdminClient.alterReplicaLogDirs(replicas, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterReplicaLogDirs(replicas, options);
    }

    @Test
    public void describeLogDirs() {
        Collection<Integer> brokers = Collections.emptyList();
        DescribeLogDirsOptions options = new DescribeLogDirsOptions();
        DescribeLogDirsResult toReturn = mock(DescribeLogDirsResult.class);
        doReturn(toReturn).when(mockedAdmin).describeLogDirs(brokers, options);

        DescribeLogDirsResult result = wrappedAdminClient.describeLogDirs(brokers, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeLogDirs(brokers, options);
    }

    @Test
    public void describeReplicaLogDirs() {
        Collection<TopicPartitionReplica> replicas = Collections.emptyList();
        DescribeReplicaLogDirsOptions options = new DescribeReplicaLogDirsOptions();
        DescribeReplicaLogDirsResult toReturn = mock(DescribeReplicaLogDirsResult.class);
        doReturn(toReturn).when(mockedAdmin).describeReplicaLogDirs(replicas, options);

        DescribeReplicaLogDirsResult result = wrappedAdminClient.describeReplicaLogDirs(replicas, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeReplicaLogDirs(replicas, options);
    }

    @Test
    public void createPartitions() {
        Map<String, NewPartitions> partitions = Collections.emptyMap();
        CreatePartitionsOptions options = new CreatePartitionsOptions();
        CreatePartitionsResult toReturn = mock(CreatePartitionsResult.class);
        doReturn(toReturn).when(mockedAdmin).createPartitions(partitions, options);

        CreatePartitionsResult result = wrappedAdminClient.createPartitions(partitions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).createPartitions(partitions, options);
    }

    @Test
    public void deleteRecords() {
        Map<TopicPartition, RecordsToDelete> records = Collections.emptyMap();
        DeleteRecordsOptions options = new DeleteRecordsOptions();
        DeleteRecordsResult toReturn = mock(DeleteRecordsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteRecords(records, options);

        DeleteRecordsResult result = wrappedAdminClient.deleteRecords(records, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteRecords(records, options);
    }

    @Test
    public void createDelegationToken() {
        CreateDelegationTokenOptions options = new CreateDelegationTokenOptions();
        CreateDelegationTokenResult toReturn = mock(CreateDelegationTokenResult.class);
        doReturn(toReturn).when(mockedAdmin).createDelegationToken(options);

        CreateDelegationTokenResult result = wrappedAdminClient.createDelegationToken(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).createDelegationToken(options);
    }

    @Test
    public void renewDelegationToken() {
        byte[] token = new byte[0];
        RenewDelegationTokenOptions options = new RenewDelegationTokenOptions();
        RenewDelegationTokenResult toReturn = mock(RenewDelegationTokenResult.class);
        doReturn(toReturn).when(mockedAdmin).renewDelegationToken(token, options);

        RenewDelegationTokenResult result = wrappedAdminClient.renewDelegationToken(token, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).renewDelegationToken(token, options);
    }

    @Test
    public void expireDelegationToken() {
        byte[] token = new byte[0];
        ExpireDelegationTokenOptions options = new ExpireDelegationTokenOptions();
        ExpireDelegationTokenResult toReturn = mock(ExpireDelegationTokenResult.class);
        doReturn(toReturn).when(mockedAdmin).expireDelegationToken(token, options);

        ExpireDelegationTokenResult result = wrappedAdminClient.expireDelegationToken(token, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).expireDelegationToken(token, options);
    }

    @Test
    public void describeDelegationToken() {
        DescribeDelegationTokenOptions options = new DescribeDelegationTokenOptions();
        DescribeDelegationTokenResult toReturn = mock(DescribeDelegationTokenResult.class);
        doReturn(toReturn).when(mockedAdmin).describeDelegationToken(options);

        DescribeDelegationTokenResult result = wrappedAdminClient.describeDelegationToken(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeDelegationToken(options);
    }

    @Test
    public void describeConsumerGroups() {
        Collection<String> groupIds = Collections.emptyList();
        DescribeConsumerGroupsOptions options = new DescribeConsumerGroupsOptions();
        DescribeConsumerGroupsResult toReturn = mock(DescribeConsumerGroupsResult.class);
        doReturn(toReturn).when(mockedAdmin).describeConsumerGroups(groupIds, options);

        DescribeConsumerGroupsResult result = wrappedAdminClient.describeConsumerGroups(groupIds, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeConsumerGroups(groupIds, options);
    }

    @Test
    public void listConsumerGroups() {
        ListConsumerGroupsOptions options = new ListConsumerGroupsOptions();
        ListConsumerGroupsResult toReturn = mock(ListConsumerGroupsResult.class);
        doReturn(toReturn).when(mockedAdmin).listConsumerGroups(options);

        ListConsumerGroupsResult result = wrappedAdminClient.listConsumerGroups(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).listConsumerGroups(options);
    }

    @Test
    public void listConsumerGroupOffsets() throws ExecutionException, InterruptedException {
        String groupId = "someGroup";
        Map<TopicPartition, OffsetAndMetadata> futureData = new HashMap<>();
        final long offset = 66L;
        final TopicPartition KEY_T1 = new TopicPartition("test", 1);
        final OffsetAndMetadata VALUE_T1 = new OffsetAndMetadata(offset, null);
        final OffsetAndMetadata EXPECTED_VALUE_T1 = VALUE_T1;
        futureData.put(KEY_T1, VALUE_T1);

        final TopicPartition KEY_T2 = new TopicPartition("test", 2);
        final OffsetAndMetadata VALUE_T2 = new OffsetAndMetadata(offset, "");
        final OffsetAndMetadata EXPECTED_VALUE_T2 = VALUE_T2;
        futureData.put(KEY_T2, VALUE_T2);

        final TopicPartition KEY_T3 = new TopicPartition("test", 3);
        final OffsetAndMetadata VALUE_T3 = new OffsetAndMetadata(offset, "ShouldNotChange");
        final OffsetAndMetadata EXPECTED_VALUE_T3 = VALUE_T3;
        futureData.put(KEY_T3, VALUE_T3);

        final TopicPartition KEY_T4 = new TopicPartition("test", 4);
        final OffsetAndMetadata VALUE_T4 = new OffsetAndMetadata(offset, "{ This is not JSON, just a trick start }");
        final OffsetAndMetadata EXPECTED_VALUE_T4 = VALUE_T4;
        futureData.put(KEY_T4, VALUE_T4);

        final TopicPartition KEY_T5 = new TopicPartition("test", 5);
        final OffsetAndMetadata VALUE_T5 = new OffsetAndMetadata(offset, "{ \"copyFlags\":1 }");
        final OffsetAndMetadata EXPECTED_VALUE_T5 = new OffsetAndMetadata(offset, null);
        futureData.put(KEY_T5, VALUE_T5);

        ListConsumerGroupOffsetsOptions options = new ListConsumerGroupOffsetsOptions();
        ListConsumerGroupOffsetsResult toReturn = mock(ListConsumerGroupOffsetsResult.class);

        KafkaFutureImpl<Map<TopicPartition, OffsetAndMetadata>> futures = new KafkaFutureImpl<>();
        futures.complete(futureData);

        doReturn(futures).when(toReturn).partitionsToOffsetAndMetadata();
        doReturn(toReturn).when(mockedAdmin).listConsumerGroupOffsets(groupId, options);

        ListConsumerGroupOffsetsResult result = wrappedAdminClient.listConsumerGroupOffsets(groupId, options);

        verify(mockedAdmin, times(1)).listConsumerGroupOffsets(groupId, options);
        assertNotNull(result);

        Map<TopicPartition, OffsetAndMetadata> returnedMap = result.partitionsToOffsetAndMetadata().get();
        assertEquals(EXPECTED_VALUE_T1, returnedMap.get(KEY_T1));
        assertEquals(EXPECTED_VALUE_T2, returnedMap.get(KEY_T2));
        assertEquals(EXPECTED_VALUE_T3, returnedMap.get(KEY_T3));
        assertEquals(EXPECTED_VALUE_T4, returnedMap.get(KEY_T4));
        assertEquals(EXPECTED_VALUE_T5, returnedMap.get(KEY_T5));
    }

    @Test
    public void deleteConsumerGroups() {
        Collection<String> groupIds = Collections.emptyList();
        DeleteConsumerGroupsOptions options = new DeleteConsumerGroupsOptions();
        DeleteConsumerGroupsResult toReturn = mock(DeleteConsumerGroupsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteConsumerGroups(groupIds, options);

        DeleteConsumerGroupsResult result = wrappedAdminClient.deleteConsumerGroups(groupIds, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteConsumerGroups(groupIds, options);
    }

    @Test
    public void deleteConsumerGroupOffsets() {
        String groupId = "someGroup";
        Set<TopicPartition> partitions = Collections.emptySet();
        DeleteConsumerGroupOffsetsOptions options = new DeleteConsumerGroupOffsetsOptions();
        DeleteConsumerGroupOffsetsResult toReturn = mock(DeleteConsumerGroupOffsetsResult.class);
        doReturn(toReturn).when(mockedAdmin).deleteConsumerGroupOffsets(groupId, partitions, options);

        DeleteConsumerGroupOffsetsResult result = wrappedAdminClient.deleteConsumerGroupOffsets(groupId, partitions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).deleteConsumerGroupOffsets(groupId, partitions, options);
    }

    @Test
    public void electLeaders() {
        ElectionType electionType = ElectionType.PREFERRED;
        Set<TopicPartition> topicPartitions = Collections.emptySet();
        ElectLeadersOptions options = new ElectLeadersOptions();
        ElectLeadersResult toReturn = null;
        doReturn(toReturn).when(mockedAdmin).electLeaders(electionType, topicPartitions, options);

        ElectLeadersResult result = wrappedAdminClient.electLeaders(electionType, topicPartitions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).electLeaders(electionType, topicPartitions, options);
    }

    @Test
    public void alterPartitionReassignments() {
        Map<TopicPartition, Optional<NewPartitionReassignment>> assignment = Collections.emptyMap();

        AlterPartitionReassignmentsOptions options = new AlterPartitionReassignmentsOptions();
        AlterPartitionReassignmentsResult toReturn = mock(AlterPartitionReassignmentsResult.class);
        doReturn(toReturn).when(mockedAdmin).alterPartitionReassignments(assignment, options);

        AlterPartitionReassignmentsResult result = wrappedAdminClient.alterPartitionReassignments(assignment, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterPartitionReassignments(assignment, options);
    }

    @Test
    public void listPartitionReassignments() {

        Optional<Set<TopicPartition>> partitions = Optional.empty();
        ListPartitionReassignmentsOptions options = new ListPartitionReassignmentsOptions();
        ListPartitionReassignmentsResult toReturn = mock(ListPartitionReassignmentsResult.class);
        doReturn(toReturn).when(mockedAdmin).listPartitionReassignments(partitions, options);

        ListPartitionReassignmentsResult result = wrappedAdminClient.listPartitionReassignments(partitions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).listPartitionReassignments(partitions, options);
    }

    @Test
    public void removeMembersFromConsumerGroup() {
        String groupId = "removeMembers";
        RemoveMembersFromConsumerGroupOptions options = new RemoveMembersFromConsumerGroupOptions();
        RemoveMembersFromConsumerGroupResult toReturn = mock(RemoveMembersFromConsumerGroupResult.class);

        doReturn(toReturn).when(mockedAdmin).removeMembersFromConsumerGroup(groupId, options);

        RemoveMembersFromConsumerGroupResult result = wrappedAdminClient.removeMembersFromConsumerGroup(groupId, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).removeMembersFromConsumerGroup(groupId, options);
    }

    @Test
    public void alterConsumerGroupOffsets() {
        String groupId = "removeMembers";
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();
        AlterConsumerGroupOffsetsOptions options = new AlterConsumerGroupOffsetsOptions();
        AlterConsumerGroupOffsetsResult toReturn = mock(AlterConsumerGroupOffsetsResult.class);

        doReturn(toReturn).when(mockedAdmin).alterConsumerGroupOffsets(groupId, offsets, options);

        AlterConsumerGroupOffsetsResult result = wrappedAdminClient.alterConsumerGroupOffsets(groupId, offsets, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterConsumerGroupOffsets(groupId, offsets, options);
    }

    @Test
    public void listOffsets() {
        Map<TopicPartition, OffsetSpec> partitionMap = Collections.emptyMap();
        ListOffsetsOptions options = new ListOffsetsOptions();
        ListOffsetsResult toReturn = mock(ListOffsetsResult.class);

        doReturn(toReturn).when(mockedAdmin).listOffsets(partitionMap, options);

        ListOffsetsResult result = wrappedAdminClient.listOffsets(partitionMap, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).listOffsets(partitionMap, options);
    }

    @Test
    public void describeClientQuotas() {
        ClientQuotaFilter filter = mock(ClientQuotaFilter.class);
        DescribeClientQuotasOptions options = new DescribeClientQuotasOptions();
        DescribeClientQuotasResult toReturn = mock(DescribeClientQuotasResult.class);

        doReturn(toReturn).when(mockedAdmin).describeClientQuotas(filter, options);

        DescribeClientQuotasResult result = wrappedAdminClient.describeClientQuotas(filter, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeClientQuotas(filter, options);
    }

    @Test
    public void alterClientQuotas() {
        Collection<ClientQuotaAlteration> alterations = Collections.emptySet();
        AlterClientQuotasOptions options = new AlterClientQuotasOptions();
        AlterClientQuotasResult toReturn = mock(AlterClientQuotasResult.class);

        doReturn(toReturn).when(mockedAdmin).alterClientQuotas(alterations, options);

        AlterClientQuotasResult result = wrappedAdminClient.alterClientQuotas(alterations, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterClientQuotas(alterations, options);
    }

    @Test
    public void describeUserScramCredentials() {
        List<String> credentials = Collections.emptyList();
        DescribeUserScramCredentialsOptions options = new DescribeUserScramCredentialsOptions();
        DescribeUserScramCredentialsResult toReturn = mock(DescribeUserScramCredentialsResult.class);

        doReturn(toReturn).when(mockedAdmin).describeUserScramCredentials(credentials, options);

        DescribeUserScramCredentialsResult result = wrappedAdminClient.describeUserScramCredentials(credentials, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeUserScramCredentials(credentials, options);
    }

    @Test
    public void alterUserScramCredentials() {
        List<UserScramCredentialAlteration> credentials = Collections.emptyList();
        AlterUserScramCredentialsOptions options = new AlterUserScramCredentialsOptions();
        AlterUserScramCredentialsResult toReturn = mock(AlterUserScramCredentialsResult.class);

        doReturn(toReturn).when(mockedAdmin).alterUserScramCredentials(credentials, options);

        AlterUserScramCredentialsResult result = wrappedAdminClient.alterUserScramCredentials(credentials, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).alterUserScramCredentials(credentials, options);
    }

    @Test
    public void describeFeatures() {
        DescribeFeaturesOptions options = new DescribeFeaturesOptions();
        DescribeFeaturesResult toReturn = mock(DescribeFeaturesResult.class);

        doReturn(toReturn).when(mockedAdmin).describeFeatures(options);

        DescribeFeaturesResult result = wrappedAdminClient.describeFeatures(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeFeatures(options);
    }

    @Test
    public void updateFeatures() {
        Map<String, FeatureUpdate> featureMap = Collections.emptyMap();
        UpdateFeaturesOptions options = new UpdateFeaturesOptions();
        UpdateFeaturesResult toReturn = mock(UpdateFeaturesResult.class);

        doReturn(toReturn).when(mockedAdmin).updateFeatures(featureMap, options);

        UpdateFeaturesResult result = wrappedAdminClient.updateFeatures(featureMap, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).updateFeatures(featureMap, options);
    }

    @Test
    public void unregisterBroker() {
        int brokerId = 1;
        UnregisterBrokerOptions options = new UnregisterBrokerOptions();
        UnregisterBrokerResult toReturn = mock(UnregisterBrokerResult.class);

        doReturn(toReturn).when(mockedAdmin).unregisterBroker(brokerId, options);

        UnregisterBrokerResult result = wrappedAdminClient.unregisterBroker(brokerId, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).unregisterBroker(brokerId, options);
    }

    @Test
    public void metrics() {
        Map<MetricName, Metric> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedAdmin).metrics();
        
        Map<MetricName, ? extends Metric> result = wrappedAdminClient.metrics();
        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).metrics();
    }

    @Test
    public void describeProducers() {
        DescribeProducersResult toReturn = mock(DescribeProducersResult.class);
        List<TopicPartition> topicPartitions = Collections.emptyList();

        DescribeProducersOptions options = new DescribeProducersOptions();
        doReturn(toReturn).when(mockedAdmin).describeProducers(topicPartitions, options);

        DescribeProducersResult result = wrappedAdminClient.describeProducers(topicPartitions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeProducers(topicPartitions, options);
    }

    @Test
    public void describeTransactions() {
        DescribeTransactionsResult toReturn = mock(DescribeTransactionsResult.class);
        Collection<String> transactions = Collections.singleton("test");

        DescribeTransactionsOptions options = new DescribeTransactionsOptions();
        doReturn(toReturn).when(mockedAdmin).describeTransactions(transactions, options);

        DescribeTransactionsResult result = wrappedAdminClient.describeTransactions(transactions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).describeTransactions(transactions, options);
    }

    @Test
    public void abortTransaction() {
        AbortTransactionResult toReturn = mock(AbortTransactionResult.class);
        TopicPartition topicPartition = new TopicPartition("topic", 0);
        AbortTransactionSpec abortTransactionSpec = new AbortTransactionSpec(topicPartition, 0L, (short) 0, 0);

        AbortTransactionOptions options = new AbortTransactionOptions();
        doReturn(toReturn).when(mockedAdmin).abortTransaction(abortTransactionSpec, options);

        AbortTransactionResult result = wrappedAdminClient.abortTransaction(abortTransactionSpec, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).abortTransaction(abortTransactionSpec, options);
    }

    @Test
    public void listTransactions() {
        ListTransactionsResult toReturn = mock(ListTransactionsResult.class);
        ListTransactionsOptions options = new ListTransactionsOptions();

        doReturn(toReturn).when(mockedAdmin).listTransactions(options);

        ListTransactionsResult result = wrappedAdminClient.listTransactions(options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).listTransactions(options);
    }

    @Test
    public void fenceProducers() {
        FenceProducersResult toReturn = mock(FenceProducersResult.class);
        Collection<String> transactions = Collections.singleton("test");

        FenceProducersOptions options = new FenceProducersOptions();
        doReturn(toReturn).when(mockedAdmin).fenceProducers(transactions, options);

        FenceProducersResult result = wrappedAdminClient.fenceProducers(transactions, options);

        assertEquals(toReturn, result);
        verify(mockedAdmin, times(1)).fenceProducers(transactions, options);
    }

}