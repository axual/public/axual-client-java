package io.axual.client.proxy.generic.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.test.TestConsumerProxy;
import io.axual.client.proxy.test.TestConsumerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticConsumerProxy.class, StaticConsumerProxy.class,
    TestConsumerProxyFactory.class, TestConsumerProxy.class, DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class StaticConsumerProxyTest {

  private static final String BACKING_FACTORY_CONFIG = "backing.factory";

  private static final String TOPIC = "general-applicationlog";
  private static final List<String> TOPICS = Arrays.asList(TOPIC + "1", TOPIC + "2", TOPIC + "3");

  private static final String APPLICATION_ID = "io.axual.test";
  private static final String APPLICATION_VERSION = "1.0";

  private static final String TENANT_PROPERTY = "tnt";
  private static final String ENVIRONMENT_PROPERTY = "envt";

  private static final String TENANT = "axual";
  private static final String ENVIRONMENT = "unit";

  private static final String GROUP_ID = "group";

  private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
  private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
  private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

  private static final BaseClientProxyConfig<ConsumerProxy<String, String>> config = new BaseClientProxyConfig<>(
      getValidConfig(), BACKING_FACTORY_CONFIG);

  private static final ConsumerRebalanceListener LISTENER = new ConsumerRebalanceListener() {
    @Override
    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
    }

    @Override
    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
    }
  };

  @Mock
  private TestConsumerProxy<String, String> mockedConsumer;

  @Before
  public void setUp() throws Exception {
    whenNew(TestConsumerProxy.class).withAnyArguments().thenReturn(mockedConsumer);
  }

  private StaticConsumerProxy<String, String, BaseClientProxyConfig<ConsumerProxy<String, String>>> consumer() {
    return spy(new StaticConsumerProxy<>(config));
  }

  private static Map<String, Object> getValidConfig() {
    Map<String, Object> consumerMap = new HashMap<>();

    consumerMap.put(BACKING_FACTORY_CONFIG, TestConsumerProxyFactory.class.getName());
    consumerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
    consumerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

    consumerMap.put(TENANT_PROPERTY, TENANT);
    consumerMap.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

    consumerMap.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
    consumerMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");
    consumerMap
        .put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    consumerMap
        .put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

    SslConfig sslConfig = SslConfig.newBuilder()
        .setKeyPassword(SSL_PASSWORD)
        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
        .setKeystorePassword(SSL_PASSWORD)
        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
        .setTruststorePassword(SSL_PASSWORD)
        .setEnableHostnameVerification(false)
        .build();
    KafkaUtil.getKafkaConfigs(sslConfig, consumerMap);

    return consumerMap;
  }

  // This method is called by tests to validate different constructors
  private void validConstructorTest(final StaticConsumerProxy consumer) {
    final BaseClientProxyConfig config = (BaseClientProxyConfig) Whitebox
        .getInternalState(consumer, "config");

    assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
    assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
    assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
    assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
    assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

    // Verify initialisation of header KafkaProducer with mocked properties from the mocked provider
    verifyNew(TestConsumerProxy.class, times(1));
  }

  @Test
  public void testConstructorWithValidConfig() {
    validConstructorTest(consumer());
  }

  @Test
  public void verify_assignment() {
    consumer().assignment();

    verify(mockedConsumer, times(1)).assignment();
  }

  @Test
  public void verify_subscribe() {
    consumer().subscription();

    verify(mockedConsumer, times(1)).subscription();
  }

  @Test
  public void verify_pattern_subscribe() {
    final Pattern pattern = Pattern.compile(".*");

    consumer().subscribe(pattern, LISTENER);

    ArgumentCaptor<Pattern> patternCaptor = ArgumentCaptor.forClass(Pattern.class);
    ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor
        .forClass(ConsumerRebalanceListener.class);
    verify(mockedConsumer, times(1)).subscribe(patternCaptor.capture(), listenerCaptor.capture());
    assertEquals(pattern, patternCaptor.getValue());
    assertEquals(LISTENER, listenerCaptor.getValue());
  }

  @Test
  public void verify_noTopics_subscribe_is_same_as_unsubscribe() {
    consumer().subscribe(Collections.<String>emptyList());

    assertEquals(0, consumer().subscription().size());
  }

  @Test
  public void verify_multipleTopics_subscribe_should_work() {
    consumer().subscribe(TOPICS);

    ArgumentCaptor<Collection> topicsCaptor = ArgumentCaptor.forClass(Collection.class);
    verify(mockedConsumer, times(1)).subscribe(topicsCaptor.capture());
    assertEquals(TOPICS, topicsCaptor.getValue());
  }

  @Test
  public void verify_subscribe_with_listener() {
    consumer().subscribe(TOPICS, LISTENER);

    ArgumentCaptor<Collection> topicsCaptor = ArgumentCaptor.forClass(Collection.class);
    ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor
        .forClass(ConsumerRebalanceListener.class);
    verify(mockedConsumer, times(1)).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
    assertEquals(TOPICS, topicsCaptor.getValue());
    assertEquals(LISTENER, listenerCaptor.getValue());
  }

  @Test
  public void subscribeTopics() {
    consumer().subscribe(TOPICS);

    ArgumentCaptor<List> topicsCaptor = ArgumentCaptor.forClass(List.class);
    verify(mockedConsumer, times(1)).subscribe(topicsCaptor.capture());
  }

  @Test
  public void subscribeTopicsAndListener() {
    consumer().subscribe(TOPICS, LISTENER);

    ArgumentCaptor<List> topicsCaptor = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor
        .forClass(ConsumerRebalanceListener.class);
    verify(mockedConsumer, times(1)).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
    assertEquals(TOPICS, topicsCaptor.getValue());
    assertEquals(LISTENER, listenerCaptor.getValue());
  }

  @Test
  public void unsubscribe() {
    consumer().unsubscribe();

    verify(mockedConsumer, times(1)).unsubscribe();
  }

  @Test
  public void assign() {
    final List<TopicPartition> topicPartitions = Collections
        .singletonList(new TopicPartition(TOPIC, 0));

    consumer().assign(topicPartitions);

    ArgumentCaptor<List> topicPartitionsCaptor = ArgumentCaptor.forClass(List.class);
    verify(mockedConsumer, times(1)).assign(topicPartitionsCaptor.capture());
    assertEquals(topicPartitionsCaptor.getValue(), topicPartitions);
  }

  @Test
  public void verify_poll() {
    final long pollTimeout = 5000L;
    final ConsumerRecords<String, String> mockedSet = new ConsumerRecords<>(
        new HashMap<TopicPartition, List<ConsumerRecord<String, String>>>());
    when(mockedConsumer.poll(pollTimeout)).thenReturn(mockedSet);

    ConsumerRecords<String, String> messages = consumer().poll(pollTimeout);

    verify(mockedConsumer, times(1)).poll(pollTimeout);
    assertEquals(messages, mockedSet);
  }

  @Test
  public void commitSync() {
    consumer().commitSync();

    verify(mockedConsumer, times(1)).commitSync();
  }

  @Test
  public void commitSyncWithDuration() {
    consumer().commitSync(Duration.ZERO);

    verify(mockedConsumer, times(1)).commitSync(Duration.ZERO);
  }

  @Test
  public void commitSyncWithMap() {
    consumer().commitSync((Map<TopicPartition, OffsetAndMetadata>) null);

    verify(mockedConsumer, times(1)).commitSync((Map<TopicPartition, OffsetAndMetadata>) null);
  }

  @Test
  public void commitSyncWithMapAndDuration() {
    consumer().commitSync((Map<TopicPartition, OffsetAndMetadata>) null, Duration.ZERO);

    verify(mockedConsumer, times(1)).commitSync(null, Duration.ZERO);
  }

  @Test
  public void commitAsync() {
    consumer().commitAsync();

    verify(mockedConsumer, times(1)).commitAsync();
  }

  @Test
  public void commitAsyncWithCallback() {
    consumer().commitAsync(null);

    verify(mockedConsumer, times(1)).commitAsync(null);
  }

  @Test
  public void commitAsyncWithMapAndCallback() {
    consumer().commitAsync(null, null);

    verify(mockedConsumer, times(1)).commitAsync(null, null);
  }

  @Test
  public void seek() {
    consumer().seek(null, -1L);

    verify(mockedConsumer, times(1)).seek(null, -1);
  }

  @Test
  public void seekToBeginning() {
    consumer().seekToBeginning(null);

    verify(mockedConsumer, times(1)).seekToBeginning(null);
  }

  @Test
  public void seekToEnd() {
    consumer().seekToEnd(null);

    verify(mockedConsumer, times(1)).seekToEnd(null);
  }

  @Test
  public void position() {
    consumer().position(null);

    verify(mockedConsumer, times(1)).position(null);
  }

  @Test
  public void committed_Partition() {
    TopicPartition partition = null;
    consumer().committed(partition);

    verify(mockedConsumer, times(1)).committed(partition);
  }

  @Test
  public void committed_PartitionDuration() {
    TopicPartition partition = null;
    Duration duration = null;
    consumer().committed(partition, duration);

    verify(mockedConsumer, times(1)).committed(partition, duration);
  }

  @Test
  public void committed_PartitionSet() {
    Set<TopicPartition> partitions = null;
    consumer().committed(partitions);

    verify(mockedConsumer, times(1)).committed(partitions);
  }

  @Test
  public void committed_PartitionSetDuration() {
    Set<TopicPartition> partitions = null;
    Duration duration = null;
    consumer().committed(partitions, duration);

    verify(mockedConsumer, times(1)).committed(partitions, duration);
  }

  @Test
  public void metrics() {
    consumer().metrics();

    verify(mockedConsumer, times(1)).metrics();
  }

  @Test
  public void partitionsFor() {
    when(mockedConsumer.partitionsFor(TOPIC)).thenReturn(null);

    List<PartitionInfo> info = consumer().partitionsFor(TOPIC);

    verify(mockedConsumer, times(1)).partitionsFor(TOPIC);
    assertNull(info);
  }

  @Test
  public void listTopics() {
    consumer().listTopics();

    verify(mockedConsumer, times(1)).listTopics();
  }

  @Test
  public void pause() {
    consumer().pause(null);

    verify(mockedConsumer, times(1)).pause(null);
  }

  @Test
  public void resume() {
    consumer().resume(null);

    verify(mockedConsumer, times(1)).resume(null);
  }

  @Test
  public void paused() {
    consumer().paused();

    verify(mockedConsumer, times(1)).paused();
  }

  @Test
  public void offsetsForTimes() {
    consumer().offsetsForTimes(null);

    verify(mockedConsumer, times(1)).offsetsForTimes(null);
  }

  @Test
  public void beginningOffsets() {
    consumer().beginningOffsets(null);

    verify(mockedConsumer, times(1)).beginningOffsets(null);
  }

  @Test
  public void endOffsets() {
    consumer().endOffsets(null);

    verify(mockedConsumer, times(1)).endOffsets(null);
  }

  @Test
  public void groupMetadata() {
    consumer().groupMetadata();

    verify(mockedConsumer, times(1)).groupMetadata();
  }

  @Test
  public void enforceRebalance() {
    consumer().enforceRebalance();

    verify(mockedConsumer, times(1)).enforceRebalance();
  }

  @Test
  public void close() throws Exception {
    consumer().close();

    verify(mockedConsumer, times(1)).close(any(Duration.class));
  }

  @Test
  public void closeWithTimeout() {
    final long time = 1000L;
    final TimeUnit timeUnit = TimeUnit.SECONDS;

    consumer().close(time, timeUnit);

    ArgumentCaptor<Duration> timeoutCaptor = ArgumentCaptor.forClass(Duration.class);

    verify(mockedConsumer, times(1)).close(timeoutCaptor.capture());

    assertEquals(timeUnit.toMillis(time), timeoutCaptor.getValue().toMillis());
  }

  @Test
  public void closeWithDuration() {
    final Duration timeout = Duration.ofSeconds(1000);

    consumer().close(timeout);

    ArgumentCaptor<Duration> timeoutCaptor = ArgumentCaptor.forClass(Duration.class);

    verify(mockedConsumer, times(1)).close(timeoutCaptor.capture());

    assertEquals(timeout, timeoutCaptor.getValue());
  }

  @Test
  public void wakeup() {
    consumer().wakeup();

    verify(mockedConsumer, times(1)).wakeup();
  }

  @Test
  public void currentLag() {
    TopicPartition topicPartition = new TopicPartition("", 0);
    consumer().currentLag(topicPartition);

    verify(mockedConsumer, times(1)).currentLag(topicPartition);
  }

  @Test
  public void enforceRebalanceWithReason() {
    String reason = "";
    consumer().enforceRebalance(reason);

    verify(mockedConsumer, times(1)).enforceRebalance(reason);
  }
}
