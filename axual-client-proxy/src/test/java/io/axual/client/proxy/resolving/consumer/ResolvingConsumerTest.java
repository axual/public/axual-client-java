package io.axual.client.proxy.resolving.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.commons.codec.Charsets;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.test.TestConsumerProxy;
import io.axual.client.proxy.test.TestConsumerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.GroupResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ResolvingConsumer.class, TestConsumerProxyFactory.class, TestConsumerProxy.class,
    DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class ResolvingConsumerTest {

  private static final String UNRESOLVED_STREAM = "general-applicationlog";
  private static final String APPLICATION_ID = "io.axual.test";
  private static final String APPLICATION_VERSION = "1.0";
  private static final String TENANT_PROPERTY = "tnt";
  private static final String ENVIRONMENT_PROPERTY = "envt";
  private static final String TOPIC_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{topic}";
  private static final String GROUP_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{group}";
  private static final String ENVIRONMENT = "unit";
  private static final String TENANT = "axual";
  private static final String GROUP_ID = "group";
  private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
  private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
  private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

  private static final String RECORD_KEY_FORMAT = "Unit Test Key %d";
  private static final String RECORD_VALUE_FORMAT = "Unit Test Value %d";

  private final TopicResolver topicResolver;
  private final GroupResolver groupResolver;

  @Mock
  private TestConsumerProxy<String, String> mockedConsumer1, mockedConsumer2, mockedConsumer3;

  public ResolvingConsumerTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put(TENANT_PROPERTY, TENANT);
    properties.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);
    properties
        .put(ResolvingConsumerConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
    properties.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
    properties.put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG,
        GroupPatternResolver.class.getName());
    properties.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
    topicResolver = new TopicPatternResolver();
    topicResolver.configure(properties);
    this.groupResolver = new GroupPatternResolver();
    groupResolver.configure(properties);
  }

  private Properties getValidProperties() {
    Properties consumerProps = new Properties();
    consumerProps.putAll(getValidMap());
    return consumerProps;
  }

  private Map<String, Object> getValidMap() {
    Map<String, Object> consumerMap = new HashMap<>();

    consumerMap.put(ResolvingConsumerConfig.BACKING_FACTORY_CONFIG,
        TestConsumerProxyFactory.class.getName());
    consumerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
    consumerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

    consumerMap.put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG,
        GroupPatternResolver.class.getName());
    consumerMap.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
    consumerMap
        .put(ResolvingConsumerConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
    consumerMap.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
    consumerMap.put(TENANT_PROPERTY, TENANT);
    consumerMap.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

    consumerMap.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
    consumerMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");
    consumerMap
        .put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    consumerMap
        .put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

    SslConfig sslConfig = SslConfig.newBuilder()
        .setKeyPassword(SSL_PASSWORD)
        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
        .setKeystorePassword(SSL_PASSWORD)
        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
        .setTruststorePassword(SSL_PASSWORD)
        .setEnableHostnameVerification(false)
        .build();
    KafkaUtil.getKafkaConfigs(sslConfig, consumerMap);

    return consumerMap;
  }

  // This method is called by tests to validate different constructors
  private void validConstructorTest(final ResolvingConsumer consumer) {
    final ResolvingConsumerConfig config = (ResolvingConsumerConfig) Whitebox
        .getInternalState(consumer, "config");

    Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
    Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

    // Verify initialisation of header KafkaProducer with mocked properties from the mocked provider
    verifyNew(TestConsumerProxy.class, times(1));
  }

  @Before
  public void setUp() throws Exception {
    whenNew(TestConsumerProxy.class).withAnyArguments()
        .thenReturn(mockedConsumer1, mockedConsumer2, mockedConsumer3);
  }

  @Test
  public void construction_ValidProperties() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    validConstructorTest(kafkaConsumer);
  }

  @Test
  public void construction_ValidMap() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(new ResolvingConsumer<>(getValidMap()));
    validConstructorTest(kafkaConsumer);
  }

  @Test
  public void verify_assignment() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.assignment();
    verify(mockedConsumer1, times(1)).assignment();
  }

  @Test
  public void verify_subscribe() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.subscription();
    verify(mockedConsumer1, times(1)).subscription();
  }

  @Test
  public void verify_pattern_subscribe() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<String, String>(getValidProperties()));
    final Pattern pattern = Pattern.compile(".*");
    final ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {
      @Override
      public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
      }

      @Override
      public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
      }
    };

    kafkaConsumer.subscribe(pattern, listener);

    ArgumentCaptor<Pattern> patternCaptor = ArgumentCaptor.forClass(Pattern.class);
    ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor
        .forClass(ConsumerRebalanceListener.class);
    verify(mockedConsumer1, times(1)).subscribe(patternCaptor.capture(), listenerCaptor.capture());
  }

  @Test
  public void verify_noTopics_subscribe_is_same_as_unsubscribe() {
    ResolvingConsumer<String, String> kafkaConsumer = new ResolvingConsumer<>(getValidProperties());
    kafkaConsumer.subscribe(Collections.<String>emptyList());
    assertEquals(0, kafkaConsumer.subscription().size());
  }

  @Test
  public void verify_multipleTopics_subscribe_should_work() {
    ResolvingConsumer<String, String> kafkaConsumer = new ResolvingConsumer<>(getValidProperties());
    kafkaConsumer.subscribe(Arrays.asList(UNRESOLVED_STREAM, UNRESOLVED_STREAM + "2"));
    verify(mockedConsumer1, times(1)).subscribe(any(Collection.class));
  }

  @Test
  public void verify_subscribe_listener_null() {
    List<String> topics = Collections.singletonList(UNRESOLVED_STREAM);
    ResolvingConsumer<String, String> kafkaConsumer = new ResolvingConsumer<>(getValidProperties());

    kafkaConsumer.subscribe(topics);

    verify(mockedConsumer1, times(0)).subscribe(topics);
  }

  @Test
  public void subscribeTopics() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    final List<String> topics = new ArrayList<>(0);

    kafkaConsumer.subscribe(topics);

    ArgumentCaptor<List> topicsCaptor = ArgumentCaptor.forClass(List.class);
    verify(mockedConsumer1, times(1)).subscribe(topicsCaptor.capture());
  }

//TODO: Do we need to find a fix for this?
// Can NOT be tested as of yet because of Private inner class: ProxyConsumerRebalanceListener
//    @Test
//    public void subscribeTopicsAndListener() {
//        final String expectedTopic = topicResolver.resolveTopic(UNRESOLVED_STREAM);
//        KafkaConsumer<String, String> kafkaConsumer = spy(new KafkaConsumer<String, String>(getValidProperties()));
//        final List<String> topics = new ArrayList<>(0);
//        topics.add(UNRESOLVED_STREAM);
//        final ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {
//            @Override
//            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
//
//            }
//
//            @Override
//            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
//
//            }
//        };
//
//        kafkaConsumer.subscribe(topics, listener);
//
//        ArgumentCaptor<List> topicsCaptor = ArgumentCaptor.forClass(List.class);
//        ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor.forClass(ConsumerRebalanceListener.class);
//        verify(mockedConsumer1, never()).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
//        verify(mockedConsumer2, times(1)).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
//        assertEquals(Collections.singletonList(expectedTopic), topicsCaptor.getValue());
//        assertEquals(listener, listenerCaptor.getValue());
//    }

  @Test
  public void unsubscribe() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.unsubscribe();

    verify(mockedConsumer1, times(1)).unsubscribe();
  }

  @Test
  public void assign() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    final List<TopicPartition> topicPartitions = new ArrayList<>(0);

    kafkaConsumer.assign(topicPartitions);

    verify(mockedConsumer1, times(1)).assign(Collections.<TopicPartition>emptySet());
  }

  private ConsumerRecord<byte[], byte[]> createRawConsumerRecord(final int itemNr,
      final boolean resolved) {
    final String keyString = String.format(RECORD_KEY_FORMAT, itemNr);
    final String valueString = String.format(RECORD_VALUE_FORMAT, itemNr);

    final int partition = itemNr;
    final long offset = itemNr;
    final long timestamp = itemNr;
    final TimestampType timestampType = TimestampType.NO_TIMESTAMP_TYPE;
    final long checksum = itemNr;
    final int serializedKeySize = itemNr;
    final int serializedValueSize = itemNr;

    byte[] key = keyString.getBytes(Charsets.UTF_8);
    byte[] value = valueString.getBytes(Charsets.UTF_8);

    String topic = resolved ? topicResolver.resolveTopic(UNRESOLVED_STREAM) : UNRESOLVED_STREAM;
    return new ConsumerRecord<>(topic, partition, offset, timestamp, timestampType, checksum,
        serializedKeySize, serializedValueSize, key, value);
  }

  private ConsumerRecords<byte[], byte[]> createRawConsumerRecords(final int start, final int end) {
    Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> contents = new HashMap<>();
    final TopicPartition topicPartition = new TopicPartition(
        topicResolver.resolveTopic(UNRESOLVED_STREAM), 0);

    final List<ConsumerRecord<byte[], byte[]>> records = new ArrayList<>();
    for (int counter = start; counter < end; counter++) {
      records.add(createRawConsumerRecord(counter, false));
    }
    contents.put(topicPartition, records);
    return new ConsumerRecords<>(contents);
  }

  private ConsumerRecords<byte[], byte[]> createConsumerRecordsResolved(final int start,
      final int end) {
    Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> contents = new HashMap<>();
    final TopicPartition topicPartition = new TopicPartition(
        topicResolver.resolveTopic(UNRESOLVED_STREAM), 0);

    final List<ConsumerRecord<byte[], byte[]>> records = new ArrayList<>();
    for (int counter = start; counter < end; counter++) {
      records.add(createRawConsumerRecord(counter, true));
    }
    contents.put(topicPartition, records);
    return new ConsumerRecords<>(contents);
  }

  private void validateConsumerRecord(final int itemNr,
      ConsumerRecord<String, String> consumerRecord) {
    Assert.assertNotNull(consumerRecord);

    Assert.assertEquals(UNRESOLVED_STREAM, consumerRecord.topic());
    Assert.assertEquals(itemNr, consumerRecord.partition());
    Assert.assertEquals(itemNr, consumerRecord.offset());
    Assert.assertEquals(itemNr, consumerRecord.timestamp());
    Assert.assertEquals(TimestampType.NO_TIMESTAMP_TYPE, consumerRecord.timestampType());
    Assert.assertEquals(itemNr, consumerRecord.serializedKeySize());
    Assert.assertEquals(itemNr, consumerRecord.serializedValueSize());
  }

  private void validateConsumerRecords(final int startOffset,
      final ConsumerRecords<String, String> consumerRecords) {
    Assert.assertNotNull(consumerRecords);
    Assert.assertFalse(consumerRecords.isEmpty());
    Set<TopicPartition> topicPartitions = consumerRecords.partitions();
    Assert.assertNotNull(topicPartitions);
    Assert.assertEquals(1, topicPartitions.size());
    for (TopicPartition topicPartition : topicPartitions) {
      int itemNr = startOffset;
      for (ConsumerRecord<String, String> consumerRecord : consumerRecords
          .records(topicPartition)) {
        validateConsumerRecord(itemNr, consumerRecord);
        itemNr++;
      }
    }
  }

  /**
   * This test calls poll() on consumer and expects 10 messages to be read.
   */
  @Test
  public void verify_poll() {
    final Long pollTimeout = 5000L;
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    ConsumerRecords mockedSet1 = createConsumerRecordsResolved(0, 10);
    // set behavior
    when(mockedConsumer1.poll(pollTimeout)).thenReturn(mockedSet1);

    // Always call subscribe() before poll() as per Kafka flow
    kafkaConsumer.subscribe(Collections.singletonList(UNRESOLVED_STREAM));
    ConsumerRecords<String, String> messages = kafkaConsumer.poll(pollTimeout);

    verify(mockedConsumer1, times(1)).poll(pollTimeout);
    validateConsumerRecords(0, messages);
  }

  @Test
  public void commitSync() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitSync();
    verify(mockedConsumer1, times(1)).commitSync();
  }

  @Test
  public void commitSyncWithDuration() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitSync(Duration.ZERO);
    verify(mockedConsumer1, times(1)).commitSync(Duration.ZERO);
  }

  @Test
  public void commitSyncWithMap() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitSync((Map<TopicPartition, OffsetAndMetadata>) null);
    verify(mockedConsumer1, times(1)).commitSync(any(Map.class));
  }

  @Test
  public void commitSyncWithMapAndDuration() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitSync(null, Duration.ZERO);
    verify(mockedConsumer1, times(1)).commitSync(any(Map.class), eq(Duration.ZERO));
  }

  @Test
  public void commitAsync() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitAsync();
    verify(mockedConsumer1, times(1)).commitAsync();
  }

  @Test
  public void commitAsyncWithCallback() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitAsync(null);
    verify(mockedConsumer1, times(1)).commitAsync(any(OffsetCommitCallback.class));
  }

  @Test
  public void commitAsyncWithMapAndCallback() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.commitAsync(null, null);
    verify(mockedConsumer1, times(1)).commitAsync(any(Map.class), any(OffsetCommitCallback.class));
  }

  @Test
  public void seek() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.seek(null, -1L);
    verify(mockedConsumer1, times(1)).seek(any(TopicPartition.class), any(Long.class));
  }

  @Test
  public void seekToBeginning() {
    ResolvingConsumer<String, String> consumer = spy(new ResolvingConsumer<>(getValidProperties()));
    consumer.seekToBeginning(null);
    verify(mockedConsumer1, times(1)).seekToBeginning(any(Collection.class));
  }

  @Test
  public void seekToEnd() {
    ResolvingConsumer<String, String> consumer = spy(new ResolvingConsumer<>(getValidProperties()));
    consumer.seekToEnd(null);
    verify(mockedConsumer1, times(1)).seekToEnd(any(Collection.class));
  }

  @Test
  public void position() {
    ResolvingConsumer<String, String> consumer = spy(new ResolvingConsumer<>(getValidProperties()));
    consumer.position(null);
    verify(mockedConsumer1, times(1)).position(null);
  }

  @Test
  public void committed_Partition() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    TopicPartition partition = null;
    kafkaConsumer.committed(partition);
    verify(mockedConsumer1, times(1)).committed(partition);
  }

  @Test
  public void committed_PartitionDuration() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    TopicPartition partition = null;
    Duration duration = null;
    kafkaConsumer.committed(partition, duration);
    verify(mockedConsumer1, times(1)).committed(partition, duration);
  }

  @Test
  public void committed_PartitionSet() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    Set<TopicPartition> partition = new HashSet<>();
    kafkaConsumer.committed(partition);
    verify(mockedConsumer1, times(1)).committed(partition);
  }

  @Test
  public void committed_PartitionSetDuration() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    Set<TopicPartition> partition = new HashSet<>();
    Duration duration = null;
    kafkaConsumer.committed(partition, duration);
    verify(mockedConsumer1, times(1)).committed(partition, duration);
  }

  @Test
  public void metrics() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.metrics();
    verify(mockedConsumer1, times(1)).metrics();
  }

  @Test
  public void partitionsFor() {
    final Node mockedNode = mock(Node.class);
    final Node[] replicas = {mockedNode, mockedNode, mockedNode, mockedNode};
    final Node[] inSyncReplicas = {mockedNode, mockedNode};
    final String resolvedTopicName = topicResolver.resolveTopic(UNRESOLVED_STREAM);
    final List<PartitionInfo> partitionInfoList = new ArrayList<>(10);
    for (int partition = 1; partition <= 10; partition++) {
      partitionInfoList.add(
          new PartitionInfo(resolvedTopicName, partition, mockedNode, replicas, inSyncReplicas));
    }

    when(mockedConsumer1.partitionsFor(resolvedTopicName)).thenReturn(partitionInfoList);

    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<String, String>(getValidProperties()));
    kafkaConsumer.subscribe(Collections.singletonList(UNRESOLVED_STREAM));
    List<PartitionInfo> retrievedPartitionInfo = kafkaConsumer.partitionsFor(UNRESOLVED_STREAM);

    verify(mockedConsumer1, times(1)).partitionsFor(resolvedTopicName);

    Assert.assertNotNull(retrievedPartitionInfo);
    Assert.assertEquals(partitionInfoList.size(), retrievedPartitionInfo.size());
    for (int counter = 0; counter < retrievedPartitionInfo.size(); counter++) {
      final PartitionInfo retrieved = retrievedPartitionInfo.get(counter);
      final PartitionInfo original = partitionInfoList.get(counter);
      Assert.assertTrue(retrieved instanceof ResolvingPartitionInfo);
      Assert.assertEquals(UNRESOLVED_STREAM, retrieved.topic());
      Assert.assertEquals(original.partition(), retrieved.partition());
    }
  }

  @Test
  public void listTopics() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.listTopics();
    verify(mockedConsumer1, times(1)).listTopics();
  }

  @Test
  public void pause() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.pause(null);
    verify(mockedConsumer1, times(1)).pause(any(Collection.class));
  }

  @Test
  public void resume() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.resume(null);
    verify(mockedConsumer1, times(1)).resume(any(Collection.class));
  }

  @Test
  public void paused() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.paused();
    verify(mockedConsumer1, times(1)).paused();
  }

  @Test
  public void offsetsForTimes() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.offsetsForTimes(null);
    verify(mockedConsumer1, times(1)).offsetsForTimes(any(Map.class));
  }

  @Test
  public void beginningOffsets() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.beginningOffsets(null);
    verify(mockedConsumer1, times(1)).beginningOffsets(any(Collection.class));
  }

  @Test
  public void endOffsets() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.endOffsets(null);
    verify(mockedConsumer1, times(1)).endOffsets(any(Collection.class));
  }

  @Test
  public void groupMetadata() {
    doReturn(new ConsumerGroupMetadata(groupResolver.resolveGroup(GROUP_ID))).when(mockedConsumer1).groupMetadata();
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    ConsumerGroupMetadata groupMetadata= kafkaConsumer.groupMetadata();
    verify(mockedConsumer1, times(1)).groupMetadata();

    assertNotNull(groupMetadata);
    assertEquals(GROUP_ID,groupMetadata.groupId());
  }

  @Test
  public void enforceRebalance() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.enforceRebalance();
    verify(mockedConsumer1, times(1)).enforceRebalance();
  }

  @Test
  public void wakeup() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
        new ResolvingConsumer<>(getValidProperties()));
    kafkaConsumer.wakeup();
    verify(mockedConsumer1, times(1)).wakeup();
  }

  @Test
  public void currentLag() {
    ResolvingConsumer<String, String> kafkaConsumer = spy(
            new ResolvingConsumer<>(getValidProperties()));
    TopicPartition topicPartition = new TopicPartition(UNRESOLVED_STREAM, 0);

    final ArgumentCaptor<TopicPartition> captor = ArgumentCaptor.forClass(TopicPartition.class);

    kafkaConsumer.currentLag(topicPartition);

    verify(mockedConsumer1, times(1)).currentLag(captor.capture());

    assertEquals("Expected resolved topic in currentLag", topicResolver.resolveTopic(UNRESOLVED_STREAM), captor.getValue().topic());
  }
}
