package io.axual.client.proxy.resolving.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ConsumerGroupDescription;
import org.apache.kafka.clients.admin.ConsumerGroupListing;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.ExtendableDeleteTopicsResult;
import org.apache.kafka.clients.admin.ExtendableDescribeTopicsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListOffsetsResult.ListOffsetsResultInfo;
import org.apache.kafka.clients.admin.MemberToRemove;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.Uuid;
import org.apache.kafka.common.acl.AccessControlEntryFilter;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.acl.AclOperation;
import org.apache.kafka.common.acl.AclPermissionType;
import org.apache.kafka.common.internals.KafkaFutureImpl;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePatternFilter;
import org.apache.kafka.common.resource.ResourceType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import io.axual.client.proxy.test.TestAdminProxy;
import io.axual.client.proxy.test.TestAdminProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.exception.NotSupportedException;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.GroupResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.tools.FactoryUtil;
import io.axual.common.tools.KafkaUtil;
import io.axual.common.tools.MapUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ResolvingAdminClient.class, TestAdminProxyFactory.class, TestAdminProxy.class, FactoryUtil.class})
@PowerMockIgnore({"javax.net.ssl.*", "jdk.internal.reflect.*"})
public class ResolvingAdminClientTest {
    private static final String TOPIC = "general-applicationlog";
    private static final String APPLICATION_ID = "io.axual.test";
    private static final String APPLICATION_VERSION = "1.0";
    private static final String GROUP = "general-group";
    private static final String TENANT_PROPERTY = "tnt";
    private static final String ENVIRONMENT_PROPERTY = "envt";
    private static final String TOPIC_PATTERN = "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{topic}";
    private static final String GROUP_PATTERN = "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{group}";
    private static final String ENVIRONMENT = "unit";
    private static final String TENANT = "axual";
    private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

    private final GroupResolver groupResolver;
    private final TopicResolver topicResolver;

    @Mock
    private TestAdminProxy mockedAdmin;

    private ResolvingAdminClient resolvingAdminClient;

    public ResolvingAdminClientTest() {
        Map<String, Object> properties = new HashMap<>();

        properties.put(TENANT_PROPERTY, TENANT);
        properties.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

        properties.put(ResolvingAdminConfig.GROUP_ID_RESOLVER_CONFIG, GroupPatternResolver.class.getName());
        properties.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
        groupResolver = new GroupPatternResolver();
        groupResolver.configure(properties);

        properties.put(ResolvingAdminConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        properties.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
        topicResolver = new TopicPatternResolver();
        topicResolver.configure(properties);
    }

    protected Map<String, Object> getValidConfigs() {
        final Map<String, Object> configs = new HashMap<>();
        configs.put(ResolvingAdminConfig.BACKING_FACTORY_CONFIG, TestAdminProxyFactory.class.getName());
        configs.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        configs.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

        configs.put(ResolvingAdminConfig.GROUP_ID_RESOLVER_CONFIG, GroupPatternResolver.class.getName());
        configs.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
        configs.put(ResolvingAdminConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
        configs.put(TENANT_PROPERTY, TENANT);
        configs.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeyPassword(SSL_PASSWORD)
                .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                .setKeystorePassword(SSL_PASSWORD)
                .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                .setTruststorePassword(SSL_PASSWORD)
                .setEnableHostnameVerification(false)
                .build();
        KafkaUtil.getKafkaConfigs(sslConfig, configs);

        return configs;
    }

    @Before
    public void setup() throws Exception {
        whenNew(TestAdminProxy.class).withAnyArguments().thenReturn(mockedAdmin);
        resolvingAdminClient = ResolvingAdminClient.create(getValidConfigs());
    }

    @Test(expected = NotSupportedException.class)
    public void createAcls() {
        resolvingAdminClient.createAcls(null);
    }

    @Test(expected = NotSupportedException.class)
    public void createAclsWithOptions() {
        resolvingAdminClient.createAcls(null, null);
    }

    @Test(expected = NotSupportedException.class)
    public void deleteAcls() {
        resolvingAdminClient.deleteAcls(null);
    }

    @Test(expected = NotSupportedException.class)
    public void deleteAclsWithOptions() {
        resolvingAdminClient.deleteAcls(null, null);
    }

    @Test(expected = NotSupportedException.class)
    public void describeConfigs() {
        resolvingAdminClient.describeConfigs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void describeConfigsWithOptions() {
        resolvingAdminClient.describeConfigs(null, null);
    }

    @Test(expected = NotSupportedException.class)
    public void alterConfigs() {
        resolvingAdminClient.alterConfigs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void incrementalAlterConfigs() {
        resolvingAdminClient.incrementalAlterConfigs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void alterReplicaLogDirs() {
        resolvingAdminClient.alterReplicaLogDirs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void describeLogDirs() {
        resolvingAdminClient.describeLogDirs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void describeReplicaLogDirs() {
        resolvingAdminClient.describeReplicaLogDirs(null);
    }

    @Test(expected = NotSupportedException.class)
    public void createPartitions() {
        resolvingAdminClient.createPartitions(null);
    }

    @Test(expected = NotSupportedException.class)
    public void alterConfigsWithOptions() {
        resolvingAdminClient.alterConfigs(null, null);
    }

    @Test
    public void describeAclsForTopics() {
        ResourcePatternFilter rf = new ResourcePatternFilter(ResourceType.TOPIC, TOPIC, PatternType.ANY);
        String expectedTopic = topicResolver.resolveTopic(TOPIC);

        AclBindingFilter aclFilter = new AclBindingFilter(rf, new AccessControlEntryFilter(null, null, AclOperation.ANY, AclPermissionType.ALLOW));
        final ArgumentCaptor<AclBindingFilter> aclBindingFilterArgumentCaptor = ArgumentCaptor.forClass(AclBindingFilter.class);
        resolvingAdminClient.describeAcls(aclFilter);
        verify(mockedAdmin, Mockito.times(1)).describeAcls(aclBindingFilterArgumentCaptor.capture(), any(DescribeAclsOptions.class));
        AclBindingFilter af = aclBindingFilterArgumentCaptor.getValue();
        String capturedTopic = af.patternFilter().name();
        assertEquals(expectedTopic, capturedTopic);
    }

    @Test
    public void describeAclsForGroup() {
        ResourcePatternFilter rf = new ResourcePatternFilter(ResourceType.GROUP, GROUP, PatternType.ANY);
        String expectedGroup = groupResolver.resolveGroup(GROUP);

        AclBindingFilter aclFilter = new AclBindingFilter(rf, new AccessControlEntryFilter(null, null, AclOperation.ANY, AclPermissionType.ALLOW));
        final ArgumentCaptor<AclBindingFilter> aclBindingFilterArgumentCaptor = ArgumentCaptor.forClass(AclBindingFilter.class);
        resolvingAdminClient.describeAcls(aclFilter);
        verify(mockedAdmin, Mockito.times(1)).describeAcls(aclBindingFilterArgumentCaptor.capture(), any(DescribeAclsOptions.class));
        AclBindingFilter af = aclBindingFilterArgumentCaptor.getValue();
        String capturedGroup = af.patternFilter().name();
        assertEquals(expectedGroup, capturedGroup);
    }

    @Test
    public void describeAcls() {
        ResourcePatternFilter rf = new ResourcePatternFilter(ResourceType.TOPIC, TOPIC, PatternType.ANY);
        String expectedTopic = topicResolver.resolveTopic(TOPIC);

        AclBindingFilter aclFilter = new AclBindingFilter(rf, new AccessControlEntryFilter(null, null, AclOperation.ANY, AclPermissionType.ALLOW));
        final ArgumentCaptor<AclBindingFilter> aclBindingFilterArgumentCaptor = ArgumentCaptor.forClass(AclBindingFilter.class);
        resolvingAdminClient.describeAcls(aclFilter);
        verify(mockedAdmin, Mockito.times(1)).describeAcls(aclBindingFilterArgumentCaptor.capture(), any(DescribeAclsOptions.class));
        AclBindingFilter af = aclBindingFilterArgumentCaptor.getValue();
        String capturedGroup = af.patternFilter().name();
        assertEquals(expectedTopic, capturedGroup);
    }

    @Test
    public void describeConsumerGroups() throws ExecutionException, InterruptedException {
        // Arguments
        final int expectedGroupSize = 1;
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        final DescribeConsumerGroupsOptions options = new DescribeConsumerGroupsOptions();

        //Result data for mock
        KafkaFutureImpl<ConsumerGroupDescription> proxyFuture = new KafkaFutureImpl<>();
        proxyFuture.complete(
                new ConsumerGroupDescription(expectedResolvedGroup, true, Collections.emptySet(),
                        "range", null, null));
        Map<String, KafkaFuture<ConsumerGroupDescription>> proxyFutures = Collections
                .singletonMap(expectedResolvedGroup, proxyFuture);
        DescribeConsumerGroupsResult proxyResult = new DescribeConsumerGroupsResult(proxyFutures);
        doReturn(proxyResult).when(mockedAdmin)
                .describeConsumerGroups(anyCollection(), any(DescribeConsumerGroupsOptions.class));

        // call listConsumerGroups
        DescribeConsumerGroupsResult result = resolvingAdminClient
                .describeConsumerGroups(Collections.singleton(GROUP), options);

        // Verify captured data
        ArgumentCaptor<Collection> collectionCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(mockedAdmin, times(1)).describeConsumerGroups(collectionCaptor.capture(), eq(options));

        Collection<String> captured = collectionCaptor.getValue();
        assertNotNull(captured);
        assertEquals(expectedGroupSize, captured.size());
        for (String capturedGroup : captured) {
            assertEquals(expectedResolvedGroup, capturedGroup);
        }

        assertNotNull(result);
        Map<String, KafkaFuture<ConsumerGroupDescription>> describedGroups = result.describedGroups();
        assertNotNull(describedGroups);
        assertEquals(expectedGroupSize, describedGroups.size());
        for (Entry<String, KafkaFuture<ConsumerGroupDescription>> entry : describedGroups.entrySet()) {
            assertEquals(GROUP, entry.getKey());
            assertNotNull(entry.getValue());
            ConsumerGroupDescription description = entry.getValue().get();
            assertNotNull(description);
            assertEquals(GROUP, description.groupId());
        }
    }

    @Test
    public void listConsumerGroupOffsets() throws ExecutionException, InterruptedException {
        // Arguments
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        final String expectedResolvedTopic = topicResolver.resolveTopic(TOPIC);
        final int expectedPartition = 0;
        final int expectedPartitionSize = 1;
        ListConsumerGroupOffsetsOptions options = new ListConsumerGroupOffsetsOptions();
        options.topicPartitions(Collections.singletonList(new TopicPartition(TOPIC, 0)));

        //Result data for mock
        ListConsumerGroupOffsetsResult proxyResult = mock(ListConsumerGroupOffsetsResult.class);
        doReturn(proxyResult).when(mockedAdmin)
                .listConsumerGroupOffsets(anyString(), any(ListConsumerGroupOffsetsOptions.class));

        final TopicPartition proxiedPartition = new TopicPartition(expectedResolvedTopic,
                expectedPartition);
        final int expectedMockOffsetSize = 1;
        final int expectedMockOffset = 12;
        final OffsetAndMetadata proxiedOffsetAndMetadata = new OffsetAndMetadata(
                expectedMockOffset);
        KafkaFutureImpl<Map<TopicPartition, OffsetAndMetadata>> proxyFuture = new KafkaFutureImpl<>();
        proxyFuture.complete(Collections.singletonMap(proxiedPartition, proxiedOffsetAndMetadata));
        doReturn(proxyFuture).when(proxyResult).partitionsToOffsetAndMetadata();

        // call listConsumerGroups
        ListConsumerGroupOffsetsResult result = resolvingAdminClient
                .listConsumerGroupOffsets(GROUP, options);

        // Verify captured data
        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<ListConsumerGroupOffsetsOptions> optionsCaptor = ArgumentCaptor
                .forClass(ListConsumerGroupOffsetsOptions.class);
        verify(mockedAdmin, times(1))
                .listConsumerGroupOffsets(stringCaptor.capture(), optionsCaptor.capture());

        assertEquals(expectedResolvedGroup, stringCaptor.getValue());
        ListConsumerGroupOffsetsOptions captured = optionsCaptor.getValue();
        assertNotNull(captured);
        List<TopicPartition> resolvedPartitions = captured.topicPartitions();
        assertNotNull(resolvedPartitions);
        assertEquals(expectedPartitionSize, resolvedPartitions.size());
        for (TopicPartition resolvedPartition : resolvedPartitions) {
            assertNotNull(resolvedPartition);
            assertEquals(expectedResolvedTopic, resolvedPartition.topic());
            assertEquals(expectedPartition, resolvedPartition.partition());
        }

        // verify result
        assertNotNull(result);
        KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> future = result
                .partitionsToOffsetAndMetadata();
        assertNotNull(future);
        Map<TopicPartition, OffsetAndMetadata> offsets = future.get();
        assertNotNull(offsets);
        assertEquals(expectedMockOffsetSize, offsets.size());
        for (Entry<TopicPartition, OffsetAndMetadata> entry : offsets.entrySet()) {
            assertNotNull(entry.getKey());
            assertEquals(TOPIC, entry.getKey().topic());
            assertEquals(expectedPartition, entry.getKey().partition());
            assertNotNull(entry.getValue());
            assertEquals(expectedMockOffset, entry.getValue().offset());
        }
    }

    @Test
    public void listConsumerGroups() throws ExecutionException, InterruptedException {
        // Arguments
        ListConsumerGroupsOptions options = new ListConsumerGroupsOptions();

        //Result data for mock
        ListConsumerGroupsResult proxyResult = mock(ListConsumerGroupsResult.class);
        doReturn(proxyResult).when(mockedAdmin)
                .listConsumerGroups(any(ListConsumerGroupsOptions.class));

        final String unresolvedValidGroupId = "validGroup";
        final String resolvedValidGroupId = groupResolver.resolveGroup(unresolvedValidGroupId);

        final KafkaFutureImpl<Collection<ConsumerGroupListing>> proxyValid = new KafkaFutureImpl<>();
        proxyValid
                .complete(Collections.singleton(new ConsumerGroupListing(resolvedValidGroupId, true)));
        doReturn(proxyValid).when(proxyResult).valid();
        final int expectedValidSize = 1;

        final KafkaFutureImpl<Collection<ConsumerGroupListing>> proxyAll = new KafkaFutureImpl<>();
        proxyAll
                .complete(Collections.singleton(new ConsumerGroupListing(resolvedValidGroupId, true)));
        doReturn(proxyAll).when(proxyResult).all();
        final int expectedAllSize = 1;

        KafkaFutureImpl<Collection<Throwable>> proxyErrors = new KafkaFutureImpl<>();
        proxyErrors.complete(Collections.emptySet());
        doReturn(proxyErrors).when(proxyResult).errors();
        final int expectedErrorSize = 0;

        // call listConsumerGroups
        ListConsumerGroupsResult result = resolvingAdminClient.listConsumerGroups(options);

        // Verify mock
        verify(mockedAdmin, times(1)).listConsumerGroups(options);

        // Verify result
        KafkaFuture<Collection<ConsumerGroupListing>> validFuture = result.valid();
        assertNotNull(validFuture);
        Collection<ConsumerGroupListing> valid = validFuture.get();
        assertEquals(expectedValidSize, valid.size());
        for (ConsumerGroupListing listing : valid) {
            assertNotNull(listing);
            assertEquals(unresolvedValidGroupId, listing.groupId());
        }

        KafkaFuture<Collection<ConsumerGroupListing>> allFuture = result.all();
        assertNotNull(allFuture);
        Collection<ConsumerGroupListing> all = allFuture.get();
        assertEquals(expectedAllSize, all.size());
        for (ConsumerGroupListing listing : all) {
            assertNotNull(listing);
            assertEquals(unresolvedValidGroupId, listing.groupId());
        }

        KafkaFuture<Collection<Throwable>> errorsFuture = result.errors();
        assertNotNull(errorsFuture);
        Collection<Throwable> allErrors = errorsFuture.get();
        assertEquals(expectedErrorSize, allErrors.size());
    }

    @Test
    public void deleteConsumerGroups() {
        // Arguments
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        DeleteConsumerGroupsOptions options = new DeleteConsumerGroupsOptions();
        final int expectedArgumentSetSize = 1;

        //Result data for mock
        DeleteConsumerGroupsResult proxyResult = mock(DeleteConsumerGroupsResult.class);
        doReturn(proxyResult).when(mockedAdmin).deleteConsumerGroups(anySet(),
                any(DeleteConsumerGroupsOptions.class));

        // call deleteConsumerGroups
        DeleteConsumerGroupsResult result = resolvingAdminClient
                .deleteConsumerGroups(Collections.singleton(GROUP), options);

        // Verify captured data
        ArgumentCaptor<Set> setCaptor = ArgumentCaptor.forClass(Set.class);
        verify(mockedAdmin, times(1))
                .deleteConsumerGroups(setCaptor.capture(), any(DeleteConsumerGroupsOptions.class));

        Set<String> captured = setCaptor.getValue();
        assertNotNull(captured);
        assertEquals(expectedArgumentSetSize, captured.size());
        for (String resolvedGroupId : captured) {
            assertEquals(expectedResolvedGroup, resolvedGroupId);
        }

        assertNotNull(result);
    }

    @Test
    public void deleteConsumerGroupOffsets() {
        // Arguments
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        final String expectedResolvedTopic = topicResolver.resolveTopic(TOPIC);
        final int expectedPartition = 0;
        final int expectedArgumentSetSize = 1;
        TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        DeleteConsumerGroupOffsetsOptions options = new DeleteConsumerGroupOffsetsOptions();

        //Result data for mock
        DeleteConsumerGroupOffsetsResult proxyResult = mock(DeleteConsumerGroupOffsetsResult.class);
        doReturn(proxyResult).when(mockedAdmin).deleteConsumerGroupOffsets(anyString(), anySet(),
                any(DeleteConsumerGroupOffsetsOptions.class));

        // call deleteConsumerGroupOffsets
        DeleteConsumerGroupOffsetsResult result = resolvingAdminClient
                .deleteConsumerGroupOffsets(GROUP,
                        Collections.singleton(topicPartition), options);

        // Verify captured data
        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Set> setCaptor = ArgumentCaptor.forClass(Set.class);
        verify(mockedAdmin, times(1))
                .deleteConsumerGroupOffsets(stringCaptor.capture(), setCaptor.capture(),
                        any(DeleteConsumerGroupOffsetsOptions.class));

        assertEquals(expectedResolvedGroup, stringCaptor.getValue());

        Set<TopicPartition> captured = setCaptor.getValue();
        assertNotNull(captured);
        assertEquals(expectedArgumentSetSize, captured.size());
        for (TopicPartition tp : captured) {
            assertNotNull(tp);
            assertEquals(expectedResolvedTopic, tp.topic());
            assertEquals(expectedPartition, tp.partition());
        }

        assertNotNull(result);
    }

    @Test(expected = NotSupportedException.class)
    public void electLeaders() {
        resolvingAdminClient.electLeaders(null, Collections.emptySet(), null);
    }

    @Test(expected = NotSupportedException.class)
    public void alterPartitionReassignments() {
        resolvingAdminClient.alterPartitionReassignments(Collections.emptyMap(), null);
    }

    @Test(expected = NotSupportedException.class)
    public void listPartitionReassignments() {
        resolvingAdminClient.listPartitionReassignments(Optional.empty(), null);
    }

    @Test
    public void removeMembersFromConsumerGroup() {
        // Arguments
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        final RemoveMembersFromConsumerGroupOptions removeMemberOptions = new RemoveMembersFromConsumerGroupOptions(
                Collections.singleton(new MemberToRemove(null))
        );

        // Result data for mock
        RemoveMembersFromConsumerGroupResult proxyResult = mock(RemoveMembersFromConsumerGroupResult.class);
        doReturn(proxyResult).when(mockedAdmin).removeMembersFromConsumerGroup(anyString(), any(RemoveMembersFromConsumerGroupOptions.class));

        // call removeMembersFromConsumerGroup
        RemoveMembersFromConsumerGroupResult result = resolvingAdminClient.removeMembersFromConsumerGroup(GROUP, removeMemberOptions);

        // Capture map for resolving verification
        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockedAdmin, times(1)).removeMembersFromConsumerGroup(stringCaptor.capture(), eq(removeMemberOptions));
        assertEquals(expectedResolvedGroup, stringCaptor.getValue());

        assertNotNull(result);
    }

    @Test
    public void alterConsumerGroupOffsets() {
        // Arguments
        final String expectedResolvedGroup = groupResolver.resolveGroup(GROUP);
        final String expectedResolvedTopic = topicResolver.resolveTopic(TOPIC);
        final int expectedPartition = 0;
        final int expectedArgumentMapSize = 1;
        TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(0);
        AlterConsumerGroupOffsetsOptions options = new AlterConsumerGroupOffsetsOptions();

        //Result data for mock
        AlterConsumerGroupOffsetsResult proxyResult = mock(AlterConsumerGroupOffsetsResult.class);
        doReturn(proxyResult).when(mockedAdmin).alterConsumerGroupOffsets(anyString(), anyMap(),
                any(AlterConsumerGroupOffsetsOptions.class));

        // call alterConsumerGroupOffsets
        AlterConsumerGroupOffsetsResult result = resolvingAdminClient
                .alterConsumerGroupOffsets(GROUP,
                        Collections.singletonMap(topicPartition, offsetAndMetadata), options);

        // Capture map for resolving verification
        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
        verify(mockedAdmin, times(1))
                .alterConsumerGroupOffsets(stringCaptor.capture(), mapCaptor.capture(), any(AlterConsumerGroupOffsetsOptions.class));

        // Verify captured data
        assertEquals(expectedResolvedGroup, stringCaptor.getValue());

        Map<TopicPartition, OffsetAndMetadata> captured = mapCaptor.getValue();
        assertNotNull(captured);
        assertEquals(expectedArgumentMapSize, captured.size());
        for (Entry<TopicPartition, OffsetAndMetadata> entry : captured.entrySet()) {
            assertNotNull(entry);
            assertNotNull(entry.getKey());
            assertEquals(expectedResolvedTopic, entry.getKey().topic());
            assertEquals(expectedPartition, entry.getKey().partition());
            assertEquals(offsetAndMetadata, entry.getValue());
        }

        assertNotNull(result);
    }

    @Test
    public void listOffsets() throws ExecutionException, InterruptedException {
        // Arguments
        final String expectedResolvedTopic = topicResolver.resolveTopic(TOPIC);
        final int expectedPartition = 0;
        final int expectedArgumentMapSize = 1;
        final OffsetSpec offsetSpec = OffsetSpec.earliest();
        TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        ListOffsetsOptions options = new ListOffsetsOptions();

        //Result data for mock
        final int expectedResultMapSize = 1;
        final TopicPartition resolvedTopicPartition = new TopicPartition(expectedResolvedTopic,
                expectedPartition);
        final ListOffsetsResultInfo resultInfo = new ListOffsetsResultInfo(0, 0, Optional.empty());
        final KafkaFutureImpl<ListOffsetsResultInfo> future = new KafkaFutureImpl<>();
        future.complete(resultInfo);
        Map<TopicPartition, KafkaFuture<ListOffsetsResultInfo>> futures = Collections
                .singletonMap(resolvedTopicPartition, future);
        ListOffsetsResult proxyResult = new ListOffsetsResult(futures);
        doReturn(proxyResult).when(mockedAdmin)
                .listOffsets(anyMap(), any(ListOffsetsOptions.class));

        // call listOffsets
        ListOffsetsResult result = resolvingAdminClient
                .listOffsets(Collections.singletonMap(topicPartition, offsetSpec), options);

        // Capture map for resolving verification
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
        verify(mockedAdmin, times(1))
                .listOffsets(mapCaptor.capture(), any(ListOffsetsOptions.class));

        // Verify captured data
        Map<TopicPartition, OffsetSpec> captured = mapCaptor.getValue();
        assertNotNull(captured);
        assertEquals(expectedArgumentMapSize, captured.size());
        for (Entry<TopicPartition, OffsetSpec> entry : captured.entrySet()) {
            assertNotNull(entry);
            assertNotNull(entry.getKey());
            assertEquals(expectedResolvedTopic, entry.getKey().topic());
            assertEquals(expectedPartition, entry.getKey().partition());
            assertEquals(offsetSpec, entry.getValue());
        }

        // verify result
        assertNotNull(result);
        Map<TopicPartition, ListOffsetsResultInfo> receivedMap = result.all().get();
        assertNotNull(receivedMap);
        assertEquals(expectedResultMapSize, receivedMap.size());
        for (Entry<TopicPartition, ListOffsetsResultInfo> entry : receivedMap.entrySet()) {
            assertNotNull(entry);
            assertNotNull(entry.getKey());
            assertEquals(TOPIC, entry.getKey().topic());
            assertEquals(expectedPartition, entry.getKey().partition());
            assertEquals(resultInfo, entry.getValue());
        }
    }

    @Test(expected = NotSupportedException.class)
    public void describeClientQuotas() {
        resolvingAdminClient.describeClientQuotas(null);
    }

    @Test(expected = NotSupportedException.class)
    public void alterClientQuotas() {
        resolvingAdminClient.alterClientQuotas(Collections.emptyList());
    }

    @Test
    public void describeTopics() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));
        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(MapUtil.put(new HashMap<>(), resolvedTopic, future));
        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));
        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC));
        assertEquals("Expected 1 topic description", 1, result.values().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.values().keySet().contains(TOPIC));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.values().get(TOPIC).get().name());
    }


    @Test
    public void describeTopicsWithOptions() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));
        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(MapUtil.put(new HashMap<>(), resolvedTopic, future));
        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));
        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC), new DescribeTopicsOptions());
        assertEquals("Expected 1 topic description", 1, result.values().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.values().keySet().contains(TOPIC));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.values().get(TOPIC).get().name());
    }

    @Test
    public void describeAllTest() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));
        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(MapUtil.put(new HashMap<>(), resolvedTopic, future));
        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));
        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC), new DescribeTopicsOptions());
        assertEquals("Expected 1 topic description", 1, result.all().get().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.all().get().keySet().contains(TOPIC));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.all().get().get(TOPIC).name());
    }

    @Test
    public void describeTopicsWithNameFuturesAndNullUuid() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));

        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(
                null,
                MapUtil.put(new HashMap<>(), resolvedTopic, future));

        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));

        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC));

        assertEquals("Expected 1 topic name", 1, result.topicNameValues().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.topicNameValues().containsKey(TOPIC));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.topicNameValues().get(TOPIC).get().name());
    }

    @Test
    public void describeTopicsWithUuidAndNullNameFutures() throws ExecutionException, InterruptedException {
        final Uuid uuid = Uuid.randomUuid();
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));

        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(
                MapUtil.put(new HashMap<>(), uuid, future),
                null);

        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));

        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC));

        assertEquals("Expected 1 topic id value", 1, result.topicIdValues().size());
        assertTrue("Expected " + uuid + " in the result set", result.topicIdValues().containsKey(uuid));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.topicIdValues().get(uuid).get().name());
    }

    @Test
    public void describeAllTopicNamesTest() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);

        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));

        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(null, MapUtil.put(new HashMap<>(), resolvedTopic, future));

        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));

        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC), new DescribeTopicsOptions());
        assertEquals("Expected 1 topic description", 1, result.allTopicNames().get().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.allTopicNames().get().containsKey(TOPIC));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.allTopicNames().get().get(TOPIC).name());
    }

    @Test
    public void describeAllTopicIdsTest() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        Uuid uuid = Uuid.randomUuid();

        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));

        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(MapUtil.put(new HashMap<>(), uuid, future), null);

        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));

        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC), new DescribeTopicsOptions());
        assertEquals("Expected 1 topic description", 1, result.allTopicIds().get().size());
        assertTrue("Expected " + uuid + " in the result set", result.allTopicIds().get().containsKey(uuid));
        assertEquals("Expected unresolved topic name in TopicDescription", TOPIC, result.allTopicIds().get().get(uuid).name());
    }

    @Test
    public void deleteTopicsWithNameFuturesAndNullUuid() throws ExecutionException, InterruptedException {
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<Void> future = KafkaFuture.completedFuture(null);

        DeleteTopicsResult deleteTopicsResult = new ExtendableDeleteTopicsResult(
                null,
                MapUtil.put(new HashMap<>(), resolvedTopic, future));

        doReturn(deleteTopicsResult).when(mockedAdmin).deleteTopics(any(Collection.class), any(DeleteTopicsOptions.class));

        DeleteTopicsResult result = resolvingAdminClient.deleteTopics(Collections.singletonList(TOPIC));

        assertEquals("Expected 1 topic name", 1, result.topicNameValues().size());
        assertTrue("Expected " + TOPIC + " in the result set", result.topicNameValues().containsKey(TOPIC));
    }

    @Test
    public void deleteTopicsWithUuidAndNullNameFutures() throws ExecutionException, InterruptedException {
        final Uuid uuid = Uuid.randomUuid();
        final String resolvedTopic = topicResolver.resolveTopic(TOPIC);
        KafkaFuture<TopicDescription> future = KafkaFuture.completedFuture(new TopicDescription(resolvedTopic, false, new ArrayList<>()));

        DescribeTopicsResult describeTopicsResult = new ExtendableDescribeTopicsResult(
                MapUtil.put(new HashMap<>(), uuid, future),
                null);

        doReturn(describeTopicsResult).when(mockedAdmin).describeTopics(any(Collection.class), any(DescribeTopicsOptions.class));

        DescribeTopicsResult result = resolvingAdminClient.describeTopics(Collections.singletonList(TOPIC));

        assertEquals("Expected 1 topic id value", 1, result.topicIdValues().size());
        assertTrue("Expected " + uuid + " in the result set", result.topicIdValues().containsKey(uuid));
    }

}
