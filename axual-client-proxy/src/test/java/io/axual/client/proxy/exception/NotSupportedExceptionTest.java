package io.axual.client.proxy.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import io.axual.common.exception.NotSupportedException;

import static org.junit.Assert.assertEquals;

public class NotSupportedExceptionTest {
    protected static final String MESSAGE = "Error message for Unit Test";
    protected static final Exception CAUSE = new RuntimeException();

    @Test
    public void constructor_message() {
        NotSupportedException notSupportedException = new NotSupportedException(MESSAGE);
        assertEquals(MESSAGE, notSupportedException.getMessage());
    }
}
