package io.axual.client.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class OffsetAndMetadataUtilTest {

    @Test
    public void cleanMetadata_Single_Null() {
        assertNull(OffsetAndMetadataUtil.INSTANCE.cleanMetadata((OffsetAndMetadata) null));
    }

    @Test
    public void cleanMetadata_Single_EmptyMetadata() {
        testCleaned("");
    }

    @Test
    public void cleanMetadata_Single_CustomMetadata() {
        testCleaned("{ This is not JSON, just a trick start }");
    }

    @Test
    public void cleanMetadata_Single_OtherJsonMetadata() {
        testCleaned("{ \"someField\": 1 }");
    }

    @Test
    public void cleanMetadata_Single_InvalidJsonMetadata() {
        testCleaned("{ This is not JSON, just a trick start }");
    }
    
    private void testCleaned(String metadata){
        final long offset = 11L;
        final OffsetAndMetadata toTest = new OffsetAndMetadata(offset, metadata);
        assertEquals(toTest, OffsetAndMetadataUtil.INSTANCE.cleanMetadata(toTest));
    }

    @Test
    public void cleanMetadata_Single_AxualMetadata() {
        final long offset = 11L;
        final String metadata = "{ \"copyFlags\":1 }";
        final String expectedMetadata = "";
        final OffsetAndMetadata toTest = new OffsetAndMetadata(offset, metadata);
        final OffsetAndMetadata cleaned = OffsetAndMetadataUtil.INSTANCE.cleanMetadata(toTest);
        assertEquals(toTest.offset(), cleaned.offset());
        assertEquals(toTest.leaderEpoch(), cleaned.leaderEpoch());
        assertEquals(expectedMetadata, cleaned.metadata());
    }


    @Test(expected = NullPointerException.class)
    public void testCleanMetadata_Map_Null() {
        OffsetAndMetadataUtil.INSTANCE.cleanMetadata((Map) null);
    }

    @Test
    public void testCleanMetadata_Map_Empty() {
        final Map<String, OffsetAndMetadata> metadataMap = new HashMap<>();
        final Map<String, OffsetAndMetadata> cleanedMap = OffsetAndMetadataUtil.INSTANCE.cleanMetadata(metadataMap);
        assertNotNull(cleanedMap);
        assertTrue(cleanedMap.isEmpty());
    }

    @Test
    public void testCleanMetadata_Map_DifferentData() {
        final Map<String, OffsetAndMetadata> metadataMap = new HashMap<>();
        final long offset = 66L;
        final String KEY_T1 = "Test1";
        final OffsetAndMetadata VALUE_T1 = new OffsetAndMetadata(offset, null);
        final OffsetAndMetadata EXPECTED_VALUE_T1 = VALUE_T1;
        metadataMap.put(KEY_T1, VALUE_T1);

        final String KEY_T2 = "Test2";
        final OffsetAndMetadata VALUE_T2 = new OffsetAndMetadata(offset, "");
        final OffsetAndMetadata EXPECTED_VALUE_T2 = VALUE_T2;
        metadataMap.put(KEY_T2, VALUE_T2);

        final String KEY_T3 = "Test3";
        final OffsetAndMetadata VALUE_T3 = new OffsetAndMetadata(offset, "ShouldNotChange");
        final OffsetAndMetadata EXPECTED_VALUE_T3 = VALUE_T3;
        metadataMap.put(KEY_T3, VALUE_T3);

        final String KEY_T4 = "Test4";
        final OffsetAndMetadata VALUE_T4 = new OffsetAndMetadata(offset, "{ This is not JSON, just a trick start }");
        final OffsetAndMetadata EXPECTED_VALUE_T4 = VALUE_T4;
        metadataMap.put(KEY_T4, VALUE_T4);

        final String KEY_T5 = "Test5";
        final OffsetAndMetadata VALUE_T5 = new OffsetAndMetadata(offset, "{ \"copyFlags\":1 }");
        final OffsetAndMetadata EXPECTED_VALUE_T5 = new OffsetAndMetadata(offset, null);
        metadataMap.put(KEY_T5, VALUE_T5);

        final String KEY_T6 = "Test6";
        final OffsetAndMetadata VALUE_T6 = new OffsetAndMetadata(offset, "{ \"someOtherField\":2 }");
        final OffsetAndMetadata EXPECTED_VALUE_T6 = VALUE_T6;
        metadataMap.put(KEY_T6, VALUE_T6);

        final Map<String, OffsetAndMetadata> cleanedMap = OffsetAndMetadataUtil.INSTANCE.cleanMetadata(metadataMap);
        assertNotNull(cleanedMap);
        assertFalse(cleanedMap.isEmpty());
        // check keys
        assertEquals(EXPECTED_VALUE_T1, cleanedMap.get(KEY_T1));
        assertEquals(EXPECTED_VALUE_T2, cleanedMap.get(KEY_T2));
        assertEquals(EXPECTED_VALUE_T3, cleanedMap.get(KEY_T3));
        assertEquals(EXPECTED_VALUE_T4, cleanedMap.get(KEY_T4));
        assertEquals(EXPECTED_VALUE_T5, cleanedMap.get(KEY_T5));
        assertEquals(EXPECTED_VALUE_T6, cleanedMap.get(KEY_T6));
    }


}