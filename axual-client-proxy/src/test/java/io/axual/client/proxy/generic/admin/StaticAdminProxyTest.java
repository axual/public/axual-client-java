package io.axual.client.proxy.generic.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AbortTransactionOptions;
import org.apache.kafka.clients.admin.AbortTransactionSpec;
import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.ConfigEntry;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateDelegationTokenOptions;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeDelegationTokenOptions;
import org.apache.kafka.clients.admin.DescribeFeaturesOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeProducersOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTransactionsOptions;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ExpireDelegationTokenOptions;
import org.apache.kafka.clients.admin.FenceProducersOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTransactionsOptions;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RenewDelegationTokenOptions;
import org.apache.kafka.clients.admin.UnregisterBrokerOptions;
import org.apache.kafka.clients.admin.UpdateFeaturesOptions;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.TopicCollection;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.acl.AccessControlEntry;
import org.apache.kafka.common.acl.AccessControlEntryFilter;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.acl.AclOperation;
import org.apache.kafka.common.acl.AclPermissionType;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePattern;
import org.apache.kafka.common.resource.ResourcePatternFilter;
import org.apache.kafka.common.resource.ResourceType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.resolving.admin.ResolvingAdminConfig;
import io.axual.client.proxy.resolving.consumer.ResolvingConsumerConfig;
import io.axual.client.proxy.test.TestAdminProxy;
import io.axual.client.proxy.test.TestAdminProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.tools.FactoryUtil;
import io.axual.common.tools.KafkaUtil;

import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_MAP;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticAdminProxy.class, TestAdminProxyFactory.class, TestAdminProxy.class,
    FactoryUtil.class})
@PowerMockIgnore({"javax.net.ssl.*", "jdk.internal.reflect.*"})
public class StaticAdminProxyTest {

  private static final String BACKING_FACTORY_CONFIG = "backing.factory";
  private static final String TOPIC = "general-applicationlog";
  private static final String APPLICATION_ID = "io.axual.test";
  private static final String APPLICATION_VERSION = "1.0";
  private static final String GROUP = "general-group";
  private static final String TENANT_PROPERTY = "tnt";
  private static final String ENVIRONMENT_PROPERTY = "envt";
  private static final String TOPIC_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{topic}";
  private static final String GROUP_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{group}";
  private static final String ENVIRONMENT = "unit";
  private static final String TENANT = "axual";
  private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
  private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
  private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

  // Static parameters used in tests below
  private static final List<String> TOPICS = Collections.singletonList(TOPIC);
  private static final List<NewTopic> NEW_TOPICS = Collections
      .singletonList(new NewTopic(TOPIC, 12, (short) 1));
  private static final ResourcePatternFilter RESOURCE_FILTER = new ResourcePatternFilter(ResourceType.TOPIC,
      TOPIC, PatternType.ANY);
  private static final AccessControlEntryFilter ACCESS_CONTROL_ENTRY_FILTER = new AccessControlEntryFilter(
      null, null, AclOperation.ANY, AclPermissionType.ALLOW);
  private static final AclBindingFilter ACL_BINDING_FILTER = new AclBindingFilter(RESOURCE_FILTER,
      ACCESS_CONTROL_ENTRY_FILTER);
  private static final List<AclBindingFilter> ACL_BINDING_FILTERS = Collections
      .singletonList(ACL_BINDING_FILTER);
  private static final List<AclBinding> ACL_BINDINGS = Collections.singletonList(
      new AclBinding(new ResourcePattern(ResourceType.TOPIC, "name", PatternType.LITERAL),
          new AccessControlEntry("", "", AclOperation.READ, AclPermissionType.ALLOW)));
  private static final ConfigResource CONFIG_RESOURCE = new ConfigResource(
      ConfigResource.Type.TOPIC, TOPIC);
  private static final List<ConfigResource> CONFIG_RESOURCES = Collections
      .singletonList(CONFIG_RESOURCE);
  private static final ConfigEntry CONFIG_ENTRY = new ConfigEntry("name", "value");
  private static final Config CONFIG = new Config(Collections.singletonList(CONFIG_ENTRY));
  private static final Map<ConfigResource, Config> ALTERCONFIGS_CONFIGS = Collections
      .singletonMap(CONFIG_RESOURCE, CONFIG);
  private static final AlterConfigOp ALTER_CONFIG_OP = new AlterConfigOp(CONFIG_ENTRY,
      AlterConfigOp.OpType.APPEND);
  private static final Map<ConfigResource, Collection<AlterConfigOp>> INCREMENTAL_ALTERCONFIGS_CONFIGS = Collections
      .singletonMap(CONFIG_RESOURCE, Collections.singletonList(ALTER_CONFIG_OP));
  private static final Map<TopicPartitionReplica, String> REPLICA_ASSIGNMENT = new HashMap<>();
  private static final Collection<Integer> BROKERS = Collections.singletonList(0);
  private static final Collection<TopicPartitionReplica> REPLICAS = Collections
      .singletonList(new TopicPartitionReplica(TOPIC, 0, 0));
  private static final Map<String, NewPartitions> NEW_PARTITIONS = Collections
      .singletonMap(TOPIC, NewPartitions.increaseTo(20));
  private static final Map<TopicPartition, RecordsToDelete> RECORDS_TO_DELETE = Collections
      .singletonMap(new TopicPartition(TOPIC, 0), RecordsToDelete.beforeOffset(10));
  private static final String CONSUMER_GROUP = "mygroup";
  private static final Collection<String> CONSUMER_GROUPS = Collections
      .singletonList(CONSUMER_GROUP);
  private static final Collection<TopicPartition> PARTITIONS = Collections
      .singletonList(new TopicPartition(TOPIC, 0));

  private static final BaseClientProxyConfig<AdminProxy> config = new BaseClientProxyConfig<AdminProxy>(
      getValidConfig(), BACKING_FACTORY_CONFIG);

  @Mock
  private TestAdminProxy mockedAdmin;

  @Before
  public void setup() throws Exception {
    whenNew(TestAdminProxy.class).withAnyArguments().thenReturn(mockedAdmin);
  }

  private StaticAdminProxy<BaseClientProxyConfig<AdminProxy>> adminClient() {
    return spy(new StaticAdminProxy<>(config));
  }

  private static Map<String, Object> getValidConfig() {
    final Map<String, Object> configs = new HashMap<>();
    configs.put(BACKING_FACTORY_CONFIG, TestAdminProxyFactory.class.getName());
    configs.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
    configs.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

    configs.put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG,
        GroupPatternResolver.class.getName());
    configs.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
    configs.put(ResolvingAdminConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
    configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
    configs.put(TENANT_PROPERTY, TENANT);
    configs.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

    SslConfig sslConfig = SslConfig.newBuilder()
        .setKeyPassword(SSL_PASSWORD)
        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
        .setKeystorePassword(SSL_PASSWORD)
        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
        .setTruststorePassword(SSL_PASSWORD)
        .setEnableHostnameVerification(false)
        .build();
    KafkaUtil.getKafkaConfigs(sslConfig, configs);

    return configs;
  }

  private void validConstructorTest(final StaticAdminProxy adminClient) {
    // Verify initialisation of Apache KafkaProducer with mocked properties from the mocked provider
    verifyNew(TestAdminProxy.class, Mockito.times(1));
  }

  @Test
  public void verify_ConstructorWithValidConfig() {
    validConstructorTest(adminClient());
  }

  @Test
  public void verify_close() {
    adminClient().close();

    verify(mockedAdmin, times(1)).close(any(Duration.class));
  }

  @Test
  public void verify_closeWithDuration() {
    adminClient().close(Duration.ofSeconds(1));

    verify(mockedAdmin, times(1)).close(any(Duration.class));
  }

  @Test
  public void verify_createTopics() {
    adminClient().createTopics(NEW_TOPICS);

    Mockito.verify(mockedAdmin, Mockito.times(1))
        .createTopics(eq(NEW_TOPICS), any(CreateTopicsOptions.class));
  }

  @Test
  public void verify_createTopicsWithOptions() {
    CreateTopicsOptions options = new CreateTopicsOptions();

    adminClient().createTopics(NEW_TOPICS, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).createTopics(NEW_TOPICS, options);
  }

  @Test
  public void verify_deleteTopics() {
    ArgumentCaptor<TopicCollection> argument = ArgumentCaptor.forClass(TopicCollection.class);
    adminClient().deleteTopics(TOPICS);

    Mockito.verify(mockedAdmin, Mockito.times(1)).deleteTopics(argument.capture(), any(DeleteTopicsOptions.class));
    Collection<String> topicNames = ((TopicCollection.TopicNameCollection) argument.getValue()).topicNames();
    assertEquals(1, topicNames.size());
    assertTrue(topicNames.contains(TOPIC));
  }

  @Test
  public void verify_deleteTopicsWithOptions() {
    DeleteTopicsOptions options = new DeleteTopicsOptions();

    adminClient().deleteTopics(TOPICS, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).deleteTopics(TOPICS, options);
  }

  @Test
  public void verify_listTopics() {
    adminClient().listTopics();

    Mockito.verify(mockedAdmin, Mockito.times(1)).listTopics(any(ListTopicsOptions.class));
  }

  @Test
  public void verify_listTopicsWithOptions() {
    ListTopicsOptions options = new ListTopicsOptions();

    adminClient().listTopics(options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).listTopics(options);
  }

  @Test
  public void verify_describeTopics() {
    adminClient().describeTopics(TOPICS);

    Mockito.verify(mockedAdmin, Mockito.times(1))
        .describeTopics(eq(TOPICS), any(DescribeTopicsOptions.class));
  }

  @Test
  public void verify_describeTopicsWithOptions() {
    DescribeTopicsOptions options = new DescribeTopicsOptions();

    adminClient().describeTopics(TOPICS, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeTopics(TOPICS, options);
  }

  @Test
  public void verify_describeCluster() {
    adminClient().describeCluster();

    Mockito.verify(mockedAdmin, Mockito.times(1))
        .describeCluster(any(DescribeClusterOptions.class));
  }

  @Test
  public void verify_describeClusterWithOptions() {
    DescribeClusterOptions options = new DescribeClusterOptions();

    adminClient().describeCluster(options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeCluster(options);
  }

  @Test
  public void verify_describeAcls() {
    adminClient().describeAcls(ACL_BINDING_FILTER);

    verify(mockedAdmin, times(1))
        .describeAcls(eq(ACL_BINDING_FILTER), any(DescribeAclsOptions.class));
  }

  @Test
  public void verify_describeAclsWithOptions() {
    DescribeAclsOptions options = new DescribeAclsOptions();

    adminClient().describeAcls(ACL_BINDING_FILTER, options);

    verify(mockedAdmin, times(1)).describeAcls(ACL_BINDING_FILTER, options);
  }

  @Test
  public void verify_createAcls() {
    adminClient().createAcls(ACL_BINDINGS);

    verify(mockedAdmin, times(1)).createAcls(eq(ACL_BINDINGS), any(CreateAclsOptions.class));
  }

  @Test
  public void verify_createAclsWithOptions() {
    CreateAclsOptions options = new CreateAclsOptions();

    adminClient().createAcls(ACL_BINDINGS, options);

    verify(mockedAdmin, times(1)).createAcls(ACL_BINDINGS, options);
  }

  @Test
  public void verify_deleteAcls() {
    adminClient().deleteAcls(ACL_BINDING_FILTERS);

    verify(mockedAdmin, times(1)).deleteAcls(eq(ACL_BINDING_FILTERS), any(DeleteAclsOptions.class));
  }

  @Test
  public void verify_deleteAclsWithOptions() {
    DeleteAclsOptions options = new DeleteAclsOptions();

    adminClient().deleteAcls(ACL_BINDING_FILTERS, options);

    verify(mockedAdmin, times(1)).deleteAcls(ACL_BINDING_FILTERS, options);
  }

  @Test
  public void verify_describeConfigs() {
    adminClient().describeConfigs(CONFIG_RESOURCES);

    verify(mockedAdmin, times(1))
        .describeConfigs(eq(CONFIG_RESOURCES), any(DescribeConfigsOptions.class));
  }

  @Test
  public void verify_describeConfigsWithOptions() {
    DescribeConfigsOptions options = new DescribeConfigsOptions();

    adminClient().describeConfigs(CONFIG_RESOURCES, options);

    verify(mockedAdmin, times(1)).describeConfigs(CONFIG_RESOURCES, options);
  }

  @Test
  public void verify_alterConfigs() {
    adminClient().alterConfigs(ALTERCONFIGS_CONFIGS);

    verify(mockedAdmin, times(1))
        .alterConfigs(eq(ALTERCONFIGS_CONFIGS), any(AlterConfigsOptions.class));
  }

  @Test
  public void verify_alterConfigsWithOptions() {
    AlterConfigsOptions options = new AlterConfigsOptions();

    adminClient().alterConfigs(ALTERCONFIGS_CONFIGS, options);

    verify(mockedAdmin, times(1)).alterConfigs(ALTERCONFIGS_CONFIGS, options);
  }

  @Test
  public void verify_incrementalAlterConfigs() {
    adminClient().incrementalAlterConfigs(INCREMENTAL_ALTERCONFIGS_CONFIGS);

    verify(mockedAdmin, times(1)).incrementalAlterConfigs(eq(INCREMENTAL_ALTERCONFIGS_CONFIGS),
        any(AlterConfigsOptions.class));
  }

  @Test
  public void verify_incrementalAlterConfigsWithOptions() {
    AlterConfigsOptions options = new AlterConfigsOptions();

    adminClient().incrementalAlterConfigs(INCREMENTAL_ALTERCONFIGS_CONFIGS, options);

    verify(mockedAdmin, times(1))
        .incrementalAlterConfigs(INCREMENTAL_ALTERCONFIGS_CONFIGS, options);
  }

  @Test
  public void verify_alterReplicaLogDirs() {
    adminClient().alterReplicaLogDirs(REPLICA_ASSIGNMENT);

    verify(mockedAdmin, times(1))
        .alterReplicaLogDirs(eq(REPLICA_ASSIGNMENT), any(AlterReplicaLogDirsOptions.class));
  }

  @Test
  public void verify_alterReplicaLogDirsWithOptions() {
    AlterReplicaLogDirsOptions options = new AlterReplicaLogDirsOptions();

    adminClient().alterReplicaLogDirs(REPLICA_ASSIGNMENT, options);

    verify(mockedAdmin, times(1)).alterReplicaLogDirs(REPLICA_ASSIGNMENT, options);
  }

  @Test
  public void verify_describeLogDirs() {
    DescribeLogDirsOptions options = new DescribeLogDirsOptions();

    adminClient().describeLogDirs(BROKERS, options);

    verify(mockedAdmin, times(1)).describeLogDirs(BROKERS, options);
  }

  @Test
  public void verify_describeLogDirsWithOptions() {
    DescribeLogDirsOptions options = new DescribeLogDirsOptions();

    adminClient().describeLogDirs(BROKERS, options);

    verify(mockedAdmin, times(1)).describeLogDirs(BROKERS, options);
  }

  @Test
  public void verify_describeReplicaLogDirs() {
    adminClient().describeReplicaLogDirs(REPLICAS);

    verify(mockedAdmin, times(1))
        .describeReplicaLogDirs(eq(REPLICAS), any(DescribeReplicaLogDirsOptions.class));
  }

  @Test
  public void verify_describeReplicaLogDirsWithOptions() {
    DescribeReplicaLogDirsOptions options = new DescribeReplicaLogDirsOptions();

    adminClient().describeReplicaLogDirs(REPLICAS, options);

    verify(mockedAdmin, times(1)).describeReplicaLogDirs(REPLICAS, options);
  }

  @Test
  public void verify_createPartitions() {
    adminClient().createPartitions(NEW_PARTITIONS);

    verify(mockedAdmin, times(1))
        .createPartitions(eq(NEW_PARTITIONS), any(CreatePartitionsOptions.class));
  }

  @Test
  public void verify_createPartitionsWithOptions() {
    CreatePartitionsOptions options = new CreatePartitionsOptions();

    adminClient().createPartitions(NEW_PARTITIONS, options);

    verify(mockedAdmin, times(1)).createPartitions(NEW_PARTITIONS, options);
  }

  @Test
  public void verify_deleteRecords() {
    adminClient().deleteRecords(RECORDS_TO_DELETE);

    verify(mockedAdmin, times(1))
        .deleteRecords(eq(RECORDS_TO_DELETE), any(DeleteRecordsOptions.class));
  }

  @Test
  public void verify_deleteRecordsWithOptions() {
    DeleteRecordsOptions options = new DeleteRecordsOptions();

    adminClient().deleteRecords(RECORDS_TO_DELETE, options);

    verify(mockedAdmin, times(1)).deleteRecords(RECORDS_TO_DELETE, options);
  }

  @Test
  public void verify_createDelegationToken() {
    adminClient().createDelegationToken();

    verify(mockedAdmin, times(1)).createDelegationToken(any(CreateDelegationTokenOptions.class));
  }

  @Test
  public void verify_createDelegationTokenWithOptions() {
    CreateDelegationTokenOptions options = new CreateDelegationTokenOptions();

    adminClient().createDelegationToken(options);

    verify(mockedAdmin, times(1)).createDelegationToken(options);
  }

  @Test
  public void verify_renewDelegationToken() {
    adminClient().renewDelegationToken(null);

    verify(mockedAdmin, times(1))
        .renewDelegationToken(eq(null), any(RenewDelegationTokenOptions.class));
  }

  @Test
  public void verify_renewDelegationTokenWithOptions() {
    RenewDelegationTokenOptions options = new RenewDelegationTokenOptions();

    adminClient().renewDelegationToken(null, options);

    verify(mockedAdmin, times(1)).renewDelegationToken(null, options);
  }

  @Test
  public void verify_expireDelegationToken() {
    adminClient().expireDelegationToken(null);

    verify(mockedAdmin, times(1))
        .expireDelegationToken(eq(null), any(ExpireDelegationTokenOptions.class));
  }

  @Test
  public void verify_expireDelegationTokenWithOptions() {
    ExpireDelegationTokenOptions options = new ExpireDelegationTokenOptions();

    adminClient().expireDelegationToken(null, options);

    verify(mockedAdmin, times(1)).expireDelegationToken(null, options);
  }

  @Test
  public void verify_describeDelegationToken() {
    adminClient().describeDelegationToken();

    verify(mockedAdmin, times(1))
        .describeDelegationToken(any(DescribeDelegationTokenOptions.class));
  }

  @Test
  public void verify_describeDelegationTokenWithOptions() {
    DescribeDelegationTokenOptions options = new DescribeDelegationTokenOptions();

    adminClient().describeDelegationToken(options);

    verify(mockedAdmin, times(1)).describeDelegationToken(options);
  }

  @Test
  public void verify_describeConsumerGroups() {
    adminClient().describeConsumerGroups(CONSUMER_GROUPS);

    verify(mockedAdmin, times(1))
        .describeConsumerGroups(eq(CONSUMER_GROUPS), any(DescribeConsumerGroupsOptions.class));
  }

  @Test
  public void verify_describeConsumerGroupsWithOptions() {
    DescribeConsumerGroupsOptions options = new DescribeConsumerGroupsOptions();

    adminClient().describeConsumerGroups(CONSUMER_GROUPS, options);

    verify(mockedAdmin, times(1)).describeConsumerGroups(CONSUMER_GROUPS, options);
  }

  @Test
  public void verify_listConsumerGroups() {
    adminClient().listConsumerGroups();

    verify(mockedAdmin, times(1)).listConsumerGroups(any(ListConsumerGroupsOptions.class));
  }

  @Test
  public void verify_listConsumerGroupsWithOptions() {
    ListConsumerGroupsOptions options = new ListConsumerGroupsOptions();

    adminClient().listConsumerGroups(options);

    verify(mockedAdmin, times(1)).listConsumerGroups(options);
  }

  @Test
  public void verify_listConsumerGroupOffsets() {
    adminClient().listConsumerGroupOffsets(CONSUMER_GROUP);

    verify(mockedAdmin, times(1))
        .listConsumerGroupOffsets(eq(CONSUMER_GROUP), any(ListConsumerGroupOffsetsOptions.class));
  }

  @Test
  public void verify_listConsumerGroupOffsetsWithOptions() {
    ListConsumerGroupOffsetsOptions options = new ListConsumerGroupOffsetsOptions();

    adminClient().listConsumerGroupOffsets(CONSUMER_GROUP, options);

    verify(mockedAdmin, times(1)).listConsumerGroupOffsets(CONSUMER_GROUP, options);
  }

  @Test
  public void verify_deleteConsumerGroups() {
    adminClient().deleteConsumerGroups(CONSUMER_GROUPS);

    verify(mockedAdmin, times(1))
        .deleteConsumerGroups(eq(CONSUMER_GROUPS), any(DeleteConsumerGroupsOptions.class));
  }

  @Test
  public void verify_deleteConsumerGroupsWithOptions() {
    DeleteConsumerGroupsOptions options = new DeleteConsumerGroupsOptions();

    adminClient().deleteConsumerGroups(CONSUMER_GROUPS, options);

    verify(mockedAdmin, times(1)).deleteConsumerGroups(CONSUMER_GROUPS, options);
  }

  @Test
  public void verify_deleteConsumerGroupOffsets() {
    Set<TopicPartition> partitions = emptySet();

    adminClient().deleteConsumerGroupOffsets(GROUP, partitions);

    verify(mockedAdmin, times(1))
        .deleteConsumerGroupOffsets(eq(GROUP), eq(partitions), any(DeleteConsumerGroupOffsetsOptions.class));
  }

  @Test
  public void verify_deleteConsumerGroupOffsetsWithOptions() {
    Set<TopicPartition> partitions = emptySet();
    DeleteConsumerGroupOffsetsOptions options = null;

    adminClient().deleteConsumerGroupOffsets(GROUP, partitions, options);

    verify(mockedAdmin, times(1))
        .deleteConsumerGroupOffsets(GROUP, partitions, options);
  }

  @Test
  public void verify_electLeaders() {
    Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments = null;
    ElectionType electionType=null;
    Set<TopicPartition> partitions=emptySet();
    adminClient().electLeaders( electionType,partitions);

    verify(mockedAdmin, times(1)).electLeaders(eq(electionType),eq(partitions),any(ElectLeadersOptions.class));
  }

  @Test
  public void verify_electLeadersWithOptions() {
    Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments = null;
    ElectionType electionType=null;
    Set<TopicPartition> partitions=emptySet();
    ElectLeadersOptions options = null;
    adminClient().electLeaders( electionType,partitions, options);

    verify(mockedAdmin, times(1)).electLeaders(electionType, partitions, options);
  }

  @Test
  public void verify_alterPartitionReassignments() {
    Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments = null;
    adminClient().alterPartitionReassignments(reassignments);

    verify(mockedAdmin, times(1)).alterPartitionReassignments(eq(reassignments), any(
        AlterPartitionReassignmentsOptions.class));
  }

  @Test
  public void verify_alterPartitionReassignmentsWithOptions() {
    Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments = null;
    AlterPartitionReassignmentsOptions options = null;
    adminClient().alterPartitionReassignments(reassignments, options);

    verify(mockedAdmin, times(1)).alterPartitionReassignments(reassignments, options);
  }

  @Test
  public void verify_listPartitionReassignments() {
    adminClient().listPartitionReassignments();

    verify(mockedAdmin, times(1)).listPartitionReassignments(any(Optional.class),
        any(ListPartitionReassignmentsOptions.class));
  }

  @Test
  public void verify_listPartitionReassignmentsWithSetAndOptions() {
    ListPartitionReassignmentsOptions options = null;

    adminClient().listPartitionReassignments(options);

    verify(mockedAdmin, times(1)).listPartitionReassignments(any(Optional.class), eq(options));
  }

  @Test
  public void verify_listPartitionReassignmentsWithPartitions() {
    Set<TopicPartition> partitions = emptySet();
    adminClient().listPartitionReassignments(partitions);

    verify(mockedAdmin, times(1)).listPartitionReassignments(any(Optional.class),
        any(ListPartitionReassignmentsOptions.class));
  }

  @Test
  public void verify_listPartitionReassignmentsWithPartitionsAndOptions() {
    ListPartitionReassignmentsOptions options = null;
    Set<TopicPartition> partitions = emptySet();
    adminClient().listPartitionReassignments(partitions, options);

    verify(mockedAdmin, times(1)).listPartitionReassignments(any(Optional.class), eq(options));
  }

  @Test
  public void verify_listPartitionReassignmentsWithOptionalPartitions() {
    Optional<Set<TopicPartition>> partitions = null;
    ListPartitionReassignmentsOptions options = null;
    adminClient().listPartitionReassignments(partitions, options);

    verify(mockedAdmin, times(1)).listPartitionReassignments(partitions, options);
  }

  @Test
  public void verify_removeMembersFromConsumerGroup() {
    RemoveMembersFromConsumerGroupOptions options = null;
    adminClient().removeMembersFromConsumerGroup(GROUP,options);

    verify(mockedAdmin, times(1)).removeMembersFromConsumerGroup(GROUP,options);
  }

  @Test
  public void verify_listOffsets() {
    Map<TopicPartition, OffsetSpec> partitions = emptyMap();
    adminClient().listOffsets(partitions);

    verify(mockedAdmin, times(1)).listOffsets(eq(partitions), any(ListOffsetsOptions.class));
  }

  @Test
  public void verify_listOffsetsWithOptions() {
    Map<TopicPartition, OffsetSpec> partitions = emptyMap();
    ListOffsetsOptions options = new ListOffsetsOptions();
    adminClient().listOffsets(partitions,options);

    verify(mockedAdmin, times(1)).listOffsets(partitions,options);
  }

  @Test
  public void verify_describeClientQuotas() {
    ClientQuotaFilter filter = null;
    adminClient().describeClientQuotas(filter);

    verify(mockedAdmin, times(1))
        .describeClientQuotas(eq(filter), any(DescribeClientQuotasOptions.class));
  }

  @Test
  public void verify_describeClientQuotasWithOptions() {
    ClientQuotaFilter filter = null;
    DescribeClientQuotasOptions options = null;
    adminClient().describeClientQuotas(filter, options);

    verify(mockedAdmin, times(1)).describeClientQuotas(filter, options);
  }

  @Test
  public void verify_alterClientQuotas() {
    Set<ClientQuotaAlteration> alterationSet = emptySet();
    adminClient().alterClientQuotas(alterationSet);

    verify(mockedAdmin, times(1))
        .alterClientQuotas(eq(alterationSet), any(AlterClientQuotasOptions.class));
  }

  @Test
  public void verify_alterClientQuotasWithOptions() {
    Set<ClientQuotaAlteration> alterationSet = emptySet();
    AlterClientQuotasOptions options = new AlterClientQuotasOptions();
    adminClient().alterClientQuotas(alterationSet,options);

    verify(mockedAdmin, times(1))
        .alterClientQuotas(alterationSet, options);
  }

  @Test
  public void verify_describeUserScramCredentials() {
    DescribeUserScramCredentialsOptions options = new DescribeUserScramCredentialsOptions();
    adminClient().describeUserScramCredentials(EMPTY_LIST, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeUserScramCredentials(EMPTY_LIST, options);
  }

  @Test
  public void verify_alterUserScramCredentials() {
    AlterUserScramCredentialsOptions options = new AlterUserScramCredentialsOptions();
    adminClient().alterUserScramCredentials(EMPTY_LIST, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).alterUserScramCredentials(EMPTY_LIST, options);
  }

  @Test
  public void verify_describeFeatures() {
    DescribeFeaturesOptions options = new DescribeFeaturesOptions();
    adminClient().describeFeatures(options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeFeatures(options);
  }

  @Test
  public void verify_updateFeatures() {
    UpdateFeaturesOptions options = new UpdateFeaturesOptions();
    adminClient().updateFeatures(EMPTY_MAP, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).updateFeatures(EMPTY_MAP, options);
  }

  @Test
  public void verify_unregisterBroker() {
    UnregisterBrokerOptions options = new UnregisterBrokerOptions();
    adminClient().unregisterBroker(1, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).unregisterBroker(1, options);
  }

  @Test
  public void verify_metrics() {
    adminClient().metrics();

    Mockito.verify(mockedAdmin, Mockito.times(1)).metrics();
  }


  @Test
  public void verify_deleteTopicsWithTopicCollection() {
    TopicCollection topicCollection = TopicCollection.ofTopicNames(TOPICS);
    adminClient().deleteTopics(topicCollection);

    Mockito.verify(mockedAdmin, Mockito.times(1)).deleteTopics(eq(topicCollection), any(DeleteTopicsOptions.class));
  }

  @Test
  public void verify_deleteTopicsWithOptionsWithTopicCollection() {
    DeleteTopicsOptions options = new DeleteTopicsOptions();
    TopicCollection topicCollection = TopicCollection.ofTopicNames(TOPICS);

    adminClient().deleteTopics(topicCollection, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).deleteTopics(topicCollection, options);
  }


  @Test
  public void verify_describeTopicsWithTopicCollection() {
    TopicCollection topicCollection = TopicCollection.ofTopicNames(TOPICS);
    adminClient().describeTopics(topicCollection);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .describeTopics(eq(topicCollection), any(DescribeTopicsOptions.class));
  }

  @Test
  public void verify_describeTopicsWithOptionsWithTopicCollection() {
    TopicCollection topicCollection = TopicCollection.ofTopicNames(TOPICS);
    DescribeTopicsOptions options = new DescribeTopicsOptions();

    adminClient().describeTopics(topicCollection, options);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeTopics(topicCollection, options);
  }

  @Test
  public void verify_describeProducers() {
    List<TopicPartition> topicPartitions = new ArrayList<>();
    adminClient().describeProducers(topicPartitions);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .describeProducers(eq(topicPartitions), any(DescribeProducersOptions.class));
  }

  @Test
  public void verify_describeProducersWithOptions() {
    DescribeProducersOptions options = new DescribeProducersOptions();
    List<TopicPartition> topicPartitions = new ArrayList<>();
    adminClient().describeProducers(topicPartitions);

    Mockito.verify(mockedAdmin, Mockito.times(1)).describeProducers(topicPartitions, options);
  }

  @Test
  public void verify_describeTransactions() {
    List<String> transactionalIds = new ArrayList<>();
    adminClient().describeTransactions(transactionalIds);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .describeTransactions(eq(transactionalIds), any(DescribeTransactionsOptions.class));
  }

  @Test
  public void verify_describeTransactionsWithOptions() {
    DescribeTransactionsOptions options = new DescribeTransactionsOptions();
    List<String> transactionalIds = new ArrayList<>();
    adminClient().describeTransactions(transactionalIds, options);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .describeTransactions(transactionalIds, options);
  }

  @Test
  public void verify_abortTransaction() {
    TopicPartition topicPartition = new TopicPartition("", 0);
    AbortTransactionSpec abortTransactionSpec = new AbortTransactionSpec(topicPartition, 1L, (short)1, 1);

    adminClient().abortTransaction(abortTransactionSpec);
    Mockito.verify(mockedAdmin, Mockito.times(1))
            .abortTransaction(eq(abortTransactionSpec), any(AbortTransactionOptions.class));
  }

  @Test
  public void verify_abortTransactionWithOptions() {
    TopicPartition topicPartition = new TopicPartition("", 0);
    AbortTransactionOptions options = new AbortTransactionOptions();
    AbortTransactionSpec abortTransactionSpec = new AbortTransactionSpec(topicPartition, 1L, (short)1, 1);

    adminClient().abortTransaction(abortTransactionSpec, options);
    Mockito.verify(mockedAdmin, Mockito.times(1))
            .abortTransaction(abortTransactionSpec, options);
  }

  @Test
  public void verify_listTransactions() {
    adminClient().listTransactions();
    Mockito.verify(mockedAdmin, Mockito.times(1))
            .listTransactions(any(ListTransactionsOptions.class));
  }

  @Test
  public void verify_listTransactionsWithOptions() {
    ListTransactionsOptions listTransactionsOptions = new ListTransactionsOptions();
    adminClient().listTransactions(listTransactionsOptions);
    Mockito.verify(mockedAdmin, Mockito.times(1))
            .listTransactions(listTransactionsOptions);
  }

  @Test
  public void verify_fenceProducers() {
    List<String> transactionalIds = new ArrayList<>();
    adminClient().fenceProducers(transactionalIds);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .fenceProducers(eq(transactionalIds), any(FenceProducersOptions.class));
  }

  @Test
  public void verify_fenceProducersWithOptions() {
    List<String> transactionalIds = new ArrayList<>();
    FenceProducersOptions fenceProducersOptions = new FenceProducersOptions();
    adminClient().fenceProducers(transactionalIds, fenceProducersOptions);

    Mockito.verify(mockedAdmin, Mockito.times(1))
            .fenceProducers(transactionalIds, fenceProducersOptions);
  }
}
