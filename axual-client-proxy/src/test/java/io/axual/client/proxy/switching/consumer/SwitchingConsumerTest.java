package io.axual.client.proxy.switching.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.commons.codec.Charsets;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.config.DeliveryStrategy;
import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.generic.DistributorConfigs;
import io.axual.client.proxy.test.TestConsumerProxy;
import io.axual.client.proxy.test.TestConsumerProxyFactory;
import io.axual.client.proxy.wrapped.consumer.WrappedConsumer;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.exception.NotSupportedException;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.fetcher.DiscoveryLoader;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.apache.kafka.common.record.TimestampType.NO_TIMESTAMP_TYPE;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SwitchingConsumer.class, TestConsumerProxyFactory.class, TestConsumerProxy.class, DiscoverySubscriber.class, DiscoveryClientRegistry.class, DiscoveryLoader.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class SwitchingConsumerTest {
    private static final String UNRESOLVED_TOPIC = "general-applicationlog";
    private static final String APPLICATION_ID = "io.axual.test";
    private static final String APPLICATION_VERSION = "1.0";
    private static final String ENVIRONMENT = "unit";
    private static final String TENANT = "axual";
    private static final String GROUP_ID = "group";
    private static final String DISCOVERY_ENDPOINT = "https://discovery.location.url";
    private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");
    private static final String DISTRIBUTOR_TIMEOUT = "1000";
    private static final String DISTRIBUTOR_DISTANCE = "2";
    private static final String TTL = "300";

    private static final String RECORD_KEY_FORMAT = "Unit Test Key %d";
    private static final String RECORD_VALUE_FORMAT = "Unit Test Value %d";

    @Mock
    private TestConsumerProxy<String, String> mockedConsumer1, mockedConsumer2, mockedConsumer3;

    @Mock
    private DiscoveryLoader mockedDiscoveryLoader;

    private DiscoveryResult discoveryResults[] = new DiscoveryResult[2];

    private void createDiscoveryProperties(int clusterId) {
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "bootstrap" + clusterId);
        properties.setProperty(DiscoveryResult.CLUSTER_PROPERTY, "cluster" + clusterId);
        properties.setProperty(DistributorConfigs.DISTRIBUTOR_TIMEOUT_CONFIG, DISTRIBUTOR_TIMEOUT);
        properties.setProperty(DiscoveryResult.INSTANCE_PROPERTY, "instance" + clusterId);
        properties.setProperty(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "schemaregistry" + clusterId);
        properties.setProperty(DiscoveryResult.SYSTEM_PROPERTY, "System" + clusterId);
        properties.setProperty(DiscoveryResult.TTL_PROPERTY, TTL);
        properties.setProperty(DistributorConfigs.DISTRIBUTOR_DISTANCE_CONFIG, DISTRIBUTOR_DISTANCE);

        this.discoveryResults[clusterId] = new DiscoveryResult(properties);
    }

    private Properties getValidProperties() {
        Properties consumerProps = new Properties();
        consumerProps.putAll(getValidMap());
        return consumerProps;
    }

    private Map<String, Object> getValidMap() {
        Map<String, Object> consumerMap = new HashMap<>();

        consumerMap.put(SwitchingConsumerConfig.BACKING_FACTORY_CONFIG, TestConsumerProxyFactory.class.getName());
        consumerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        consumerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
        consumerMap.put(CommonConfig.ENDPOINT, DISCOVERY_ENDPOINT);
        consumerMap.put(CommonConfig.TENANT, TENANT);
        consumerMap.put(CommonConfig.ENVIRONMENT, ENVIRONMENT);

        consumerMap.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        consumerMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");
        consumerMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeyPassword(SSL_PASSWORD)
                .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                .setKeystorePassword(SSL_PASSWORD)
                .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                .setTruststorePassword(SSL_PASSWORD)
                .setEnableHostnameVerification(false)
                .build();
        KafkaUtil.getKafkaConfigs(sslConfig, consumerMap);

        return consumerMap;
    }

    // This method is called by tests to validate different constructors
    private void validConstructorTest(final SwitchingConsumer consumer) throws Exception {
        final SwitchingConsumerConfig config = (SwitchingConsumerConfig) Whitebox.getInternalState(consumer, "config");

        Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
        Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

        // Verify initialisation of Apache KafkaProducer with mocked properties from the mocked provider
        verifyNew(TestConsumerProxy.class, times(1));
    }

    private void setSwitch(DiscoveryResult discoveryResult, boolean needsPropertiesChanged) {
        when(mockedDiscoveryLoader.getDiscoveryResult()).thenReturn(discoveryResult);
        if (needsPropertiesChanged) {
            when(mockedDiscoveryLoader.discoveryChanged()).thenReturn(Boolean.TRUE).thenReturn(Boolean.FALSE);
        } else {
            when(mockedDiscoveryLoader.discoveryChanged()).thenReturn(Boolean.FALSE);
        }
    }

    @Before
    public void setUp() throws Exception {
        whenNew(TestConsumerProxy.class).withAnyArguments().thenReturn(mockedConsumer1, mockedConsumer2, mockedConsumer3);

        createDiscoveryProperties(0);
        createDiscoveryProperties(1);
        setSwitch(discoveryResults[0], false);

        spy(DiscoveryClientRegistry.class);
        PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments().thenReturn(mockedDiscoveryLoader);
    }

    @After
    public void tearDown() throws Exception {
        DiscoveryClientRegistry.cleanUp();
    }

    @Test
    public void construction_ValidProperties() throws Exception {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        validConstructorTest(consumer);
    }

    @Test
    public void construction_ValidMap() throws Exception {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidMap()));
        validConstructorTest(consumer);
    }

    @Test
    public void verify_assignment() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.assignment();
        verify(mockedConsumer1, times(1)).assignment();
    }

    @Test
    public void verify_subscribe() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.subscription();
        verify(mockedConsumer1, times(0)).subscription();
    }

    @Test
    public void verify_pattern_subscribe() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.subscribe((Pattern) null);
        verify(consumer, times(1)).subscribe((Pattern) null, null);
    }

    @Test
    public void verify_pattern_subscribe_fail() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        final Pattern pattern = mock(Pattern.class);
        final ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {

            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {

            }
        };

        consumer.subscribe(pattern, listener);

        ArgumentCaptor<Pattern> patternCaptor = ArgumentCaptor.forClass(Pattern.class);
        ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor.forClass(ConsumerRebalanceListener.class);
        verify(mockedConsumer1, times(1)).subscribe(patternCaptor.capture(), listenerCaptor.capture());
        Assert.assertEquals(pattern, patternCaptor.getValue());
    }

    @Test(expected = NotSupportedException.class)
    public void verify_pattern_subscribe_not_supported() {
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.assign(Collections.singletonList(new TopicPartition("sometopic", 0)));
        final Pattern pattern = mock(Pattern.class);

        consumer.subscribe((Pattern) null, null);
    }

    @Test
    public void verify_noTopics_subscribe_is_same_as_unsubscribe() {
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.subscribe(Collections.<String>emptyList());
        Assert.assertEquals(0, consumer.subscription().size());
    }

    @Test
    public void verify_multipleTopics_subscribe_should_work() {
        //TODO: Should this test be implemented?
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.subscribe(Arrays.asList("topic1", "topic2"));
        assertEquals(2, consumer.subscription().size());
    }

    @Test
    public void verify_subscribe_listener_null() {
        List<String> topics = Collections.singletonList(UNRESOLVED_TOPIC);

        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.subscribe(topics);
        verify(mockedConsumer1, times(0)).subscribe(topics);
    }

    @Test
    public void subscribeTopics() {
        SwitchingConsumer<String, String> consumer = Mockito.spy(new SwitchingConsumer<>(getValidProperties()));
        doNothing().when(consumer).subscribe(any(Collection.class), any(ConsumerRebalanceListener.class));

        final List<String> topics = Arrays.asList("1", "2", "3");
        consumer.subscribe(topics);

        ArgumentCaptor<Collection> topicsCaptor = ArgumentCaptor.forClass(Collection.class);
        ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor.forClass(ConsumerRebalanceListener.class);
        verify(consumer, times(1)).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
        assertNull(listenerCaptor.getValue());
        assertEquals(3, (topicsCaptor.getValue()).size());
    }

    @Test
    public void subscribeTopicsAndListener() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        final List<String> topics = new ArrayList<>(0);
        topics.add(UNRESOLVED_TOPIC);
        final ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
            }
        };

        consumer.subscribe(topics, listener);

        ArgumentCaptor<List> topicsCaptor = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<ConsumerRebalanceListener> listenerCaptor = ArgumentCaptor.forClass(ConsumerRebalanceListener.class);
        verify(mockedConsumer1, times(1)).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
        verify(mockedConsumer2, never()).subscribe(topicsCaptor.capture(), listenerCaptor.capture());
        assertEquals(Collections.singletonList(UNRESOLVED_TOPIC), topicsCaptor.getValue());
    }

    @Test
    public void unsubscribe() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        consumer.unsubscribe();

        verify(mockedConsumer1, times(1)).unsubscribe();
    }

    @Test
    public void assign() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        final List<TopicPartition> topicPartitions = new ArrayList<>(0);
        consumer.assign(topicPartitions);
        verify(mockedConsumer1, times(1)).assign(topicPartitions);
    }

    @Test(expected = NotSupportedException.class)
    public void verify_assign_not_supported() {
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.subscribe(Collections.singletonList("topic"));

        consumer.assign(Collections.singletonList(new TopicPartition("sometopic", 0)));
    }

    private ConsumerRecord<byte[], byte[]> createRawConsumerRecord(final int itemNr) {
        final String keyString = String.format(RECORD_KEY_FORMAT, itemNr);
        final String valueString = String.format(RECORD_VALUE_FORMAT, itemNr);

        final int partition = itemNr;
        final long offset = itemNr;
        final long timestamp = itemNr;
        final TimestampType timestampType = NO_TIMESTAMP_TYPE;
        final long checksum = itemNr;
        final int serializedKeySize = itemNr;
        final int serializedValueSize = itemNr;

        byte[] key = keyString.getBytes(Charsets.UTF_8);
        byte[] value = valueString.getBytes(Charsets.UTF_8);

        return new ConsumerRecord<>(UNRESOLVED_TOPIC, partition, offset, timestamp, timestampType, checksum, serializedKeySize, serializedValueSize, key, value);
    }

    private ConsumerRecords<byte[], byte[]> createRawConsumerRecords(final int start, final int end) {
        Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> contents = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(UNRESOLVED_TOPIC, 0);

        final List<ConsumerRecord<byte[], byte[]>> records = new ArrayList<>();
        for (int counter = start; counter < end; counter++) {
            records.add(createRawConsumerRecord(counter));
        }
        contents.put(topicPartition, records);
        return new ConsumerRecords<>(contents);
    }

    private void validateConsumerRecord(final int itemNr, ConsumerRecord<String, String> consumerRecord) {
        Assert.assertNotNull(consumerRecord);

        Assert.assertEquals(UNRESOLVED_TOPIC, consumerRecord.topic());
        Assert.assertEquals(itemNr, consumerRecord.partition());
        Assert.assertEquals(itemNr, consumerRecord.offset());
        Assert.assertEquals(itemNr, consumerRecord.timestamp());
        Assert.assertEquals(NO_TIMESTAMP_TYPE, consumerRecord.timestampType());
        Assert.assertEquals(itemNr, consumerRecord.serializedKeySize());
        Assert.assertEquals(itemNr, consumerRecord.serializedValueSize());
    }

    private void validateConsumerRecords(final int startOffset, final ConsumerRecords<String, String> consumerRecords) {
        Assert.assertNotNull(consumerRecords);
        Assert.assertFalse(consumerRecords.isEmpty());
        Set<TopicPartition> topicPartitions = consumerRecords.partitions();
        Assert.assertNotNull(topicPartitions);
        Assert.assertEquals(1, topicPartitions.size());
        for (TopicPartition topicPartition : topicPartitions) {
            int itemNr = startOffset;
            for (ConsumerRecord<String, String> consumerRecord : consumerRecords.records(topicPartition)) {
                validateConsumerRecord(itemNr, consumerRecord);
                itemNr++;
            }
        }
    }

    /**
     * This test calls poll() on consumer and expects 10 messages to be read.
     */
    @Test
    public void verify_poll() throws Exception {
        final Long pollTimeout = 5000L;
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        ConsumerRecords mockedSet1 = createRawConsumerRecords(0, 10);
        // set behavior
        when(mockedConsumer1.poll(pollTimeout)).thenReturn(mockedSet1);

        // Always call subscribe() before poll() as per Kafka flow
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        ConsumerRecords<String, String> messages = consumer.poll(pollTimeout);

        // first consumer is created with knowledge of topic and poll should be called on this one
        verify(mockedConsumer1, times(1)).poll(pollTimeout);
        // second consumer should never be called
        verify(mockedConsumer2, never()).poll(pollTimeout);

        validateConsumerRecords(0, messages);
    }

    @Test
    public void pollDuration() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.poll(null);
        verify(mockedConsumer1, times(1)).poll(null);
    }

    /**
     * This test calls poll() on consumer, sets the switch flag to true and triggers the switch by
     * calling poll() again. 10 messages are read on first poll() and another 10 on second poll().
     */
    @Test
    public void verify_poll_with_switch() throws Exception {
        final Long pollTimeout = 5000L;
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));

        // mock data to return
        ConsumerRecords mockedSet1 = createRawConsumerRecords(0, 10);
        ConsumerRecords mockedSet2 = createRawConsumerRecords(10, 20);

        // set behavior
        when(mockedConsumer1.poll(pollTimeout)).thenReturn(mockedSet1);
        when(mockedConsumer2.poll(pollTimeout)).thenReturn(mockedSet2);

        // Always call subscribe() before poll() as per Kafka flow
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        ConsumerRecords<String, String> messages1 = consumer.poll(pollTimeout);

        setSwitch(discoveryResults[1], true);

        ConsumerRecords<String, String> messages2 = consumer.poll(pollTimeout); // triggers switch

        verify(mockedConsumer1, times(1)).poll(pollTimeout);
        verify(mockedConsumer2, times(1)).poll(pollTimeout);
        verify(mockedConsumer3, never()).poll(pollTimeout);

        validateConsumerRecords(0, messages1);
        validateConsumerRecords(10, messages2);
    }

    @Test
    public void commitSync() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitSync();
        verify(mockedConsumer1, times(1)).commitSync();
    }

    @Test
    public void commitSyncWithDuration() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitSync(Duration.ZERO);
        verify(mockedConsumer1, times(1)).commitSync((Duration.ZERO));
    }

    @Test
    public void commitSyncWithMap() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitSync((Map<TopicPartition, OffsetAndMetadata>) null);
        verify(mockedConsumer1, times(1)).commitSync((Map<TopicPartition, OffsetAndMetadata>) null);
    }

    @Test
    public void commitSyncWithMapAndDuration() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitSync(null, Duration.ZERO);
        verify(mockedConsumer1, times(1)).commitSync(null, Duration.ZERO);
    }

    @Test
    public void commitAsync() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitAsync();
        verify(mockedConsumer1, times(1)).commitAsync();
    }

    @Test
    public void commitAsyncWithCallback() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitAsync(null);
        verify(mockedConsumer1, times(1)).commitAsync(any(OffsetCommitCallback.class));
    }

    @Test
    public void commitAsyncWithMapAndCallback() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.commitAsync(null, null);
        verify(mockedConsumer1, times(1)).commitAsync(any(Map.class), any(OffsetCommitCallback.class));
    }

    @Test
    public void seek() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.seek(null, -1L);
        verify(mockedConsumer1, times(1)).seek(any(TopicPartition.class), any(Long.class));
    }

    @Test
    public void seekOffsetAndMetadata() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.seek(null, null);
        verify(mockedConsumer1, times(1)).seek(any(TopicPartition.class), any(OffsetAndMetadata.class));
    }

    @Test
    public void seekToBeginning() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.seekToBeginning(null);
        verify(mockedConsumer1, times(1)).seekToBeginning(null);
    }

    @Test
    public void seekToEnd() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.seekToEnd(null);
        verify(mockedConsumer1, times(1)).seekToEnd(null);
    }

    @Test
    public void position() {
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.position(null);
        verify(mockedConsumer1, times(1)).position(null);
    }

    @Test
    public void positionTimeout() {
        SwitchingConsumer<String, String> consumer = new SwitchingConsumer<>(getValidProperties());
        consumer.position(null, null);
        verify(mockedConsumer1, times(1)).position(null, null);
    }

    @Test
    public void committed_Partition() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        TopicPartition partition = null;
        consumer.committed(partition);
        verify(mockedConsumer1, times(1)).committed(partition);
    }

    @Test
    public void committed_PartitionDuration() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        TopicPartition partition = null;
        Duration duration = null;
        consumer.committed(partition, duration);
        verify(mockedConsumer1, times(1)).committed(partition, duration);
    }

    @Test
    public void committed_PartitionSet() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        Set<TopicPartition> partition = null;
        consumer.committed(partition);
        verify(mockedConsumer1, times(1)).committed(partition);
    }

    @Test
    public void committed_PartitionSetDuration() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        Set<TopicPartition> partition = null;
        Duration duration = null;
        consumer.committed(partition, duration);
        verify(mockedConsumer1, times(1)).committed(partition, duration);
    }

    @Test
    public void metrics() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.metrics();

        verify(mockedConsumer1, Mockito.atLeast(1)).metrics();
    }

    @Test
    public void partitionsFor() {
        final Node mockedNode = mock(Node.class);
        final Node[] replicas = {mockedNode, mockedNode, mockedNode, mockedNode};
        final Node[] inSyncReplicas = {mockedNode, mockedNode};
        final List<PartitionInfo> partitionInfoList = new ArrayList<>(10);
        for (int partition = 1; partition <= 10; partition++) {
            partitionInfoList.add(new PartitionInfo(UNRESOLVED_TOPIC, partition, mockedNode, replicas, inSyncReplicas));
        }

        when(mockedConsumer1.partitionsFor(UNRESOLVED_TOPIC)).thenReturn(partitionInfoList);

        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        List<PartitionInfo> retrievedPartitionInfo = consumer.partitionsFor(UNRESOLVED_TOPIC);

        verify(mockedConsumer1, times(1)).partitionsFor(UNRESOLVED_TOPIC);
        verify(mockedConsumer2, never()).partitionsFor(Matchers.anyString());

        Assert.assertNotNull(retrievedPartitionInfo);
        Assert.assertEquals(partitionInfoList.size(), retrievedPartitionInfo.size());
        for (int counter = 0; counter < retrievedPartitionInfo.size(); counter++) {
            final PartitionInfo retrieved = retrievedPartitionInfo.get(counter);
            final PartitionInfo original = partitionInfoList.get(counter);
            Assert.assertEquals(UNRESOLVED_TOPIC, retrieved.topic());
            Assert.assertEquals(original.partition(), retrieved.partition());
        }
    }

    @Test
    public void partitionsForTimeout() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.partitionsFor(null, null);
        verify(mockedConsumer1, times(1)).partitionsFor(null, null);
    }

    @Test
    public void listTopics() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.listTopics();
        verify(mockedConsumer1, times(1)).listTopics();
    }

    @Test
    public void listTopicsTimeout() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.listTopics(null);
        verify(mockedConsumer1, times(1)).listTopics(null);
    }

    @Test
    public void pause() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.pause(Collections.<TopicPartition>emptyList());
        verify(mockedConsumer1, times(1)).pause(Collections.<TopicPartition>emptyList());
    }

    @Test
    public void resume() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.resume(Collections.<TopicPartition>emptyList());
        verify(mockedConsumer1, times(1)).resume(Collections.<TopicPartition>emptyList());
    }

    @Test
    public void paused() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.paused();
        verify(mockedConsumer1, times(0)).paused();
    }

    @Test
    public void offsetsForTimes() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.offsetsForTimes(null);
        verify(mockedConsumer1, times(1)).offsetsForTimes(null);
    }

    @Test
    public void offsetsForTimesTimeout() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.offsetsForTimes(null, null);
        verify(mockedConsumer1, times(1)).offsetsForTimes(null, null);
    }

    @Test
    public void beginningOffsets() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.beginningOffsets(null);
        verify(mockedConsumer1, times(1)).beginningOffsets(null);
    }

    @Test
    public void beginningOffsetsTimeout() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.beginningOffsets(null, null);
        verify(mockedConsumer1, times(1)).beginningOffsets(null, null);
    }

    @Test
    public void endOffsets() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.endOffsets(null);
        verify(mockedConsumer1, times(1)).endOffsets(null);
    }

    @Test
    public void endOffsetsTimeout() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.endOffsets(null, null);
        verify(mockedConsumer1, times(1)).endOffsets(null, null);
    }

    @Test
    public void groupMetadata() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.groupMetadata();
        verify(mockedConsumer1, times(1)).groupMetadata();
    }

    @Test
    public void enforceRebalance() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.enforceRebalance();
        verify(mockedConsumer1, times(1)).enforceRebalance();
    }

    @Test
    public void wakeup() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.wakeup();
        verify(mockedConsumer1, times(1)).wakeup();
    }

    @Test
    public void verify_consumer_atLeastOnce_switch() {
        final long pollTimeout = 5000L;

        // set strategy to AT LEAST ONCE and create new consumer
        Properties consumerProperties = getValidProperties();
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(consumerProperties));

        // setup mock objects and behavior
        Map<TopicPartition, List<ConsumerRecord<String, String>>> records = new HashMap<>();
        when(mockedConsumer1.poll(pollTimeout)).thenReturn(new ConsumerRecords<>(records));
        when(mockedConsumer2.poll(pollTimeout)).thenReturn(new ConsumerRecords<>(records));

        // execute test
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        consumer.poll(pollTimeout);
        final Long timestampBeforeSwitch = System.currentTimeMillis();

        setSwitch(discoveryResults[1], true);

        consumer.poll(pollTimeout); // trigger switch
        final Long timestampAfterSwitch = System.currentTimeMillis();
        final Long switchTime = timestampAfterSwitch - timestampBeforeSwitch;

        Assert.assertTrue(switchTime >= Long.valueOf(TTL));
        Assert.assertTrue(switchTime <= Long.valueOf(TTL) + Long.valueOf(DISTRIBUTOR_TIMEOUT));

        // verify behavior
        verify(mockedConsumer1, times(1)).poll(pollTimeout);
        verify(mockedConsumer2, times(1)).poll(pollTimeout);
        verify(mockedConsumer3, never()).poll(pollTimeout);
    }

    @Test
    public void verify_consumer_atMostOnce_switch() {
        final long pollTimeout = 5000L;

        // set strategy to AT MOST ONCE and create new consumer
        Properties consumerProperties = getValidProperties();
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        consumerProperties.put(SwitchingConsumerConfig.DELIVERY_STRATEGY_ON_SWITCH, DeliveryStrategy.AT_MOST_ONCE.toString());
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(consumerProperties));

        // setup mock objects and expectations
        Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> records = new HashMap<>();
        when(mockedConsumer1.poll(pollTimeout)).thenReturn(new ConsumerRecords(records));
        when(mockedConsumer2.poll(pollTimeout)).thenReturn(new ConsumerRecords(records));

        // execute test
        consumer.subscribe(Collections.singletonList(UNRESOLVED_TOPIC));
        consumer.poll(pollTimeout);

        setSwitch(discoveryResults[1], true);
        consumer.poll(pollTimeout); // trigger switch

        final Long timestampAfterSwitch = System.currentTimeMillis();
        final Long switchTime = timestampAfterSwitch - discoveryResults[1].getTimestamp();

        long minWaitTime = Math.max(
                Long.valueOf(TTL),
                Long.valueOf(DISTRIBUTOR_DISTANCE) * Long.valueOf(DISTRIBUTOR_TIMEOUT)
        );

        Assert.assertTrue(switchTime >= minWaitTime);
        Assert.assertTrue(switchTime <= minWaitTime + Long.valueOf(DISTRIBUTOR_TIMEOUT));

        // verify behavior
        verify(mockedConsumer1, times(1)).poll(pollTimeout);
        verify(mockedConsumer2, times(1)).poll(pollTimeout);
        verify(mockedConsumer3, never()).poll(pollTimeout);

        ArgumentCaptor<WrappedConsumer> consumerCaptor = ArgumentCaptor.forClass(WrappedConsumer.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);

    }

    @Test
    public void currentLag() {
        TopicPartition topicPartition = new TopicPartition("sometopic", 0);
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.currentLag(topicPartition);
        verify(mockedConsumer1, times(1)).currentLag(topicPartition);
    }

    @Test
    public void enforceRebalanceWithReason() {
        SwitchingConsumer<String, String> consumer = spy(new SwitchingConsumer<>(getValidProperties()));
        consumer.enforceRebalance("reason");
        verify(mockedConsumer1, times(1)).enforceRebalance("reason");
    }
}
