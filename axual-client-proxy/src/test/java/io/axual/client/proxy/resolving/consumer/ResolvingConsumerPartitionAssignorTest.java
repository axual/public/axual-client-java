package io.axual.client.proxy.resolving.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerPartitionAssignor;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.TopicPartition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.axual.common.resolver.TopicResolver;

import static io.axual.client.proxy.resolving.consumer.ResolvingConsumerPartitionAssignorConfig.BACKING_ASSIGNOR_CONFIG;
import static io.axual.client.proxy.resolving.consumer.ResolvingConsumerPartitionAssignorConfig.TOPIC_RESOLVER_CONFIG;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResolvingConsumerPartitionAssignorTest {

    @Mock
    private ConsumerPartitionAssignor mockAssignor;

    @Mock
    private TopicResolver mockResolver;

    private Cluster dummyCluster;

    private ResolvingConsumerPartitionAssignor resolvingAssignor;

    @Before
    public void createFixtures() {
        Map<String, Object> configMap = new HashMap<>();
        configMap.put(BACKING_ASSIGNOR_CONFIG, mockAssignor);
        configMap.put(TOPIC_RESOLVER_CONFIG, mockResolver);
        resolvingAssignor = new ResolvingConsumerPartitionAssignor();
        resolvingAssignor.configure(configMap);

        dummyCluster = new Cluster("test", new ArrayList<>(), new ArrayList<>(), new HashSet<>(), new HashSet<>());
    }

    @Test
    public void ownedPartitionsAreKept() {
        ArgumentCaptor<ConsumerPartitionAssignor.GroupSubscription> captor =
                ArgumentCaptor.forClass(ConsumerPartitionAssignor.GroupSubscription.class);

        // given that we keep any partition assignments when we unresolve
        when(mockResolver.unresolveTopicPartitions(any())).thenAnswer(this::returnInput);
        // we capture the group assignments that we pass to the internal assigner
        when(mockAssignor.assign(any(), captor.capture())).thenReturn(new ConsumerPartitionAssignor.GroupAssignment(new HashMap<>()));

        // when we call the resolving assignor with a non empty list of topic partition assignments
        resolvingAssignor.assign(dummyCluster, mySubscription());

        // then the partition assignments were passed on to the encapsulated assigner
        final ConsumerPartitionAssignor.GroupSubscription capturedSub = captor.getValue();
        capturedSub.groupSubscription().values().forEach(subscription -> assertTrue(subscription.ownedPartitions().size() > 0));
    }

    /**
     * Creates a GroupSubscription for "foo" with just one subscription with a non-empty TopicPartition list.
     */
    private ConsumerPartitionAssignor.GroupSubscription mySubscription() {
        Map<String, ConsumerPartitionAssignor.Subscription> subscriptionMap = new HashMap<>();

        final ConsumerPartitionAssignor.Subscription subscription =
                new ConsumerPartitionAssignor.Subscription(Collections.singletonList("topic"), ByteBuffer.allocate(1),
                        Arrays.asList(new TopicPartition("topic", 1), new TopicPartition("topic", 2)));
        subscriptionMap.put("foo", subscription);

        return new ConsumerPartitionAssignor.GroupSubscription(subscriptionMap);
    }

    /**
     * Create a result for the unresolve() call which is simply equal to the input.
     */
    private Set<TopicPartition> returnInput(InvocationOnMock invocation) {
        List<TopicPartition> inputPartitions = (List<TopicPartition>) invocation.getArguments()[0];
        return new HashSet<>(inputPartitions);
    }

}
