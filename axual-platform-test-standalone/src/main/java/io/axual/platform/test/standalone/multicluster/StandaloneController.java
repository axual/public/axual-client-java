package io.axual.platform.test.standalone.multicluster;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.Positive;

import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.standalone.config.StandaloneApplicationConfig;
import io.axual.platform.test.standalone.config.StandaloneStreamConfig;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j()
@RestController()
@RequestMapping(path = "/standalone", produces = MediaType.APPLICATION_JSON_VALUE)
public class StandaloneController {
    private final StandaloneService service;

    @Autowired
    public StandaloneController(StandaloneService service) {
        log.entry();
        this.service = service;
        log.exit();
    }

    @GetMapping(path = "/instance")
    public InstanceUnitConfig getInstance() {
        log.entry();
        InstanceUnitConfig instanceUnitConfig = this.service.getInstanceConfig();
        return log.exit(instanceUnitConfig);
    }

    @GetMapping(path = "/ttl")
    public long getTtl() {
        log.entry();
        long result = service.getTTL();
        return log.exit(result);
    }

    @GetMapping(path = "/ttl/set")
    public long setTtl(@RequestParam(name = "value") long value) {
        log.entry(value);
        long result = service.setTTL(value);
        return log.exit(result);
    }

    @GetMapping(path = "/distributor/distance")
    public long getDistributorDistance() {
        log.entry();
        long result = service.getDistributorDistance();
        return log.exit(result);
    }

    @GetMapping(path = "/distributor/distance/set")
    public long setDistributorDistance(@RequestParam(name = "value") long value) {
        log.entry(value);
        long result = service.setDistributorDistance(value);
        return log.exit(result);
    }

    @GetMapping(path = "/distributor/timeout")
    public long getDistributorTimeout() {
        log.entry();
        long result = service.getDistributorTimeout();
        return log.exit(result);
    }

    @GetMapping(path = "/distributor/timeout/set")
    public long setDistributorTimeout(@RequestParam(name = "value") long value) {
        log.entry(value);
        long result = service.setDistributorTimeout(value);
        return log.exit(result);
    }

    @GetMapping(path = "/cluster")
    public List<String> getAllClusterConfigs() {
        log.entry();
        List<String> clusters = service.getAllClusterConfigs().stream().map(cnf -> cnf.getName()).collect(Collectors.toList());
        return log.exit(clusters);
    }

    @GetMapping(path = "/cluster/{clusterName}")
    public ClusterUnitConfig getClusterConfig(@PathVariable(name = "clusterName") String clusterName) {
        log.entry(clusterName);
        ClusterUnitConfig result = this.service.getClusterConfig(clusterName);
        return log.exit(result);
    }

    @GetMapping(path = "/stream")
    public Map<String, StandaloneStreamConfig> getStreams() {
        log.entry();
        Map<String, StandaloneStreamConfig> result = service.getStreams();
        result.putAll(service.getRawTopics());
        return log.exit(result);
    }

    @GetMapping(path = "/stream/create/{name}")
    public StandaloneStreamConfig createStream(
            @PathVariable(name = "name") String name,
            @RequestParam(name = "isRaw") boolean isRaw,
            @RequestParam(name = "environment", required = false) String environment,
            @RequestParam(name = "partitions", required = false, defaultValue = "3") @Positive int partitions,
            @RequestParam(name = "keyClass", required = false) String keyClass,
            @RequestParam(name = "valueClass", required = false) String valueClass
    ) {
        log.entry(name, isRaw, environment, partitions, keyClass, valueClass);
        StandaloneStreamConfig streamConfig = service.createTopic(isRaw, name, environment, partitions, keyClass, valueClass);
        return log.exit(streamConfig);
    }


    @GetMapping(path = "/application")
    public Set<StandaloneApplicationConfig> getApplication(@RequestParam(name = "applicationId", required = false) String applicationId) {
        log.entry(applicationId);
        Set<StandaloneApplicationConfig> configs = this.service.getApplications();
        if (applicationId != null) {
            return configs.stream().filter(cc -> applicationId.equals(cc.getApplicationId())).collect(Collectors.toSet());
        }
        return log.exit(configs);
    }

    @GetMapping(path = "/application/create/{applicationId}")
    public StandaloneApplicationConfig createApplicationId(
            @PathVariable(name = "applicationId") String applicationId,
            @RequestParam(name = "environment", required = false) String environment,
            @RequestParam(name = "clusterName", required = false) String clusterName
    ) {
        log.entry(applicationId, environment, clusterName);
        StandaloneApplicationConfig applicationConfig = this.service.addApplication(applicationId, environment, clusterName);
        return log.exit(applicationConfig);
    }

    @GetMapping(path = "/assignment")
    public Map<String, List<StandaloneApplicationConfig>> getAssignments(
    ) {
        log.entry();
        Map<String, List<StandaloneApplicationConfig>> assignments = service.getApplicationsPerCluster();
        return log.exit(service.getApplicationsPerCluster());
    }

    @GetMapping(path = "/assignment/{clusterName}")
    public List<StandaloneApplicationConfig> getAssignmentsForCluster(
            @PathVariable(name = "clusterName", required = true) String clusterName
    ) {
        log.entry(clusterName);
        // Get all applications for this cluster
        List<StandaloneApplicationConfig> assignments = service.getApplicationsForCluster(clusterName);
        return log.exit(assignments);
    }

    @GetMapping(path = "/assignment/{clusterName}/set/{applicationId}")
    public StandaloneApplicationConfig setAssignmentToCluster(
            @PathVariable(name = "clusterName", required = true) String clusterName,
            @PathVariable(name = "applicationId", required = true) String applicationId,
            @RequestParam(name = "environment", required = false) String environment
    ) {
        log.entry(clusterName, applicationId, environment);
        StandaloneApplicationConfig applicationConfig = service.assignApplicationToCluster(applicationId, environment, clusterName);
        return log.exit(applicationConfig);
    }
}
