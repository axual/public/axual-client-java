package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import javax.validation.constraints.NotNull;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import lombok.ToString;

@ToString
public class StandAloneSslConfig {

    private PasswordConfig keyPassword;

    @NotNull
    private String keystoreLocation;

    private PasswordConfig keystorePassword;

    @NotNull
    private String truststoreLocation;

    private PasswordConfig truststorePassword;

    private boolean hostnameVerificationEnabled = false;

    public PasswordConfig getKeyPassword() {
        return keyPassword;
    }

    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword == null ? null : new PasswordConfig(keyPassword);
    }

    public String getKeystoreLocation() {
        return keystoreLocation;
    }

    public void setKeystoreLocation(String keystoreLocation) {
        this.keystoreLocation = keystoreLocation;
    }

    public PasswordConfig getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword == null ? null : new PasswordConfig(keystorePassword);
    }

    public String getTruststoreLocation() {
        return truststoreLocation;
    }

    public void setTruststoreLocation(String truststoreLocation) {
        this.truststoreLocation = truststoreLocation;
    }

    public PasswordConfig getTruststorePassword() {
        return truststorePassword;
    }

    public void setTruststorePassword(String truststorePassword) {
        this.truststorePassword = truststorePassword == null ? null : new PasswordConfig(truststorePassword);
    }

    public boolean isHostnameVerificationEnabled() {
        return hostnameVerificationEnabled;
    }

    public void setHostnameVerificationEnabled(boolean hostnameVerificationEnabled) {
        this.hostnameVerificationEnabled = hostnameVerificationEnabled;
    }

    public SslConfig getSslConfig() {
        return SslConfig.newBuilder()
                .setKeystoreLocation(keystoreLocation)
                .setKeystorePassword(keystorePassword)
                .setKeyPassword(keyPassword)
                .setTruststoreLocation(truststoreLocation)
                .setTruststorePassword(truststorePassword)
                .setEnableHostnameVerification(hostnameVerificationEnabled)
                .build();
    }
}
