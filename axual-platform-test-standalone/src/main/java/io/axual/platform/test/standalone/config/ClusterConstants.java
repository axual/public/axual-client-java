package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ClusterConstants {
    public static final String DEFAULT_TENANT = "axual";
    public static final String DEFAULT_INSTANCE_NAME = "standalone";
    public static final String DEFAULT_SYSTEM = "platform-test-standalone";
    public static final String DEFAULT_ENVIRONMENT = "local";
    public static final String DEFAULT_BIND_ADDRESS = "127.0.0.1";
    public static final String DEFAULT_ADVERTISED_ADDRESS = "127.0.0.1";
    public static final String DEFAULT_TOPIC_PATTERN = "{tenant}-{instance}-{environment}-{topic}";
    public static final String DEFAULT_GROUPID_PATTERN = "{tenant}-{instance}-{environment}-{group}";
    public static final String DEFAULT_TRANSACTIONAL_ID_PATTERN = "{tenant}-{environment}-{app.id}-{transactional.id}";
    public static final boolean DEFAULT_USE_ADVANCED_ACL = false;
    public static final boolean DEFAULT_USE_VALUE_HEADERS = false;
    public static final boolean DEFAULT_ENABLE_DISTRIBUTION = true;
    public static final String DEFAULT_SINGLE_CLUSTER_NAME = "cluster1";
    public static final long MIN_TTL = 100L;
    public static final long DEFAULT_TLL = 5000L;
    public static final long MIN_DISTRIBUTOR_DISTANCE = 1L;
    public static final long DEFAULT_DISTRIBUTOR_DISTANCE = 1L;
    public static final long MIN_DISTRIBUTOR_TIMEOUT = 100L;
    public static final long DEFAULT_DISTRIBUTOR_TIMEOUT = 10000L;
}
