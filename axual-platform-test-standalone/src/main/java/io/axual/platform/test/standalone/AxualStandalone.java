package io.axual.platform.test.standalone;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;
import org.springframework.beans.factory.DisposableBean;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.SslUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.standalone.config.ClusterSettings;
import io.axual.platform.test.standalone.config.InstanceConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AxualStandalone implements DisposableBean {
    private final String environment;
    private final SslConfig sslConfig;
    protected PlatformUnit platformUnit = null;

    public AxualStandalone(InstanceConfig config) {
        this.environment = config.getEnvironment();
        this.sslConfig = getSslConfig(config);
    }

    protected static SslConfig getSslConfig(InstanceConfig config) {
        return config.getSslConfig() != null
                ? config.getSslConfig().getSslConfig()
                : SslUnit.getDefaultSslConfig();
    }

    public PlatformUnit getPlatformUnit() {
        return platformUnit;
    }

    public String getEnvironment() {
        return environment;
    }

    protected Schema getSchema(String clazz) {
        try {
            Class randomClass = Thread.currentThread().getContextClassLoader().loadClass(clazz);
            Method getClassSchema = randomClass.getMethod("getClassSchema");
            return (Schema) getClassSchema.invoke(randomClass.newInstance());
        } catch (ClassNotFoundException e) {
            log.error("Could not find class for name {}", clazz);
        } catch (Exception e) {
            log.error("Could not create a Schema from class {}", clazz, e);
        }
        return null;
    }

    protected InstanceUnitConfig getInstanceUnitConfig(InstanceConfig config) {
        return new InstanceUnitConfig()
                .setName(config.getInstance())
                .setSystem(config.getSystem())
                .setTenant(config.getTenant())
                .setDiscoveryBindAddress(config.getBindAddress())
                .setDiscoveryAdvertisedAddress(config.getAdvertisedAddress())
                .setDiscoveryPort(config.getEndpointPort())
                .setSslConfig(sslConfig)
                .setEnableDistribution(false);
    }

    protected List<ClusterUnitConfig> createClusterUnitConfigList(InstanceConfig config, InstanceUnitConfig instanceUnitConfig, List<ClusterSettings> clusterSettings) {
        List<ClusterUnitConfig> clusterUnitConfigList = new ArrayList<>(clusterSettings.size());
        for (ClusterSettings clusterSetting : clusterSettings) {
            ClusterUnitConfig clusterUnitConfig = new ClusterUnitConfig()
                    .setName(clusterSetting.getName())
                    .setBindAddress(config.getBindAddress())
                    .setAdvertisedAddress(config.getAdvertisedAddress())
                    .setBrokerPort(clusterSetting.getBrokerPort())
                    .setZookeeperPort(clusterSetting.getZookeeperPort())
                    .setSchemaRegistryPort(clusterSetting.getSchemaRegistryPort())
                    .setTopicPattern(clusterSetting.getTopicPattern())
                    .setGroupPattern(clusterSetting.getGroupIdPattern())
                    .setUseAdvancedAcl(clusterSetting.isUseAdvancedAcl())
                    .setUseValueHeaders(clusterSetting.isUseValueHeaders())
                    .setSslConfig(sslConfig);
            clusterUnitConfigList.add(clusterUnitConfig);
            instanceUnitConfig.addCluster(clusterUnitConfig.getName());
        }
        return clusterUnitConfigList;
    }

    protected void createAndStartPlatformUnit(InstanceConfig config, InstanceUnitConfig instanceUnitConfig, List<ClusterUnitConfig> clusterUnitConfigList) {
        platformUnit = new PlatformUnit(instanceUnitConfig, clusterUnitConfigList);

        platformUnit.start();
        platformUnit.getInstance().getDiscoveryUnit().setTtl(config.getTtl());
        platformUnit.getInstance().getDiscoveryUnit().setDistributorDistance(config.getDistributorDistance());
        platformUnit.getInstance().getDiscoveryUnit().setDistributorTimeout(config.getDistributorTimeout());

        config.getStreams().forEach((topicConfig -> {
            StreamConfig streamConfig = new StreamConfig();
            streamConfig.setName(topicConfig.getName());
            streamConfig.setPartitions(topicConfig.getPartitions());
            if (topicConfig.getKeyClass() != null) {
                streamConfig.setKeySchema(getSchema(topicConfig.getKeyClass()));
            }

            if (topicConfig.getValueClass() != null) {
                streamConfig.setValueSchema(getSchema(topicConfig.getValueClass()));
            }
            streamConfig.setEnvironment(topicConfig.getEnvironment() == null ? config.getEnvironment() : topicConfig.getEnvironment());
            final boolean isRawTopic = topicConfig.getIsRawTopic() != null && topicConfig.getIsRawTopic().booleanValue();
            if (isRawTopic) {
                platformUnit.getInstance().addRawTopic(streamConfig);
            } else {
                platformUnit.getInstance().addStream(streamConfig);
            }
        }));

        // create applicationIds
        config.getApplications().forEach(
                app -> {
                    ClientConfig clientConfig = ClientConfig.newBuilder()
                            .setTenant(config.getTenant())
                            .setApplicationId(app.getApplicationId())
                            .setEnvironment(app.getEnvironment() == null ? config.getEnvironment() : app.getEnvironment())
                            .setSslConfig(sslConfig)
                            .setEndpoint(platformUnit.getInstance().getDiscoveryUnit().getUrl())
                            .build();
                    ClusterUnit assignedCluster = platformUnit.getCluster(app.getAssignedCluster());
                    platformUnit.getInstance().directApplicationTo(clientConfig, assignedCluster);
                });

        if (config.getStreams().isEmpty()) {
            log.warn("No topics were specified, fix configuration");
        }
    }

    @Override
    public void destroy() throws Exception {
        if (platformUnit != null) {
            platformUnit.stop();
        }
    }

}
