package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;
import lombok.ToString;

import static io.axual.platform.test.standalone.config.ClusterConstants.*;

@Data
@ToString
public class ClusterSettings {
    @NotBlank
    private String name;

    @Positive(message = "The schema registry port is required, and must be a number greater than 0")
    private int schemaRegistryPort;

    @Positive(message = "The zookeeper port is required, and must be a number greater than 0")
    private int zookeeperPort;

    @Positive(message = "The broker port is required, and must be a number greater than 0")
    private int brokerPort;

    @NotBlank
    private String topicPattern = DEFAULT_TOPIC_PATTERN;

    @NotBlank
    private String groupIdPattern = DEFAULT_GROUPID_PATTERN;

    private String transactionalIdPattern = DEFAULT_TRANSACTIONAL_ID_PATTERN;

    private boolean useAdvancedAcl = DEFAULT_USE_ADVANCED_ACL;

    private boolean useValueHeaders = DEFAULT_USE_VALUE_HEADERS;
}
