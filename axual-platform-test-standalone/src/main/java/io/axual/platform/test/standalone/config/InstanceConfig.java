package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

import static io.axual.platform.test.standalone.config.ClusterConstants.*;

@Validated
@Data
@ToString
public class InstanceConfig {
    @NotBlank
    protected String tenant = DEFAULT_TENANT;

    @NotBlank
    protected String instance = DEFAULT_INSTANCE_NAME;

    @NotBlank
    protected String system = DEFAULT_SYSTEM;

    @NotBlank
    protected String environment = DEFAULT_ENVIRONMENT;

    @Min(value = MIN_TTL)
    protected long ttl = DEFAULT_TLL;

    @Min(value = MIN_DISTRIBUTOR_TIMEOUT)
    protected long distributorTimeout = DEFAULT_DISTRIBUTOR_TIMEOUT;

    @Min(value = MIN_DISTRIBUTOR_DISTANCE)
    protected long distributorDistance = DEFAULT_DISTRIBUTOR_DISTANCE;

    @Size(min = 1, message = "At least one application ID is required")
    protected List<StandaloneApplicationConfig> applications = new ArrayList<>();

    @NotBlank
    protected String bindAddress = DEFAULT_BIND_ADDRESS;

    @NotBlank
    protected String advertisedAddress = DEFAULT_ADVERTISED_ADDRESS;

    @Positive(message = "The endpoint port is required, and must be a number greater than 0")
    protected int endpointPort;

    @Valid
    protected StandAloneSslConfig sslConfig = null;

    @Valid
    @NestedConfigurationProperty
    @Size(min = 1, message = "At least one stream config is required")
    protected List<StandaloneStreamConfig> streams = new ArrayList<>();
}
