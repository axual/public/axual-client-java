package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

public final class SerdeConstants {
    // Axual Client message header is defined as follows:
    //
    // Offset Length Description
    // 0      2      Magic bytes
    // 2      2      Version
    //
    // The following additional fields are defined in V1:
    // 4      4      Extended header length
    // 8      1      Message flag bits
    // n = 9
    //
    // The following additional fields are defined in V2:
    // 9      16     Message id
    // 25     8      Serialization timestamp
    // n = 33
    //
    // The following fields are defined in all versions:
    // n      ...    Body
    //
    // Where:
    //  Magic bytes = fixed hex 0x0BEB
    //  Version = two-byte encoded version number
    //  Extended header length = size in bytes of the fields that follow
    //  Message flag bits:
    //    0: First order original/copy flag. Original means the message was produced on the local
    //       cluster. Copy means it is a message that was copied by the Axual  distributor.
    //       In V1 a value of 1 means "Original". In V2+ a value of 0 means "Original".
    //    1: Second order original/copy flag. Only applicable to V2+. 0 means "Original".
    //  Message Id = binary encoded UUID identifying the message
    //  Serialization timestamp = Time in millis on the system serializing the message
    //  Body = Body of the message (key or value)

    // Magic bytes that should never change
    public static final int MAGIC_INT = 0x0BEB;

    // Version 0 constants
    public static final int MAGIC_OFFSET = 0;
    public static final int VERSION_OFFSET = 2;

    // Version 1 constants
    public static final int VERSION_V1 = 1;
    public static final int VERSION_V2 = 2;
    public static final int HEADER_SIZE = 8;
    public static final int VERSION_V1_EXTENDED_HEADER_SIZE = 1;
    public static final int VERSION_V2_EXTENDED_HEADER_SIZE = 25;
    public static final int EXTENDED_HEADER_SIZE = VERSION_V2_EXTENDED_HEADER_SIZE;

    public static final int EXTENDED_HEADER_SIZE_OFFSET = 4;
    public static final int COPY_FLAGS_OFFSET = 8;
    public static final int MESSAGE_ID_MSB_OFFSET = 9;
    public static final int MESSAGE_ID_LSB_OFFSET = 17;
    public static final int SERIALIZATION_TIME_OFFSET = 25;

    // Message flag bits, represented as bit position from the right, 0-7 are valid positions
    public static final int MESSAGE_FLAG_FIRST_ORDER_COPY_POSITION = 0;
    public static final int MESSAGE_FLAG_SECOND_ORDER_COPY_POSITION = 1;

    private SerdeConstants() {
    }
}
