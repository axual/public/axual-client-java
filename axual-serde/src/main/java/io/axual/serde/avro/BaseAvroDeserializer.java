package io.axual.serde.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.tools.KafkaUtil;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;

public class BaseAvroDeserializer<T> implements Deserializer<T> {
    public static final String SPECIFIC_KEY_SCHEMA_CONFIG = "key.schema";
    public static final String SPECIFIC_VALUE_SCHEMA_CONFIG = "value.schema";
    private KafkaAvroDeserializer deserializer = null;
    private Schema specificSchema = null;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Map<String, Object> effectiveConfig = new HashMap<>(configs);
        // set SR SSL config
        KafkaUtil.setSchemaRegistrySSLConfig(effectiveConfig);
        final BaseAvroDeserializerConfig config = new BaseAvroDeserializerConfig(new HashMap<>(effectiveConfig));
        final SchemaRegistryClient schemaRegistryClient = SchemaRegistryUtil.createSrClient(
                config.getSslConfig(),
                config.getInnerConfig());

        if (deserializer != null) {
            deserializer.close();
        }

        deserializer = new KafkaAvroDeserializer(schemaRegistryClient);
        deserializer.configure(effectiveConfig, isKey);

        // Get a specific schema version
        Object schemaObj = effectiveConfig.get(isKey ? SPECIFIC_KEY_SCHEMA_CONFIG : SPECIFIC_VALUE_SCHEMA_CONFIG);
        if (schemaObj instanceof Schema) {
            this.specificSchema = (Schema) schemaObj;
        }
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        return deserialize(topic, data);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(String topic, byte[] data) {
        return (T) deserializer.deserialize(topic, data, specificSchema);
    }

    public Object deserialize(String topic, byte[] data, Schema targetSchema) {
        return deserializer.deserialize(topic, data, targetSchema);
    }

    @Override
    public void close() {
        if (deserializer != null) {
            deserializer.close();
        }
    }
}
