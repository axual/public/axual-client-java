package io.axual.serde.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.http.conn.ssl.NoopHostnameVerifier;

import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import io.axual.common.config.SslConfig;
import io.axual.common.exception.PropertyNotSetException;
import io.axual.common.tools.SslUtil;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.RestService;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;

public class SchemaRegistryUtil {

    private SchemaRegistryUtil() {
        // All methods are static
    }

    public static SchemaRegistryClient createSrClient(SslConfig config, AbstractKafkaSchemaSerDeConfig serdeConfig) {
        // Determine whether we connect to schema registry through either http or https. If both
        // are found in the URL-list an exception is thrown.
        List<String> srUrls = serdeConfig.getSchemaRegistryUrls();
        if (srUrls.isEmpty()) {
            throw new PropertyNotSetException(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG);
        }

        // If we end up here, we only found HTTPS urls, so we set up a secure rest service and
        // underpin that to the schema registry client returned.
        RestService restService = new RestService(srUrls);

        if (config != null) {
            SSLContext sslContext = SslUtil.createSslContext(config);
            SSLSocketFactory socketFactory = sslContext.getSocketFactory();
            restService.setSslSocketFactory(socketFactory);
            if (!config.getEnableHostnameVerification()) {
                restService.setHostnameVerifier(new NoopHostnameVerifier());
            }
        }

        final int maxSchemaObject = serdeConfig.getMaxSchemasPerSubject();
        final Map<String, Object> originals = serdeConfig.originalsWithPrefix("");
        return new CachedSchemaRegistryClient(restService, maxSchemaObject, originals);
    }
}
