package io.axual.serde.utils;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.UUID;

import io.axual.serde.SerdeConstants;

/**
 * Utility class for Serde operations on individual headers with no offset.
 */
public class SerdeUtilsWithoutOffset {
    private SerdeUtilsWithoutOffset() {
    }

    /**
     * Check if First Order Copy Bit is set
     *
     * @param data Message Flag byte
     * @return Bit status
     */
    public static boolean isFirstOrderCopyWithoutOffset(byte[] data) {
        return BitUtils.isBitSet(data[0], SerdeConstants.MESSAGE_FLAG_FIRST_ORDER_COPY_POSITION);
    }

    /**
     * Check if Second Order Copy Bit is set
     *
     * @param data Message Flag byte
     * @return Bit status
     */
    public static boolean isSecondOrderCopyWithoutOffset(byte[] data) {
        return BitUtils.isBitSet(data[0], SerdeConstants.MESSAGE_FLAG_SECOND_ORDER_COPY_POSITION);
    }

    /**
     * Set First Order Copy Bit
     *
     * @param data Message Flag byte
     */
    public static void setFirstOrderCopyWithoutOffset(byte[] data) {
        data[0] = BitUtils.setBit(data[0], SerdeConstants.MESSAGE_FLAG_FIRST_ORDER_COPY_POSITION);
    }

    /**
     * Set Second Order Copy Bit
     *
     * @param data Message Flag byte
     */
    public static void setSecondOrderCopyWithoutOffset(byte[] data) {
        data[0] = BitUtils.setBit(data[0], SerdeConstants.MESSAGE_FLAG_SECOND_ORDER_COPY_POSITION);
    }

    /**
     * Parse Message ID from Kafka headers as {@link UUID}
     *
     * @param data byte buffer
     * @return UUID obtained from buffer
     */
    public static UUID getMessageIdWithoutOffset(byte[] data) {
        return new UUID(BitUtils.toLong(data, 0), BitUtils.toLong(data, Long.SIZE / Byte.SIZE));
    }

    /**
     * Parse Serialization timestamp as {@linkplain long}
     *
     * @param data byte buffer
     * @return long obtained from buffer
     */
    public static long getSerializationTimestampWithoutOffset(byte[] data) {
        return BitUtils.toLong(data, 0);
    }

    /**
     * Parse Version as {@linkplain short}
     *
     * @param data byte buffer
     * @return int obtained from buffer
     */
    public static int getVersionWithoutOffset(byte[] data) {
        return BitUtils.getShortInt(data, 0);
    }
}
