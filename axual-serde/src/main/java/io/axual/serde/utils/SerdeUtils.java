package io.axual.serde.utils;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.UUID;

import static io.axual.serde.SerdeConstants.COPY_FLAGS_OFFSET;
import static io.axual.serde.SerdeConstants.EXTENDED_HEADER_SIZE_OFFSET;
import static io.axual.serde.SerdeConstants.HEADER_SIZE;
import static io.axual.serde.SerdeConstants.MAGIC_INT;
import static io.axual.serde.SerdeConstants.MAGIC_OFFSET;
import static io.axual.serde.SerdeConstants.MESSAGE_ID_LSB_OFFSET;
import static io.axual.serde.SerdeConstants.MESSAGE_ID_MSB_OFFSET;
import static io.axual.serde.SerdeConstants.SERIALIZATION_TIME_OFFSET;
import static io.axual.serde.SerdeConstants.VERSION_OFFSET;
import static io.axual.serde.SerdeConstants.VERSION_V1;

public class SerdeUtils {
    private SerdeUtils() {
    }

    public static boolean containsValueHeader(byte[] data) {
        if (data == null || data.length < HEADER_SIZE) {
            return false;
        }

        if (BitUtils.getShortInt(data, MAGIC_OFFSET) != MAGIC_INT) {
            return false;
        }

        int version = BitUtils.getShortInt(data, VERSION_OFFSET);
        if (version >= VERSION_V1) {
            int extendedHeaderSize = BitUtils.getInt(data, EXTENDED_HEADER_SIZE_OFFSET);
            return data.length >= HEADER_SIZE + extendedHeaderSize;
        }

        return false;
    }

    public static int getValueHeaderVersion(byte[] data) {
        if (!containsValueHeader(data)) {
            return -1;
        }

        return BitUtils.getShortInt(data, VERSION_OFFSET);
    }

    public static int getValueHeaderSize(byte[] data) {
        if (!containsValueHeader(data)) {
            return 0;
        }

        int version = BitUtils.getShortInt(data, VERSION_OFFSET);
        if (version >= VERSION_V1) {
            int extendedHeaderSize = BitUtils.getInt(data, EXTENDED_HEADER_SIZE_OFFSET);
            if (data.length < HEADER_SIZE + extendedHeaderSize) {
                return 0;
            }
            return HEADER_SIZE + extendedHeaderSize;
        }

        return 0;
    }

    public static UUID getMessageId(byte[] data) {
        if (getValueHeaderVersion(data) == VERSION_V1) {
            return null;
        } else {
            return new UUID(BitUtils.getLong(data, MESSAGE_ID_MSB_OFFSET), BitUtils.getLong(data, MESSAGE_ID_LSB_OFFSET));
        }
    }

    public static long getSerializationTimestamp(byte[] data) {
        if (getValueHeaderVersion(data) == VERSION_V1) {
            return 0L;
        } else {
            return BitUtils.getLong(data, SERIALIZATION_TIME_OFFSET);
        }
    }

    public static byte importCopyFlags(byte[] data) {
        byte rawCopyFlags = data[COPY_FLAGS_OFFSET];
        // In V1 the lowest bit is flipped from V2, so for proper in-memory representation we flip
        // it back.
        if (getValueHeaderVersion(data) == VERSION_V1) {
            return BitUtils.flipBit(rawCopyFlags, 0);
        }
        return rawCopyFlags;
    }

    public static byte exportCopyFlags(int version, byte copyFlags) {
        if (version == VERSION_V1) {
            copyFlags = BitUtils.flipBit(copyFlags, 0);
        }
        return copyFlags;
    }
}
