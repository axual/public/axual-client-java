package io.axual.serde.utils;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

public class BitUtils {
    private BitUtils() {
    }

    public static boolean isBitSet(byte dataByte, int position) {
        return ((dataByte >> position) & 1) == 1;
    }

    public static byte clearBit(byte dataByte, int position) {
        return (byte) (dataByte & ~(1 << position));
    }

    public static byte setBit(byte dataByte, int position) {
        return (byte) ((dataByte & 0xff) | (1 << position));
    }

    public static byte flipBit(byte dataByte, int position) {
        return (byte) ((dataByte & 0xff) ^ (1 << position));
    }

    static int getInt(byte[] buffer, int offset) {
        return (buffer[offset] & 0xFF) << 24 |
                (buffer[offset + 1] & 0xFF) << 16 |
                (buffer[offset + 2] & 0xFF) << 8 |
                (buffer[offset + 3] & 0xFF);
    }

    public static long getLong(byte[] buffer, int offset) {
        return ((long) (buffer[offset] & 0xFF)) << 56 |
                ((long) (buffer[offset + 1] & 0xFF)) << 48 |
                ((long) (buffer[offset + 2] & 0xFF)) << 40 |
                ((long) (buffer[offset + 3] & 0xFF)) << 32 |
                ((long) (buffer[offset + 4] & 0xFF)) << 24 |
                ((long) (buffer[offset + 5] & 0xFF)) << 16 |
                ((long) (buffer[offset + 6] & 0xFF)) << 8 |
                (buffer[offset + 7] & 0xFF);
    }

    public static int getShortInt(byte[] buffer, int offset) {
        return (buffer[offset] & 0xFF) << 8 |
                (buffer[offset + 1] & 0xFF);
    }

    public static void putInt(byte[] buffer, int offset, int value) {
        buffer[offset] = (byte) (value >> 24);
        buffer[offset + 1] = (byte) (value >> 16);
        buffer[offset + 2] = (byte) (value >> 8);
        buffer[offset + 3] = (byte) (value /*>> 0*/);
    }

    public static void putShortInt(byte[] buffer, int offset, int value) {
        buffer[offset] = (byte) (value >> 8);
        buffer[offset + 1] = (byte) (value /*>> 0*/);
    }

    public static byte[] putLong(byte[] buffer, int offset, long value) {
        buffer[offset] = (byte) (value >> 56);
        buffer[offset + 1] = (byte) (value >> 48);
        buffer[offset + 2] = (byte) (value >> 40);
        buffer[offset + 3] = (byte) (value >> 32);
        buffer[offset + 4] = (byte) (value >> 24);
        buffer[offset + 5] = (byte) (value >> 16);
        buffer[offset + 6] = (byte) (value >> 8);
        buffer[offset + 7] = (byte) (value /*>> 0*/);
        return buffer;
    }

    /**
     * Convert a long value to a byte array using big-endian.
     *
     * @param val value to convert
     * @return the byte array
     */
    public static byte[] toBytes(long val) {
        byte[] b = new byte[Long.SIZE / Byte.SIZE];
        for (int i = 7; i > 0; i--) {
            b[i] = (byte) val;
            val >>>= 8;
        }
        b[0] = (byte) val;
        return b;
    }

    /**
     * Convert an int value to a byte array
     *
     * @param val value
     * @return the byte array
     */
    public static byte[] toBytes(int val) {
        byte[] b = new byte[Integer.SIZE / Byte.SIZE];
        for (int i = 3; i > 0; i--) {
            b[i] = (byte) val;
            val >>>= 8;
        }
        b[0] = (byte) val;
        return b;
    }

    /**
     * Convert a short value to a byte array of {@link Short} bytes long.
     *
     * @param val value
     * @return the byte array
     */
    public static byte[] toBytes(short val) {
        byte[] b = new byte[Short.SIZE / Byte.SIZE];
        b[1] = (byte) val;
        val >>= 8;
        b[0] = (byte) val;
        return b;
    }

    /**
     * Convert byte buffer to {@linkplain long} using given offset.
     *
     * @param bytes  byte buffer
     * @param offset position to start reading the byte buffer
     * @return long obtained from buffer
     */
    static long toLong(byte[] bytes, int offset) {
        long l = 0;
        for (int i = offset; i < offset + 8; i++) {
            l <<= 8;
            l ^= bytes[i] & 0xFF;
        }
        return l;
    }

}
