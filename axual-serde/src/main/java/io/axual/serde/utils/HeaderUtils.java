package io.axual.serde.utils;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static io.axual.serde.utils.BitUtils.toBytes;

public class HeaderUtils {
    private HeaderUtils() {
    }

    public static Integer decodeIntegerHeader(Header header) {
        if (header != null) {
            return BitUtils.getInt(header.value(), 0);
        }
        return null;
    }

    public static byte[] encodeIntegerValue(int value) {
        return BitUtils.toBytes(value);
    }

    public static void addIntegerHeader(Headers headers, String key, Integer value) {
        if (value != null) {
            headers.add(new RecordHeader(key, encodeIntegerValue(value)));
        }
    }

    public static Long decodeLongHeader(Header header) {
        if (header != null) {
            return BitUtils.toLong(header.value(), 0);
        }
        return null;
    }

    public static void addLongHeader(Headers headers, String key, Long value) {
        if (value != null) {
            headers.add(new RecordHeader(key, encodeLongValue(value)));
        }
    }

    public static byte[] encodeLongValue(long value) {
        return BitUtils.toBytes(value);
    }

    public static String decodeStringHeader(Header header) {
        if (header != null) {
            return new String(header.value(), StandardCharsets.UTF_8);
        }
        return null;
    }

    public static void addStringHeader(Headers headers, String key, String value) {
        if (value != null) {
            headers.add(new RecordHeader(key, encodeStringValue(value)));
        }
    }

    public static byte[] encodeStringValue(String value) {
        return value.getBytes(StandardCharsets.UTF_8);
    }

    public static UUID decodeUuidHeader(Header header) {
        if (header != null) {
            return new UUID(
                    BitUtils.toLong(header.value(), 0),
                    BitUtils.toLong(header.value(), Long.SIZE / 8));
        }
        return null;
    }

    public static void addUuidHeader(Headers headers, String key, UUID value) {
        if (value != null) {
            headers.add(new RecordHeader(key, encodeUuidValue(value)));
        }
    }

    public static byte[] encodeUuidValue(UUID value) {
        byte[] msb = toBytes(value.getMostSignificantBits());
        byte[] lsb = toBytes(value.getLeastSignificantBits());
        byte[] result = new byte[msb.length + lsb.length];
        System.arraycopy(msb, 0, result, 0, msb.length);
        System.arraycopy(lsb, 0, result, msb.length, lsb.length);
        return result;
    }
}
