package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import java.util.UUID;

import io.axual.serde.utils.BitUtils;
import io.axual.serde.utils.SerdeUtils;

import static io.axual.serde.SerdeConstants.COPY_FLAGS_OFFSET;
import static io.axual.serde.SerdeConstants.VERSION_V2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class SerdeUtilsTest {
    private static byte[] CONTENT = "axual".getBytes();

    @Test
    public void basics_validateHeader_correctBehaviour() {
        for (int version : TestObjects.VERSIONS) {
            byte[] original = TestObjects.getSerializedMessage(true, version, false, false, CONTENT);
            assertTrue(SerdeUtils.containsValueHeader(original));
            assertEquals(version, SerdeUtils.getValueHeaderVersion(original));

            byte[] copy = TestObjects.getSerializedMessage(true, version, true, true, CONTENT);
            assertTrue(SerdeUtils.containsValueHeader(copy));
            assertEquals(version, SerdeUtils.getValueHeaderVersion(copy));
        }
    }

    @Test
    public void isAxualConsumerMessage_invalidMagic_returnFalse() {
        assertFalse(SerdeUtils.containsValueHeader(null));
        assertFalse(SerdeUtils.containsValueHeader(new byte[]{0, 0}));

        for (int version : TestObjects.VERSIONS) {
            byte[] validMesage = TestObjects.getSerializedMessage(true, version, false, false, CONTENT);
            assertNotEquals(null, validMesage);
            byte[] invalidFirst = validMesage.clone();
            invalidFirst[0] = 0;
            assertFalse(SerdeUtils.containsValueHeader(invalidFirst));
            byte[] invalidSecond = validMesage.clone();
            invalidSecond[1] = 0;
            assertFalse(SerdeUtils.containsValueHeader(invalidSecond));
        }
    }

    @Test
    public void setCopy_validateAllVersions_correctBehaviour() {
        for (int version : TestObjects.VERSIONS) {
            byte[] message = TestObjects.getSerializedMessage(true, version, false, false, CONTENT);
            byte copyFlags = SerdeUtils.importCopyFlags(message);
            copyFlags |= 3;
            message[COPY_FLAGS_OFFSET] = SerdeUtils.exportCopyFlags(version, copyFlags);
            assertTrue(SerdeUtils.containsValueHeader(message));
            assertEquals(3, SerdeUtils.importCopyFlags(message));
        }
    }

    // This test probably needs some work when we start using the other 7 bits in the infoByte
    @Test
    public void getCopyFlags_bothPossibilities_correctReturned() {
        assertEquals(1, SerdeUtils.exportCopyFlags(SerdeConstants.VERSION_V1, (byte) 0));
        assertEquals(0, SerdeUtils.exportCopyFlags(SerdeConstants.VERSION_V1, (byte) 1));
        assertEquals(0, SerdeUtils.exportCopyFlags(VERSION_V2, (byte) 0));
        assertEquals(1, SerdeUtils.exportCopyFlags(VERSION_V2, (byte) 1));

        assertEquals(3, SerdeUtils.exportCopyFlags(SerdeConstants.VERSION_V1, (byte) 2));
        assertEquals(2, SerdeUtils.exportCopyFlags(SerdeConstants.VERSION_V1, (byte) 3));
        assertEquals(2, SerdeUtils.exportCopyFlags(VERSION_V2, (byte) 2));
        assertEquals(3, SerdeUtils.exportCopyFlags(VERSION_V2, (byte) 3));
    }

    @Test
    public void uuidSerialization_serializeAndDeserialize_sameReturned() {
        UUID gen = UUID.randomUUID();
        byte[] buf = new byte[16];
        BitUtils.putLong(buf, 0, gen.getMostSignificantBits());
        BitUtils.putLong(buf, 8, gen.getLeastSignificantBits());
        long hi = BitUtils.getLong(buf, 0);
        long lo = BitUtils.getLong(buf, 8);
        UUID copy = new UUID(hi, lo);
        assertEquals(gen, copy);
    }
}
