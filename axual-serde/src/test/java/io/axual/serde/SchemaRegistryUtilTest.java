package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import io.axual.common.config.SslConfig;
import io.axual.common.tools.SslUtil;
import io.axual.serde.avro.SchemaRegistryUtil;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.RestService;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        SchemaRegistryUtil.class,
        SslUtil.class,
        SSLContext.class
})
public class SchemaRegistryUtilTest {
    @Mock
    SSLContext mockSslContext;

    @Mock
    SSLSocketFactory mockSslSocketFactory;

    @Mock
    CachedSchemaRegistryClient mockCachedSchemaRegistryClient;

    @Mock
    RestService mockRestService;

    @Captor
    ArgumentCaptor<NoopHostnameVerifier> argumentCaptor;

    @Before
    public void prepareTest() throws Exception {
        mockStatic(SslUtil.class);

        whenNew(CachedSchemaRegistryClient.class).withAnyArguments().thenReturn(mockCachedSchemaRegistryClient);
        when(SslUtil.createSslContext(any())).thenReturn(mockSslContext);
        when(mockSslContext.getSocketFactory()).thenReturn(mockSslSocketFactory);
        whenNew(RestService.class).withAnyArguments().thenReturn(mockRestService);
    }

    @Test
    public void createSrClient_Success_withoutHostNameValidation() throws Exception {
        SslConfig sslConfig = SslConfig.newBuilder().setEnableHostnameVerification(false).build();
        Map<String, String> m = new HashMap<>();
        m.put("schema.registry.url", "http://dummy-registry");
        SchemaRegistryUtil.createSrClient(sslConfig, new KafkaAvroDeserializerConfig(m));

        verify(mockRestService, times(1)).setHostnameVerifier(argumentCaptor.capture());
        verifyNew(CachedSchemaRegistryClient.class).withArguments(anyString(), anyInt(), any());
        Assert.assertSame(NoopHostnameVerifier.class, argumentCaptor.getValue().getClass());
    }

    @Test
    public void createSrClient_Success_withHostNameValidation() throws Exception {
        SslConfig sslConfig = SslConfig.newBuilder().setEnableHostnameVerification(true).build();
        Map<String, String> m = new HashMap<>();
        m.put("schema.registry.url", "http://dummy-registry");
        SchemaRegistryUtil.createSrClient(sslConfig, new KafkaAvroDeserializerConfig(m));

        verify(mockRestService, times(0)).setHostnameVerifier(argumentCaptor.capture());
        verifyNew(CachedSchemaRegistryClient.class).withArguments(anyString(), anyInt(), any());
    }
}
