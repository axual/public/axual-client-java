package io.axual.discovery.client.fetcher;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.exception.DiscoveryException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.net.ssl.*", "jdk.internal.reflect.*"})
@PrepareForTest({DiscoveryLoader.class, DiscoveryFetcher.class})
public class DiscoveryLoaderTest {

    private static final String CLUSTER = "C";
    private static final long TTL = 1234L;

    @Mock
    private DiscoveryFetcher mockFetcher;
    @Mock
    private DiscoveryResult mockResult;

    private DiscoveryLoader target;

    @Before
    public void init() throws Exception {
        whenNew(DiscoveryFetcher.class).withAnyArguments().thenReturn(mockFetcher);
        target = new DiscoveryLoader(null);
    }

    @Test
    public void test_discoveryChanged_firstFetchNull() {
        assertTrue(target.discoveryChanged());

        verify(mockFetcher).executeRequest(null);
    }

    @Test
    public void test_discoveryChanged_unchanged() {
        assertTrue(target.discoveryChanged());
        assertFalse(target.discoveryChanged());
        assertNull(target.getDiscoveryResult());
        verify(mockFetcher, times(1)).executeRequest(any());
    }

    @Test
    public void test_discoveryChanged_invalidate() {
        assertTrue(target.discoveryChanged());
        when(mockFetcher.executeRequest(any())).thenReturn(mockResult);
        when(mockResult.getCluster()).thenReturn(CLUSTER);
        when(mockResult.getTtl()).thenReturn(TTL);
        target.invalidate();

        assertTrue(target.discoveryChanged());
        verify(mockFetcher, times(2)).executeRequest(any());
    }

    @Test(expected = DiscoveryException.class)
    public void test_discoveryChanged_after_close() throws Exception {
        target.close();
        target.discoveryChanged();
    }

    @Test(expected = DiscoveryException.class)
    public void test_invalidate_after_close() throws Exception {
        target.close();
        target.invalidate();
    }

    @Test(expected = DiscoveryException.class)
    public void test_getDiscoveryResult_after_close() throws Exception {
        target.close();
        target.getDiscoveryResult();
    }

}
