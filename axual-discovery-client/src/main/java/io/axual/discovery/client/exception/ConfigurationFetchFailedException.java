package io.axual.discovery.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * Error specifically used when there is an error trying to get the configuration with the
 * configuration fetcher
 */
public class ConfigurationFetchFailedException extends DiscoveryException {
    static final String MESSAGE_FORMAT = "An error occured trying to fetch the configuration.%nUrl used: %s";

    /**
     * Creates a new ConfigurationFetchFailedException
     *
     * @param url the url of the fetched properties originalException the original error
     * @param originalException The cause of the fetch properties failure
     */
    public ConfigurationFetchFailedException(String url, Exception originalException) {
        super(String.format(MESSAGE_FORMAT, url), originalException);
    }
}
