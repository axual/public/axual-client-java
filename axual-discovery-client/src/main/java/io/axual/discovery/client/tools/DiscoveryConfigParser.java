package io.axual.discovery.client.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Map;

import io.axual.common.config.CommonConfig;
import io.axual.common.config.ConfigParser;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryConfig;

public abstract class DiscoveryConfigParser {
    private final String endpointSuffix;

    public DiscoveryConfigParser(String endpointSuffix) {
        this.endpointSuffix = endpointSuffix;
    }

    protected DiscoveryConfig parse(Map input, Map<String, String> parameters) {
        // Extract specific Axual configuration
        String applicationId = ConfigParser.parseStringConfig(input, CommonConfig.APPLICATION_ID, true, null);
        String applicationVersion = ConfigParser.parseStringConfig(input, CommonConfig.APPLICATION_VERSION, false, "unknown");

        // Parse Discovery API endpoint
        String propertyName = input.containsKey(CommonConfig.ENDPOINT) ? CommonConfig.ENDPOINT : ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
        String endpoint = ConfigParser.parseStringConfig(input, propertyName, true, null);
        String suffix = endpointSuffix;
        if (suffix.startsWith("/")) {
            suffix = suffix.substring(1);
        }
        endpoint = endpoint.endsWith("/") ? endpoint + suffix : endpoint + "/" + suffix;

        // Construct the DiscoveryConfig object holding all information for the Discovery API
        return DiscoveryConfig.newBuilder()
                .setApplicationId(applicationId)
                .setApplicationVersion(applicationVersion)
                .setEndpoint(endpoint)
                .setParameters(parameters)
                .setSslConfig(KafkaUtil.parseSslConfig(input))
                .build();
    }
}
