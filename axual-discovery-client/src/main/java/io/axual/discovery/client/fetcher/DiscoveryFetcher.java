package io.axual.discovery.client.fetcher;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import io.axual.common.tools.SslUtil;
import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.exception.ConfigurationFetchFailedException;
import io.axual.discovery.client.exception.DiscoveryException;
import io.axual.discovery.client.tools.VersionUtil;

import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;

public class DiscoveryFetcher implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryFetcher.class);
    private static final String HTTP_SCHEME = "http://";
    private static final String HTTPS_SCHEME = "https://";

    private final TypeReference<Properties> propertiesTypeReference = new TypeReference<Properties>() {
    };

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final DiscoveryConfig config;

    private final CloseableHttpClient httpClient;
    private boolean isClosed = false;

    public DiscoveryFetcher(final DiscoveryConfig config) {
        this.config = config;

        if (!isHyperTextUrl(config.getEndpoint())) {
            throw new DiscoveryException(config.getEndpoint() + " is not a valid URL");
        }
        try {
            this.httpClient = createHttpClient();
        } catch (Exception e) {
            throw new DiscoveryException("Error found during httpClient configuration", e);
        }
    }

    private CloseableHttpClient createHttpClient() {
        HttpClientBuilder builder = HttpClients.custom()
                .setDefaultHeaders(Arrays.asList(
                        new BasicHeader(HttpHeaders.CACHE_CONTROL, "no-cache"),
                        new BasicHeader("X-Application-Id", config.getApplicationId()),
                        new BasicHeader("X-Application-Version", config.getApplicationVersion()),
                        new BasicHeader("X-Client-Library-Version", VersionUtil.getProjectVersion(this.getClass()))
                ));

        if (config.getEndpoint().startsWith(HTTPS_SCHEME)) {
            if (!config.getSslConfig().getEnableHostnameVerification()) {
                builder.setSSLHostnameVerifier(new NoopHostnameVerifier());
            }
            builder.setSSLContext(SslUtil.createSslContext(config.getSslConfig()));
        }

        return builder.build();
    }

    private String getUrl(String lastCluster) {
        // Validate the Discovery API URL and pass append query parameters
        String url = config.getEndpoint();

        if (isHyperTextUrl(url)) {
            StringBuilder builder = new StringBuilder(url);
            if (!url.endsWith("/")) {
                builder.append("/");
            }
            builder.append("?applicationId=");
            builder.append(config.getApplicationId());
            if (lastCluster != null) {
                builder.append("&lastCluster=");
                builder.append(lastCluster);
            }
            for (Map.Entry<String, String> entry : config.getParameters().entrySet()) {
                builder.append("&");
                builder.append(entry.getKey());
                builder.append("=");
                builder.append(entry.getValue());
            }
            return builder.toString();
        }

        // Throw exception when URL is invalid
        throw new IllegalArgumentException("Provided URL, " + config.getEndpoint() + " was invalid");
    }

    // Call the DiscoveryAPI's REST interface to fetch target cluster coordinates.
    // Returns:
    // - null if an error occurred during the query.
    // - empty Properties when no active clusters were offered in the result.
    // - non-empty Properties when an active cluster was found, its details carried as properties.
    public DiscoveryResult executeRequest(String lastCluster) {
        if (isClosed) {
            throw new DiscoveryException("Illegal call, DiscoveryFetcher is closed");
        }

        String url = getUrl(lastCluster);
        LOG.debug("Fetching Discovery API properties from {}", url);

        Properties properties = null;

        try {
            if (isHyperTextUrl(url)) {
                // Load configuration from Discovery API through https
                properties = execute(url);
            }
        } catch (Exception e) {
            throw new ConfigurationFetchFailedException(url, e);
        }

        LOG.debug("Fetched Discovery API properties: {}", properties);
        // Returns null when Discovery API returns an error or empty cluster configuration
        return properties != null && !properties.isEmpty() ? new DiscoveryResult(properties) : null;
    }

    private boolean isHyperTextUrl(String url) {
        return url.startsWith(HTTPS_SCHEME) || url.startsWith(HTTP_SCHEME);
    }

    private Properties execute(String url) throws IOException {
        // Execute the https request
        HttpResponse response = httpClient.execute(new HttpGet(url));

        // Parse the response
        int responseCode = response.getStatusLine().getStatusCode();
        if (responseCode == HTTP_OK) {
            try (InputStream is = response.getEntity().getContent()) {
                return objectMapper.readValue(is, propertiesTypeReference);
            }
        } else if (responseCode == HTTP_NO_CONTENT) {
            // No active target cluster found, return empty set of properties
            LOG.info("Empty response from Discovery API, no active clusters found");
            return new Properties();
        } else if (responseCode == HTTP_FORBIDDEN) {
            LOG.warn("Forbidden to query Discovery API");
        } else {
            LOG.warn("Unexpected response code from Discovery API: {}", responseCode);
        }

        return null;
    }

    @Override
    public void close() throws IOException {
        LOG.debug("Closing Discovery Fetcher");
        if (!isClosed) {
            httpClient.close();
            isClosed = true;
        }
    }
}
