package io.axual.discovery.client;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.axual.common.config.CommonConfig;
import io.axual.common.tools.MapUtil;
import io.axual.common.tools.ParseUtil;

// Container that holds returned properties by the Discovery API
public class DiscoveryResult {
    // Logical namespace properties
    public static final String TENANT_PROPERTY = CommonConfig.TENANT;
    public static final String INSTANCE_PROPERTY = CommonConfig.INSTANCE;
    public static final String ENVIRONMENT_PROPERTY = CommonConfig.ENVIRONMENT;

    // Physical system components
    public static final String SYSTEM_PROPERTY = CommonConfig.SYSTEM_PROPERTY;
    public static final String CLUSTER_PROPERTY = CommonConfig.CLUSTER_PROPERTY;

    // Timeout that indicates how long the returned properties are valid
    public static final String TTL_PROPERTY = "ttl";

    // Default values for missing properties
    private static final long DEFAULT_TTL = 10000L; //10s in ms

    // The map containing all returned configs by DiscoveryAPI
    private final Map<String, Object> properties;
    private final long timestamp;

    public DiscoveryResult() {
        properties = Collections.unmodifiableMap(new HashMap<>());
        timestamp = System.currentTimeMillis();
    }

    public DiscoveryResult(Properties props) {
        properties = Collections.unmodifiableMap(MapUtil.objectToStringMap(props));
        timestamp = System.currentTimeMillis();
    }

    @Override
    public int hashCode() {
        return properties.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (o instanceof DiscoveryResult) {
            // Compare only the contents of the discovery, not its timestamp
            return properties.equals(((DiscoveryResult) o).properties);
        }
        return false;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTenant() {
        return MapUtil.stringValue(properties, TENANT_PROPERTY);
    }

    public String getInstance() {
        return MapUtil.stringValue(properties, INSTANCE_PROPERTY);
    }

    public String getEnvironment() {
        return MapUtil.stringValue(properties, ENVIRONMENT_PROPERTY);
    }

    public String getSystem() {
        return MapUtil.stringValue(properties, SYSTEM_PROPERTY);
    }

    public String getCluster() {
        return MapUtil.stringValue(properties, CLUSTER_PROPERTY);
    }

    public long getTtl() {
        return ParseUtil.parseLong(MapUtil.stringValue(properties, TTL_PROPERTY), DEFAULT_TTL);
    }

    public Map<String, Object> getConfigs() {
        return properties;
    }

    @Override
    public String toString() {
        return "DiscoveryResult: " + properties.toString();
    }
}
