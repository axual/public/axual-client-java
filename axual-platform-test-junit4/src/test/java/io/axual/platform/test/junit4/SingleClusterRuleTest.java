package io.axual.platform.test.junit4;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-junit4
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Collections;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleClusterRuleTest extends AbstractRuleTest {

  private static final Logger LOG = LoggerFactory.getLogger(SingleClusterRuleTest.class);

  @Rule
  public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit();

  public SingleClusterRuleTest() {
    super(LOG);
  }

  @Test
  public void testSingleCluster() {
    runTest(platformUnit.platform(), platformUnit.cluster(),
        Collections.singletonList(platformUnit.cluster()), 50, 10000, 30000);
  }
}
