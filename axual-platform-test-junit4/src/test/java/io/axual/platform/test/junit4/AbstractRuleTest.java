package io.axual.platform.test.junit4;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-junit4
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.StreamConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractRuleTest {

  private final Logger LOG;

  protected static final StreamConfig STREAM_CONFIG = new StreamConfig()
      .setName("test-topic")
      .setEnvironment("extension")
      .setPartitions(1);

  AbstractRuleTest(Logger log) {
    LOG = log;
  }

  protected void runTest(PlatformUnit platform, ClusterUnit produceCluster,
      Collection<ClusterUnit> consumeClusters, int nrOfMessages, long produceTimoutMs,
      long consumeTimoutMs) {
    Collection<ProducedMessage<String, String>> records = produceToCluster(
        platform, produceCluster, nrOfMessages, produceTimoutMs);

    assertFalse(records.isEmpty());

    for (ClusterUnit consumeCluster : consumeClusters) {
      assertTrue(consumeFromCluster(platform, consumeCluster, records, consumeTimoutMs));
    }
  }

  protected Collection<ProducedMessage<String, String>> produceToCluster(PlatformUnit platformUnit,
      ClusterUnit clusterUnit, int nrOfMessages, long timeoutMs) {
    final List<ProducedMessage<String, String>> produced = new ArrayList<>(nrOfMessages);
    final List<ProducerMessage<String, String>> failed = new ArrayList<>(nrOfMessages);
    final CustomProducerCallback callback = new CustomProducerCallback(produced, failed);

    platformUnit.addStream(STREAM_CONFIG);
    ClientConfig clientConfig = platformUnit.getInstance()
        .getClientConfig("clusterProducer", true, STREAM_CONFIG.getEnvironment(), clusterUnit);

    ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
        .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
        .setKeySerializer(new StringSerializer())
        .setValueSerializer(new StringSerializer())
        .build();

    try (AxualClient axualClient = new AxualClient(clientConfig);
        Producer<String, String> producer = axualClient.buildProducer(producerConfig)) {
      for (int i = 1; i <= nrOfMessages; i++) {
        final ProducerMessage<String, String> record = ProducerMessage.<String, String>newBuilder()
            .setStream(STREAM_CONFIG.getName())
            .setKey("key " + i)
            .setValue("value" + i)
            .build();
        producer.produce(record, callback);
      }
    }

    Awaitility.await("WaitForProduce").atMost(timeoutMs, TimeUnit.MILLISECONDS)
        .until(() -> nrOfMessages <= produced.size() + failed.size());

    if (produced.size() < nrOfMessages) {
      throw new RuntimeException("Could not produce all messages");
    }
    return produced;
  }

  protected boolean consumeFromCluster(PlatformUnit platformUnit,
      ClusterUnit clusterUnit, Collection<ProducedMessage<String, String>> toMatch,
      long timeoutMs) {
    ClientConfig clientConfig = platformUnit.getInstance()
        .getClientConfig("clusterConsumer", true, STREAM_CONFIG.getEnvironment(), clusterUnit);

    CustomProcessor processor = new CustomProcessor(toMatch, clusterUnit.getName());

    ConsumerConfig<String, String> consumerConfig = ConsumerConfig.<String, String>builder()
        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
        .setKeyDeserializer(new StringDeserializer())
        .setValueDeserializer(new StringDeserializer())
        .setMaximumPollSize(10)
        .setStream(STREAM_CONFIG.getName())
        .build();

    try (AxualClient axualClient = new AxualClient(clientConfig);
        Consumer<String, String> consumer = axualClient.buildConsumer(consumerConfig, processor)) {
      Future<ConsumeFailedException> future = consumer.startConsuming();
      Awaitility.await("WaitForConsumed").atMost(timeoutMs, TimeUnit.MILLISECONDS)
          .pollInterval(100, TimeUnit.MILLISECONDS)
          .until(() -> !future.isDone() && processor.allFound());
    }

    return processor.allFound();
  }

  protected static class CustomProducerCallback implements ProduceCallback<String, String> {

    private static final Logger LOG = LoggerFactory.getLogger(CustomProducerCallback.class);
    private final Collection<ProducedMessage<String, String>> producedMessages;
    private final Collection<ProducerMessage<String, String>> failed;

    public CustomProducerCallback(
        Collection<ProducedMessage<String, String>> producedMessages,
        Collection<ProducerMessage<String, String>> failed) {
      this.producedMessages = producedMessages;
      this.failed = failed;
    }

    @Override
    public void onComplete(ProducedMessage<String, String> message) {
      LOG.info("Produced key '{}' value '{}' offset '{}' cluster '{}'",
          message.getMessage().getKey(),
          message.getMessage().getValue(), message.getOffset(), message.getCluster());
      producedMessages.add(message);
    }

    @Override
    public void onError(ProducerMessage<String, String> message, ExecutionException exception) {
      LOG.info("Produced key '{}' value '{}'", message.getKey(),
          message.getValue(), exception);
      failed.add(message);
    }
  }

  protected static class CustomProcessor implements Processor<String, String> {

    private static final Logger LOG = LoggerFactory.getLogger(CustomProcessor.class);

    private final Collection<ProducedMessage<String, String>> toMatch;
    private final String cluster;

    public CustomProcessor(
        Collection<ProducedMessage<String, String>> toMatch, String cluster) {
      this.toMatch = new ArrayList<>(toMatch);
      this.cluster = cluster;
    }

    @Override
    public void processMessage(ConsumerMessage<String, String> message) {
      LOG.info("Consumed key '{}' value '{}' offset '{}' cluster '{}'", message.getKey(),
          message.getValue(), message.getOffset(), cluster);
      final AtomicBoolean found = new AtomicBoolean(false);
      toMatch.removeIf(produced -> {
        if (!found.get()
            && Objects.equals(produced.getMessage().getKey(), message.getKey())
            && Objects.equals(produced.getMessage().getValue(), message.getValue())) {
          found.set(true);
          return true;
        }
        return false;
      });
    }

    public boolean allFound() {
      final boolean allFound = toMatch.isEmpty();
      LOG.debug("Allfound for cluster '{}' found '{}'", cluster, allFound);
      return allFound;
    }
  }
}
