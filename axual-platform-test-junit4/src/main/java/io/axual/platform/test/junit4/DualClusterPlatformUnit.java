package io.axual.platform.test.junit4;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-junit4
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;

/**
 * Test with an Axual Test Platform using two clusters
 */
@InterfaceStability.Evolving
public class DualClusterPlatformUnit extends BasePlatformUnit<DualClusterPlatformUnit> {

  /**
   * Constructs a default test platform with two clusters and distribution disabled
   */
  public DualClusterPlatformUnit() {
    super(new PlatformUnit(2, false));
  }

  /**
   * Constructs a default test platform with two clusters and distribution disabled using the
   * specified keyStoreType
   *
   * @param keystoreType the type of keystore to use
   */
  public DualClusterPlatformUnit(SslConfig.KeystoreType keystoreType) {
    super(new PlatformUnit(2, false, keystoreType));
  }

  /**
   * Constructs a default test platform with two clusters
   *
   * @param enableDistribution flag to enable/disable distribution between the clusters
   */
  public DualClusterPlatformUnit(boolean enableDistribution) {
    super(new PlatformUnit(2, enableDistribution));
  }

  /**
   * Constructs a test platform with two clusters according to the provided instance and cluster
   * configurations
   *
   * @param instanceConfig The instance configuration to use
   * @param clusterA       The configuration for the first cluster
   * @param clusterB       The configuration for the second cluster
   */
  public DualClusterPlatformUnit(InstanceUnitConfig instanceConfig, ClusterUnitConfig clusterA,
      ClusterUnitConfig clusterB) {
    super(new PlatformUnit(instanceConfig, clusterA, clusterB));
  }

  /**
   * Constructs a default test platform with two clusters using the provided topic and group
   * patterns
   *
   * @param topicPatternA the Topic pattern to use for the first cluster
   * @param topicPatternB the Topic pattern to use for the second cluster
   * @param groupPatternA the Group pattern to use for the first cluster
   * @param groupPatternB the Group pattern to use for the second cluster
   */
  public DualClusterPlatformUnit(String topicPatternA, String topicPatternB, String groupPatternA,
      String groupPatternB) {
    super(new PlatformUnit(new String[]{topicPatternA, topicPatternB},
        new String[]{groupPatternA, groupPatternB}));
  }

  /**
   * Get the cluster controller for the first cluster
   *
   * @return the cluster controller
   */
  public ClusterUnit clusterA() {
    return platform().getCluster(0);
  }

  /**
   * Get the cluster controller for the second cluster
   *
   * @return the cluster controller
   */
  public ClusterUnit clusterB() {
    return platform().getCluster(1);
  }
}
