package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import io.axual.common.tools.ResourceUtil;
import io.axual.streams.proxy.generic.factory.TopologyFactory;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        WrappedStreams.class
})
@PowerMockIgnore({"javax.net.ssl.*", "jdk.internal.reflect.*"})
public class WrappedStreamsTest {

    WrappedStreams wrappedStreams;

    @Mock
    KafkaStreams mockedStreams;

    @Before
    public void setUp() throws Exception {
        whenNew(KafkaStreams.class).withAnyArguments().thenReturn(mockedStreams);
        wrappedStreams = new WrappedStreams(validConfigs());
    }

    private Map<String, Object> validConfigs() {
        TopologyFactory topologyFactory = builder -> {
            builder.stream("something").foreach((k, v) -> {
                throw new RuntimeException("Should never reach this");
            });
            return builder.build();
        };

        Map<String, Object> configs = new HashMap<>();
        configs
                .put(WrappedStreamsConfig.TOPOLOGY_FACTORY_CONFIG, topologyFactory);
        configs.put(StreamsConfig.APPLICATION_ID_CONFIG, "io.axual.teststream");
        configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, "notsecret");
        configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
                ResourceUtil.getResourceAsFile("ssl/axual.client.keystore.jks"));
        configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "notsecret");
        configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
                ResourceUtil.getResourceAsFile("ssl/axual.client.truststore.jks"));
        configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "notsecret");
        configs.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9999");
        return configs;
    }

    @Test
    public void setStateListener() {
        KafkaStreams.StateListener listener = mock(KafkaStreams.StateListener.class);

        wrappedStreams.setStateListener(listener);

        verify(mockedStreams, Mockito.times(1)).setStateListener(listener);
    }

    @Test
    public void state() {
        wrappedStreams.state();

        verify(mockedStreams, Mockito.times(1)).state();
    }

    @Test
    public void metrics() {
        wrappedStreams.metrics();

        verify(mockedStreams, Mockito.times(1)).metrics();
    }

    @Test
    public void start() {
        wrappedStreams.start();

        verify(mockedStreams, Mockito.times(1)).start();

    }

    @Test
    public void stop() {
        wrappedStreams.stop();

        verify(mockedStreams, Mockito.times(1)).close();
    }

    @Test
    public void close() {
        wrappedStreams.close();

        verify(mockedStreams, Mockito.times(1)).close();
    }

    @Test
    public void closeDuration() {
        wrappedStreams.close(Duration.ZERO);

        verify(mockedStreams, Mockito.times(1)).close(Duration.ZERO);
    }

    @Test
    public void cleanUp() {
        wrappedStreams.cleanUp();

        verify(mockedStreams, Mockito.times(1)).cleanUp();
    }

    @Test
    public void setUncaughtExceptionHandler() {
        StreamsUncaughtExceptionHandler handler = mock(StreamsUncaughtExceptionHandler.class);
        wrappedStreams.setUncaughtExceptionHandler(handler);

        verify(mockedStreams, Mockito.times(1)).setUncaughtExceptionHandler(handler);
    }

    @Test
    public void testSetUncaughtExceptionHandler() {
        Thread.UncaughtExceptionHandler handler = mock(Thread.UncaughtExceptionHandler.class);
        wrappedStreams.setUncaughtExceptionHandler(handler);

        verify(mockedStreams, Mockito.times(1)).setUncaughtExceptionHandler(handler);
    }

    @Test
    public void allMetadata() {
        wrappedStreams.allMetadata();

        verify(mockedStreams, Mockito.times(1)).allMetadata();
    }

    @Test
    public void allMetadataForStore() {
        String storeName = "";
        wrappedStreams.allMetadataForStore(storeName);

        verify(mockedStreams, Mockito.times(1)).allMetadataForStore(storeName);
    }

    @Test
    public void queryMetadataForKey() {
        String storeName = "";
        Object key = null;
        Serializer keySerializer = mock(Serializer.class);
        wrappedStreams.queryMetadataForKey(storeName, key, keySerializer);

        verify(mockedStreams, Mockito.times(1)).queryMetadataForKey(storeName, key, keySerializer);
    }

    @Test
    public void testQueryMetadataForKey() {
        String storeName = "";
        Object key = null;
        StreamPartitioner partitioner = mock(StreamPartitioner.class);
        wrappedStreams.queryMetadataForKey(storeName, key, partitioner);

        verify(mockedStreams, Mockito.times(1)).queryMetadataForKey(storeName, key, partitioner);
    }

    @Test
    public void store() {
        StoreQueryParameters parameters = mock(StoreQueryParameters.class);
        wrappedStreams.store(parameters);

        verify(mockedStreams, Mockito.times(1)).store(parameters);
    }

    @Test
    public void addStreamThread() {
        wrappedStreams.addStreamThread();

        verify(mockedStreams, Mockito.times(1)).addStreamThread();
    }

    @Test
    public void removeStreamThread() {
        wrappedStreams.removeStreamThread();

        verify(mockedStreams, Mockito.times(1)).removeStreamThread();
    }

    @Test
    public void removeStreamThreadDuration() {
        wrappedStreams.removeStreamThread(Duration.ZERO);

        verify(mockedStreams, Mockito.times(1)).removeStreamThread(Duration.ZERO);
    }

    @Test
    public void metadataForAllStreamsClients() {
        wrappedStreams.metadataForAllStreamsClients();

        verify(mockedStreams, Mockito.times(1)).metadataForAllStreamsClients();
    }

    @Test
    public void streamsMetadataForStore() {
        String storeName = "storeName";
        wrappedStreams.streamsMetadataForStore(storeName);

        verify(mockedStreams, Mockito.times(1)).streamsMetadataForStore(storeName);
    }

    @Test
    public void metadataForLocalThreads() {
        wrappedStreams.metadataForLocalThreads();

        verify(mockedStreams, Mockito.times(1)).metadataForLocalThreads();
    }
}