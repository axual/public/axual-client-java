package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.processor.internals.DefaultKafkaClientSupplier;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.tools.MapUtil;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;
import io.axual.streams.proxy.generic.streams.StaticStreamsProxy;
import io.axual.streams.proxy.switching.SwitchingStreamsConfig;
import io.axual.streams.proxy.switching.SwitchingStreamsFactory;
import io.axual.streams.proxy.wrapped.WrappedStreamsConfig;
import io.axual.streams.proxy.wrapped.WrappedStreamsFactory;

@InterfaceStability.Evolving
public class AxualStreams extends StaticStreamsProxy<AxualStreamsConfig> implements StreamsProxy {

    public AxualStreams(Map<String, Object> configs) {
        super(createChain(configs));
    }

    public AxualStreams(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    @SuppressWarnings("unchecked")
    private static ClientProxyInitializer<StreamsProxy, AxualStreamsConfig> createChain(Map<String, Object> configs) {
        final AxualStreamsConfig config = new AxualStreamsConfig(configs);

        // Filter any switching proxies from the proxy chain, since we handle switching outside
        // of the KafkaStreams object, not from within the producer/consumer/admin clients.
        final boolean switching = config.getProxyChain().containsElement(ProxyTypeRegistry.SWITCHING_PROXY_ID);
        final String downstreamChain = ProxyChain
                .newBuilder(config.getProxyChain())
                .remove(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                .build()
                .toString();

        // Construct configs for downstream proxies
        final Map<String, Object> downstreamConfigs = new HashMap<>(config.getDownstreamConfigs());

        // Set the downstream application id for Kafka Streams
        downstreamConfigs.put(StreamsConfig.APPLICATION_ID_CONFIG, config.getApplicationId());

        // Pass the default serde downstream
        downstreamConfigs.put(AxualSerdeConfig.BACKING_KEY_SERDE_CONFIG, config.getDefaultKeySerde());
        downstreamConfigs.put(AxualSerdeConfig.BACKING_VALUE_SERDE_CONFIG, config.getDefaultValueSerde());

        // Pass the proxy chain to both key and value serdes
        downstreamConfigs.put(AxualSerdeConfig.KEY_SERDE_CHAIN_CONFIG, downstreamChain);
        downstreamConfigs.put(AxualSerdeConfig.VALUE_SERDE_CHAIN_CONFIG, downstreamChain);

        // Hard-configure the AxualSerde to be used by Kafka Streams
        downstreamConfigs.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, AxualSerde.class);
        downstreamConfigs.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, AxualSerde.class);

        // Pass a custom ClientSupplier that instantiates the Axual clients for KafkaStreams
        downstreamConfigs.put(AxualClientSupplier.CHAIN_CONFIG, downstreamChain);
        downstreamConfigs.put(WrappedStreamsConfig.KAFKA_CLIENT_SUPPLIER_CONFIG, new AxualClientSupplier(new DefaultKafkaClientSupplier()));

        // Set the default uncaught exception handler
        downstreamConfigs.putIfAbsent(
                WrappedStreamsConfig.UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG,
                (UncaughtExceptionHandlerFactory) streams -> (StreamsUncaughtExceptionHandler) t -> StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.REPLACE_THREAD);

        // When a switching KafkaStreams is requested, we wrap the entire thing created above in a
        // SwitchingStreams object and return it
        if (switching) {
            downstreamConfigs.put(SwitchingStreamsConfig.BACKING_FACTORY_CONFIG, WrappedStreamsFactory.class.getName());
            return new ClientProxyInitializer<>(config, new SwitchingStreamsFactory().create(downstreamConfigs));
        }

        // No switching is requested, so create a plain KafkaStreams object and return that
        return new ClientProxyInitializer<>(config, new WrappedStreamsFactory().create(downstreamConfigs));
    }
}
