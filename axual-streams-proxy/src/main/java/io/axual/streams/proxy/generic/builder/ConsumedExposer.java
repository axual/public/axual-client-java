package io.axual.streams.proxy.generic.builder;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.Consumed;

public class ConsumedExposer<K,V> extends Consumed<K,V> {
    /**
     * Create an instance of {@link Consumed} from an existing instance.
     *
     * @param consumed the instance of {@link Consumed} to copy
     */
    public ConsumedExposer(Consumed<K, V> consumed) {
        super(consumed);
    }

    public Serde<K> keySerde() {
        return keySerde;
    }

    public Serde<V> valueSerde() {
        return valueSerde;
    }
}
