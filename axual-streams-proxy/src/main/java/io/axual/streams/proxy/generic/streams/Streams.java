package io.axual.streams.proxy.generic.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyQueryMetadata;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsMetadata;
import org.apache.kafka.streams.ThreadMetadata;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.processor.StreamPartitioner;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.axual.common.annotation.InterfaceStability;

@InterfaceStability.Evolving
public interface Streams extends AutoCloseable {
    void setStateListener(KafkaStreams.StateListener listener);

    KafkaStreams.State state();

    Map<MetricName, ? extends Metric> metrics();

    void start();

    void stop();

    void close();

    /**
     * @deprecated Use the {@link #close(Duration)} method.
     */
    @Deprecated
    default void close(long timeout, TimeUnit timeUnit) {
        close(Duration.ofMillis(timeUnit.toMicros(timeout)));
    }

    void close(Duration timeout);

    void cleanUp();

    void setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler eh);

    /**
     * @deprecated use {@link io.axual.streams.proxy.generic.streams.Streams#setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler)} instead.
     */
    @Deprecated
    void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh);

    /**
     *
     * @deprecated
     */
    @Deprecated
    Collection<org.apache.kafka.streams.state.StreamsMetadata> allMetadata();

    /**
     *
     * @deprecated
     */
    @Deprecated
    Collection<org.apache.kafka.streams.state.StreamsMetadata> allMetadataForStore(String storeName);

    /**
     *
     * @deprecated
     */
    @Deprecated
    Set<org.apache.kafka.streams.processor.ThreadMetadata> localThreadsMetadata();

    <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, Serializer<K> keySerializer);

    <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner);

    <T> T store(StoreQueryParameters<T> storeQueryParameters);

    Optional<String> addStreamThread();

    Optional<String> removeStreamThread();

    Optional<String> removeStreamThread(Duration timeout);

    Collection<StreamsMetadata>	metadataForAllStreamsClients();

    Collection<StreamsMetadata>	streamsMetadataForStore(String storeName);

    Set<ThreadMetadata>	metadataForLocalThreads();

}
