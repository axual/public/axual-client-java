package io.axual.streams.proxy.generic.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyQueryMetadata;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.processor.ThreadMetadata;
import org.apache.kafka.streams.state.StreamsMetadata;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.axual.client.proxy.generic.client.StaticClientProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;

public class StaticStreamsProxy<C extends BaseClientProxyConfig<StreamsProxy>> extends StaticClientProxy<StreamsProxy, C> implements StreamsProxy {
    public StaticStreamsProxy(C config) {
        super(config);
    }

    public StaticStreamsProxy(ClientProxyInitializer<StreamsProxy, C> initializer) {
        super(initializer);
    }

    @Override
    public void setStateListener(KafkaStreams.StateListener listener) {
        proxiedObject.setStateListener(listener);
    }

    @Override
    public KafkaStreams.State state() {
        return proxiedObject.state();
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return proxiedObject.metrics();
    }

    @Override
    public void start() {
        proxiedObject.start();
    }

    @Override
    public void stop() {
        proxiedObject.stop();
    }

    @Override
    public void cleanUp() {
        proxiedObject.cleanUp();
    }

    @Override
    public void setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler eh) {
        proxiedObject.setUncaughtExceptionHandler(eh);
    }

    /**
     * @deprecated use {@link io.axual.streams.proxy.generic.streams.StaticStreamsProxy#setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler)} instead.
     */
    @Deprecated
    @Override
    public void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh) {
        proxiedObject.setUncaughtExceptionHandler(eh);
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Collection<StreamsMetadata> allMetadata() {
        return proxiedObject.allMetadata();
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Collection<StreamsMetadata> allMetadataForStore(String storeName) {
        return proxiedObject.allMetadataForStore(storeName);
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Set<ThreadMetadata> localThreadsMetadata() {
        return proxiedObject.localThreadsMetadata();
    }

    @Override
    public <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, Serializer<K> keySerializer) {
        return proxiedObject.queryMetadataForKey(storeName, key, keySerializer);
    }

    @Override
    public <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner) {
        return proxiedObject.queryMetadataForKey(storeName, key, partitioner);
    }

    @Override
    public <T> T store(StoreQueryParameters<T> storeQueryParameters) {
        return proxiedObject.store(storeQueryParameters);
    }

    @Override
    public Optional<String> addStreamThread() {
        return proxiedObject.addStreamThread();
    }

    @Override
    public Optional<String> removeStreamThread() {
        return proxiedObject.removeStreamThread();
    }

    @Override
    public Optional<String> removeStreamThread(Duration timeout) {
        return proxiedObject.removeStreamThread(timeout);
    }

    @Override
    public Collection<org.apache.kafka.streams.StreamsMetadata> metadataForAllStreamsClients() {
        return proxiedObject.metadataForAllStreamsClients();
    }

    @Override
    public Collection<org.apache.kafka.streams.StreamsMetadata> streamsMetadataForStore(String storeName) {
        return proxiedObject.streamsMetadataForStore(storeName);
    }

    @Override
    public Set<org.apache.kafka.streams.ThreadMetadata> metadataForLocalThreads() {
        return proxiedObject.metadataForLocalThreads();
    }
}
