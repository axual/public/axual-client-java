package io.axual.streams.proxy.switching;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.client.proxy.switching.generic.SwitchingProxyConfig;
import io.axual.common.annotation.InterfaceStability;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;
import io.axual.streams.proxy.wrapped.WrappedStreamsConfig;

@InterfaceStability.Evolving
public class SwitchingStreamsConfig extends SwitchingProxyConfig<StreamsProxy> {
    public static final String BACKING_FACTORY_CONFIG = "switchingstreams.backing.factory";

    private final UncaughtExceptionHandlerFactory uehf;

    public SwitchingStreamsConfig(Map<String, Object> configs) {
        super(configs, "streams", BACKING_FACTORY_CONFIG);

        uehf = getConfiguredInstance(WrappedStreamsConfig.UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG, UncaughtExceptionHandlerFactory.class);
    }

    public UncaughtExceptionHandlerFactory getUncaughtExceptionHandlerFactory() {
        return uehf;
    }
}
