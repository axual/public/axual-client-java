package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

public class AxualSerde<T> implements Serde<T> {
    private final boolean preconfigured;
    private Serializer<T> serializer;
    private Deserializer<T> deserializer;

    public AxualSerde() {
        preconfigured = false;
    }

    public AxualSerde(Map<String, Object> configs, boolean isKey) {
        preconfigured = true;
        AxualSerdeConfig<T> config = new AxualSerdeConfig<>(new HashMap<>(configs), isKey);
        deserializer = config.getDeserializer();
        serializer = config.getSerializer();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        if (!preconfigured) {
            AxualSerdeConfig<T> config = new AxualSerdeConfig<>(new HashMap<>(configs), isKey);
            deserializer = config.getDeserializer();
            serializer = config.getSerializer();
        }
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<T> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<T> deserializer() {
        return deserializer;
    }
}
