package io.axual.streams.proxy.generic.factory;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import io.axual.common.annotation.InterfaceStability;
import io.axual.streams.proxy.generic.streams.Streams;

/**
 * Interface which can be used to provide exception handlers to determine the behaviour when there
 * is an uncaught error in streaming.
 */
@InterfaceStability.Evolving
public interface UncaughtExceptionHandlerFactory {
    StreamsUncaughtExceptionHandler create(Streams streams);
}
