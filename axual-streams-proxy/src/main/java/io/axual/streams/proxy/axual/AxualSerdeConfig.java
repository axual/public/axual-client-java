package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.axual.serde.AxualDeserializer;
import io.axual.client.proxy.axual.serde.AxualDeserializerConfig;
import io.axual.client.proxy.axual.serde.AxualSerializer;
import io.axual.client.proxy.axual.serde.AxualSerializerConfig;
import io.axual.client.proxy.generic.proxy.BaseSerdeConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.client.proxy.header.serde.HeaderSerializerConfig;

public class AxualSerdeConfig<T> extends BaseSerdeConfig {
    public static final String KEY_SERDE_CHAIN_CONFIG = "axualserde.key.serde.chain";
    public static final String VALUE_SERDE_CHAIN_CONFIG = "axualserde.value.serde.chain";
    public static final String BACKING_KEY_SERDE_CONFIG = "axualserde.backing.key.serde";
    public static final String BACKING_VALUE_SERDE_CONFIG = "axualserde.backing.value.serde";

    private final Deserializer<T> deserializer;
    private final Serializer<T> serializer;

    public AxualSerdeConfig(Map<String, ?> configs, boolean isKey) {
        super(configs, isKey);

        // Filter the serde settings downstream
        filterDownstream(KEY_SERDE_CHAIN_CONFIG, VALUE_SERDE_CHAIN_CONFIG, BACKING_KEY_SERDE_CONFIG, BACKING_VALUE_SERDE_CONFIG);
        // Filter the value header setting downstream, so the serde chain never produces byte arrays containing value headers
        filterDownstream(HeaderSerializerConfig.ENABLE_VALUE_HEADERS_CONFIG);

        // Get the configured proxy chain
        final ProxyChain chain = ProxyChainUtil.parseProxyChain(configs, isKey ? KEY_SERDE_CHAIN_CONFIG : VALUE_SERDE_CHAIN_CONFIG);
        // Set up the backing serde
        final Serde<T> backingSerde = getConfiguredInstance(isKey ? BACKING_KEY_SERDE_CONFIG : BACKING_VALUE_SERDE_CONFIG, Serde.class);

        // Set up the serializer
        final Map<String, Object> serializerConfigs = new HashMap<>(configs);
        serializerConfigs.put(AxualSerializerConfig.CHAIN_CONFIG, chain);
        serializerConfigs.put(AxualSerializerConfig.DEFAULT_SERIALIZER_CONFIG, backingSerde.serializer());
        serializer = new AxualSerializer<>();
        serializer.configure(serializerConfigs, isKey);

        // Set up the deserializer
        final Map<String, Object> deserializerConfigs = new HashMap<>(configs);
        deserializerConfigs.put(AxualDeserializerConfig.CHAIN_CONFIG, chain);
        deserializerConfigs.put(AxualDeserializerConfig.DEFAULT_DESERIALIZER_CONFIG, backingSerde.deserializer());
        deserializer = new AxualDeserializer<>();
        deserializer.configure(deserializerConfigs, isKey);
    }

    public Deserializer<T> getDeserializer() {
        return deserializer;
    }

    public Serializer<T> getSerializer() {
        return serializer;
    }
}
