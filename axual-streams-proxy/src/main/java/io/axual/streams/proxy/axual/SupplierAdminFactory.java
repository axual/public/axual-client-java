package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.KafkaClientSupplier;

import java.util.Map;

import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.wrapped.admin.WrappedAdminClient;

public class SupplierAdminFactory extends SupplierClientProxyFactory<AdminProxy> {
    public SupplierAdminFactory(KafkaClientSupplier supplier) {
        super(supplier);
    }

    @Override
    public AdminProxy create(Map<String, Object> configs) {
        return new WrappedAdminClient(supplier.getAdmin(configs), configs);
    }
}
